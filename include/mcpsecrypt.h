/* Copyright (c) 2006-2014 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _mcpsecrypt_h_
#define _mcpsecrypt_h_


/** @file mcpsecrypt.h
 *  @brief MCP Security Feature Core APIs.
 *
 *  This file @c mcpsecrypt.h provides the interface definitions for
 *  MCP Security Feature Core APIs.
 *
 */

/** @page mcpsecryptHPg Header: mcpsecrypt.h MCP Security Feature Core APIs

@section mcpsecryptHName mcpsecrypt.h HEADER NAME

    mcpsecrypt.h -- MCP Security Feature Core APIs.

@section mcpsecryptHSynop mcpsecrypt.h SYNOPSIS

    This file provides the interface definitions for
    MCP Security Feature Core APIs.

@section mcpsecryptHDesc mcpsecrypt.h DESCRIPTION

    This file, @c mcpsecrypt.h provides the interface definitions for
    MCP Security Feature Core APIs.

*/

/* system headers */
#include <stddef.h>
#include <stdlib.h>

#include "include/enphase.h"
#include "include/emudbg.h"
#include "include/mcpsec.h"
#include "include/mcpsecmsg.h"

/**
 * @defgroup mcpsecryptHGroups mcpsecrypt Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines -------------------------- */

/**
 * @defgroup mcpsecryptHDefines mcpsecrypt Header Defines
 */
/*@{*/


/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs ------------------------- */

/**
 * @defgroup mcpsecryptHTypedefs mcpsecrypt Header Typedefs
 */
/*@{*/

/*@}*/  /* END of mcpsecrypt Header Typedef Group */

/* ------------------------------------ Extern Declarations -------------- */

/**
 * @defgroup mcpsecryptHExtdata mcpsecrypt Header Extern Data Definitions
 */
/*@{*/


/*@}*/  /* END of mcpsecrypt Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined -------------- */

/**
 * @defgroup mcpsecryptHGlobdata mcpsecrypt Header Global Data Defined
 */
/*@{*/

/*@}*/  /* END of mcpsecrypt Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined -------------- */

/**
 * @defgroup mcpsecryptHStatdata mcpsecrypt Header Static Data Defined
 */
/*@{*/

/*@}*/  /* END of mcpsecrypt Header Static Data Defined Group */

/* ------------------------------------ Prototypes ----------------------- */

/**
 * @defgroup mcpsecryptHProto mcpsecrypt Header Prototypes
 */
/*@{*/

/** mcpsecryptMessageSubjectToEncryption - return if msg may be encrypted. */
extern bool mcpsecryptMessageSubjectToEncryption(eMcpMsgId msgId);

/** mcpsecryptPlcSecuritySystemSettingsGet - get PLC security system settings. */
extern void mcpsecryptPlcSecuritySystemSettingsGet(
            tMcpSecSystemSettings * pSysSecSettings);

/** \defgroup mcpsecrypt ``mcpsecrypt Encypt/Decrypt Interfaces'' */
/* @{ */

/**
    To state the obvious, encryption is a function related to message
    transmission.  It applies to both the Envoy and the security feature aware
    PLD, e.g. a Frigate PCU. In general, any MCP message may be encrypted
    prior to transmission.  In practice, here's the guidelines we plan to
    implement:

    - Messages directed to non-Frigate devices *will not be* encrypted at the
      message level.  They may be encrypted at the frame level if the device
      supports it, using an Aurora modem: this is unrelated to security feature
      being documented here.

    - Messages directed to a Frigate device on a system that has not been
      marked for secure operation *will not be* encrypted.

    - Messages directed to a Frigate device on a system that has been marked
      for secure operation *may* be encrypted:

        - For secure mode operation, polling sequences in the context of a
          non-*gone* devices will generally be encrypted if the
          device's `secStatus` is verified and secure mode is active at
          the system level.

        - As a corollary, if the device is being treated as  *gone*, the device
          will be transitioned to `secStatus` *unknown*. Once the device
          leaves the *gone* state, it will be marked for `secInfo` polling
          to allow its `secStatus` to be transitioned to verified by a later,
          successful, `secInfo` polling sequence.
          A *gone* device has a `secStatus` of unknown.

    - Messages broadcast from an Envoy in secure operation will also be sent
      encrypted if the system is in secure mode.

    - For a PLD that is security-feature capable, any unencrypted message that
      is not related to a `devInfo` polling sequence or a `secInfo`
      polling sequence will not be processed.  Appropriate `secCnts` counter
      will be incremented.

    Maximum application message length of 300 bytes is used to set application
    message ring entry size, to accommodate padding for encryption and
    decryption in place the ring element size will set to \\( (304 + 16) =
    320\\).

    On the Envoy the basic information used in the determination of whether a
    given message will be encrypted are:

    - Type of PCU, Frigate PCU or not.

    - Whether the device is in the *gone* state or not.

    - Whether the system is set for secure mode operations.

    - Whether the device has an active secure mode session, i.e. `setStatus` is
      verified.

    - The type of MCP message.

    Let's describe the method of formatting an encrypted message:

    - First message is formatted in the *normal* fashion.

    - The length of  padding is computed.  The formula for computing padding
      length is:
      \\[((msgLen \mod{} 16) == 0)\ ?\ 0\ :\ (16 - (msgLen \mod{} 16))\\].

    - The unpadded length of the message is recorded in the lower 15 bits of
      the `msgLen` field of the `tMcpMsgHdr` is the original application layer
      message length from the original formatting of the message.
      That is, the lower 15 bits of the `msgLen` field do not include
      the number of bytes added for padding.

    - The high order bit of the `msgLen` in the message header will be set to
      indicate that the message is encrypted.

    - The body of the message past the application message header is encrypted
      by calling `secEncrypt`.  The prototype for `secEncrypt` is shown below:

    - The masterKeyIndex is needed in the envoy side for decrypt.

    - The `pData` parameter is set to the first byte following the
      application message header.

    - The `dataLen` parameter is set the *padded* length of the message.

    - The `pMsgIV` is used to return the message CBC IV for the
      encryption so the it can be sent with the encrypted message body.  By
      convention, the IV will be placed in to the message, just after the MCP
      message header, and just be for the encrypted message body.  Padding, if
      needed follows the encrypted message body.

    - This function returns ENP_OK if secure session is active allowing
      encryption, else it returns ENP_ERROR.
*/
extern int mcpsecryptEncrypt(u_int8_t * pData,
                             u_int16_t dataLen, tMcpSecMsgIVPtr pMsgIV);

/**
    The implementation of the secure feature makes the rules for encrypted
    messages key, see the previous section.  This approach allows for simple
    common approach to decryption of received messages.

    To state the obvious, decryption is a function related to message
    reception. It applies to both the Envoy and the security feature aware
    PLD, e.g. a Frigate PCU. In general, any MCP message to a security feature
    aware PLD or a to secure mode Envoy from a security feature PLD may
    require decryption.

    The `tMcpMsgHdr` that all MCP messages contain will not be encrypted.  The
    high-order bit of the `msgLen` length field (16-bits) will be used to
    indicate the message body is needing decryption by the receiver.

    The padded length of the message is determined (see prior section).

    The body of the message past the application message header is decrypted
    by calling `mcpsecryptDecrypt`.
    The prototype for `mcpsecryptDecrypt` is shown below:

    - The `pMsgIV` is for reference to the message IV,
      the message CBC IV for the decryption.

    - The `pData` parameter is set to the first byte following the
      application message header.

    - The `dataLen` parameter is set the *padded* length of the message.

    - This function returns ENP_OK if secure session is active allowing
      decryption, else it returns ENP_ERROR.

*/
extern int mcpsecryptDecrypt(tMcpSecMsgIVPtr pMsgIV,
                             u_int8_t * pData, u_int16_t dataLen);

/** mcpsecryptEncryptMessage - encrypt a message. */
extern int mcpsecryptEncryptMessage(tL2FrmAppMsg * l2AppMsg);

/** mcpsecryptDecryptMessage - decrypt a message.

    Decrypt a message.

    \param[in] l2AppMsg (tL2FrmAppMsg *) .
    \return (int) ENP_OK / ENP_ERROR.
*/
extern int mcpsecryptDecryptMessage(tL2FrmAppMsg * l2AppMsg);

/* @} */

/** mcpsecryptLengthMatchingBytesInBlock - return length of matching bytes. */
extern u_int16_t mcpsecryptLengthMatchingBytesInBlock(u_int8_t * pData,
                    u_int16_t blkLen, u_int8_t bValue);

/** mcpsecryptXor - xor data block, for simulation test only. */
extern void mcpsecryptXor(u_int8_t * pDst, u_int8_t * pSrc, u_int16_t len);

/*@}*/  /* END of mcpsecrypt Header Prototypes Group */

/*@}*/  /* END mcpsecrypt Header Definitions and Declarations */

#endif /* _mcpsecrypt_h_ */

/* ------------------------------------------------------------------------
   $Log:$
   ----------------------------------------------------------------------- */

/* END of mcpsecrypt.h */

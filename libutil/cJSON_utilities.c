#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"

cJSON* GetOrCreateJSONObject(cJSON *parent, const char *name)
{
	cJSON* item = cJSON_GetObjectItem(parent, name);
	if (item != NULL) return item;
	item = cJSON_CreateObject();
	cJSON_AddItemToObject(parent, name, item);
	return item;
}


cJSON* GetOrCreateJSONArray(cJSON *parent, const char *name)
{
	cJSON* item = cJSON_GetObjectItem(parent, name);
	if (item != NULL) return item;
	item = cJSON_CreateArray();
	cJSON_AddItemToObject(parent, name, item);
	return item;
}

cJSON* GetOrCreateFormattedJSONObject(cJSON *parent, const char *fmt, ...)
{
	char buffer[256];
	va_list args;
	va_start(args, fmt);
	vsprintf(buffer, fmt, args);
	va_end(args);
	cJSON* item = cJSON_GetObjectItem(parent, buffer);
	if (item != NULL) return item;
	item = cJSON_CreateObject();
	cJSON_AddItemToObject(parent, buffer, item);
	return item;
}
cJSON* GetOrCreateJSONInteger(cJSON *parent, const char *name, int initVal)
{
	cJSON* item = cJSON_GetObjectItem(parent, name);
	if (item != NULL) return item;
	item = cJSON_CreateNumber((double)initVal);
	cJSON_AddItemToObject(parent, name, item);
	return item;
}

int ReadJSONInteger(cJSON *parent, const char *name, int initVal)
{
	cJSON* item = cJSON_GetObjectItem(parent, name);
	if (item != NULL && item->type == cJSON_Number) return item->valueint;
	return initVal;
}
double ReadJSONDouble(cJSON *parent, const char *name, double initVal)
{
	cJSON* item = cJSON_GetObjectItem(parent, name);
	if (item != NULL && item->type == cJSON_Number) return item->valuedouble;
	return initVal;
}

cJSON* GetOrCreateJSONDouble(cJSON *parent, const char *name, double initVal)
{
	cJSON* item = cJSON_GetObjectItem(parent, name);
	if (item != NULL) return item;
	item = cJSON_CreateNumber(initVal);
	cJSON_AddItemToObject(parent, name, item);
	return item;
}
cJSON* GetOrCreateJSONString(cJSON *parent, const char *name, const char* msg)
{
	cJSON* item = cJSON_GetObjectItem(parent, name);
	if (item != NULL) return item;
	item = cJSON_CreateString(msg);
	cJSON_AddItemToObject(parent, name, item);
	return item;
}

char *ReadJSONString(cJSON *parent, const char *name, char* msg)
{
	cJSON* item = cJSON_GetObjectItem(parent, name);
	if (item != NULL && item->type == cJSON_String) return item->valuestring;
	return msg;
}

void AddFormattedJSONString(cJSON *item, const char *name, const char *msg, ...)
{
	char buffer[256];
	va_list args;
	va_start(args, msg);
	vsprintf(buffer, msg, args);
	va_end(args);
	if (item != NULL) cJSON_AddStringToObject(item, name, buffer);
}

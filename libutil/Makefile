#
# Generate an absolute path for the top of this package
#
SBTTOP ?= $(shell (cd .. ; pwd))

#
# Unconditionally share this SBTTOP value with all of our children.  The conditional
# assignment above will have imported one, if it existed, and this statement will
# export a new one otherwise.
#
export SBTTOP

#
# Conditionally include makedefs from the SBTTOP directory.  Hopefully there is one,
# but if not, that's okay too.
#
-include $(SBTTOP)/makedefs

CFLAGS += $(DEBUGFLAGS)

#
# If S isn't set, change into the ARCH directory and set S to this component's
# source directory.
#
ifeq ($(S),)
export SVNVER:=$(shell git rev-parse --short HEAD)
MAKECMDGOALS ?= all
$(MAKECMDGOALS):
	$(AT)[ -e $(ARCH) ] || mkdir $(ARCH)
	$(AT)$(MAKE) S=$(CURDIR) -C $(ARCH) -f $(CURDIR)/Makefile $(MAKECMDGOALS)
else

#
# What to compile
#


OBJS:= fifo.o  log.o cJSON.o os.o \
    cJSON_utilities.o  crc32.o \
     emuutil.o 


# ideally the C preprocessor would take advantage of the -I flag to set 
# the correct path.  But in GCC 4.6 (Ubuntu 12.04) that doesn't work 
# correctly.  So we specify the absolute path here.
conv.o: conv.S ../../include/sbt/miconfig30.json
	$(CC) $(CFLAGS) -g -D__conv_json__='"../../include/sbt/miconfig30.json"' -c $< -o $@

VPATH = $(S)

#
# Include (and cause Make to generate) dependencies
#
-include $(OBJS:.o=.d)

all:	 libutil.a

libutil.a:	$(OBJS)
	$(AR) -rv $@ $^
	$(RANLIB) $@

LDLIBS += -lm -lpthread -lrt

unittests:	test_util test_fifo
test_util: test_suntime test_long_to_short_serial
test_suntime: test_suntime.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS) 

test_long_to_short_serial: test_long_to_short_serial.o

test_arcpac_conversions: test_arcpac_conversions.o cJSON.o cJSON_utilities.o temp_calc.o
	$(CC) -g -o $@ $^ $(LDLIBS) 

test_fifo: test_fifo.o log.o cJSON.o cJSON_utilities.o
	$(CC) $(CFLAGS) -o $@ $^ $(LDLIBS)

clean:
	rm -f *.o *.d libutil.a  test_fifo

.PHONY: clean all

endif

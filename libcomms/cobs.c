/*
 * @copyright
 *  Copyright 2014, SolarBridge Technologies, LLC
 *  All rights reserved.
 *
 * @file cobs.c
 *  Consistent Overhead Byte Stuffing function implementation
 *
 * @date Sep 2, 2014
 *
 * @author: Julie Haugh <j.haugh@solarbridgetech.com>
 */

#include <inttypes.h>
#include <errno.h>

#include "sbt/cobs.h"
#include "sbt/log.h"

/*!
 * \addtogroup cobs_api
 * @{
 */

/*!*
 * Decode a COBS encoded block of data into the same location as the encoded
 * data.
 *
 * The COBS encoded data is decoded and stored back in the same location as the
 * original.  This routine write past the end of the actual output and may
 * return a length one byte longer than the input, and may added a spurious NUL
 * byte to the end.
 *
 * This function is appropriate for uses where input is known to always
 * terminate with a NUL byte, or where the length of the data may be recovered
 * from the data itself.  Additionally, it avoids the end-of-buffer test and
 * thus has slightly better performance on data sets which include a large
 * number of zero bytes.
 */
int cobs_decode_in_place(uint8_t *in_packet, size_t size)
{
    uint8_t *start_output = in_packet;
    uint8_t *out_packet = in_packet;
    uint8_t *end_input = in_packet + size;
    uint8_t code;
    uint8_t count;

    /*
     * Validate some parameters.
     */
    if (in_packet == 0 || size > (COBS_MAX_BLOCK + 2))
    {
        LOG_WARNING("deoode size too large: %d\n", size);
        return -EINVAL;
    }

    while (in_packet < end_input)
    {
        count = code = *in_packet++;

        while (--count)
            *out_packet++ = *in_packet++;

        if (code != (COBS_MAX_BLOCK + 1)) *out_packet++ = '\0';
    }
    return out_packet - start_output;
}

/**
 * Decode a COBS encoded block of data into the same location as the encoded
 * data.
 *
 * The COBS encoded data is decoded and stored into a user-provided location.
 * The exact size of the originally encoded data is always returned.
 *
 * It is appropriate for uses where the size of the data cannot be readily
 * recovered from the input or where the output buffer sizes are fixed and
 * overruns cannot be tolerated.
 *
 * This function assumes a properly formatted COBS encoded packet.  Bad things
 * will happen otherwise.
 */
int cobs_decode(const uint8_t *in_packet, size_t size, uint8_t *out_packet)
{
    uint8_t *start_output = out_packet;
    const uint8_t *end_input = in_packet + size;
    uint8_t code;
    uint8_t count;

    /*
     * Validate some parameters.
     */
    if (in_packet == (uint8_t *) 0 || out_packet == (uint8_t *) 0 || size > 512)
    {
        errno = EINVAL;
        return -1;
    }

    while (in_packet < end_input)
    {
        count = code = *in_packet++;

        while (--count)
            *out_packet++ = *in_packet++;

        if (code != (COBS_MAX_BLOCK + 1) && in_packet < end_input)
            *out_packet++ = '\0';
    }
    return out_packet - start_output;
}

/**
 * COBS encode a block of data into a user-provided location.
 *
 * The data is COBS encoded and stored into a user-provided location.  The
 * caller is encouraged to use the cobs_get_required_size() macro if a
 * dynamically allocated buffer is to be used.
 *
 * This routine enforces an input length of 512 bytes, which may result in an
 * output packet greater than 512 bytes.
 */
#if 1
int cobs_encode(const uint8_t *in_packet, size_t size, uint8_t *out_packet)
{
    uint8_t *start_output = out_packet;
    const uint8_t *end_input = in_packet + size;
    uint8_t *code_ptr;

    /*
     * Validate some parameters.
     */
    if (in_packet == (uint8_t *) 0 || out_packet == (uint8_t *) 0 || size > 512)
    {
        errno = EINVAL;
        return -1;
    }

    /*
     *  Set up the initial code pointer and store 0x00 as a placeholder.  The
     *  code values will be written back into *code_ptr once they are known.
     */
    *(code_ptr = out_packet++) = 0x00;

    while (in_packet < end_input)
    {
        if (*in_packet != 0)
        {
            /*
             * Protect against excessive runs of non-0x00 bytes by checking the
             * expected code value against the limit.
             */
            if (out_packet - code_ptr > COBS_MAX_BLOCK)
            {
                /*
                 * Emit a new maximum code length value and save the new code
                 * location for the end of this run.
                 */
                *code_ptr = (COBS_MAX_BLOCK + 1);
                *(code_ptr = out_packet++) = 0x00;
            }

            /*
             * Now, copy the actual data value.
             */
            *out_packet++ = *in_packet++;
        }
        else
        {
            in_packet++;
            *code_ptr = out_packet - code_ptr;
            *(code_ptr = out_packet++) = 0x00;
        }
    }

    /*
     * Set the final block length.
     */
    *code_ptr = out_packet - code_ptr;

    return out_packet - start_output;
}
#else
/* Stuffs "length" bytes of data at the location pointed to by
 * "input", writing the output to the location pointed to by
 * "output". Returns the number of bytes written to "output".
 *
 * Remove the "restrict" qualifiers if compiling with a
 * pre-C99 C dialect.
 */
int32_t cobs_encode(const uint8_t *input, size_t length, uint8_t *output)
{
    uint32_t read_index = 0;
    uint32_t write_index = 1;
    uint32_t code_index = 0;
    uint8_t code = 1;

    while(read_index < length)
    {
        if(input[read_index] == 0)
        {
            output[code_index] = code;
            code = 1;
            code_index = write_index++;
            read_index++;
        }
        else
        {
            output[write_index++] = input[read_index++];
            code++;
            if(code == 0xFF)
            {
                output[code_index] = code;
                code = 1;
                code_index = write_index++;
            }
        }
    }

    output[code_index] = code;

    return write_index;
}

#endif
/*!
 * @}
 */


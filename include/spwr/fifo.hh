/**
 * @copyright
 * Copyright 2018, SunPower Corp.
 * All rights reserved.
 *
 * @file fifo.hh
 * C++ thread based FIFO API
 *
 * @date 2018-Jul-1
 * @author Miles Bintz <mbintz@sunpower.com>
 */

#ifndef _FIFO_HH_
#define _FIFO_HH_

#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

#include <exception>

//! @addtogroup libutil_api
//! @{

class Fifo
{
    private:
        pthread_mutex_t m_mutex;
        pthread_cond_t m_msgavailable;

        uint32_t m_maxmsgs;
        uint32_t m_msgSize;
        uint32_t m_index;
        uint32_t m_outdex;
        uint8_t *m_pStorage;

        uint32_t next(uint32_t idx)
        {
            idx++;
            if (idx >= m_maxmsgs) idx = 0;
            return idx;
        }

    public:
        Fifo(uint32_t msgs, uint32_t msgSize);
        ~Fifo();

        void Add(void *pItem, uint32_t len);
        void Remove(void *pItem, size_t *pLen, int32_t timeout = -1);
        void Flush();
};

//! @}
#endif

/**
 * @copyright
 * Copyright 2018, SunPower Corp
 * All rights reserved.
 *
 * @file linbo.hh
 * Line Interface Board Packet API
 *
 * @date 2018-Jul-1
 * @author Miles Bintz <mbintz@sunpower.com>
 */

#ifndef _LINBO_HH_
#define _LINBO_HH_

#include <stdlib.h>
#include <stdint.h>

#include "spwr/transcieverif.hh"
#include "spwr/cobsuart.hh"
#include "sbt/scooterProto.h"

// /////////////////////////////////////////////////////////////
//
// Define and extend some structure definitions from ScooterProto.h
//
// /////////////////////////////////////////////////////////////

#pragma pack(push,1)

//! This comes from sSCCmdHeaderV2_t but we leave off the device field
//! so we can describe the onion a little better in this implementation
typedef struct _sLInBoV2Payload
{
    //! Logical unit selector.  LUN1 implements the the sidecar
    //! "version 1" protocol
    uint8_t lun;

    //! Flags can toggle binary settings within the sidecar.  @todo determine
    //! whether a flag sticks or is set only for the duration of this one
    //! command-response
    uint32_t flags;

    //! The payload itself
    // 5 is sizeof(lun + flags) @TODO describe this dynamically?
    uint8_t buf[COBS_MAX_BLOCK - sizeof(sSCCmdHeader_t) - 5];
} sLInBoV2Packet;

//! This union aggregates all the command buffers that could be possibly
//! be sent to a Line Interface Board
typedef union _uLInBoCommands
{
    uint8_t buf[COBS_MAX_BLOCK - sizeof(sSCCmdHeader_t)];
    sSCCmdWritePLC_t plc;
    sSCCmdPing_t png;
    sSCCmdUpgrade_t upg;
    sSCCmdConfig_t cfg;
    sSCCmdWhiteList_t wls;
    sSCCmdFilter_t flt;
    sSCCmdResetStatusFlags_t rfl;
    sSCCmdWNConfig_t wcf;
    sLInBoV2Packet v2p;
    //! @todo - add the eASIC support here
} uLInBoCommands;

//! Define the basic structure of a line-interface board packet based
//! on the legacy definitions in scooterProto.h
typedef struct
{
    //! All commands and responses to a line interface board start with
    //! a header (which contains the device/command byte)
    sSCCmdHeader_t hdr;

    //! And then the rest of the command/response payload...
    uLInBoCommands cmd;
} sLInBoPacket;

typedef struct
{
    //! leave room for a prefix in case we want to prepend headers
    //! without copying
    uint8_t pre[32];

    //! Statically allocate a buffer (the upper size is bounded
    //!  by COBS)
    uint8_t buf[COBS_MAX_BLOCK];
} sPaddedBuffer;

#pragma pack(pop)

// /////////////////////////////////////////////////////////////
//
// Define construction classes for Line Interface Board packets
//
// /////////////////////////////////////////////////////////////

//! API to make Line Interface Board packet creation easier...
class LInBoPacket
{
    private:
        void init(eSC_INTF_CMD cmd);

    protected:
        sPaddedBuffer m_buf;

        //! m_pPacket is a union that overlays the above buffer
        sLInBoPacket *m_pPacket;

        //! How many bytes are needed to transmit this payload?
        size_t m_length;

        //! Which type of command (or response) are we dealing with?
        eSC_INTF_CMD m_cmdType;

    public:
        LInBoPacket();
        LInBoPacket(eSC_INTF_CMD cmd);
        LInBoPacket(void *pBuf);

        ~LInBoPacket();

        //! Return the payload (for when it's time to transmit)
        uint8_t* buf();

        //! return the number of bytes that need transmitting
        size_t len();
};

class LInBoPing : public LInBoPacket
{
    public:
        //! Default constructor just uses a pre-defined ping string
        LInBoPing();

        //! Alternate constructor allows us to define the ping payload
        LInBoPing(const uint8_t *pBuf, size_t len);
        LInBoPing(void *pBuf);
};


#endif // __LINBO_HH__

#include "cJSON.h"

cJSON* GetOrCreateJSONObject(cJSON *parent, const char *name); 
cJSON* GetOrCreateJSONArray(cJSON *parent, const char *name); 
cJSON* GetOrCreateFormattedJSONObject(cJSON *parent, const char *fmt, ...);
cJSON* GetOrCreateJSONInteger(cJSON *parent, const char *name, int initVal);
int ReadJSONInteger(cJSON *parent, const char *name, int initVal);
double ReadJSONDouble(cJSON *parent, const char *name, double initVal);
cJSON* GetOrCreateJSONDouble(cJSON *parent, const char *name, double initVal);
cJSON* GetOrCreateJSONString(cJSON *parent, const char *name, const char* msg);
char *ReadJSONString(cJSON *parent, const char *name, char* msg);
void AddFormattedJSONString(cJSON *item, const char *name, const char *msg, ...);

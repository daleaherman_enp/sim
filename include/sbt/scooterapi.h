/******************************************************************************
 * scooterapi.h
 *
 * This file implements all communications through sidecar/plcdev board.
 *
 * Copyright (c) 2015 Sunpower Corp.  All rights reserved.
 *
 * Created on: June 8, 2015
 *
 * Author: Miles Bintz <mbintz@sunpower.com>
 *
 ****************************************************************************/

#ifndef _SCOOTERAPI_H_
#define _SCOOTERAPI_H_

#include <stdint.h>
#include "scooterProto.h"
#include "connection.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \addtogroup sidecar_api Sidecar API
 * @{
 */

typedef struct sScooter_t* hScooter_t;

typedef int32_t (*scooterCB_t)(void *pCookie, uint32_t reason, uint32_t val0,
                                uint32_t val1);

typedef int32_t (*scooterMeterCB_t)(void *pCookie, eSC_INTF_CMD cmd, const uint8_t *pBuf);

int32_t ScooterNew(hScooter_t *pHandle, connection_handle_t hConn);
int32_t ScooterDelete(hScooter_t handle);

int32_t ScooterCallbackSet(hScooter_t handle, scooterCB_t pfncb, void *pCookie);
int32_t ScooterRegisterMeterCallback(hScooter_t handle, scooterMeterCB_t pfncb, void *pCookie);

int32_t ScooterExecute(hScooter_t handle, uint8_t *pTxbuf, uint32_t txlen,
                        uint8_t *pRxbuf, size_t *rxlen, eSCLun_t *pLun);
int32_t ScooterWrite(hScooter_t handle, uint8_t *pTxbuf, uint32_t txlen);
int32_t ScooterRead(hScooter_t handle, uint8_t *pRxbuf, size_t *rxlen, eSCLun_t *pLun);
int32_t ScooterReadPLC(hScooter_t handle, uint8_t *pRxbuf, size_t *rxlen);
int32_t ScooterTimeoutSet(hScooter_t handle, uint32_t timeout);
int32_t ScooterTimeoutGet(hScooter_t handle);

int32_t ScooterPackWriteTx(uint8_t *pOutbuf, uint32_t flags, const uint8_t *pInbuf,
                             uint32_t inlen);

int32_t ScooterPackEcho(uint8_t *pOutbuf, uint8_t *pInbuf, uint32_t inlen);
int32_t ScooterPackAsciiHex(uint8_t *pOutbuf, const char *hexStr);

int32_t ScooterPackUpgradeData(uint8_t *pOutbuf, uint32_t ui32address,
                                  uint8_t *pBuf, uint32_t inlen);
int32_t ScooterPackUpgradeCommit(uint8_t *pOutbuf);
int32_t ScooterPackUpgradeStatus(uint8_t *pOutbuf);
int32_t ScooterPackVersionGet(uint8_t *pOutbuf);
int32_t ScooterPackPLCStatsReset(uint8_t *pOutbuf);
int32_t ScooterPackPLCStatsRead(uint8_t *pOutbuf);
int32_t ScooterPackResetSidecar(uint8_t *pOutbuf);
int32_t ScooterPackFFTStart(uint8_t *pOutbuf);
int32_t ScooterPackFFTRead(uint8_t *pOutbuf, uint8_t bin, uint8_t nSamples, uint16_t idxStart);
int32_t ScooterFFTRead(hScooter_t hScooter, eSCFFTBIN_t bin, int16_t *pFFTSamples);

int32_t ScooterPackSCStatsReset(uint8_t *pOutbuf);
int32_t ScooterPackSCUpgradeCommand(uint8_t *pOutbuf);
int32_t ScooterPackSCStatsRead(uint8_t *pOutbuf);
int32_t ScooterPackSCUpgradeData(uint8_t *pOutbuf, uint32_t address,
                               uint32_t nBytes, uint8_t *pPayload);
int32_t ScooterPackUpgradeCommit(uint8_t *pOutbuf);
int32_t ScooterLunSet(hScooter_t handle, uint8_t lun);

int32_t ScooterLunGet(hScooter_t handle);

int32_t ScooterPackRawModeSet(uint8_t *pOutbuf, uint32_t ui32Mode);
int32_t ScooterPackAnalogDataRead(uint8_t *pOutbuf);
int32_t ScooterPackGridDataRead(uint8_t *pOutbuf);
int32_t ScooterPackStatusFlagsReset(uint8_t *pOutbuf, uint32_t flags);
int32_t ScooterPackConfig(uint8_t *pOutbuf, const sSCConfig_t *pCfg);
int32_t ScooterPackWhitelistWrite(uint8_t *pOutbuf, const sSC_whitelist *pList);
int32_t ScooterPackWhitelistClear(uint8_t *pOutbuf);
int32_t ScooterPackFilterSet(uint8_t *pOutbuf, const sSC_filtering_options *pFilter);
int32_t ScooterPackCTConfig(uint8_t *pOutbuf, sWN_set_ct_configuration_t *pConfig);

int32_t ScooterPackWattnodeStatsRead(uint8_t *pOutbuf);
int32_t ScooterPackWattnodeStatsReset(uint8_t *pOutbuf);
int32_t ScooterPackWattnodePoll(uint8_t *pOutbuf);
int32_t ScooterPackLoopbackSet(uint8_t *pOutbuf, uint32_t mode);
int32_t ScooterPackV2(uint8_t *pOutbuf, uint8_t ui8LUN, uint16_t ui16Flags,
                        uint8_t *pIn, uint32_t inlen);
int32_t ScooterPackHistogramRead(uint8_t *pOutbuf, uint16_t index, uint8_t name);
int32_t ScooterPackCSMAStatsRead(uint8_t *pOutbuf);
int32_t ScooterPackSCReset(uint8_t *pOutbuf);

int32_t ScooterUnpackV2(const sSCRspV2_t *pInput, sSCRsp_t *pOutput);
int32_t ScooterUnpackEcho(const uint8_t *pInbuf, uint8_t *pEcho, uint32_t inlen);
int32_t ScooterUnpackVersion(const uint8_t *pInBuf, sSC_version *pVersion);
int32_t ScooterUnpackPLCStatsRead(const uint8_t *pInBuf, sSCPLCStats_t *pPLCStats);
int32_t ScooterUnpackSCStatsRead(const uint8_t *pInBuf, sSC_stats *pSCStats);
int32_t ScooterUnpackAnalogDataRead(const uint8_t *pInBuf, sSCAnalogData *pAdata);
int32_t ScooterUnpackGridDataRead(const uint8_t *pInBuf, sSCGridData *pGdata);
int32_t ScooterUnpackWattnodeStatsRead(const uint8_t *pInBuf, sWN_stats *pWNstats);
int32_t ScooterUnpackWattnodePoll(const uint8_t *pInBuf, sWN_data *pWNpoll);
int32_t ScooterUnpackWattnodeFastPower(const uint8_t *pInBuf, sWN_FAST_Power *pWNFast);

int32_t ScooterUnpackWhitelistResponse(const uint8_t *pInBuf, sSC_SetWhitelistResponse *pResp);
int32_t ScooterUnpackAck(const uint8_t *pInBuf, eSC_INTF_CMD exp, sSCAck_t *pAck);
int32_t ScooterUnpackConfig(const uint8_t *pInBuf, sSCConfig_t *pCfg);
int32_t ScooterUnpackCSMAStatsRead(const uint8_t *pInBuf, sSCCSMAStats_t *pcsmaStats);
int32_t ScooterUnpackHistogram(const uint8_t *pInBuf, sSCHistogram_t *pHistogram);
#ifndef WIN32
static const uint8_t SCOOTER_MAX_PAYLOAD = 240;
#else
#define SCOOTER_MAX_PAYLOAD 240
#endif // !WIN32
/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif // _SCOOTERAPI_H_

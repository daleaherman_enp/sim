/*
 * @copyright
 * Copyright 2014, SolarBridge Technologies, LLC\n
 * All rights reserved.
 *
 * @file scooter_uart.c
 *  Scooter-side UART interface
 *
 * @date Sep 11, 2014
 * @author Julie Haugh <j.haugh@solarbridgetech.com>
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <alloca.h>
#include <memory.h>
#include <assert.h>
#include <sys/ioctl.h>
#ifdef __gnu_linux__
#include <linux/usbdevice_fs.h>
#include <linux/serial.h>
#endif

#include <time.h>
#include <pthread.h>
#include <semaphore.h>

#include "sbt/os.h"
#include "sbt/scooterProto.h"
#include "sbt/log.h"
#include "sbt/errno.h"
#include "sbt/scooter_uart.h"
#include "sbt/cobs.h"
#include "sbt/crc.h"
#include "sbt/fifo.h"

#ifndef MIN
    #define MIN(x,y) ((x) < (y) ? (x) : (y))
#endif

typedef enum
{
    COBS_WAITING,
    COBS_SCANNING,
    COBS_OVERRUN,
    COBS_DONE
} cobs_state_t;

int32_t g_errcrc;

static void uart_packet_dump(const uint8_t *prefix, uint8_t *buffer, int length)
{
    int i;

    fprintf(stderr, "%s(%d): ", prefix,length);
    for (i = 0; i < length; i++)
    {
        if (i > 0 && i % 16 == 0)
        {
            fputc('\n', stderr);
        }

        fprintf(stderr, "%02x ", buffer[i]);
    }
    fputc('\n', stderr);
    fflush(stderr);
}


//! \addtogroup uart_config
//! @{

//! The max lenght of the string to hold the device name
#define UART_DEVNAME_LENGTH     126

//! The max size of a raw COBS buffer
#define COBS_PACKET_LENGTH      (SCOOTER_MAX_PACKET + 1)

//! @}

struct scooter_uart_t
{
    int32_t uart_fd;
    uint32_t uart_debug;
    uint32_t uart_read_timeout;
    size_t cobs_length;
    cobs_state_t cobs_state;
    uint8_t cobs_packet_in[COBS_PACKET_LENGTH];
    uint8_t devname[UART_DEVNAME_LENGTH+1];

    // track the thread ID of the read thread for this UART
    pthread_t thid;
    int32_t threadexit;
    bool bRunning;
    pfnUartCB_t pfncb;
    void *pCookie;
    hFifo ackfifo;
    hFifo fifo;

};

//! \addtogroup uart_api UART API
//! @{


char * UartDeviceName(scooter_uart_handle_t hThis)
{
    if (hThis == NULL)
        return NULL;
    return (hThis->devname);
}


int32_t UartDebugSet(scooter_uart_handle_t hThis, uint32_t ui32Flags)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    hThis->uart_debug = ui32Flags;

    return 0;
}

/*!
 * @details Set the number of milliseconds to wait for a message response
 *
 * @param timeout - Response timeout in milliseconds
 */
int32_t UartTimeoutReadSet(scooter_uart_handle_t hThis, uint32_t timeout)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    hThis->uart_read_timeout = timeout;

    return 0;
}

/*!
 * @details Get the number of milliseconds to wait for a message response
 *
 * @param timeout - Response timeout in milliseconds
 */
int32_t UartTimeoutReadGet(scooter_uart_handle_t hThis, uint32_t *pTimeout)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    *pTimeout = hThis->uart_read_timeout;

    return 0;
}

/*!
 * @details Close the UART connection and reset the open connection indication.
 */
int32_t UartClose(scooter_uart_handle_t hThis)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    hThis->bRunning = false;
    pthread_join(hThis->thid, NULL);

    if (hThis->uart_fd != -1)
    {
        LOG_VERBOSE_LO("Closing fd %d\n", hThis->uart_fd);
        close(hThis->uart_fd);
    }
    else
    {
        LOG_VERBOSE_LO("Attempting to close an unopened UART\n");
    }

    fifoDestroy(hThis->ackfifo);
    fifoDestroy(hThis->fifo);

    hThis->uart_fd = -1;
    hThis->uart_debug = 0;

    return 0;
}

/*!
 * @details Update the input buffer state, using a single byte of data.
 *
 * @param cobs_char - The character which was just received from the UART.
 *
 * @returns The new value of cobs_state.
 */
static void uart_cobs_receive(scooter_uart_handle_t hThis, uint8_t cobs_char)
{
    switch (hThis->cobs_state)
    {
        case COBS_WAITING:
            if (cobs_char == 0x00)
            {
                hThis->cobs_state = COBS_SCANNING;
                hThis->cobs_length = 0;
            }
            return;

        case COBS_SCANNING:
            if (cobs_char == 0x00)
            {
                if (hThis->cobs_length > 0)
                {
                    hThis->cobs_state = COBS_DONE;
                    return;
                }
                else
                {
                    return;
                }
            }
            if (hThis->cobs_length < sizeof(hThis->cobs_packet_in))
            {
                hThis->cobs_packet_in[hThis->cobs_length++] = cobs_char;
                return;
            }
            hThis->cobs_state = COBS_OVERRUN;
            return;

        case COBS_OVERRUN:
            if (cobs_char == 0x00)
            {
                hThis->cobs_state = COBS_WAITING;
                hThis->cobs_length = 0;
            }
            return;

        case COBS_DONE:
            //
            // being in the "done" state indicates that a callback needs to be
            // made.  After the callback is made the state should be set to
            // waiting
            //
            return;

        default:
            //@todo handle this?
            assert(0);
            return;
    }
}

/*!
 * @details Flush all unread characters from the UART input.
 * @todo - figure out why flush and drain don't seem to be working
 */
int32_t UartFlush(scooter_uart_handle_t hThis)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    LOG_VERBOSE_MED("UartFlush\n");
#if 0
    if (hThis->uart_fd != -1)
    {
        tcflush(hThis->uart_fd, TCIFLUSH);
        // tcdrain(hThis->uart_fd);
    }
    return 0;
#else
    return fifoFlush(hThis->fifo);
#endif
}

int32_t UartConnectionNew(scooter_uart_handle_t *pHandle)
{
    scooter_uart_handle_t hThis;

    if (!pHandle)
    {
        return -EINVAL;
    }

    hThis = calloc(1, sizeof(struct scooter_uart_t));

    if (!hThis)
    {
        return -ENOMEM;
    }

    hThis->cobs_state = COBS_WAITING;
    hThis->uart_fd = -1;
    hThis->uart_debug = 0;
    hThis->uart_read_timeout = 500;

    *pHandle = hThis;

    return 0;
}

int32_t UartConnectionDelete(scooter_uart_handle_t hThis)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    if (hThis->uart_fd != -1)
    {
        return -EADDRINUSE;
    }

    memset(hThis, 0xfe, sizeof(struct scooter_uart_t));

    free(hThis);

    return 0;
}

static void *uart_read_thread(void *parm)
{
    scooter_uart_handle_t hThis;
    hThis = (scooter_uart_handle_t)parm;
    struct pollfd poll_fd;
    uint16_t cobs_crc;
    uint16_t data_crc;
    int newlen;
    char c;

    hThis->cobs_state = COBS_WAITING;
    hThis->cobs_length = 0;

    while(hThis->bRunning)
    {
        int rc;

        poll_fd.fd = hThis->uart_fd;
        poll_fd.events = POLLIN;
        poll_fd.revents = 0;

        rc = poll(&poll_fd, 1, 10);
        if (rc < 0)
        {
            LOG_ERROR("poll() failed\n");
            hThis->threadexit = __LINE__;
            return 0;
        }
        else if(rc == 0)
        {
            // if our poll times out then just loop back around and read again
            continue;
        }

        //
        // if we get here then there is at least one byte ready to be read
        //
        rc = read(hThis->uart_fd, &c, 1);

        //
        // a return code of less-than-zero is never acceptable.  Bail.
        //
        if(rc < 0)
        {
            hThis->threadexit = __LINE__;
            return 0;
        }

        //
        // this should just never happen but shouldn't be catastrophic.
        //
        if(rc == 0)
        {
            LOG_WARNING("0-byte read after a non-zero poll\n");
            continue;
        }

        //
        // And by the time we get here we should have exactly one byte read.
        // Pass it to the COBS decoder
        //
        uart_cobs_receive(hThis, c);

        if(hThis->cobs_state == COBS_DONE)
        {
            //
            // We have a properly framed COBS-encoded packet with a length of
            // cobs_length.  Decode it, then strip what should be a trailing NUL
            // byte.
            //
            newlen = cobs_decode_in_place(hThis->cobs_packet_in,
                                 hThis->cobs_length);

        }
        else if(hThis->cobs_state == COBS_OVERRUN)
        {
            //
            // note the error and keep gathering bytes
            //
            LOG_WARNING("Overrun\n");
            continue;
        }
        else
        {
            //
            // For any other state we just keep on keepin' on, one character
            // at a time
            //
            continue;
        }

        //
        // If we get here we're in COBS_DONE state
        //

        if(newlen < 0)
        {
            LOG_WARNING("WARNING - cobs decode length < 0\n");
            continue;
        }
        else
        {
            LOG_VERBOSE_MED("cobs_decode: length = %d\n", newlen);
        }

        if (hThis->cobs_packet_in[newlen - 1] == 0)
        {
            newlen--;
        }

        if (hThis->uart_debug & TRACE_RX)
        {
            uart_packet_dump("RX", hThis->cobs_packet_in, newlen);
        }

        //
        // A COBS-encoded packet cannot be 1 or 3 bytes long.
        //
        if (newlen < 4 && newlen != 2)
        {
            LOG_ERROR("incorrect length %ld\n", newlen);
        }
        else
        {
            LOG_VERBOSE_MED("COBS packet with good length\n");
        }

        //
        // The packet is now a device address, status flags, packet, then two byte
        // CRC-16-CCITT.
        //
        cobs_crc = hThis->cobs_packet_in[newlen - 2]
                   | (hThis->cobs_packet_in[newlen - 1] << 8);

        data_crc = crc16ccitt(hThis->cobs_packet_in, newlen - 2,
                                        0xFFFF);
        if (cobs_crc != data_crc)
        {
            g_errcrc++;
            LOG_ERROR("Expected CRC %04x, got %04x (len = %ld)\n",
                      data_crc, cobs_crc, newlen);

            continue;
        }
        else
        {
            LOG_VERBOSE_MED("scooter_uart: CRCs good length %d B01 %d %d\n",
                    newlen - 2, hThis->cobs_packet_in[0],
                    hThis->cobs_packet_in[1]);
        }

        //
        // Strip the CRC off the end -- it's all done with.
        //
        newlen -= 2;

        //
        // At this point we're ready to inform someone that we've got a scooter
        // frame for them
        //
        if(hThis->pfncb)
        {
            rc = hThis->pfncb(hThis->pCookie, hThis->cobs_packet_in, newlen);
            // @todo decide what to do with an error from the callback (if
            // anything)
            if(rc != 0)
            {
                LOG_WARNING("Callback returned %d\n", rc);
            }
        }

        //
        // and reset for another COBS frame
        //
        hThis->cobs_state = COBS_WAITING;
        hThis->cobs_length = 0;
    }
    hThis->threadexit = __LINE__;
    pthread_exit(0);
    return 0;
}

typedef struct
{
    uint8_t ackdev;
    uint8_t reason;
} acknak_t;

int32_t defaultReadCallback(void *pCookie, uint8_t *pBuf, const size_t rxLen)
{
	scooter_uart_handle_t handle = (scooter_uart_handle_t)pCookie;
    sSCRsp_t *pRsp = (sSCRsp_t*)pBuf;
    int rc;
    acknak_t an;

    // we enter here in the context of the Connection's read thread.  pBuf
    // is a raw (Layer 2) scooterProto frame.  It exists only in pBuf for now.
    //
    // There is a window to the north and a door to the east.  There is a
    // night-stand with an open drawer on the south wall.  What would you like
    // to do?
    switch(pRsp->header.dev)
    {
        case eSC_INTF_ACK:
        case eSC_INTF_NACK:
            // acks and nacks need to be signaled to the tx'ing thread
            an.ackdev = pRsp->header.dev;
            an.reason = pRsp->payload[0];
            rc = fifoAdd(handle->ackfifo, (uint8_t*) &an, sizeof(an));
            if (rc != 0)
            {
                LOG_ERROR("Couldn't push ack to ackfifo %d\n", rc);
            }
            break;
        case eSC_INTF_WNPOLL:
        case eSC_INTF_WN_FAST_POWER:
            // default callback -- just drop it.  You can only get WattNode
            // messages if you "disconnect" this callback (ex. by using scooterapi.)

            //
            // Do Nothing
            //
            break;

        default:
        {
            rc = fifoAdd(handle->fifo, pBuf, rxLen);
            if (rc != 0)
            {
                LOG_ERROR("Couldn't push buffer to read fifo %d\n", rc);
            }
            break;
        }
    }
    return 0;
}

#ifndef B460800
#define B460800 0x1006
#endif
/*!
 * @details Open the UART device and configure the termios settings for the UART proper.
 *
 * @param device - Name of the physical device to open.
 *
 * @returns 0 = success, -1 = failure.
 */
int32_t UartDeviceOpen(scooter_uart_handle_t hThis, const uint8_t *device)
{
    struct termios options;
    int32_t rc;
    char *pComma;
    uint32_t br;
    int newbaud = 38400;
#ifdef __gnu_linux__
    int closestSpeed;
    struct serial_struct ss;
#endif

    if (!hThis || !device)
    {
        return -EINVAL;
    }
#ifndef B460K
    br = B38400;
#else
    br = B460800;
#endif

    pComma=strchr(device, ',');
    if(pComma == NULL)
    {
        strncpy(hThis->devname, device, UART_DEVNAME_LENGTH);
    }
    else
    {
        int devlen;
        devlen = MIN((long)pComma - (long)device, UART_DEVNAME_LENGTH);
        strncpy(hThis->devname, device, devlen);
        // now extract the over-ridden baud-rate
        newbaud = strtol(pComma + 1, 0, 0);
        LOG_VERBOSE_MED("OPEN Uart %s baud %ld\n", hThis->devname, newbaud);
    }

    // Note: On OS X this open call takes a long time.
    rc = open(hThis->devname, O_RDWR | O_NONBLOCK | O_NDELAY);
    if (rc < 0)
    {
        return -errno;
    }
    else
    {
        hThis->uart_fd = rc;
    }

    tcgetattr(hThis->uart_fd, &options);
    cfmakeraw(&options);
    options.c_cflag |= (CLOCAL | CREAD);

    // make sure we're not using a custom baud rate unless we hit the default
    // case below
#ifdef __gnu_linux__
    ioctl(hThis->uart_fd, TIOCGSERIAL, &ss);
    ss.flags &= ~ASYNC_SPD_MASK;
    ioctl(hThis->uart_fd, TIOCSSERIAL, &ss);
#endif

    switch(newbaud)
    {
        case 38400:
            br = B38400;
            break;
        case 115200:
            br = B115200;
            break;

        case 230400:
            br = B230400;
            break;

        case 460800:
            br = B460800;
            LOG_VERBOSE_LO("Using over-ride baud-rate of 460800\n");
            break;

        default:
#ifdef __gnu_linux__
            LOG_INFO("Using custom baud-rate of %d\n", newbaud);
            ioctl(hThis->uart_fd, TIOCGSERIAL, &ss);
            ss.flags = (ss.flags & ~ASYNC_SPD_MASK) | ASYNC_SPD_CUST;
            ss.custom_divisor = (ss.baud_base + (newbaud / 2)) / newbaud;
            closestSpeed = ss.baud_base / ss.custom_divisor;

            if (closestSpeed < newbaud * 98 / 100 || closestSpeed > newbaud * 102 / 100)
            {
                LOG_WARNING("Cannot set serial port speed to %d. Closest possible is %d\n", newbaud, closestSpeed);
            }

            ioctl(hThis->uart_fd, TIOCSSERIAL, &ss);
            br = B38400;
#endif
            break;
    }



    rc = cfsetispeed(&options, br);
    if(rc != 0)
    {
        LOG_ERROR("Could not set input baud rate\n");
    }

    rc = cfsetospeed(&options, br);
    if(rc != 0)
    {
        LOG_ERROR("Could not set output baud rate\n");
    }

    tcflush(hThis->uart_fd, TCIFLUSH);
    rc = tcsetattr(hThis->uart_fd, TCSANOW, &options);
    if (rc < 0)
    {
        close(hThis->uart_fd);
        hThis->uart_fd = -1;
        return -2;
    }

    rc = fifoCreate(&hThis->fifo, 8, COBS_PACKET_LENGTH);
    assert(rc == 0);

    rc = fifoCreate(&hThis->ackfifo, 8, sizeof(acknak_t));
    assert(rc == 0);

//    hThis->bWaitingForAck = false;


    hThis->bRunning = true;
    hThis->pfncb = defaultReadCallback;
    hThis->pCookie = (void*)hThis;
    rc = pthread_create(&hThis->thid, NULL, uart_read_thread, (void*)hThis);
    if(rc != 0)
    {
        LOG_ERROR("Unable to create read thread.  rc = %d\n", rc);
        return -rc;
    }

    return 0;
}

/*!
 * Register a callback to be called when the read thread has a valid COBS frame.
 *
 * @param hThis is a handle to the UART object instance
 * @param pfn is a pointer to a function to call when a complete COBS frame has
 *        been received
 * @param pCookie is a value to be passed to the callback
 *
 * @return 0 for success, < 0 for error
 */
int32_t UartRegisterCallback(scooter_uart_handle_t hThis, pfnUartCB_t pfn,
                               void *pCookie,
                               pfnUartCB_t *oldpfn, void **ppOldcookie)
{
    if(!hThis) return -EINVAL;

    if(oldpfn)
    {
        *oldpfn = hThis->pfncb;
    }
    if(ppOldcookie)
    {
        *ppOldcookie = hThis->pCookie;
    }

    hThis->pCookie = pCookie;
    hThis->pfncb = pfn;
    return 0;
}

int32_t uartreadAck(scooter_uart_handle_t hThis, uint32_t timeout,
                 uint8_t *packet, size_t *length, int32_t *device)
{
    int rc;
    uint32_t off = 1;
    uint8_t rbuf[COBS_MAX_BLOCK];
    size_t len = COBS_MAX_BLOCK;
    int minlen;
    int dev;

    if(!hThis) return -EINVAL;

   	rc = fifoRemove(hThis->ackfifo, rbuf, &len, timeout);
    if(rc != 0)
    {
        LOG_VERBOSE_MED("%d (ACK)\n", rc);
        return rc;
    }

    dev = rbuf[0];
    if (device != NULL)
    {
        *device = dev;
    }

    // this is the cobs offset that would normally be used to skip over the
    // device (and "LUN" on a PLC Rx)
    if(dev == eSC_INTF_WRITE_PLC)
    {
        off = 2;
    }

    // adust for dev, or (dev and lun) -- whichever the case may be
    len -= off;

    /*
     * Copy the packet out to the caller and tell them how long it actually was.
     * Make sure we don't over-flow the callers supplied buffer.  The caller
     * must be sure to pre-set *length to the size of their buffer.
     */
    minlen = MIN(*length, len);
    {
        memcpy(packet, rbuf + off, minlen);
    }
    *length = minlen;

    return 0;
}

int32_t uartread(scooter_uart_handle_t hThis, uint32_t timeout,
                 uint8_t *packet, size_t *length, int32_t *device)
{
    int rc;
    uint32_t off = 1;
    uint8_t rbuf[COBS_MAX_BLOCK];
    size_t len = COBS_MAX_BLOCK;
    int minlen;
    int dev;

    if(!hThis) return -EINVAL;

   	rc = fifoRemove(hThis->fifo, rbuf, &len, timeout);
    if(rc != 0)
    {
        LOG_VERBOSE_MED("%d\n", rc);
        return rc;
    }

    dev = rbuf[0];
    if (device != NULL)
    {
        *device = dev;
    }

    // this is the cobs offset that would normally be used to skip over the
    // device (and "LUN" on a PLC Rx)
    if(dev == eSC_INTF_WRITE_PLC)
    {
        off = 2;
    }

    // adust for dev, or (dev and lun) -- whichever the case may be
    len -= off;

    /*
     * Copy the packet out to the caller and tell them how long it actually was.
     * Make sure we don't over-flow the callers supplied buffer.  The caller
     * must be sure to pre-set *length to the size of their buffer.
     */
    minlen = MIN(*length, len);
    {
        memcpy(packet, rbuf + off, minlen);
    }
    *length = minlen;

    return 0;
}

/*!
 * @details Read a packet from the UART, returning the address and any sub-address.
 *
 * @param packet - Pointer to the raw packet, as it was received.
 * @param length - Pointer to the length of the decoded (raw) packet.
 * @param device - Pointer to the device that was received.
 * @param status - Pointer to a byte to place status.
 *
 */
int32_t UartRead(scooter_uart_handle_t hThis, uint8_t *packet, size_t *length,
                 int32_t *device)
{

    return uartread(hThis, hThis->uart_read_timeout, packet, length, device);
}

/*!
 * @details Read a CRC-checked and COBS framed packet from the UART
 *
 * @param hThis is a UART object handle
 * @param packet - Pointer to the raw packet, as it was received.
 * @param length - Pointer to the length of the decoded (raw) packet.
 * @param timeout in milliseconds to wait for a response to start
 *
 */
int32_t UartReadL2(scooter_uart_handle_t hThis, uint8_t *packet,
                   size_t *length, uint32_t timeout)
{
    int rc;

    if(!hThis) return -EINVAL;

    rc = fifoRemove(hThis->fifo, packet, length, timeout);
    if(rc != 0)
    {
        return rc;
    }

    return 0;
}


/*!
 * @details Write a packet to the UART, using the appropriate addresses and
 * optional sub-address
 *
 * @param packet - The raw packet to transmit.
 * @param length - The size of the raw packet in bytes.
 * @param device - The device address.
 *
 * @return 0 for success,
 */
int32_t UartWrite(scooter_uart_handle_t hThis, const uint8_t *packet,
                  size_t length, int32_t device)
{
    uint8_t *cobs_in;
    uint8_t *cobs_out;
    uint16_t crc;
    int pre_cobs_length;
    int post_cobs_length;
    int cobs_length_needed;
    int32_t rc, dev;
    uint8_t status[SCOOTER_MAX_PACKET];
    size_t rxlen;

    if ((hThis->uart_fd == -1) || (length > SCOOTER_MAX_PACKET))
    {
        return -EINVAL;
    }

    cobs_in = alloca(length + 1 + 1 + 2);

    cobs_in[0] = device;

    // For PLC we place status flags after the device
    if(device == ePLC_INTF_WRITE_TX)
    {
        pre_cobs_length = length + 4;
        cobs_in[1] = PLC_FLAGS_SYNCHRONOUS_PLC;
        memcpy(cobs_in + 2, packet, length);

        crc = crc16ccitt(cobs_in, length + 2, 0xFFFF);

        cobs_in[length + 2] = (uint8_t) (crc & 0xFF);
        cobs_in[length + 3] = (uint8_t) ((crc >> 8) & 0xFF);
    }
    else
    {
        pre_cobs_length = length + 3;
        memcpy(cobs_in + 1, packet, length);

        crc = crc16ccitt(cobs_in, length + 1, 0xFFFF);

        cobs_in[length + 1] = (uint8_t) (crc & 0xFF);
        cobs_in[length + 2] = (uint8_t) ((crc >> 8) & 0xFF);
    }

    cobs_length_needed = cobs_get_required_size(pre_cobs_length);

    cobs_out = alloca(cobs_length_needed);
    cobs_out[0] = 0;
    post_cobs_length = cobs_encode(cobs_in, pre_cobs_length, cobs_out + 1);
    cobs_out[post_cobs_length + 1] = 0;

    post_cobs_length += 2;

    LOG_VERBOSE_MED("\n");
    if (hThis->uart_debug & TRACE_TX)
    {
        // uart_packet_dump(cobs_out, post_cobs_length);
        uart_packet_dump("TX", cobs_in, pre_cobs_length);

    }

    fifoFlush(hThis->ackfifo);
    rc = write(hThis->uart_fd, cobs_out, post_cobs_length);
    if (rc != post_cobs_length)
    {
        LOG_WARNING("Unable to send entire payload\n");
        return -SBT_EUARTWRITEFAIL;
    }

    if (device == ePLC_INTF_WRITE_TX)
    {
        LOG_VERBOSE_MED("Waiting for PLC TX Ack\n");
//        hThis->bWaitingForAck = true;

        while (true)
        {
            dev = 0;
            rxlen = sizeof(status);
            // here we have to wait for the longest PLC packet possible to transmit
            // and then for a 6-7 byte response to come back @ 38.4K
            rc = uartreadAck(hThis, 2000, status, &rxlen, &dev);
            if ((rc == 0) && (dev == ePLC_INTF_NACK))
            {
                continue;
            }
            if ((rc == 0) && (dev != ePLC_INTF_ACK))
            {
                LOG_WARNING(
                        "Valid COBS frame But not ACK.  Old message?  "
                                "Discarding and waiting again... rc %d dev %d\n",
                        rc, dev);
                uart_packet_dump("BADCOBS:", status, rxlen);
                rxlen = sizeof(status);
                rc = uartreadAck(hThis, 2000, status, &rxlen, &dev);
                LOG_INFO("uartreadAck rc %d status %04x rxlen %d dev %d\n",
                        rc, status[0], rxlen, dev);
                if ((rc == 0) && (dev == ePLC_INTF_NACK))
                {
                    continue;
                }
                if (rc != 0)
                {
                    return -SBT_EUARTWRITEFAIL;
                }
                if (dev != ePLC_INTF_ACK)
                {
                    return -SBT_EMISSINGACK;
                }

                // by the time we get here, rc = 0 and the device is an ack...
            }
            if (rc || rxlen != 1 || dev != ePLC_INTF_ACK)
            {
                LOG_ERROR("Write-ack failed (%d, %d, %d)\n",
                          rc, rxlen, dev);
                return -SBT_EUARTWRITEFAIL;
            }
            else
            {
                switch (status[0])
                {
                case PLC_STATUS_OK:
                    return 0;
                case PLC_STATUS_FAIL:
                    return -SBT_EPLCERROR;
                case PLC_STATUS_BUSY:
                    return -SBT_EPLCBUSY;
                default:
                    return -SBT_EBADVALUE;
                }
            }
        }
    }

    return 0;
}

/*!
 * @details Write a packet to the UART, using the appropriate addresses and
 * optional sub-address
 *
 * @param packet - The raw packet to transmit.
 * @param length - The size of the raw packet in bytes.
 *
 * @return 0 for success,
 */
int32_t UartWriteL2(scooter_uart_handle_t hThis, const uint8_t *packet,
                  size_t length)
{
    uint8_t *cobs_in;
    uint8_t *cobs_out;
    uint16_t crc;
    int pre_cobs_length;
    int post_cobs_length;
    int cobs_length_needed;
    int32_t rc;

    if ((hThis->uart_fd == -1) || (length > SCOOTER_MAX_PACKET))
    {
        return -EINVAL;
    }

    //
    // Allocate room for payload plus CRC
    //
    pre_cobs_length = length + 2;
    cobs_in = alloca(pre_cobs_length);

    //
    // Copy the payload
    //
    memcpy(cobs_in, packet, length);

    //
    // Calculate the CRC
    //
    crc = crc16ccitt(cobs_in, length, 0xFFFF);

    //
    // Concatenate the CRC16 at the end of the payload
    //
    cobs_in[length + 0] = (uint8_t) (crc & 0xFF);
    cobs_in[length + 1] = (uint8_t) ((crc >> 8) & 0xFF);

    //
    // Now figure out the space need for the equivalent COBS encoded frame
    //
    cobs_length_needed = cobs_get_required_size(pre_cobs_length);
    cobs_out = alloca(cobs_length_needed);

    //
    // COBS frames start and end with 0x00
    //
    cobs_out[0] = 0;

    //
    // Do the encode, skipping over the leading 0
    //
    post_cobs_length = cobs_encode(cobs_in, pre_cobs_length, cobs_out + 1);

    //
    // And add the trailing 0
    //
    cobs_out[post_cobs_length + 1] = 0;

    //
    // Include the leading and trailing zero in the frame length
    //
    post_cobs_length += 2;

    LOG_VERBOSE_MED("\n");
    if (hThis->uart_debug & TRACE_TX)
    {
        // uart_packet_dump(cobs_out, post_cobs_length);
        uart_packet_dump("TXL2", cobs_in, pre_cobs_length);

    }

    //
    // Transmit the payload.  One and done.  Nothing special after the write...
    //
    rc = write(hThis->uart_fd, cobs_out, post_cobs_length);
    if (rc != post_cobs_length)
    {
        LOG_WARNING("Unable to send entire payload\n");
        return -SBT_EUARTWRITEFAIL;
    }

    return 0;
}

int32_t UartRawWrite(scooter_uart_handle_t hThis, const uint8_t *packet,
                  size_t length, int32_t device)
{
    uint8_t *cobs_in;
    uint8_t *cobs_out;
    uint16_t crc;
    int pre_cobs_length;
    int post_cobs_length;
    int cobs_length_needed;
    int32_t rc;
    useconds_t timeout_us;


    if ((hThis->uart_fd == -1) || (length > SCOOTER_MAX_PACKET))
    {
        return -EINVAL;
    }

    cobs_in = alloca(length + 1 + 1 + 2);

    cobs_in[0] = device;

    // For PLC we place status flags after the device
    if(device == ePLC_INTF_WRITE_TX)
    {
        pre_cobs_length = length + 4;
        cobs_in[1] = 0; // Write ACK not requested
        memcpy(cobs_in + 2, packet, length);

        crc = crc16ccitt(cobs_in, length + 2, 0xFFFF);

        cobs_in[length + 2] = (uint8_t) (crc & 0xFF);
        cobs_in[length + 3] = (uint8_t) ((crc >> 8) & 0xFF);
    }
    else
    {
        pre_cobs_length = length + 3;
        memcpy(cobs_in + 1, packet, length);

        crc = crc16ccitt(cobs_in, length + 1, 0xFFFF);

        cobs_in[length + 1] = (uint8_t) (crc & 0xFF);
        cobs_in[length + 2] = (uint8_t) ((crc >> 8) & 0xFF);
    }

    cobs_length_needed = cobs_get_required_size(pre_cobs_length);

    cobs_out = alloca(cobs_length_needed);
    cobs_out[0] = 0;
    post_cobs_length = cobs_encode(cobs_in, pre_cobs_length, cobs_out + 1);
    cobs_out[post_cobs_length + 1] = 0;

    post_cobs_length += 2;

    LOG_VERBOSE_MED("\n");
    if (hThis->uart_debug & TRACE_TX)
    {
        uart_packet_dump("TXRAW:", cobs_out, post_cobs_length);
    }

    rc = write(hThis->uart_fd, cobs_out, post_cobs_length);
    if (rc != post_cobs_length)
    {
        LOG_WARNING("Unable to send entire payload\n");
        return -SBT_EUARTWRITEFAIL;
    }

    /* Pace with the speed of PLC before returning
     * Bit rate 2400/s
     * UART transmit time ~ 10% of PLC
     * Timeout 1.1 * length * 8 bits in us.
     */
    // 1000000 * 8.8 = 8,800,000 / 2,400 = 88000 / 24
    length += 3; // account for PLC pre-amble
    timeout_us = (length * 88000) / 24;
    timeout_us += 5 * 1000; // account for USB framing
    rc = tsleep(timeout_us);
    if(rc)
    {
        return -EAGAIN;
    }

    return 0;
}

/*!
 * @details Send USB reset to the UART device.
 *
 */
int32_t UartReset(scooter_uart_handle_t hThis)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    LOG_VERBOSE_MED("Uart USB reset\n");

    if (hThis->uart_fd != -1)
    {
#ifdef __gnu_linux__
      return(ioctl(hThis->uart_fd, USBDEVFS_RESET, 0));
#endif
    }
    return 0;
}


//! @}

#if defined SELFTEST
#include <stdio.h>
#include <string.h>

//! \addtogroup connection_tests
//! @{

#define TEST(desc, call, test, rc, line) \
    do { \
        printf("%40.40s:", desc); \
        rc = call; \
        if (rc test) { \
            printf(" [PASS]\n"); \
        } else { \
            printf(" [FAIL] (line %d)\n", line); \
        } \
    } while(0)

#define DEFAULT_DEVICE "/dev/ttyUSB0"

int main(int argc, char*argv[])
{
    scooter_uart_handle_t hConn;
    int32_t rc;
    uint32_t i, j;
    static char pBuf_low[COBS_MAX_BLOCK];
    static char pBuf_up[COBS_MAX_BLOCK];
    static uint8_t pBuf_resp[COBS_MAX_BLOCK];
    size_t len;
    int32_t dev;
    int32_t txlen = 5, err_len = 0;
    char *pDev = DEFAULT_DEVICE;
    static char pTestStr[40];

    if((argc >= 2) && (0 == strcmp(argv[1], "-C")))
    {
        pDev = argv[2];
        fprintf(stderr, "Using device '%s'\n", pDev);
    }
    memset(pBuf_low, 0xab, COBS_MAX_BLOCK);
    memset(pBuf_up, 0xcd, COBS_MAX_BLOCK);
    memset(pBuf_resp, 0xef, COBS_MAX_BLOCK);

    TEST("Create Connection", UartConnectionNew(&hConn), >= 0, rc, __LINE__);
    TEST("Open Connection", UartDeviceOpen(hConn, pDev), >= 0, rc, __LINE__);
    TEST("Set Timeout", UartTimeoutReadSet(hConn, 500), >= 0, rc, __LINE__);
    TEST("Set debug level", UartDebugSet(hConn, 0x1), >= 0, rc, __LINE__);


    //
    // COBS packets can not be 1 or 3 bytes long
    //
    for(i = 1; i < COBS_MAX_BLOCK - 1; i++)
    {
        txlen = i;
        TEST("Set Timeout", UartTimeoutReadSet(hConn, 100 + (10 * i)), >= 0, rc, __LINE__);
        snprintf(pTestStr, 40, "UartWrite len = %d", i);
        for(j = 0; j < txlen; j++)
        {
            pBuf_low[j] = 'a' + (j % 26);
            pBuf_up[j]  = 'A' + (j % 26);
        }
        pBuf_low[j] = 0;
        pBuf_up[j] = 0;
        j++;

        snprintf(pTestStr, 40, "UartWrite len = %d\n", j);
        TEST(pTestStr, UartWrite(hConn, pBuf_low, j, ePLC_INTF_ECHO), >= 0, rc, __LINE__);
        if (rc < 0) goto fail;

        TEST("UartFlush", UartFlush(hConn), >= 0, rc, __LINE__);

        snprintf(pTestStr, 40, "UartRead \n");
        TEST(pTestStr, UartRead(hConn, pBuf_resp, &len, &dev), >= 0, rc, __LINE__);
        pBuf_resp[len] = 0;

        // if (rc < 0) goto fail;
        if (len != j)
        {
            err_len++;
            fprintf(stderr, "Unexpected response length (%d vs %ld)\n", i+1, (long int)len);
        }
        if (dev != ePLC_INTF_ECHO)
        {
            fprintf(stderr, "Unexpected responding device (%d)\n", dev);
        }
        else
        {
            if (strncmp(pBuf_resp, pBuf_up, i) != 0)
            {
                pBuf_resp[len] = 0;
                fprintf(stderr, "Response differs.  RXed: '%s'\n", pBuf_resp);
            }
        }
    }

    fprintf(stderr,"err_len = %d, g_errcrc = %d\n", err_len, g_errcrc);

    TEST("Closing connection", UartClose(hConn), >= 0, rc, __LINE__);
    TEST("Deleting connection", UartConnectionDelete(hConn), >= 0, rc, __LINE__);
    return(0);

    fail:
    TEST("Closing connection (fail-path)", UartClose(hConn), >= 0, rc, __LINE__);
    return(1);
}
#endif

// "License": Public Domain
// I, Mathias Panzenb, place this file hereby into the public domain. Use it at
// your own risk for whatever you like.

#ifndef PORTABLE_ENDIAN_H__
#define PORTABLE_ENDIAN_H__

#include <stdlib.h>
#include <stdint.h>

#ifdef DOXYGEN
/*!
 * \addtogroup endian_api Endianess Conversion Functions
 * @{
 */

//! Convert from host to big endian for 16 bit objects
#   define htobe16(x)

//! Convert from host to little endian for 16 bit objects
#   define htole16(x)

//! Convert from big endian to host format for 16 bit objects
#   define be16toh(x)

//! Convert from little endian to host format for 16 bit objects
#   define le16toh(x)

//! Convert from host to big endian for 32 bit objects
#   define htobe32(x)

//! Convert from host to little endian for 32 bit objects
#   define htole32(x)

//! Convert from big endian to host format for 32 bit objects
#   define be32toh(x)

//! Convert from little endian to host format for 32 bit objects
#   define le32toh(x)


//! Convert from host to big endian for 64 bit objects
#   define htobe64(x)

//! Convert from host to little endian for 64 bit objects
#   define htole64(x)

//! Convert from big endian to host for 64 bit objects
#   define be64toh(x)

//! Convert from little endian to host for 64 bit objects
#   define le64toh(x)

/*!
 * @}
 */

#else

//
// Modern GCC Compilers will define BYTE_ORDER, BIG_ENDIAN, and LITTLE_ENDIAN.
// Guard against toolchains that don't set these defines.  Since MIPS and ARM
// will both be built using GCC this is mainly targeted at Windows-hosted
// compilers (ie. Microsoft Visual C).
//
#if !defined __BYTE_ORDER__ || !defined __ORDER_BIG_ENDIAN__ || !defined __ORDER_LITTLE_ENDIAN__
#undef __BYTE_ORDER__
#undef __ORDER_BIG_ENDIAN__
#undef __ORDER_LITTLE_ENDIAN__

#define __ORDER_BIG_ENDIAN__ 4321
#define __ORDER_LITTLE_ENDIAN__ 1234
#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__
// #warning "Incomplete BYTE_ORDER definitions.  Assumed and forced LITTLE endian."
#endif


#if (defined(_WIN16) || defined(_WIN32) || defined(_WIN64)) && !defined(__WINDOWS__)

#	define __WINDOWS__

#endif

#if defined(__linux__) || defined(__CYGWIN__)

#	include <endian.h>

#elif defined(__APPLE__)

#	include <libkern/OSByteOrder.h>

#	define htobe16(x) OSSwapHostToBigInt16(x)
#	define htole16(x) OSSwapHostToLittleInt16(x)
#	define be16toh(x) OSSwapBigToHostInt16(x)
#	define le16toh(x) OSSwapLittleToHostInt16(x)

#	define htobe32(x) OSSwapHostToBigInt32(x)
#	define htole32(x) OSSwapHostToLittleInt32(x)
#	define be32toh(x) OSSwapBigToHostInt32(x)
#	define le32toh(x) OSSwapLittleToHostInt32(x)

#	define htobe64(x) OSSwapHostToBigInt64(x)
#	define htole64(x) OSSwapHostToLittleInt64(x)
#	define be64toh(x) OSSwapBigToHostInt64(x)
#	define le64toh(x) OSSwapLittleToHostInt64(x)

#	define __BYTE_ORDER    BYTE_ORDER
#	define __BIG_ENDIAN    BIG_ENDIAN
#	define __LITTLE_ENDIAN LITTLE_ENDIAN
#	define __PDP_ENDIAN    PDP_ENDIAN

#elif defined(__OpenBSD__)

#	include <sys/endian.h>

#elif defined(__NetBSD__) || defined(__FreeBSD__) || defined(__DragonFly__)

#	include <sys/endian.h>

#	define be16toh(x) betoh16(x)
#	define le16toh(x) letoh16(x)

#	define be32toh(x) betoh32(x)
#	define le32toh(x) letoh32(x)

#	define be64toh(x) betoh64(x)
#	define le64toh(x) letoh64(x)

#elif defined(__WINDOWS__)

#   ifdef htons
#   undef htons
#   endif

#   ifdef htonl
#   undef htonl
#   endif

#   ifdef ntohs
#   undef ntohs
#   endif

#   ifdef ntohl
#   undef ntohl
#   endif

#	include <winsock2.h>
//#	include <sys/param.h>

#	if BYTE_ORDER == LITTLE_ENDIAN

#		define htobe16(x) htons(x)
#		define htole16(x) (x)
#		define be16toh(x) ntohs(x)
#		define le16toh(x) (x)

#		define htobe32(x) htonl(x)
#		define htole32(x) (x)
#		define be32toh(x) ntohl(x)
#		define le32toh(x) (x)

#		define htobe64(x) htonll(x)
#		define htole64(x) (x)
#		define be64toh(x) ntohll(x)
#		define le64toh(x) (x)

#	elif BYTE_ORDER == BIG_ENDIAN

		/* that would be xbox 360 */
#		define htobe16(x) (x)
#		define htole16(x) __builtin_bswap16(x)
#		define be16toh(x) (x)
#		define le16toh(x) __builtin_bswap16(x)

#		define htobe32(x) (x)
#		define htole32(x) __builtin_bswap32(x)
#		define be32toh(x) (x)
#		define le32toh(x) __builtin_bswap32(x)

#		define htobe64(x) (x)
#		define htole64(x) __builtin_bswap64(x)
#		define be64toh(x) (x)
#		define le64toh(x) __builtin_bswap64(x)

#	else

#		error byte order not supported

#	endif

#	define __BYTE_ORDER    BYTE_ORDER
#	define __BIG_ENDIAN    BIG_ENDIAN
#	define __LITTLE_ENDIAN LITTLE_ENDIAN
#	define __PDP_ENDIAN    PDP_ENDIAN

#else

#	error platform not supported

#endif

/**
 * Given a pointer to a 32-bit aligned buffer, iterate across length
 * words and convert from host-endianess to big endian.
 *
 * @param pData - pointer to buffer to convert
 * @param length - length, in words, of the buffer
 */
static inline void htobe32_buffer(uint32_t *pData, size_t length)
{
    register size_t i;

    // Swap the data to Big Endian 32-bit
    for (i = 0; i < length; i++)
    {
        pData[i] = htobe32(pData[i]);
    }
}
#endif // DOXYGEN

#endif

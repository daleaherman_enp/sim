// Copyright (c) 2018-2019 Enphase Energy, Inc. All rights reserved.
//
// This file contains reference encoder functions for converting
// a SunPower AGF Profile into Enphase PCU AGF Records
//

#ifndef _PCU_HH_
#define _PCU_HH_


enum
{
   AckOkRsp = 0, AckFailRsp, UnAckReq, AckReq
};
enum
{
   EnphaseApp = 0x6, EnphaseMacCtrl = 0x5, EnphaseLegacy = 7 };

enum
{
   Directed = 0, McastMod, BcastCoord, BcastAll
};
#define cMcpSecMsgLenMask           0x3fff // on msgHdr->msgLen
typedef struct sL2TxFrameInfo {


   int                  protocolId;
   int                scopeInd;
   uint8_t                    protoVer;
   struct
   {
      uint8_t                    msgSrcEmu;
      uint16_t                   modAddr;
      uint8_t                 domainAddr[6];
   } reassPattern;
   int                  ackCtrl;
   uint8_t                    segMsgDataLen;
   bool                        firstSeg;
   uint8_t                    segsRemaining;
} tL2TxFrameInfo;
enum class eL2ScopeCode { Directed = 0, McastMod, BcastCoord, BcastAll } ;


class cPCUSim
{
public:
	cPCUSim(tSerialNumber *serialNumber);
	~cPCUSim();
	int ProcessPLCMessage(const uint8_t *InMsg, uint32_t InMsgLen, uint8_t *OutMsg, uint16_t *OutLen, tPmiDataConfigData **pPmiData);
	void Ticker(int numTicks); // process any internal ticks ticks are in cycles
	void SetOperatingCondition(float freq, float acvRms, float dcmv); // for processing if implemented
	void ResetPCU(int type); // force internal reset - reload from files...

private:

    int PacketFilter(const tL2TxFrameInfo *pL2);
    int LoadNonVolatileData();
	int HandleMcpMsgIdPollCmdTlv(tMcpPollCmdTlv *cmdMsg, uint8_t *OutMsg, uint16_t *OutLen);
	void modmcpSendPollRsp(u_int16_t _keil_xdata pollReqFlags, u_int8_t  _keil_xdata cmdSeqNum, uint8_t *OutMsg, uint16_t *OutLen);
	
	uint32_t TimeStamp;
	int m_sendResponseToPacket;
	tMcpPollRspImageInfoTlv m_imageInfo;
	
	
	tPlcAppCtl plcAppCtl;
	
    tAsicId m_asicId;
	
	uint8_t m_devType;
    
    uint32_t m_grpMask;
	tManuData plcAppManuData;
	u_int8_t      _keil_xdata  priorEventMsgsWoAck;
	uint32_t            _keil_xdata UnackdIntervalSecs;
	tDmsInfo             _keil_xdata Dms;
	tMcpImgInventoryMsg  _keil_xdata imgInventory;
	uint8_t             _keil_xdata lastRxMsgOnModemType;
	bool                 _keil_xdata mcpFlashBlank;
	uint8_t             _keil_xdata lastCmdCondSeqNum;
	tMcpPcuPwrIntervalIe _keil_xdata powerData;
	uint32_t            _keil_xdata prevIntervalSecs;
	tAgfRecStatus        _keil_xdata agfRecStatus[cAgfFunctionTypeNumOf];
	tAgfPldRecord        _keil_xdata tmpRecord;
	tPaInfo              _keil_xdata paInfoData;
	tPaInfoDriver        _keil_xdata paInfoDriverData;
	tL2FrameHdrUnpacked  _keil_xdata l2UnpInitInfo;
	uint16_t            _keil_xdata pollRspStatus;
	uint8_t bMPPTunlocked;
	uint16_t ErroredCycles;
	uint8_t PcuStatusBits2;
	u_int32_t     _keil_xdata  eventDataTimestamp;
	uint8_t             _keil_xdata LastMcpCondFlags[5];
	u_int8_t      _keil_xdata  McpCondFlags[5];
	uint16_t 			 _keil_xdata lastModAddr;
	tPmiData _keil_xdata                    pmiData;
	u_int32_t     _keil_xdata  eventBits[cMcpPcuEventBitsSize];
	tMcpDevInfoMsg_Ver3   devInfo;
	u_int32_t _keil_xdata            plcIntervalSecCount;
	tAsicId      uniqueAsicIdNum;
	uint8_t CurrentOdometer[6];
	uint8_t m_agf_array[0xa20]; 
	uint8_t m_cyclesPerSec; // freq...
    // PMI settings
	// FW/PT pointers (saved files)
	// internal memory
	// SKIPS, COUNTERS
	// discovery state
	//  
};

#endif // _PCU_HH_

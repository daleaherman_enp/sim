/* Copyright (c) 2006-2014 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef AGF_H_
#define AGF_H_

/** @file agf.h
 *  @brief High Level AGF Definitions
 *
 *  This file @c agf.h provides enPhase agf definitions.
 */

/** @page agfHPg Header: agf.h enPhase AGF Definitions

 @section agfHName agf.h HEADER NAME

 agf.h -- enPhase AGF Definitions.

 @section agfHSynop agf.h SYNOPSIS

 This file, @c agf.h, provides definitions for agf/rule 21 functionality.

 */

#include <stddef.h>
#include <stdlib.h>
#include "include/enphase.h"

/**
 * @defgroup agfHGroups agf Header Definitions and Declarations
 */
/*@{*/

/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup agfHDefines agf Header Defines
 */
/*@{*/

/*defines the value for VV11 hold reactive power */
#define cAGF_VV11_HOLD_REACTIVE_POWER   0x02

/*defines the values for agf function enable/disable*/
#define cAGF_FUNC_ENABLE            0x01
#define cAGF_FUNC_DISABLE           0x00

/*defines for the curve type*/
#define cAGF_TYPE_A_CURVE           0x00
#define cAGF_TYPE_B_CURVE           0x01

/*defines for the fw21 mode selection parameter*/
#define cAGF_FW21_HYSTEREIS_ENABLE  0x02
#define cAGF_FW21_RECONNECT_ENABLE  0x04

#define cAgfTrans6Points    6
#define cAgfTrans8Points    8
#define cAgfTrans10Points  10
#define cAgfTransPtPair     2
#define cAgfTransPtTriple   3
#define cAgfTransPtQuad     4

#define cAgfTrans2Values    2
#define cAgfTrans2Points    2
#define cAgfTrans2Curves    2
#define cAgfTransCurveHigh  0
#define cAgfTransCurveLow   1

/*@}*//* END of agf Defines Group */

/* ------------------------------------ Typedefs -------------------------- */

/**
 * @defgroup agfHTypedefs agf Header Typedefs
 */
/*@{*/

///////////////////////////////////////////////////////////////////////////////
// AGF COMMON TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This is an enumeration of the supported AGF Functions.
 */

/**
 * This is the basic building block for AGF data types.  It is 32 bits.
 */
typedef int32_t tAgfInverterDataWord;

/**
 * Common AGF query message struct.  'type' field is not used.
 */
typedef struct _tAGFQueryMsg
{
    tAgfInverterDataWord    functionType;

} tAGFQueryMsg;

/**
 * The basic curve point. Contains two datawords.
 */
typedef struct _tAgfCurvePoint
{
    tAgfInverterDataWord    value[2];

} tAgfCurvePoint;

/**
 * This is an inverter curve point. It consists of two basic curve points.
 */
typedef struct _tAgfInverterCurvePoint
{
    tAgfCurvePoint  point[2];

} tAgfInverterCurvePoint;

/**
 * This is the basic inverter curve. It contains 10 inverter curve points.
 */
typedef struct _tAgfInverterCurve
{
    tAgfInverterCurvePoint  points[10];

} tAgfInverterCurve;

/**
 * This is the VRT inverter curve. It contains 8 inverter curve points.
 */
typedef struct _tAgfInverterCurveVRT
{
    tAgfInverterCurvePoint  points[8];

} tAgfInverterCurveVRT;

/**
 * This is the basic inverter curve pair which consists of two inverter curves.
 */
typedef struct _tAgfInverterCurvePair
{
    tAgfInverterCurve   curve[2];

} tAgfInverterCurvePair;

/**
 * This is the VRT inverter curve pair which consists of two inverter VRT
 * curves.
 */
typedef struct _tAgfInverterCurvePairVRT
{
    tAgfInverterCurveVRT   curve[2];

} tAgfInverterCurvePairVRT;

/**
 * This is the WVAR inverter curve. It contains 10 inverter curve points.
 */
typedef struct _tAgfInverterCurveWVAR
{
    tAgfCurvePoint  points[cAgfTrans10Points];

} tAgfInverterCurveWVAR;

///////////////////////////////////////////////////////////////////////////////
// AGF VV11 TYPE DEFS
///////////////////////////////////////////////////////////////////////////////


/**
 * This structure is a control structure that contains flags for the various
 * AGF functionality to indicate if it is enabled or disabled.
 */
typedef struct _tAGFVVARMsg
{

    /**
     * This field should be set to either cAGF_VVAR_ENABLE or cAGF_VVAR_DISABLE
     * as defined above.
     */
    tAgfInverterDataWord    isEnabled;

    tAgfInverterDataWord    startPoint;
    tAgfInverterDataWord    stopPoint;

    tAgfInverterCurvePair   curves;

    tAgfInverterDataWord    maxSlopeDecrease;
    tAgfInverterDataWord    maxSlopeIncrease;

    /**
     * This field should be set to either cAgfTypeACurve or
     * cAGF_TYPE_B_CURVE as defined above.
     */
    tAgfInverterDataWord    curveType;
    tAgfInverterDataWord    typeBMin;
    tAgfInverterDataWord    typeBMax;

} tAgfVVARMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF FRT TYPE DEFS
///////////////////////////////////////////////////////////////////////////////


/**
 * This structure is a control structure that contains data for the
 * AGF FRT functionality.
 */
typedef struct _tAgfFRTMsg
{
    tAgfInverterCurvePair   curves;
    tAgfInverterDataWord    maxTime;
    tAgfInverterDataWord    time;

} tAgfFRTMsg;


///////////////////////////////////////////////////////////////////////////////
// AGF VRT TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF VRT functionality.
 */
typedef struct _tAgfVRTMsg
{
    tAgfInverterCurvePairVRT    curves;
    tAgfInverterDataWord        maxTime;

} tAgfVRTMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF FIXED POWER FACTOR TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF FPF functionality.
 */
typedef struct _tAgfFPFMsg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterDataWord    lockInVoltage;
    tAgfInverterDataWord    powerFactor;    // abs of power factor
    tAgfInverterDataWord    lockOutVoltage;
    tAgfInverterDataWord    startpoint;

} tAgfFPFMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF POWER RATE LIMITING TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF PRL functionality.
 */
typedef struct _tAgfPRLMsg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterDataWord    limit;

} tAgfPRLMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF SOFT START TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF SS functionality.
 */
typedef struct _tAgfSSMsg
{
    tAgfInverterDataWord    mode;
    tAgfInverterDataWord    reconnectFrequencyLow;
    tAgfInverterDataWord    reconnectFrequencyHigh;
    tAgfInverterDataWord    reconnectVoltageLow;
    tAgfInverterDataWord    reconnectVoltageHigh;
    tAgfInverterDataWord    reconnectAiTime;
    tAgfInverterDataWord    reconnectPowerRampRate;
    tAgfInverterDataWord    shortAiFailTime;
    tAgfInverterDataWord    shortAiTime;
    tAgfInverterDataWord    aiTime;
} tAgfSSMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF VW TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF VW51 functionality.
 */
typedef struct _tAgfVWMsg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterCurvePair   curves;
    tAgfInverterDataWord    maxSlopeDecrease;
    tAgfInverterDataWord    maxSlopeIncrease;

    /**
     * This field should be set to either cAGF_TYPE_A_CURVE or
     * cAGF_TYPE_B_CURVE as defined above.
     */
    tAgfInverterDataWord    curveType;
    tAgfInverterDataWord    typeBMin;
    tAgfInverterDataWord    typeBMax;
    tAgfInverterDataWord    timeConstant;

} tAgfVWMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF INV2 TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF INV2 functionality.
 */
typedef struct _tAgfINV2Msg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterDataWord    powerMax;
    tAgfInverterDataWord    rampRate;
    tAgfInverterDataWord    timeWindow;
    tAgfInverterDataWord    timeoutPeriod;

} tAgfINV2Msg;


///////////////////////////////////////////////////////////////////////////////
// AGF WP TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF WP functionality.
 */
typedef struct _tAgfWPMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    wstr;
    tAgfInverterDataWord    pfstr;
    tAgfInverterDataWord    wstop;
    tAgfInverterDataWord    pfstop;
    tAgfInverterDataWord    slope;
    tAgfInverterDataWord    lockInVoltage;
    tAgfInverterDataWord    lockOutVoltage;

} tAgfWPMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF TV TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF TV functionality.
 */
typedef struct _tAgfTVMsg
{
    tAgfInverterDataWord    isEnabled;

} tAgfTVMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF FW TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF FW functionality.
 */
typedef struct _tAgfFWMsg
{
    tAgfInverterDataWord    modeSelection;
    tAgfInverterDataWord    frequencyStart;
    tAgfInverterDataWord    frequencyStopHigh;
    tAgfInverterDataWord    frequencyStopLow;
    tAgfInverterDataWord    frequencySlope;
    tAgfInverterDataWord    frequencyTimeSlope;
    tAgfInverterDataWord    frequencyMaxTimeSlope;
    tAgfInverterDataWord    frequencyStopTime;
    tAgfInverterDataWord    frequencyStartDelay;

} tAgfFWMsg;

typedef struct _tAgfPLPMsg
{
    tAgfInverterDataWord    maxVar;
    tAgfInverterDataWord    maxWatt;
} tAgfPLPMsg;

typedef struct _tAgfACAVEMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    aveHigh;

} tAgfACAVEMsg;

typedef struct _tAgfISLNDMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    tone;
    tAgfInverterDataWord    harmonics_diff;
    tAgfInverterDataWord    harmonics_increase;
    tAgfInverterDataWord    harmonics_max;
    tAgfInverterDataWord    fundamental_diff;
    tAgfInverterDataWord    freq_deviation;

} tAgfISLNDMsg;

typedef struct _tAgfIACMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    fast;
    tAgfInverterDataWord    slow;

} tAgfIACMsg;

typedef struct _tAgfVECTMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    trip_angle;
    tAgfInverterDataWord    trip_time;
    tAgfInverterDataWord    trip_angle_islanding;

} tAgfVECTMsg;

typedef struct _tAgfROCOFMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    cycles;
    tAgfInverterDataWord    constant;

} tAgfROCOFMsg;

typedef struct _tAgfCookieData
{
    u_int8_t  type;
    u_int8_t  version;
    u_int8_t  md5[8];
} tAgfCookieData;

typedef struct _tAgfCookieMsg
{
    u_int8_t       count;
    u_int8_t       fillBytes[3];
    tAgfCookieData cookie[1];
} tAgfCookieMsg;

// Same as tAgfVWMsg.  Re-Defined here as TRX requires a unique message type for
// each transform type.
typedef struct _tAgfVW52Msg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterCurvePair   curves;
    tAgfInverterDataWord    maxSlopeDecrease;
    tAgfInverterDataWord    maxSlopeIncrease;

    /**
     * This field should be set to either cAgfTypeACurve or
     * cAGF_TYPE_B_CURVE as defined above.
     */
    tAgfInverterDataWord    curveType;
    tAgfInverterDataWord    typeBMin;
    tAgfInverterDataWord    typeBMax;
    tAgfInverterDataWord    timeConstant;

} tAgfVW52Msg;

// FW22/FWCHA structure.  Very similar to FW21, but a few naming changes to line-up
// with Andy's AGF record description.
typedef struct _tAgfFW22Msg
{
    tAgfInverterDataWord    modeSelection;
    tAgfInverterDataWord    frequencyStart;
    tAgfInverterDataWord    frequencyStop;
    tAgfInverterDataWord    frequencyStopLow;
    tAgfInverterDataWord    frequencySlope;
    tAgfInverterDataWord    frequencyMaxTimeSlope;
    tAgfInverterDataWord    frequencyTimeSlope;
    tAgfInverterDataWord    frequencyStopTime;
    tAgfInverterDataWord    frequencyStartDelay;

} tAgfFW22Msg;


typedef struct _tAgfWVARMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterCurveWVAR   curves;
    tAgfInverterDataWord    maxSlopeDecrease;
    tAgfInverterDataWord    maxSlopeIncrease;
    tAgfInverterDataWord    timeConstant;

} tAgfWVARMsg;

/*@}*//* END of agf Header Typedef Group */

/* ------------------------------------ Extern Declarations --------------- */

/**
 * @defgroup agfHExtdata agf Header Extern Data Definitions
 */
/*@{*/
/*@}*//* END of agf Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup agfHGlobdata agf Header Global Data Defined
 */
/*@{*/

/*@}*//* END of agf Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup agfHStatdata agf Header Static Data Defined
 */
/*@{*/
/*@}*//* END of agf Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup agfHProto agf Header Prototypes
 */
/*@{*/
/*@}*//* END of agf Header Prototypes Group */

/*@}*//* END agf Header Definitions and Declarations */

extern u_int8_t
AgfRcrdResponse(u_int16_t recTypeVer);

extern u_int8_t _keil_xdata AgfRcrdResponseBlk;

extern u_int8_t
SetAgfRcrd(u_int32_t _keil_xdata * acdData);
#endif /* AGF_H_ */

/* ------------------------------------------------------------------------
 $Log:$
 ------------------------------------------------------------------------ */

/* END of agf.h */


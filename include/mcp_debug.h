/* Copyright (c) 2006-2011 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _mcp_debug_h_
#define _mcp_debug_h_

/** @file mcp_debug.h
 *  Header for Module Control Protocol (MCP) debug messages
 *
 */

#if defined(PCU)
#define 

#elif defined(ENPHASE)
#define  __attribute__ ((__packed__))

#elif defined(SUNPOWER)
// ADD SUNPOWER DEFINE FOR  HERE

#else
#error "Please specify build type in the Makefile"

#endif




// Some Debug requests can apply to the b-side of a dually when that is
// needed it is indicated by oring in the following:
#define cMcpDbgfBside           cBit6

// Some Debug requests need to indicate a clearing action is desired.
// This is indicated by oring in the following:
#define cMcpDbgfClear           cBit7

#define cMcpDbgfReqMask         0x3f

// NOTE:  for dually if bit7 of the debugReq field is set then the
//        object of the command is on the 'B-side' of the dually.

typedef enum _eMcpDebugReq {
    cMcpDebugReqNone,
    cMcpDebugReqReset,
    cMcpDebugReqPeek,
    cMcpDebugReqPoke,
    cMcpDebugReqTest,
    cMcpDebugReqCaptureSet,
    cMcpDebugReqHarness,
    cMcpDebugReqFlash,
    cMcpDebugReqComm,
    cMcpDebugReqDbgFunc2,  // Power test control
    // PWRTSTCLR: pcu, clear n/a, a|b side
    cMcpDebugReqDbgPwrTstCtl = cMcpDebugReqDbgFunc2,
    cMcpDebugReqDbgDreq = cMcpDebugReqDbgFunc2,
    cMcpDebugReqDbgFunc3,
    // PWR: pcu, clear NOT valid, a|b side
    cMcpDebugReqDbgPwrMonData = cMcpDebugReqDbgFunc3,
    cMcpDebugReqDbgFunc4,  // get comm debug counters
    // COMM: all, clear, a side only
    cMcpDebugReqDbgCommDbgCnts = cMcpDebugReqDbgFunc4,
    cMcpDebugReqDbgFunc5,
    // BOOLS: all, clear (see next function), a|b side
    cMcpDebugReqDbgBools = cMcpDebugReqDbgFunc5,
    cMcpDebugReqDbgFunc6,
    // MOD (set/clr) BOOL: all, for set or clear, a|b side
    cMcpDebugReqDbgModBool = cMcpDebugReqDbgFunc6,
    cMcpDebugReqDbgFunc7,
    // MPPT: pcu, clear NOT valid, a|b side
    cMcpDebugReqDbgMppt = cMcpDebugReqDbgFunc7,
    cMcpDebugReqDbgFunc8,
    // SKIP: pcu, clear valid, a|b side
    cMcpDebugReqDbgSkip = cMcpDebugReqDbgFunc8,
    cMcpDebugReqDbgFunc9,
    // PLL: pcu, clear valid, a|b side
    cMcpDebugReqDbgPll = cMcpDebugReqDbgFunc9,
    cMcpDebugReqDbgFunc10,
    // IUP: dually, clear valid, a|b side
    cMcpDebugReqDbgIup = cMcpDebugReqDbgFunc10, // dually only
    cMcpDebugReqDbgFunc11,
    cMcpDebugReqDbgPlcData = cMcpDebugReqDbgFunc11,
    // (corvus1 only) Corona PLM, controller cnts,
    //                Aurora PLM counts, Aurora PLM detailed
    cMcpDebugReqDbgFunc12,
    cMcpDebugReqDbgQreq = cMcpDebugReqDbgFunc12,
    cMcpDebugReqDbgFunc13,
    cMcpDebugReqDbgFunc14,
    cMcpDebugReqDbgFunc15,
    cMcpDebugReqNumOf
} eMcpDebugReq;

typedef enum _eMcpModuleMemType {
    cMcpModuleMemTypeNone,
    cMcpModuleMemTypeXData,         // "normal" external memory 0x0 - 0xffff
    cMcpModuleMemTypeIData,         // idata 0x80 - 0xff
    cMcpModuleMemTypeDData,         // data 0x00 - 0x7f
    cMcpModuleMemTypeNumOf
} eMcpModuleMemType;


typedef enum _eMcpDbgCapMode {
    cMcpDbgCapModeEveryCycle,
    cMcpDbgCapModeEveryBin,
    cMcpDbgCapModeMpptData,
    cMcpDbgCapModeNumOf
} eMcpDbgCapMode;

typedef enum _eMcpMemTestType {
    cMcpMemTestTypeStaticDataPattern,
    cMcpMemTestTypeAlternatingDataPattern,
    cMcpMemTestTypeIncrementingDataPattern,
    cMcpMemTestTypeNumOf
} eMcpMemTestType;

typedef enum _eMcpFlashCmd {
    cMcpFlashCmdNone,
    cMcpFlashCmdReadWords,      // read 16-bit words count <= len
    cMcpFlashCmdWriteWords,     // write a 16-bit words
    cMcpFlashCmdEraseBlock,     // erase a flash block
    cMcpFlashCmdNumOf
} eMcpFlashCmd;

#define cMcpDbgCommCtlStart     1
#define cMcpDbgCommCtlContinue  2
#define cMcpDbgCommCtlStop      3

#define cMcpDbgCommCtlActMask   0x0f
#define cMcpDbgCommCtlFlagsMask 0xf0
#define cMcpDbgCommCtlRspDbgOnly 0x80

#define cMcpDbgCommModeSource   1
#define cMcpDbgCommModeSink     2
#define cMcpDbgCommModeLpbk     3

typedef enum _eMcpTestPollReqType {
    cMcpTestPollReqTypeNone,
    cMcpTestPollReqTypeReadAndClear,
    cMcpTestPollReqTypeRead,
    cMcpTestPollReqTypeIntervalReadAndClear,
    cMcpTestPollReqTypeIntervalRead,
    cMcpTestPollReqTypeEventReadAndClear,
    cMcpTestPollReqTypeEventRead,
    cMcpTestPollReqTypeNumOf
} eMcpTestPollReqType;

typedef struct  _tMcpDebugHarness {
    u_int8_t                    debugReq;
    u_int8_t                    fillVal;
    u_int16_t                   fillLen;
} tMcpDebugHarness;

#define cMcpDbgCommCdataLen     126
#define cMcpDbgComm1FragCdataLen    26
typedef struct  _tMcpDebugCommCtl {
    u_int8_t                    debugReq;   /*!< use @c eMcpDebugReq. */
    u_int8_t                    testCtl;
    u_int8_t                    mode;
    u_int8_t                    sinkRspModulus;
    u_int16_t                   msDelay;
    u_int16_t                   msgCnt;  // for Source mode, 0 continuous
    u_int16_t                   rxCnt;
    u_int16_t                   txCnt;
    u_int32_t                   cookie;
    u_int8_t                    cdata[cMcpDbgCommCdataLen];
} tMcpDebugCommCtl;

#define cMcpDbgCmdCdataLen      256
#define cMcpDbgCmd1FragCdataLen 34
typedef struct  _tMcpDebugCmd {
    /* Usage of comamnd parameter (cmdParm):
     *      - peek/poke use eMcpModuleMemType,
     *      - test uses eMcpMemTestType,
     *      - flash uses eMcpFlashCmd
     */
    u_int8_t                    debugReq;   /*!< use @c eMcpDebugReq. */
    u_int8_t                    cmdParm;
    u_int16_t                   count;
    u_int32_t                   addr;
    u_int8_t                    cdata[cMcpDbgCmdCdataLen];
} tMcpDebugCmd;

typedef struct  _tMcpDebugPwrTstCtl {
    u_int8_t                    debugReq;   /*!< use @c eMcpDebugReq. */
    u_int8_t                    pwrTstEnable;
    u_int8_t                    pwrTstMode; // 0 for AC, 1 for DC
    u_int8_t                    pwrTstIReqVal;
} tMcpDebugPwrTstCtl;

// register is 9 bit integer
// data in 1's complement format
typedef struct  _tMcpDebugReqRegister {
    u_int8_t                    debugReq;   /*!< use @c eMcpDebugReq. */
    u_int8_t                    enable;     // lsb is enable bit
    u_int8_t                    signBit;    // lsb is sign bit
    u_int8_t                    value;
} tMcpDebugReqRegister;

typedef struct  _tMcpDebugH2ReqRegister {
    u_int8_t                    debugReq;   /*!< use @c eMcpDebugReq. */
    u_int8_t                    enable;     // lsb is enable bit
    u_int8_t                    msbValue;   // the value is a short integer
    u_int8_t                    lsbValue;
} tMcpDebugH2ReqRegister;

typedef struct  _tMcpDebugBoolsData
{
    u_int8_t                    debugReq;
    u_int8_t                    isMod;
    u_int8_t                    boolNum;
    u_int8_t                    value;

    u_int8_t                    bool_0x20;
    u_int8_t                    bool_0x21;
    u_int8_t                    bool_0x22;
    u_int8_t                    bool_0x23;
    u_int8_t                    bool_0x24;
    u_int8_t                    bool_0x25;
    u_int8_t                    bool_0x26;
    u_int8_t                    bool_0x27;
    u_int8_t                    bool_0x28;
    u_int8_t                    bool_0x29;
    u_int8_t                    bool_0x2a;
    u_int8_t                    bool_0x2b;
    u_int8_t                    bool_0x2c;
    u_int8_t                    bool_0x2d;
    u_int8_t                    bool_0x2e;
    u_int8_t                    bool_0x2f;
} tMcpDebugBoolsData;

typedef struct  _tMcpDebugPwrMonData
{
    u_int8_t                    debugReq;   /*!< use @c eMcpDebugReq. */
    u_int8_t                    reserved1;
    u_int8_t                    statusBits0;
    u_int8_t                    statusBits1;
    u_int32_t                   vrmsmV;
    u_int32_t                   freqInClkCyc;
    u_int32_t                   dcVmV;
    u_int32_t                   dcCmA;
    u_int32_t                   vL1mV;
    u_int32_t                   vL2mV;
    u_int32_t                   acPwrmA;
    u_int32_t                   dcPwrmA;
    u_int8_t                    skipSecs;
    u_int8_t                    maxSkip1Sec;
    u_int16_t                   reconnTmr;
    int8_t                      tempC;
    u_int8_t                    reserved2;
} tMcpDebugPwrMonData;

// tMcpNsrbPwrInterval  - used in msg from NSR-B
typedef struct  _tMcpNsrbPwrIntervalIe {
    u_int8_t                    fillByte1;
    u_int8_t                    temp;             // temp in deg C
    u_int8_t                    fillByte2;
    u_int8_t                    acCurrOffset[3];  // enphase devices side measurement
    u_int8_t                    status[2];        // 2 byte value
    u_int8_t                    config;
    u_int8_t                    acCurrOffsetBase[3]; // grid side measurement
    u_int8_t                    fillByte3;
    u_int8_t                    voltRmsL1[3];    // 3 byte value
    u_int8_t                    fillByte4;
    u_int8_t                    freq[3];         // 3 byte value
    u_int8_t                    fillByte5;
    u_int8_t                    voltRmsL2[3];    // 3 byte value
    u_int8_t                    fillByte6;
    u_int8_t                    voltRmsL3[3];    // 3 byte value
    u_int16_t                   stateChngCnt;    // count of opens and closes of relay
} tMcpNsrbPwrIntervalIe;

typedef struct  _tMcpDebugPwrMonDataNsrb
{
    u_int8_t                    debugReq;   /*!< use @c eMcpDebugReq. */
    tMcpNsrbPwrIntervalIe       nsrbData;
    u_int16_t                   reconnTmr;
} tMcpDebugPwrMonDataNsrb;

typedef struct  _tMcpDebugPwrMonDataReactive
{
    /*  0 */ u_int8_t           debugReq;   /*!< use @c eMcpDebugReq. */
    /*  1 */ u_int8_t           chanTemp;   /* signed from PCU - convert with emuutlInt8ToInt */
    /*  2 */ u_int16_t          fillb2;
    /*  4 */ u_int8_t           skipSecs;
    /*  5 */ u_int8_t           maxSkip1Sec;
    /*  6 */ u_int16_t          mJoulesProduced[3]; /* 48-bit unsigned */
    /* 12 */ u_int16_t          vrmsmV[2];          /* 24-bit unsigned */
    /* 16 */ u_int16_t          freqInClkCyc[2];    /* 24-bit unsigned */
    /* 20 */ u_int16_t          dcVmV[2];           /* 24-bit unsigned */
    /* 24 */ u_int16_t          dcCmA[2];           /* 24-bit signed */
    /* 28 */ u_int16_t          powerUsedmJ[3];     /* 48-bit unsigned */
    /* 34 */ u_int16_t          sumLeadingVAr[3];   /* 48-bit unsigned */
    /* 40 */ u_int16_t          sumLaggingVAr[3];   /* 48-bit unsigned */
    /* 46 */ u_int16_t          vL1mV[2];           /* 32-bit unsigned */
    /* 50 */ u_int16_t          vL2mV[2];           /* 32-bit unsigned */
    /* 54 */ u_int16_t          acPwrmW[2];         /* 24-bit signed */
    /* 58 */ u_int16_t          dcPwrmW[2];         /* 24-bit signed */
    /* 62 */ u_int16_t          measuredVAmVAr[2];  /* 24-bit unsigned */
    /* 66 */ u_int16_t          leadingPwrmVAr[2];  /* 24-bit unsigned */
    /* 70 */ u_int16_t          laggingPwrmVAr[2];  /* 24-bit unsigned */
    /* 74 */ u_int8_t           vl1vl2Bits;
    /* 75 */ u_int8_t           vl3IACbits;
    /* 76 */ u_int8_t           statusBits0;
    /* 77 */ u_int8_t           statusBits1;
    /* 78 */ u_int8_t           statusBits2;
    /* 79 */ u_int8_t           statusBits3;
    /* 80 */ u_int16_t          reconnTmr;
    /* 82 */   int16_t          dreq;             /* 9-bit signed value */
    /* 84 */   int16_t          qreq;             /* 9-bit signed value */
} tMcpDebugPwrMonDataReactive;

/*

MPPT Debug Information:
Here's the vars that are needed to troubleshoot MPPT in the field.
PowerInput1:                               DS  3
PowerInput2:                               DS  3
DeltaPwr:                                  DS  3
TotalPwr:                                  DS  3
DeltaPwrNorm:                              DS  3
MpptVinAdjust:                             DS  3
PreviousMpptVinAdjust:                     DS  3
DcVoltageSetpoint:                         DS  3
DcVoltageInput:                            DS  3
DcVoltage:                                 DS  3
CurrentVinError:                           DS  3
PreviousVinError:                          DS  3
Calculated_Iin:                            DS  3
PreviousCalculated_Iin:                    DS  3
CalculatedPowerIn:                         DS  3
DcCurrentInput:                            DS  3
Noff                                       DS  1
Noff_Raw                                   DS  1
CalculatedPowerOut:                        DS  3
AcAveNorm:                                 DS  3
PowerMax:                                  DS  3
IreqMax:                                   DS  1
IreqCanidate                               DS  1
BurstPowerV8                               DS  1
PowerV8                                    DS  1
ACPower:                                   DS  3

*/

typedef struct  _tMcpDebugMpptData
{
    u_int8_t                    debugReq;
    u_int8_t                    filla2;
    u_int8_t                    filla3;
    u_int8_t                    filla4;
    u_int32_t                   powerInput1;
    u_int32_t                   powerInput2;
    u_int32_t                   deltaPwr;
    u_int32_t                   totalPwr;
    u_int32_t                   deltaPwrNorm;
    u_int32_t                   mpptVinAdjust;
    u_int32_t                   previousMpptVinAdjust;
    u_int32_t                   dcVoltageSetpoint;
    u_int32_t                   dcVoltageInput;
    u_int32_t                   dcVoltage;
    u_int32_t                   currentVinError;
    u_int32_t                   previousVinError;
    u_int32_t                   calculated_Iin;
    u_int32_t                   previousCalculated_Iin;
    u_int32_t                   calculatedPowerIn;
    u_int32_t                   dcCurrentInput;
    u_int8_t                    noff;
    u_int8_t                    noff_Raw;
    u_int8_t                    ireqForDisplay;
    u_int8_t                    fillq2;
    u_int32_t                   calculatedPowerOut;
    u_int8_t                    ireqMax;
    u_int8_t                    ireqCanidate;
    u_int8_t                    burstPowerV8;
    u_int8_t                    powerV8;
    u_int32_t                   fillyw1;
    u_int32_t                   fillyw2;
} tMcpDebugMpptData;



/*

Skip Debug Counters (PCU-only, A and/or B side)

variable name             byte           meaning

CntrF540                    1  register F540 counter exceeded limits
CntrF541                    1  register F541 counter exceeded limits
CntrF542                    1  register F542 counter exceeded limits
CntrF543                    1  register F543 counter exceeded limits
CntrF544                    1  register F544 counter exceeded limits
CntrF545                    1  register F545 counter exceeded limits
CntrF535                    1  register F535 counter exceeded limits
CntrZxCenter                1  zero cross happened at unexpected time
CntrZxHigh                  1  voltage at zerocross too high
VrecThrCntr                 1  vrec counter too high
HotVrecThrCntr              1  hotvrec counter too high
IslandCntrHi                2  island counter byte
                                 (REAL NAME: RECONNECTION TIMER)
SuspensionDbgCntr           2  suspension accumulatorcounter
HighSkipRate_Counter        1  high skiprate
HighErroredSeconds_Counter  1  high errored seconds

*/
typedef struct  _tMcpDebugSkipData
{
    u_int8_t                    debugReq;
    u_int8_t                    filla2;
    u_int8_t                    filla3;
    u_int8_t                    filla4;

    u_int8_t                    cntrF540;
    u_int8_t                    cntrF541;
    u_int8_t                    cntrF542;
    u_int8_t                    cntrF543;

    u_int8_t                    cntrF544;
    u_int8_t                    cntrF545;
    u_int8_t                    cntrF535;
    u_int8_t                    cntrZxCenter;

    u_int8_t                    cntrZxHigh;
    u_int8_t                    vrecThrCntr;
    u_int8_t                    hotVrecThrCntr;
    u_int8_t                    filld1;

    u_int16_t                   islandCntrHi;
    u_int16_t                   suspensionDbgCntr;

    u_int8_t                    highSkipRate_Counter;
    u_int8_t                    highErroredSeconds_Counter;
    u_int8_t                    fillf2;
    u_int8_t                    fillf3;

    u_int8_t                    fillg1;
    u_int8_t                    fillg2;
    u_int8_t                    fillg3;
    u_int8_t                    fillg4;
} tMcpDebugSkipData;

typedef struct  _tMcpDebugSkipDataHarrier
{
    u_int8_t                    debugReq;
    u_int8_t                    cntrSPO;
    u_int8_t                    cntrSSO;
    u_int8_t                    cntrSTO;

    u_int8_t                    cntrUO;
    u_int8_t                    cntrDCA;
    u_int8_t                    cntrHSD;
    u_int8_t                    cntrETC;

    u_int8_t                    cntrPLL;
    u_int8_t                    cntrFLPE;
    u_int8_t                    cntrBFDL;
    u_int8_t                    cntrPSE1;

    u_int8_t                    cntrPSE2;
    u_int8_t                    cntrAMT;
    u_int8_t                    cntrIO;
    u_int8_t                    cntrLOWV;

    u_int8_t                    cntrZxCenter;
    u_int8_t                    cntrZxHi;
    u_int8_t                    cntrOCP;
    u_int8_t                    softwareSkip;

    u_int16_t                   islandCntr;
    u_int8_t                    archipelagoCntr;
    u_int8_t                    fillf4;

    u_int16_t                   suspensionDbgCntr;
    u_int8_t                    highSkipRate_Counter;
    u_int8_t                    highErroredSeconds_Counter;
} tMcpDebugSkipDataHarrier;

typedef struct  _tMcpDebugSkipDataHeron
{
    u_int8_t                    debugReq;
    u_int8_t                    cntrSPO;
    u_int8_t                    cntrSSO;
    u_int8_t                    cntrSTO;

    u_int8_t                    cntrUO;
    u_int8_t                    cntrDCA;
    u_int8_t                    cntrHSD;
    u_int8_t                    cntrETC;

    u_int8_t                    cntrHIGHV;
    u_int8_t                    cntrPLL;
    u_int8_t                    cntrDVDT;
    u_int8_t                    cntrPSE1;

    u_int8_t                    cntrPSE2;
    u_int8_t                    cntrAMT;
    u_int8_t                    cntrLOWV;
    u_int8_t                    cntrZxHigh;

    u_int8_t                    softwareSkip;
    u_int8_t                    archipelagoCntr;
    u_int16_t                   islandCntr;

    u_int16_t                   suspensionDbgCntr;
    u_int8_t                    highSkipRate_Counter;
    u_int8_t                    highErroredSeconds_Counter;

    u_int8_t                    fillg1;
    u_int8_t                    fillg2;
    u_int8_t                    fillg3;
    u_int8_t                    fillg4;
} tMcpDebugSkipDataHeron;

/*

PLL Debug Counters (PCU-only, A and/or B side)

The pll routine is jumping between two states: Qualify and Monitor
"Qualify" does some rough checks and if it seems ok, then jumps to "Monitor".
Only when discovering abnormal frequency we jump back to "Qualify".
The old counters freeze at each jump (for debug reasons)

variable name     byte   state    meaning

PllQualGood         1   qualify   good measurements
                                    (free running rollover upcounter)
PllQualNoUpdate     1   qualify   no ASIC frequency updates
                                    (zero cross not seen)
PllQualThreshold    1   qualify   overlap threshold error

PllMonGood          1   monitor   good measurements
                                    (free running rollover upcounter)
PllMonNoUpdate      1   monitor   no ASIC frequency updates
                                    (zero cross not seen)
PllMonThreshold     1   monitor   overlap threshold error
PllMonPhaseErr      1   monitor   calculated phase error too big
PllMonIsland        1   monitor   islandig
                                    (THIS IS THE REAL THING WITH THE TANK!)
PllMonFineFreq      1   monitor   fine frequency error
PllMonFinePhase     1   monitor   rough frequency error

*/

typedef struct  _tMcpDebugPllData
{
    u_int8_t                    debugReq;
    u_int8_t                    filla2;
    u_int8_t                    filla3;
    u_int8_t                    filla4;

    u_int8_t                    pllQualGood;
    u_int8_t                    pllQualNoUpdate;
    u_int8_t                    pllQualThreshold;
    u_int8_t                    fillb3;

    u_int8_t                    pllMonGood;
    u_int8_t                    pllMonNoUpdate;
    u_int8_t                    pllMonThreshold;
    u_int8_t                    pllMonPhaseErr;

    u_int8_t                    pllMonIsland;
    u_int8_t                    pllMonFineFreq;
    u_int8_t                    pllMonFinePhase;
    u_int8_t                    filld3;

    u_int8_t                    fille1;
    u_int8_t                    fille2;
    u_int8_t                    fille3;
    u_int8_t                    fille4;

} tMcpDebugPllData;

/*

The IUP (Interchip Uart Protocol) (DUALLY A/B side) debug counters

variable name             byte          meaning

IUPRunning                  1       free running IUP loop-counter
IUPTotalErrorCntr           3       free running error upcounter
IUPNoPacketCntr             1       no packets at all (pegged at 0xFF)
IUPMissingPacketCntr        1       some packets are missing (pegged at 0xFF)
IUPBadPacketCrcCntr         1       packets with bad crc (pegged at 0xFF)
IUPNotInSynchPacketCntr     1       packets arrive unexpectedly (pegged at 0xFF)
IUP_Error_Integrator        3       up-down error integrator

*/

typedef struct  tMcpDebugIupData {
    u_int8_t                     debugReq;
    u_int8_t                     filla2;
    u_int8_t                     filla3;
    u_int8_t                     filla4;

    u_int8_t                     iupRunning;
    u_int8_t                     iupNoPacketCntr;
    u_int8_t                     iupMissingPacketCntr;
    u_int8_t                     fillb3;

    u_int8_t                     iupBadPacketCrcCntr;
    u_int8_t                     iupNotInSynchPacketCntr;
    u_int8_t                     filld2;
    u_int8_t                     filld3;

    u_int32_t                    iup_Error_Integrator; // really u24
    u_int32_t                    iupTotalErrorCntr; // really u24

    u_int8_t                     fillf1;
    u_int8_t                     fillf2;
    u_int8_t                     fillf3;
    u_int8_t                     fillf4;
} tMcpDebugIupData;

typedef struct  _tMcpDebugCommData
{
    u_int8_t                    debugReq;   /*!< use @c eMcpDebugReq. */
    u_int8_t                    reserved1;
    u_int8_t                    reserved2;
    u_int8_t                    reserved3;
    tMcpSsiMeasures             ssi;
    tPlcCounters                plmCnts;
} tMcpDebugCommData;

// For corvus1 (raven1) based platforms and later ..
typedef struct  _tMcpDebugPlcData
{
    u_int8_t                    debugReq;   /*!< use @c eMcpDebugReq. */
    u_int8_t                    reserved1;
    u_int8_t                    reserved2;
    u_int8_t                    reserved3;
    tMcpSsiMeasures             ssi;
    tPlcCounters                coronaPlmCnts;

    tControllerCnts             controllerCnts;
    tPlcCounters                auroraPlmCnts;
    tAuroraDetailedCounters     auroraDetailedCnts;

} tMcpDebugPlcData;

typedef struct  _tMcpDebugMsg {
    /* debug request / response data follows here. */
    union {
        tMcpDebugCommCtl        comm;
        tMcpDebugCmd            cmd;
        tMcpDebugHarness        harness;
        tMcpDebugPwrTstCtl      pwrTstCtl;
        tMcpDebugPwrMonData     pwrMonData;
        tMcpDebugPwrMonDataNsrb pwrMonDataNsrb;
        tMcpDebugPwrMonDataReactive pwrMonDataReactive;
        tMcpDebugCommData       commData;
        tMcpDebugBoolsData      boolData;
        tMcpDebugMpptData       mpptData;
        tMcpDebugSkipData       skipData;
        tMcpDebugSkipDataHeron  skipDataHeron;
        tMcpDebugSkipDataHarrier skipDataHarrier;
        tMcpDebugPllData        pllData;
        tMcpDebugIupData        iupData;
        tMcpDebugPlcData        plcData; // corvus1 and later only
        tMcpDebugReqRegister    reqRegister;  // heron power train
        tMcpDebugH2ReqRegister  h2ReqRegister;  // heron2 power train
    } u;
} tMcpDebugMsg;

#endif /* _mcp_debug_h_ */


/* END of mcp_debug.h */



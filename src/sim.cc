
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#ifndef WIN32
#include <unistd.h>
#include <sys/time.h>
#endif // !WIN32
#include <assert.h>
#include "sbt/log.h"
#include "sbt/errno.h"
#include "sbt/connection.h"
//#include "sbt/xaction.h"
#include "sbt/scooter_uart.h"
#include "sbt/MMap.hh"
#include <list>
//#include "enphase/enph_xaction.h"
//#include "include/mcp_sunpower.h"

#include "include/enphase.h"
#include "include/pmi.h"
#include "include/agf.h"
#include "include/dll.h"
#include "include/mcp_counters.h"
#include "include/mcp_debug.h"
#include "include/mcpsec.h"
#include "include/ring.h"
#include "include/timer.h"
#include "include/emuutl.h"
#include "include/emudbg.h"
#include "include/mcp.h"
#include "include/modmcp.h"
#include "enphase/emuutil.h"
#include "pcu.hh"
#include "pv_array.hh"



#define MAX_CONNECTIONS (4)


void *multi_process(void *p)
{

    cPV_ARRAY *pPV;
	cPCUSim *pPCU;
	cJSON *pArrays, *pArray;
	cJSON *pInverters;
	cJSON *pInv;
	cJSON *conn;
	cJSON *name;
    int rc;
	int numInv = 0;
	cJSON *serial;
	tSerialNumber devSerial;

	pArrays = (cJSON *)p;
	pArray = cJSON_GetObjectItem(pArrays, "array");
	conn = cJSON_GetObjectItem(pArray, "connection");
	name = cJSON_GetObjectItem(pArray, "name");
	pPV = new cPV_ARRAY((char *)conn->valuestring, (char *)name->valuestring);
	if ((rc = pPV->OpenConnection(100)) != 0) {
		printf("Connection to %s failed %d\n", (char *)conn->valuestring, rc);
		delete pPV;
		return NULL;
	}
	printf("%s: connection:%s(", name->valuestring, conn->valuestring);
	pInverters = cJSON_GetObjectItem(pArray, "inverters");
	numInv = cJSON_GetArraySize(pInverters);
	for (int i = 0; i < numInv; i++) {
		pInv = cJSON_GetArrayItem(pInverters, i);
		pInv = cJSON_GetObjectItem(pInv, "inverter");
		serial = cJSON_GetObjectItem(pInv, "serialNumber");
		if (i != (numInv-1)) {
			printf("%s,", serial->valuestring);
		}
		else
			printf("%s", serial->valuestring);
		emumtilStrToSerialNum(serial->valuestring, &devSerial); // this needs to be in JSON
		pPCU = new cPCUSim(&devSerial);
		pPV->AddPCU(pPCU);
	}
	printf(")\n");
	while (1) {
		pPV->WaitPLCMessage(50);
		pPV->Ticker(2);
	}
}
int main(int argc, char* argv[]) 

{
    pthread_t threads[MAX_CONNECTIONS];
	int iret[MAX_CONNECTIONS];
	int i;
	int numArrays=0;
	
	cJSON *pRoot = nullptr;
	cJSON *pArray;

	// load JSON file and create thread for each pv_array.
	
	MMap *pFileBuf;
    try
    {
        pFileBuf = new MMap(argv[1]);
    }
    catch (int e)
    {
        LOG_ERROR("Could not open '%s'\n", argv[1]);
        return 0;
    }

    pRoot = cJSON_Parse((const char *)pFileBuf->buf());
    pArray = cJSON_GetObjectItem(pRoot, "arrays");
    numArrays = cJSON_GetArraySize(pArray);
	if (numArrays<=0 || numArrays > MAX_CONNECTIONS) {
		printf("Bad Json-no arrays or too many\n");
		exit(1);
	}
	


	
	for (i=0; i < numArrays; i++) {
		
		iret[i] = pthread_create(&threads[i], NULL, multi_process, (void *)cJSON_GetArrayItem(pArray, i));
	}


	
	for (i=0; i < numArrays; i++) {
		pthread_join( threads[i], NULL);
	}
	cJSON_Delete(pRoot);
	delete pFileBuf;

	for (i=0; i < numArrays; i++) {
		printf("threads %d returns %d\n", i, iret[i]);
	}
	/* ... */ 
}
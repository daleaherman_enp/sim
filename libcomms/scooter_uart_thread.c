/*
 * @copyright
 * Copyright 2014, SolarBridge Technologies, LLC\n
 * All rights reserved.
 *
 * @file scooter_uart.c
 *  Scooter-side UART interface
 *
 * @date Sep 11, 2014
 * @author Julie Haugh <j.haugh@solarbridgetech.com>
 */

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <poll.h>
#include <alloca.h>
#include <memory.h>
#include <assert.h>
#include <sys/ioctl.h>
#ifdef __gnu_linux__
#include <linux/usbdevice_fs.h>
#include <pthread.h>
#include <errno.h>
#endif

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>

#include "sbt/os.h"
#include "sbt/scooterProto.h"
#include "sbt/log.h"
#include "sbt/errno.h"
#include "sbt/scooter_uart.h"
#include "sbt/cobs.h"
#include "sbt/crc.h"

#ifndef MIN
    #define MIN(x,y) ((x) < (y) ? (x) : (y))
#endif

typedef enum
{
    COBS_WAITING,
    COBS_SCANNING,
    COBS_OVERRUN,
    COBS_DONE
} cobs_state_t;

int32_t g_errcrc;

static void uart_packet_dump(uint8_t *buffer, int length)
{
    int i;

    for (i = 0; i < length; i++)
    {
        if (i > 0 && i % 16 == 0)
        {
            fputc('\n', stderr);
        }

        fprintf(stderr, "%02x ", buffer[i]);
    }
    fputc('\n', stderr);
    fflush(stderr);
}


//! \addtogroup uart_config
//! @{

//! The max length of the string to hold the device name
#define UART_DEVNAME_LENGTH     126

//! The max size of a raw COBS buffer
#define COBS_PACKET_LENGTH      (SCOOTER_MAX_PACKET + 1)

//! @}

struct scooter_uart_t
{
    int32_t uart_fd;
    uint32_t uart_debug;
    uint32_t uart_read_timeout;
    size_t cobs_length;
    cobs_state_t cobs_state;
    uint8_t cobs_packet_in[COBS_PACKET_LENGTH];
    uint8_t devname[UART_DEVNAME_LENGTH];
};


#define THREAD
/*
 * 20150629 Hans Magnusson
 * A separate thread is created when a uart is opened. This thread reads from the uart and puts
 * assembled packets into a fifo. When the application writes plc messages to the uart a semaphore
 * is setup to let the main thread sleep until write ack is returned or times out.
 * The read thread decodes write acks and naks and send a signal to the main thread that is sleeping
 * on waiting for the write ack.
 * 
 * TODO:
 * 1. Finish L2 read/write calls with FIFO management
 * 2. Add filter to discard spurious packets.   
 * 3. Add the possibility of multiple readers to allow white listed packets to go to 
 *    the normal main and other packets goes to a separate thread for processing.
 * 4. No error checking is done on pthread calls.
 * 5. Ping fails after 1 - 3 packets with out of sequence. It looks as if a packet is lost. Works after the
 *    initial hiccup.
 */
#ifdef THREAD

pthread_cond_t  write_done;      // signal condition that write ack has been received
pthread_mutex_t write_ack_mutex = PTHREAD_MUTEX_INITIALIZER;
bool            write_ack;       // confirm that write_ack was received and not write_nak
bool            write_nak;       // confirm write_nak was received instead of write_ack
pthread_cond_t  read_done;       // read completed (i.e. FIFO has at least one entry if initially empty)
pthread_mutex_t read_mutex      = PTHREAD_MUTEX_INITIALIZER;

scooter_uart_handle_t l_hThis;   // Copy of uart handle for use by the read thread.

pthread_t       read_thread_id;

#define THREAD_TIMEOUT 1
/*
 * FIFO stores received cobs decoded packets with valid CRC
 * uartread picks up first available packet from the fifo. If fifo is
 * empty the read sleeps on read_done (waiting for a new packet to come in).
 */
#define FIFO_SIZE      4
int     fifo_rp = 0;
int     fifo_wp = 0;
typedef struct  read_buffer_fifo {
    int32_t device;
    size_t  length;
    int32_t flags;
    uint8_t packet[256];
} s_read_buffer_fifo_t;

s_read_buffer_fifo_t scooter_fifo[FIFO_SIZE];


static int32_t uart_decode_packet(scooter_uart_handle_t hThis, int newlen);
static void uart_cobs_receive(scooter_uart_handle_t hThis, uint8_t cobs_char);

static int32_t fifo_add(int32_t device, size_t length, uint8_t *packet)
{
    int rc;

    if ((fifo_rp <= fifo_wp) || ((fifo_wp + 1) < fifo_rp)) {
	scooter_fifo[fifo_wp].device = device;
	scooter_fifo[fifo_wp].length = length;
	memcpy(scooter_fifo[fifo_wp].packet, packet, length);

	// LOG_INFO("%s:%d: trying to lock read_done\n", );	
	rc = pthread_mutex_lock(&read_mutex);

	if (fifo_wp == 3)
	    fifo_wp = 0;
	else
	    fifo_wp++;

	rc = pthread_cond_signal(&read_done);
	// LOG_INFO("%s:%d: locked read_done\n", );	
	
	rc = pthread_mutex_unlock(&read_mutex);
	// LOG_INFO("%s:%d: signal read_done\n", );	

	return 0;
    }
    return -1;
}

static int32_t fifo_remove(uint8_t *packet, size_t *length, int32_t *device)
{
    if (fifo_rp != fifo_wp) {
	*device = scooter_fifo[fifo_rp].device;
	*length = scooter_fifo[fifo_rp].length;
	memcpy(packet, scooter_fifo[fifo_rp].packet, scooter_fifo[fifo_rp].length);
	// LOG_INFO("FIFO RP %d WP %d Length %d\n", fifo_rp, fifo_wp, *length);
	if (fifo_rp == 3)
	    fifo_rp = 0;
	else
	    fifo_rp++;

	return 0;
    } else {
	*device = 0;
	*length = 0;
    }
    return -1;
}

/*
 * Modified from original uartread to first poach the fifo with assembled packets.
 * If fifo is empty we'll wait on read_done for the timeout period specified.
 */
int32_t uartread(scooter_uart_handle_t hThis, uint32_t timeout,
                 uint8_t *packet, size_t *length,
                 int32_t *device)
{

    struct timespec ts;
    struct timeval  tp;
    int    rc, flag;

    // LOG_INFO("%s:%d: Enter uartread fifo_rp %d fifo_wp %d\n",  fifo_rp, fifo_wp);
    rc = pthread_mutex_lock(&read_mutex);

    // LOG_INFO("%s:%d: locked read_mutex fifo_rp %d fifo_wp %d\n",  fifo_rp, fifo_wp);

    if (fifo_rp == fifo_wp) {

	// LOG_INFO("%s:%d: lock read_mutex\n", );
	rc = pthread_cond_init(&read_done, NULL);
	// LOG_INFO("%s:%d: wait read_done\n", );
	// assert(rc != ENOTLOCKED);
	rc = gettimeofday(&tp, NULL);
	ts.tv_sec  = tp.tv_sec;
	ts.tv_nsec = tp.tv_usec * 1000;
	ts.tv_sec  += (timeout / 1000) + 1;
	// ts.tv_nsec += 1000000 * (timeout % 1000);
	rc = pthread_cond_timedwait(&read_done, &read_mutex, &ts);
	// LOG_INFO("%s:%d: wait read_done returned %d timeout %d\n",  rc, timeout);
    }
    flag = fifo_remove(packet, length, device);
    rc = pthread_mutex_unlock(&read_mutex);

    // LOG_INFO("%s:%d: uartread fifo_rp %d fifo_wp %d flag %d\n",  fifo_rp, fifo_wp, flag);
    if (flag == 0)
	return SUCCESS;
    return -ETIMEDOUT;
}


/*
 * uart_fast_read
 * Called from the read thread to continously receive data from uart. 
 * First the in-buffer is checked for bytes available. If greater than zero read into locak
 * buffer. If no bytes are available setup a poll for one byte for one second. Read what is currently in the
 * in-buffer and copy into the local buffer.
 * If more than 300 bytes are received discard the data.
 * When the local buffer has data in it go on to scan for valid cobs packet.
 * If the buffer is partial return to thread and get called again. The buffer and length are static and preserved
 * between calls. When called again additional received data is appended to the buffer and cobs checking continous.
 * When a complete cobs packet is found it is passed to uart_decode_packet for processing. If valid CRC it will be
 * posted in the fifo otherwise discarded. Write-ack and nak are individually processed and send signal to main thread
 * to unblock writes. Superflous bytes in the buffer are moved to the beginning and the length is adjusted. This allows
 * follow on data to be assembe into a new packet.
 */

static int32_t uart_fast_read(scooter_uart_handle_t hThis)
{
    struct pollfd poll_fd;
    int bytes_avail;
    int  bufpos, i;
    static int  buflen = 0;
    static char buffer[300];
    
    int rc;

    if(!hThis) return -EINVAL;

    poll_fd.fd = hThis->uart_fd;
    poll_fd.events = POLLIN;
    poll_fd.revents = 0;

    // LOG_INFO("%s:%d: enter uart_fast_read\n", );
    bytes_avail = 0;
    ioctl(hThis->uart_fd, FIONREAD, &bytes_avail);
    // LOG_INFO("%s:%d: uart_fast_read poll bytes_avail %d\n",  bytes_avail);
    if ((bytes_avail > 0) && (bytes_avail + buflen < 300)) {
	bytes_avail = read(hThis->uart_fd, &buffer[buflen], bytes_avail);
	buflen += bytes_avail;
    } else if (bytes_avail + buflen >= 300) {
	buflen = 0;
	return -1;
    } else {
	/* One second timeout */
	if ((rc = poll(&poll_fd, 1, 1000)) <= 0)
	    {
		return -1;
	    }
	bytes_avail = 0;
	ioctl(hThis->uart_fd, FIONREAD, &bytes_avail);
	// LOG_INFO("%s:%d: uart_fast_read poll bytes_avail %d\n",  bytes_avail);
	if ((bytes_avail > 0) && (bytes_avail + buflen < 300)) {
	    bytes_avail = read(hThis->uart_fd, &buffer[buflen], bytes_avail);
	    buflen += bytes_avail;
	} else {
	    return -1;
	}
    }
    // LOG_INFO("%s:%d: uart_fast_read buflen %d\n",  buflen);
    /* If first packet it has length greater than 4. We can figure out 
     * type of packet and how long it should be.
     */
    while (((hThis->cobs_state == COBS_SCANNING) || (hThis->cobs_state == COBS_WAITING)) && (buflen > 0)) {
	int cobs_length;
	bufpos = 0;
	while (bufpos < buflen) {
	    uart_cobs_receive(hThis, buffer[bufpos++]);
	    switch(hThis->cobs_state) {
	    case COBS_WAITING: case COBS_SCANNING:
		break;
	    case COBS_DONE: 
		cobs_length = cobs_decode_in_place(hThis->cobs_packet_in, hThis->cobs_length);
		rc = uart_decode_packet(hThis, cobs_length);

		hThis->cobs_state  = COBS_WAITING;
		hThis->cobs_length = 0;
		/* Buffer will be cleared from the processed cobs packet and surplus is moved to the beginning.
		 * If we got an overrun discard and start over from current position.
		 */
	    case COBS_OVERRUN:
		i = 0;
		while (bufpos < buflen) {
		    buffer[i++] = buffer[bufpos++];
		}
		bufpos = 0;
		buflen = i;
		break;
	    }
	}
	if (bufpos == buflen)
	    buflen = 0;
    }
    
    /* Unfinished code to decode header to determine length of packet. 
     * This will allow for a single read call to finish the rest of the packet.
     * It is unclear if this will be truly useful.
     *
    if ((hThis->cobs_state == COBS_SCANNING) && (bufpos > 8)) {
	int   c_in;
	int   c_out = 0;
	int   count = 0;
	uint8_t decode_header[8];
	for (c_in = 0; c_in < 8; c_in++) {
	    if (count == 0) {
		count = hThis->cobs_packet_in[c_in];
	    } else {
		count--;
		if (count == 0)
		    decode_header[c_out++] = '\0';
		else
		    decode_header[c_out++] = hThis->cobs_packet_in[c_in];
	    }
	}
	if (c_out > 6) {
	    int missing_bytes;
	    if (decode_header[0] == ePLC_INTF_WRITE_TX) {

		missing_bytes = decode_header[1] - buflen;
		if (missing_bytes > 0) {

		}
	    } else if (decode_header[0] == eSC_INTF_CMD_VERSION) {

	    }
	}
    }
    */

    return 0;
}

/*
 * uart_decode_packet is taken from the original uartread function to do
 * the cobs decode and crc check. This version also decodes write ack/nak
 * and sends a signal to unblock the write routine.
 * If a valid packet is found it is stuck into the fifo.
 */

static int32_t uart_decode_packet(scooter_uart_handle_t hThis, int newlen)
{
    int cobs_offset, rc;
    uint16_t cobs_crc;
    uint16_t data_crc;

    if(newlen < 0)
    {
        return -EOVERFLOW;
    }
    else
    {
        LOG_VERBOSE_MED("cobs_decode: length = %d\n", newlen);
    }

    if (hThis->cobs_packet_in[newlen - 1] == 0)
    {
        newlen--;
    }

    LOG_VERBOSE_MED("<<< Recv\n");
    if (hThis->uart_debug & TRACE_RX)
    {
        uart_packet_dump(hThis->cobs_packet_in, newlen);
    }

    /*
     * A COBS-encoded packet cannot be 1 or 3 bytes long.  No, really -- it can
     * be two, or greater than four.  But 1 and 3 -- straight out.
     */
    if (newlen < 4 && newlen != 2)
    {
        LOG_ERROR("incorrect length %ld\n", newlen);
        return -SBT_EMISMATCH;
    }
    else
    {
        LOG_VERBOSE_MED("Length correct\n");
    }

    /*
     * hThis is an ACK -- discard it, then deal with it later.
     *
     * @todo Post the ACK to the channel.
     */
    if (newlen == 2)
    {
        LOG_VERBOSE_MED("cobs_length = 2\n");
        return 0;
    }

    cobs_offset = 1;

    // LOG_INFO("in decode fb %02x newlen %d offset %d\n",  hThis->cobs_packet_in[0], newlen, cobs_offset);
    /*
     * The packet is now a device address, status flags, packet, then two byte
     * CRC-16-CCITT.
     */
    switch(hThis->cobs_packet_in[0])
    {
        case ePLC_INTF_WRITE_TX:
            cobs_offset = 2;
            break;

        case ePLC_INTF_ACK:
	    rc = pthread_mutex_lock(&write_ack_mutex);
	    write_nak = false;
	    write_ack = true;
	    rc = pthread_cond_signal(&write_done);
	    // LOG_INFO("signal write_ack rc %d\n",  rc);
	    rc = pthread_mutex_unlock(&write_ack_mutex);
	    // LOG_INFO("unlock write_ack %d\n",  rc);
	    return 0;
	    break;

        case ePLC_INTF_NACK:
	    rc = pthread_mutex_lock(&write_ack_mutex);
	    write_nak = true;
	    write_ack = false;
	    rc = pthread_cond_signal(&write_done);
	    rc = pthread_mutex_unlock(&write_ack_mutex);
	    // LOG_INFO("signal write_NAK\n");
	    return 0;
	    break;



        // case ePLC_INTF_LOCAL_MODBUS:
        // case ePLC_INTF_EXTERN_MODBUS:
        // case ePLC_INTF_SET_RX_GAIN:
        // case ePLC_INTF_SET_TX_AMPLITUDE:
            // these are not implemented yet
        //    LOG_ERROR("Not implemented yet: %d\n",
        //              hThis->cobs_packet_in[0]);
        //    return -ENOSYS;

        case ePLC_INTF_ECHO:
        case ePLC_INTF_READ_STATS:
        case ePLC_INTF_RESET_STATS:
        case ePLC_INTF_UPGRD_DATA:
        case ePLC_INTF_UPGRD_COMMIT:
        case ePLC_INTF_UPGRD_STAT:
        case ePLC_INTF_VERSION:
        case ePLC_INTF_RESET_SCSTATS:
        case ePLC_INTF_READ_SCSTATS:
        case ePLC_INTF_ADATA:
        case ePLC_INTF_WRITE_WHITELIST:
        case ePLC_INTF_CLEAR_WHITELIST:
        case ePLC_INTF_SET_FILTERING:
        case ePLC_INTF_RESET:
        case ePLC_INTF_READ_WNSTATS:
        case ePLC_INTF_WNPOLL:
        case eSC_INTF_SET_CONFIG:
        case eSC_TESTINTF_NULL_COMMAND:

        case eSC_TESTINTF_READ_INTERNAL_VOLTAGES:
        case eSC_TESTINTF_GET_VERSION:  // get FW/HW version information
        case eSC_TESTINTF_READ_AC_LINE_VOLTAGES: // PSOC will measure RMS voltage on L1/L2 and report values
        case eSC_TESTINTF_READ_CT_VOLTAGES: // PSOC will measure RMS voltage on ALL CT inputs and report values - should do individually to make sure no xtalk.
        case eSC_TESTINTF_PLC_LOOPBACK_TEST:   // sends PLC command and checks for successful message recieved and RSSI
        case eSC_TESTINTF_PLC_ENABLE_TEST:   // disables the PLC_N_WRITE_ENABLE and sends PLC command, RSSI should measure low and no message received.
        case eSC_TESTINTF_PLC_TX_CAP_TEST:  // do not enable the TX CAP, should detect change in RSSI ???
        case eSC_TESTINTF_WRITE_TEST_DATA:  // write the reserved 28 bytes to the test segment sSC_EEPROM_TestData
        case eSC_TESTINTF_READ_TEST_DATA:   // read the 28 bytes from the test segment
        case eSC_TESTINTF_SET_LED: // set the two lEDS according to sSC_led_interface
        case eSC_TESTINTF_SUPERVISORY_TEST: // stops the watchdog, should generate reset, set alert in status
        case eSC_TESTINTF_RESET_STATUS: // reset the status word in order to test watchdog/supervisor
        case eSC_TESTINTF_READ_RTS: // returns current value of RTS in sSC_cts_rts_interface
        case eSC_TESTINTF_WRITE_CTS: // sets the CTS to 0,1 based on sSC_cts_rts_interface
        case eSC_TESTINTF_READ_STATUS: // return the status word
        case eSC_TESTINTF_RESET:
        case eSC_TESTINTF_WRITE_BOOT_DATA:
        case eSC_TESTINTF_READ_BOOT_DATA:
        case eSC_TESTINTF_WRITE_CAL_DATA:
        case eSC_TESTINTF_READ_CAL_DATA:
        case eSC_TESTINTF_SET_SERIAL:
        case eSC_TESTINTF_READ_SERIAL:
        case eSC_INTF_RESET_STATUS_FLAGS:
            // no action necessary.
            break;

        default:
            LOG_ERROR("bad device address %d\n",
                      hThis->cobs_packet_in[0]);
            return -SBT_EBADMODE;
    }

    cobs_crc = hThis->cobs_packet_in[newlen - 2]
               | (hThis->cobs_packet_in[newlen - 1] << 8);

    data_crc = crc16ccitt(hThis->cobs_packet_in, newlen - 2,
                                    0xFFFF);
    if (cobs_crc != data_crc)
    {
        g_errcrc++;
        LOG_ERROR("Expected CRC %04x, got %04x (len = %ld)\n", 
                  data_crc, cobs_crc, newlen);

        return -SBT_EBADCRC;
    }
    else
    {
        LOG_VERBOSE_MED("CRCs good\n");
    }

    /*
     * Strip the CRC off the end -- it's all done with.
     */
    newlen -= 2;

    /*
     * Copy the packet out to the caller and tell them how long it actually was.
     * Make sure we don't over-flow the callers supplied buffer.  The caller
     * must be sure to pre-set *length to the size of their buffer.
     */
    {
        int minlen;
	int rc;

        minlen = (newlen - cobs_offset);

	rc = fifo_add(hThis->cobs_packet_in[0], minlen, hThis->cobs_packet_in + cobs_offset);
    }

    return 0;
}


static void *uart_read_thread(void *parm)
{
  int rc;
  
  // LOG_INFO("read thread started\n");	
  rc = pthread_mutex_lock(&read_mutex);
  rc = UartFlush(l_hThis);
  rc = pthread_cond_signal(&read_done);
  rc = pthread_mutex_unlock(&read_mutex);

  while (1) {

    rc = uart_fast_read(l_hThis);
    // LOG_INFO("Fast read return %d\n",  rc);	
  }
  pthread_cond_destroy(&write_done);
  pthread_cond_destroy(&read_done);
}



#endif




//! \addtogroup uart_api UART API
//! @{


char * UartDeviceName(scooter_uart_handle_t hThis)
{
  if (hThis == NULL)
    return NULL;
  return(hThis->devname);
}


int32_t UartDebugSet(scooter_uart_handle_t hThis, uint32_t ui32Flags)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    hThis->uart_debug = ui32Flags;

    return 0;
}

/*!
 * @details Set the number of milliseconds to wait for a message response
 *
 * @param timeout - Response timeout in milliseconds
 */
int32_t UartTimeoutReadSet(scooter_uart_handle_t hThis, uint32_t timeout)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    hThis->uart_read_timeout = timeout;

    return 0;
}

/*!
 * @details Get the number of milliseconds to wait for a message response
 *
 * @param timeout - Response timeout in milliseconds
 */
int32_t UartTimeoutReadGet(scooter_uart_handle_t hThis, uint32_t *pTimeout)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    *pTimeout = hThis->uart_read_timeout;

    return 0;
}

/*!
 * @details Close the UART connection and reset the open connection indication.
 */
int32_t UartClose(scooter_uart_handle_t hThis)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    if (hThis->uart_fd != -1)
    {
        LOG_VERBOSE_LO("Closing fd %d\n", hThis->uart_fd);
        close(hThis->uart_fd);
    }
    else
    {
        LOG_VERBOSE_LO("Attempting to close an unopened UART\n");
    }

    hThis->uart_fd = -1;
    hThis->uart_debug = 0;

    return 0;
}

/*!
 * @details Update the input buffer state, using a single byte of data.
 *
 * @param cobs_char - The character which was just received from the UART.
 *
 * @returns The new value of cobs_state.
 */
static void uart_cobs_receive(scooter_uart_handle_t hThis, uint8_t cobs_char)
{
    switch (hThis->cobs_state)
    {
        case COBS_WAITING:
            if (cobs_char == 0x00)
            {
                hThis->cobs_state = COBS_SCANNING;
                hThis->cobs_length = 0;
            }
            return;

        case COBS_SCANNING:
            if (cobs_char == 0x00)
            {
                if (hThis->cobs_length > 0)
                {
                    hThis->cobs_state = COBS_DONE;
                    return;
                }
                else
                {
                    return;
                }
            }
            if (hThis->cobs_length < sizeof(hThis->cobs_packet_in))
            {
                hThis->cobs_packet_in[hThis->cobs_length++] = cobs_char;
                return;
            }
            hThis->cobs_state = COBS_OVERRUN;
            return;

        case COBS_OVERRUN:
            if (cobs_char == 0x00)
            {
                hThis->cobs_state = COBS_WAITING;
                hThis->cobs_length = 0;
            }
            return;

        case COBS_DONE:
            //@todo review this.  Should done transition to waiting?
            return;

        default:
            //@todo handle this?
            assert(0);
            return;
    }
}

/*!
 * @details Flush all unread characters from the UART input.
 * @todo - figure out why flush and drain don't seem to be working
 */
int32_t UartFlush(scooter_uart_handle_t hThis)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    LOG_VERBOSE_MED("UartFlush\n");
#ifndef THREAD
    if (hThis->uart_fd != -1)
    {
        tcflush(hThis->uart_fd, TCIFLUSH);
        // tcdrain(hThis->uart_fd);
    }
#endif
    return 0;
}

int32_t UartConnectionNew(scooter_uart_handle_t *pHandle)
{
    scooter_uart_handle_t hThis;

    if (!pHandle)
    {
        return -EINVAL;
    }

    hThis = calloc(1, sizeof(struct scooter_uart_t));

    if (!hThis)
    {
        return -ENOMEM;
    }

    hThis->cobs_state = COBS_WAITING;
    hThis->uart_fd = -1;
    hThis->uart_debug = 0;
    hThis->uart_read_timeout = 500;

    *pHandle = hThis;

    return 0;
}

int32_t UartConnectionDelete(scooter_uart_handle_t hThis)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    if (hThis->uart_fd != -1)
    {
        return -EADDRINUSE;
    }

    memset(hThis, 0xfe, sizeof(struct scooter_uart_t));

    free(hThis);

#ifdef THREAD
    pthread_cancel(read_thread_id);

#endif
    return 0;
}

/*
 * @details Open the UART device and configure the termios settings for the UART proper.
 *
 * @param device - Name of the physical device to open.
 *
 * @returns 0 = success, -1 = failure.
 */
int32_t UartDeviceOpen(scooter_uart_handle_t hThis, const uint8_t *device)
{
    struct termios options;
    int32_t rc;

    if (!hThis || !device)
    {
        return -EINVAL;
    }
    strncpy(hThis->devname, device, UART_DEVNAME_LENGTH);

    // fprintf(stderr, "Attempting to open device '%s'\n", hThis->devname);
    // Note: On OS X this open call takes a long time.
    rc = open(hThis->devname, O_RDWR | O_NONBLOCK);
    // rc = open(hThis->devname, O_RDWR);
    // fprintf(stderr, "open() = %d'\n", rc);
    if (rc < 0)
    {
        return -errno;
    }
    else
    {
        hThis->uart_fd = rc;
    }

    tcgetattr(hThis->uart_fd, &options);
    cfmakeraw(&options);
    options.c_cflag |= (CLOCAL | CREAD);

    //cfmakeraw(&options);
    cfsetispeed(&options, B38400);
    cfsetospeed(&options, B38400);
    //options.c_cc[VMIN] = 1;
    //options.c_cc[VTIME] = 0;
    tcflush(hThis->uart_fd, TCIFLUSH);
    rc = tcsetattr(hThis->uart_fd, TCSANOW, &options);
    if (rc < 0)
    {
        close(hThis->uart_fd);
        hThis->uart_fd = -1;
        return -2;
    }

#ifdef THREAD
    l_hThis = hThis;
    rc = pthread_create(&read_thread_id, NULL, uart_read_thread, NULL);
    rc = pthread_mutex_lock(&read_mutex);
    rc = pthread_cond_init(&read_done, NULL);
    rc = pthread_cond_wait(&read_done, &read_mutex);
    rc = pthread_mutex_unlock(&read_mutex);
#endif


    return 0;
}


#ifdef THREAD
int32_t unbuffered_uartread(scooter_uart_handle_t hThis, uint32_t timeout,
                 uint8_t *packet, size_t *length,
                 int32_t *device)
#else
int32_t uartread(scooter_uart_handle_t hThis, uint32_t timeout,
                 uint8_t *packet, size_t *length,
                 int32_t *device)
#endif
{
    struct pollfd poll_fd;
    uint16_t cobs_crc;
    uint16_t data_crc;
    uint8_t status;
    int newlen;
    // static char orig[300];

    int cobs_offset;
    int rc;

    if(!hThis) return -EINVAL;

    //LOG_VERBOSE_MED("fd = %d, timeout = %d, cobs_state = %d\n",
    //         hThis->uart_fd, timeout, hThis->cobs_state);

    /*
     * @todo After making a UART into an "object" the initial response wait
     * time should be made configurable.  For the time being, it will be 500ms
     * because that seems like an eternity.
     */
    poll_fd.fd = hThis->uart_fd;
    poll_fd.events = POLLIN;
    poll_fd.revents = 0;

    if ((rc = poll(&poll_fd, 1, timeout)) <= 0)
    {
        if (rc < 0)
        {
            LOG_ERROR("poll() failed\n");
            return -SBT_EUARTREADFAIL;
        }
        else
        {
            // LOG_VERBOSE_MED("poll() timed out\n");
            return -ETIMEDOUT;
        }
    }

    /*
     * At this point, there is at least one "POLLIN" event, which means there
     * is at least one byte.  Now I shift from "uart_read_timeout" per
     * character to 25ms.
     */
    hThis->cobs_state = COBS_WAITING;
    hThis->cobs_length = 0;

    while (hThis->cobs_state != COBS_DONE && hThis->cobs_state != COBS_OVERRUN
            && poll(&poll_fd, 1, 200) > 0)
    {
        uint8_t c;
        int rc;

        if ((rc = read(hThis->uart_fd, &c, 1)) != 1)
        {
            LOG_ERROR("read() failed with rc = %d\n", rc);

            hThis->cobs_state = COBS_DONE;
            return -SBT_EUARTREADFAIL;
        }
        uart_cobs_receive(hThis, c);
    }

    if (hThis->cobs_state == COBS_OVERRUN)
    {
        LOG_ERROR("overrun!\n");

        return -SBT_ECOBSOVERRUN;
    }

    // memcpy(orig, hThis->cobs_packet_in, 256);

    /*
     * I have a properly framed COBS-encoded packet with a length of
     * cobs_length.  Now I do a bunch of stuff on it.  The first of which is
     * decode, then strip what should be a trailing NUL byte.
     */
    newlen = cobs_decode_in_place(hThis->cobs_packet_in,
                         hThis->cobs_length);
    if(newlen < 0)
    {
        return -EOVERFLOW;
    }
    else
    {
        LOG_VERBOSE_MED("cobs_decode: length = %d\n", newlen);
    }

    if (hThis->cobs_packet_in[newlen - 1] == 0)
    {
        newlen--;
    }

    LOG_VERBOSE_MED("<<< Recv\n");
    if (hThis->uart_debug & TRACE_RX)
    {
        uart_packet_dump(hThis->cobs_packet_in, newlen);
    }

    /*
     * A COBS-encoded packet cannot be 1 or 3 bytes long.  No, really -- it can
     * be two, or greater than four.  But 1 and 3 -- straight out.
     */
    if (newlen < 4 && newlen != 2)
    {
        LOG_ERROR("incorrect length %ld\n", newlen);
        return -SBT_EMISMATCH;
    }
    else
    {
        LOG_VERBOSE_MED("Length correct\n");
    }

    /*
     * hThis is an ACK -- discard it, then deal with it later.
     *
     * @todo Post the ACK to the channel.
     */
    if (newlen == 2)
    {
        LOG_VERBOSE_MED("cobs_length = 2\n");
        return 0;
    }

    cobs_offset = 1;

    /*
     * The packet is now a device address, status flags, packet, then two byte
     * CRC-16-CCITT.
     */
    switch(hThis->cobs_packet_in[0])
    {
        case ePLC_INTF_WRITE_TX:
#if 0
            // In the future, when receiving, device 1 (PLC) has status in byte[1]
            status = hThis->cobs_packet_in[1];
            if(status != PLC_STATUS_OK)
            {
                LOG_WARNING("RX'ed PLC reply with unexpected status %d\n",
                             status);
            }
#else
            // For now, device 1, byte 1 is a do-not-care
#endif
            cobs_offset = 2;
            break;

        // case ePLC_INTF_LOCAL_MODBUS:
        // case ePLC_INTF_EXTERN_MODBUS:
        // case ePLC_INTF_SET_RX_GAIN:
        // case ePLC_INTF_SET_TX_AMPLITUDE:
            // these are not implemented yet
        //    LOG_ERROR("Not implemented yet: %d\n",
        //              hThis->cobs_packet_in[0]);
        //    return -ENOSYS;

        case ePLC_INTF_ECHO:
        case ePLC_INTF_READ_STATS:
        case ePLC_INTF_RESET_STATS:
        case ePLC_INTF_ACK:
        case ePLC_INTF_NACK:
        case ePLC_INTF_UPGRD_DATA:
        case ePLC_INTF_UPGRD_COMMIT:
        case ePLC_INTF_UPGRD_STAT:
        case ePLC_INTF_VERSION:
        case ePLC_INTF_RESET_SCSTATS:
        case ePLC_INTF_READ_SCSTATS:
        case ePLC_INTF_ADATA:
        case ePLC_INTF_WRITE_WHITELIST:
        case ePLC_INTF_CLEAR_WHITELIST:
        case ePLC_INTF_SET_FILTERING:
        case ePLC_INTF_RESET:
        case ePLC_INTF_READ_WNSTATS:
        case ePLC_INTF_WNPOLL:
        case eSC_INTF_SET_CONFIG:
        case eSC_TESTINTF_NULL_COMMAND:

        case eSC_TESTINTF_READ_INTERNAL_VOLTAGES:
        case eSC_TESTINTF_GET_VERSION:  // get FW/HW version information
        case eSC_TESTINTF_READ_AC_LINE_VOLTAGES: // PSOC will measure RMS voltage on L1/L2 and report values
        case eSC_TESTINTF_READ_CT_VOLTAGES: // PSOC will measure RMS voltage on ALL CT inputs and report values - should do individually to make sure no xtalk.
        case eSC_TESTINTF_PLC_LOOPBACK_TEST:   // sends PLC command and checks for successful message recieved and RSSI
        case eSC_TESTINTF_PLC_ENABLE_TEST:   // disables the PLC_N_WRITE_ENABLE and sends PLC command, RSSI should measure low and no message received.
        case eSC_TESTINTF_PLC_TX_CAP_TEST:  // do not enable the TX CAP, should detect change in RSSI ???
        case eSC_TESTINTF_WRITE_TEST_DATA:  // write the reserved 28 bytes to the test segment sSC_EEPROM_TestData
        case eSC_TESTINTF_READ_TEST_DATA:   // read the 28 bytes from the test segment
        case eSC_TESTINTF_SET_LED: // set the two lEDS according to sSC_led_interface
        case eSC_TESTINTF_SUPERVISORY_TEST: // stops the watchdog, should generate reset, set alert in status
        case eSC_TESTINTF_RESET_STATUS: // reset the status word in order to test watchdog/supervisor
        case eSC_TESTINTF_READ_RTS: // returns current value of RTS in sSC_cts_rts_interface
        case eSC_TESTINTF_WRITE_CTS: // sets the CTS to 0,1 based on sSC_cts_rts_interface
        case eSC_TESTINTF_READ_STATUS: // return the status word
        case eSC_TESTINTF_RESET:
        case eSC_TESTINTF_WRITE_BOOT_DATA:
        case eSC_TESTINTF_READ_BOOT_DATA:
        case eSC_TESTINTF_WRITE_CAL_DATA:
        case eSC_TESTINTF_READ_CAL_DATA:
        case eSC_TESTINTF_SET_SERIAL:
        case eSC_TESTINTF_READ_SERIAL:
        case eSC_INTF_RESET_STATUS_FLAGS:
            // no action necessary.
            break;

        default:
            LOG_ERROR("bad device address %d\n", hThis->cobs_packet_in[0]);
            return -SBT_EBADMODE;
    }

    cobs_crc = hThis->cobs_packet_in[newlen - 2]
               | (hThis->cobs_packet_in[newlen - 1] << 8);

    data_crc = crc16ccitt(hThis->cobs_packet_in, newlen - 2,
                                    0xFFFF);
    if (cobs_crc != data_crc)
    {
        g_errcrc++;
        LOG_ERROR("Expected CRC %04x, got %04x (len = %ld)\n", 
                  data_crc, cobs_crc, newlen);

        return -SBT_EBADCRC;
    }
    else
    {
        LOG_VERBOSE_MED("CRCs good\n");
    }

    /*
     * Strip the CRC off the end -- it's all done with.
     */
    newlen -= 2;

    if (device != NULL)
    {
        *device = hThis->cobs_packet_in[0];
    }

    /*
     * Copy the packet out to the caller and tell them how long it actually was.
     * Make sure we don't over-flow the callers supplied buffer.  The caller
     * must be sure to pre-set *length to the size of their buffer.
     */
    {
        int minlen;
        minlen = MIN((newlen - cobs_offset), *length);
        memcpy(packet, hThis->cobs_packet_in + cobs_offset, minlen);
        *length = minlen;
    }

    return 0;
}

/*!
 * @details Read a packet from the UART, returning the address and any sub-address.
 *
 * @param packet - Pointer to the raw packet, as it was received.
 * @param length - Pointer to the length of the decoded (raw) packet.
 * @param device - Pointer to the device that was received.
 * @param status - Pointer to a byte to place status.
 *
 */
int32_t UartRead(scooter_uart_handle_t hThis, uint8_t *packet, size_t *length,
                 int32_t *device)
{

    return uartread(hThis, hThis->uart_read_timeout, packet, length, device);
}

/*!
 * @details Read a CRC-checked and COBS framed packet from the UART
 *
 * @param packet - Pointer to the raw packet, as it was received.
 * @param length - Pointer to the length of the decoded (raw) packet.
 * @param device - Pointer to the device that was received.
 * @param status - Pointer to a byte to place status.
 *
 */
int32_t UartReadL2(scooter_uart_handle_t hThis, uint8_t *packet,
                   size_t *length, uint32_t timeout)
{
    struct pollfd poll_fd;
    uint16_t cobs_crc;
    uint16_t data_crc;
    uint8_t status;
    int newlen;
    // static char orig[300];

    int cobs_offset;
    int rc;

    if(!hThis) return -EINVAL;

    //LOG_VERBOSE_MED("fd = %d, timeout = %d, cobs_state = %d\n",
    //         hThis->uart_fd, timeout, hThis->cobs_state);

    /*
     * @todo After making a UART into an "object" the initial response wait
     * time should be made configurable.  For the time being, it will be 500ms
     * because that seems like an eternity.
     */
    poll_fd.fd = hThis->uart_fd;
    poll_fd.events = POLLIN;
    poll_fd.revents = 0;

    if ((rc = poll(&poll_fd, 1, timeout)) <= 0)
    {
        if (rc < 0)
        {
            LOG_ERROR("poll() failed\n");
            return -SBT_EUARTREADFAIL;
        }
        else
        {
            // LOG_VERBOSE_MED("%s:%d: poll() timed out\n", __FILE__, __LINE__);
            return -ETIMEDOUT;
        }
    }

    /*
     * At this point, there is at least one "POLLIN" event, which means there
     * is at least one byte.  Now I shift from "uart_read_timeout" per
     * character to 25ms.
     */
    hThis->cobs_state = COBS_WAITING;
    hThis->cobs_length = 0;

    while (hThis->cobs_state != COBS_DONE && hThis->cobs_state != COBS_OVERRUN
            && poll(&poll_fd, 1, 200) > 0)
    {
        uint8_t c;
        int rc;

        if ((rc = read(hThis->uart_fd, &c, 1)) != 1)
        {
            LOG_ERROR("read() failed with rc = %d\n", rc);

            hThis->cobs_state = COBS_DONE;
            return -SBT_EUARTREADFAIL;
        }
        uart_cobs_receive(hThis, c);
    }

    if (hThis->cobs_state == COBS_OVERRUN)
    {
        LOG_ERROR("overrun!\n");

        return -SBT_ECOBSOVERRUN;
    }

    /*
     * I have a properly framed COBS-encoded packet with a length of
     * cobs_length.  Now I do a bunch of stuff on it.  The first of which is
     * decode, then strip what should be a trailing NUL byte.
     */
    newlen = cobs_decode_in_place(hThis->cobs_packet_in,
                         hThis->cobs_length);
    if(newlen < 0)
    {
        return -EOVERFLOW;
    }
    else
    {
        LOG_VERBOSE_MED("cobs_decode: length = %d\n", newlen);
    }

    if (hThis->cobs_packet_in[newlen - 1] == 0)
    {
        newlen--;
    }

    LOG_VERBOSE_MED("<<< Recv\n");
    if (hThis->uart_debug & TRACE_RX)
    {
        uart_packet_dump(hThis->cobs_packet_in, newlen);
    }

    /*
     * A COBS-encoded packet cannot be 1 or 3 bytes long.  No, really -- it can
     * be two, or greater than four.  But 1 and 3 -- straight out.
     */
    if (newlen < 4 && newlen != 2)
    {
        LOG_ERROR("incorrect length %ld\n", newlen);
        return -SBT_EMISMATCH;
    }
    else
    {
        LOG_VERBOSE_MED("Length correct\n");
    }

    /*
     * This is a null COBS frame.  We don't generate such things anywhere.
     */
    if (newlen == 2)
    {
        LOG_VERBOSE_MED("cobs_length = 2\n");
        *length = 0;
        return 0;
    }

    cobs_crc = hThis->cobs_packet_in[newlen - 2]
               | (hThis->cobs_packet_in[newlen - 1] << 8);

    data_crc = crc16ccitt(hThis->cobs_packet_in, newlen - 2,
                                    0xFFFF);
    if (cobs_crc != data_crc)
    {
        g_errcrc++;
        LOG_ERROR("Expected CRC %04x, got %04x (len = %ld)\n", 
                  data_crc, cobs_crc, newlen);

        return -SBT_EBADCRC;
    }
    else
    {
        LOG_VERBOSE_MED("CRCs good\n");
    }

    /*
     * Strip the CRC off the end -- it's all done with.
     */
    newlen -= 2;

    /*
     * Copy the packet out to the caller and tell them how long it actually was.
     * Make sure we don't over-flow the callers supplied buffer.  The caller
     * must be sure to pre-set *length to the size of their buffer.
     */
    {
        int minlen;
        minlen = MIN(newlen, *length);
        memcpy(packet, hThis->cobs_packet_in, minlen);
        *length = minlen;
    }

    return 0;
}

#define PLC_FLAGS_RQACK         0x01
/*!
 * @details Write a packet to the UART, using the appropriate addresses and
 * optional sub-address
 *
 * @param packet - The raw packet to transmit.
 * @param length - The size of the raw packet in bytes.
 * @param device - The device address.
 *
 * @return 0 for success,
 */
int32_t UartWrite(scooter_uart_handle_t hThis, const uint8_t *packet,
                  size_t length, int32_t device)
{
    uint8_t *cobs_in;
    uint8_t *cobs_out;
    uint16_t crc;
    int pre_cobs_length;
    int post_cobs_length;
    int cobs_length_needed;
    int32_t rc, dev;
    uint8_t status[SCOOTER_MAX_PACKET];
    size_t rxlen;

    if ((hThis->uart_fd == -1) || (length > SCOOTER_MAX_PACKET))
    {
        return -EINVAL;
    }

    cobs_in = alloca(length + 1 + 1 + 2);

    cobs_in[0] = device;

    // For PLC we place status flags after the device
    if(device == ePLC_INTF_WRITE_TX)
    {
        pre_cobs_length = length + 4;
        cobs_in[1] = PLC_FLAGS_RQACK;
        memcpy(cobs_in + 2, packet, length);

        crc = crc16ccitt(cobs_in, length + 2, 0xFFFF);

        cobs_in[length + 2] = (uint8_t) (crc & 0xFF);
        cobs_in[length + 3] = (uint8_t) ((crc >> 8) & 0xFF);
    }
    else
    {
        pre_cobs_length = length + 3;
        memcpy(cobs_in + 1, packet, length);

        crc = crc16ccitt(cobs_in, length + 1, 0xFFFF);

        cobs_in[length + 1] = (uint8_t) (crc & 0xFF);
        cobs_in[length + 2] = (uint8_t) ((crc >> 8) & 0xFF);
    }

    cobs_length_needed = cobs_get_required_size(pre_cobs_length);

    cobs_out = alloca(cobs_length_needed);
    cobs_out[0] = 0;
    post_cobs_length = cobs_encode(cobs_in, pre_cobs_length, cobs_out + 1);
    cobs_out[post_cobs_length + 1] = 0;

    post_cobs_length += 2;

    LOG_VERBOSE_MED("\n");
    if (hThis->uart_debug & TRACE_TX)
    {
        // uart_packet_dump(cobs_out, post_cobs_length);
        uart_packet_dump(cobs_in, pre_cobs_length);

    }

#ifdef THREAD
    if (device == ePLC_INTF_WRITE_TX) {
	rc = pthread_mutex_lock(&write_ack_mutex);
	write_nak = false;
	write_ack = false;
	// LOG_INFO("%s:%d: Write locked write_ack_mutex\n", );	
    }
#endif

    rc = write(hThis->uart_fd, cobs_out, post_cobs_length);
    if (rc != post_cobs_length)
    {
        LOG_WARNING("Unable to send entire payload\n", );
        return -SBT_EUARTWRITEFAIL;
    }

#ifdef THREAD
    if (device == ePLC_INTF_WRITE_TX) {
	struct timespec ts;
	struct timeval  tp;
	int    nak_timeout = 20;
	int    timeout     = 500;

	while ((nak_timeout-- > 0) && (write_ack == false)) {
	    rc = pthread_cond_init(&write_done, NULL);
	    // LOG_INFO("%s:%d: initialize write_done %d\n",  rc);

	    rc = gettimeofday(&tp, NULL);
	    ts.tv_sec   =  tp.tv_sec;
	    ts.tv_nsec  =  tp.tv_usec * 1000;
	    ts.tv_sec   += (timeout / 1000) + 1;
	    // ts.tv_nsec  += 1000000 * (timeout % 1000);
	    rc = pthread_cond_timedwait(&write_done, &write_ack_mutex, &ts);
	    // LOG_INFO("%s:%d: Wait on write_done nak_timeout %d ack %d nak %d sec %d rc %d\n",  nak_timeout, write_ack, write_nak, ts.tv_sec, rc);
	    rc = pthread_mutex_unlock(&write_ack_mutex);
	    if (write_ack)
		break;
	    rc = pthread_mutex_lock(&write_ack_mutex);
	    // LOG_INFO("%s:%d: relock write_ack %d\n",  rc);

	}
	if (write_ack) {
	    // LOG_INFO("%s:%d: write_ack received\n", );
	    return SUCCESS;
	}
	if (nak_timeout == 0)
	    return -SBT_EMISSINGACK;
	return -SBT_EUARTWRITEFAIL;
    }
#else

    if (device == ePLC_INTF_WRITE_TX)
    {
        LOG_VERBOSE_MED("Waiting for PLC TX Ack\n");

        while (true)
        {
            dev = 0;
            rxlen = sizeof(status);
            // here we have to wait for the longest PLC packet possible to transmit
            // and then for a 6-7 byte response to come back @ 38.4K
            rc = uartread(hThis, 2000, status, &rxlen, &dev);
            if ((rc == 0) && (dev == ePLC_INTF_NACK))
            {
                continue;
            }
            if ((rc == 0) && (dev != ePLC_INTF_ACK))
            {
                LOG_WARNING(
                        "Valid COBS frame But not ACK.  Old message?  "
                                "Discarding and waiting again... rc %d dev %d\n",
                         rc, dev);
                uart_packet_dump(status, rxlen);
                rxlen = sizeof(status);
                rc = uartread(hThis, 2000, status, &rxlen, &dev);
                LOG_INFO("uartread rc %d status %04x rxlen %d dev %d\n",
                         rc, status[0], rxlen, dev);
                if ((rc == 0) && (dev == ePLC_INTF_NACK))
                {
                    continue;
                }
                if (rc != 0)
                {
                    return -SBT_EUARTWRITEFAIL;
                }
                if (dev != ePLC_INTF_ACK)
                {
                    return -SBT_EMISSINGACK;
                }

                // by the time we get here, rc = 0 and the device is an ack...
            }
            if (rc || rxlen != 1 || dev != ePLC_INTF_ACK)
            {
                LOG_ERROR("Write-ack failed (%d, %d, %d)\n",
                         rc, rxlen, dev);
                return -SBT_EUARTWRITEFAIL;
            }
            else
            {
                switch (status[0])
                {
                case PLC_STATUS_OK:
                    return 0;
                case PLC_STATUS_FAIL:
                    return -SBT_EPLCERROR;
                case PLC_STATUS_BUSY:
                    return -SBT_EPLCBUSY;
                default:
                    return -SBT_EBADVALUE;
                }
            }
        }
    }
#endif
    return 0;
}

/*!
 * @details Write a packet to the UART, using the appropriate addresses and
 * optional sub-address
 *
 * @param packet - The raw packet to transmit.
 * @param length - The size of the raw packet in bytes.
 *
 * @return 0 for success,
 */
int32_t UartWriteL2(scooter_uart_handle_t hThis, const uint8_t *packet,
                  size_t length)
{
    uint8_t *cobs_in;
    uint8_t *cobs_out;
    uint16_t crc;
    int pre_cobs_length;
    int post_cobs_length;
    int cobs_length_needed;
    int32_t rc, dev;
    uint8_t status[SCOOTER_MAX_PACKET];
    size_t rxlen;

    if ((hThis->uart_fd == -1) || (length > SCOOTER_MAX_PACKET))
    {
        return -EINVAL;
    }

    //
    // Allocate room for payload plus CRC
    //
    pre_cobs_length = length + 2;
    cobs_in = alloca(pre_cobs_length);

    //
    // Copy the payload
    //
    memcpy(cobs_in, packet, length);

    //
    // Calculate the CRC
    //
    crc = crc16ccitt(cobs_in, length, 0xFFFF);

    //
    // Concatenate the CRC16 at the end of the payload
    //
    cobs_in[length + 0] = (uint8_t) (crc & 0xFF);
    cobs_in[length + 1] = (uint8_t) ((crc >> 8) & 0xFF);

    //
    // Now figure out the space need for the equivalent COBS encoded frame
    //
    cobs_length_needed = cobs_get_required_size(pre_cobs_length);
    cobs_out = alloca(cobs_length_needed);

    //
    // COBS frames start and end with 0x00
    //
    cobs_out[0] = 0;

    //
    // Do the encode, skipping over the leading 0
    //
    post_cobs_length = cobs_encode(cobs_in, pre_cobs_length, cobs_out + 1);

    //
    // And add the trailing 0
    //
    cobs_out[post_cobs_length + 1] = 0;

    if (hThis->uart_debug & TRACE_TX)
    {
        // uart_packet_dump(cobs_out, post_cobs_length);
        uart_packet_dump(cobs_in, pre_cobs_length);

    }

    //
    // Include the leading and trailing zero in the frame length
    //
    post_cobs_length += 2;

    LOG_VERBOSE_MED("\n");
    if (hThis->uart_debug & TRACE_TX)
    {
        // uart_packet_dump(cobs_out, post_cobs_length);
        uart_packet_dump(cobs_in, pre_cobs_length);

    }

    //
    // Transmit the payload.  One and done.  Nothing special after the write...
    //
    rc = write(hThis->uart_fd, cobs_out, post_cobs_length);
    if (rc != post_cobs_length)
    {
        LOG_WARNING("Unable to send entire payload\n",
                    );
        return -SBT_EUARTWRITEFAIL;
    }

    return 0;
}

int32_t UartRawWrite(scooter_uart_handle_t hThis, const uint8_t *packet,
                  size_t length, int32_t device)
{
    uint8_t *cobs_in;
    uint8_t *cobs_out;
    uint16_t crc;
    int pre_cobs_length;
    int post_cobs_length;
    int cobs_length_needed;
    int32_t rc, dev;
    useconds_t timeout_us;


    if ((hThis->uart_fd == -1) || (length > SCOOTER_MAX_PACKET))
    {
        return -EINVAL;
    }

    cobs_in = alloca(length + 1 + 1 + 2);

    cobs_in[0] = device;

    // For PLC we place status flags after the device
    if(device == ePLC_INTF_WRITE_TX)
    {
        pre_cobs_length = length + 4;
        cobs_in[1] = 0; // Write ACK not requested
        memcpy(cobs_in + 2, packet, length);

        crc = crc16ccitt(cobs_in, length + 2, 0xFFFF);

        cobs_in[length + 2] = (uint8_t) (crc & 0xFF);
        cobs_in[length + 3] = (uint8_t) ((crc >> 8) & 0xFF);
    }
    else
    {
        pre_cobs_length = length + 3;
        memcpy(cobs_in + 1, packet, length);

        crc = crc16ccitt(cobs_in, length + 1, 0xFFFF);

        cobs_in[length + 1] = (uint8_t) (crc & 0xFF);
        cobs_in[length + 2] = (uint8_t) ((crc >> 8) & 0xFF);
    }

    cobs_length_needed = cobs_get_required_size(pre_cobs_length);

    cobs_out = alloca(cobs_length_needed);
    cobs_out[0] = 0;
    post_cobs_length = cobs_encode(cobs_in, pre_cobs_length, cobs_out + 1);
    cobs_out[post_cobs_length + 1] = 0;

    post_cobs_length += 2;

    LOG_VERBOSE_MED("\n");
    if (hThis->uart_debug & TRACE_TX)
    {
        uart_packet_dump(cobs_out, post_cobs_length);
    }

    rc = write(hThis->uart_fd, cobs_out, post_cobs_length);
    if (rc != post_cobs_length)
    {
        LOG_WARNING("Unable to send entire payload\n", );
        return -SBT_EUARTWRITEFAIL;
    }

    /* Pace with the speed of PLC before returning
     * Bit rate 2400/s
     * UART transmit time ~ 10% of PLC
     * Timeout 1.1 * length * 8 bits in us.
     */
    // 1000000 * 8.8 = 8,800,000 / 2,400 = 88000 / 24
    length += 3; // account for PLC pre-amble
    timeout_us = (length * 88000) / 24;
    timeout_us += 5 * 1000; // account for USB framing
    rc = tsleep(timeout_us);
    if(rc)
    {
        return -EAGAIN;
    }

    return 0;
}

/*!
 * @details Send USB reset to the UART device.
 *
 */
int32_t UartReset(scooter_uart_handle_t hThis)
{
    if (!hThis)
    {
        return -EINVAL;
    }

    LOG_VERBOSE_MED("Uart USB reset\n");

    if (hThis->uart_fd != -1)
    {
#ifdef __gnu_linux__
      return(ioctl(hThis->uart_fd, USBDEVFS_RESET, 0));
#endif
    }
    return 0;
}


//! @}

#if defined SELFTEST
#include <stdio.h>
#include <string.h>

//! \addtogroup connection_tests
//! @{

#define TEST(desc, call, test, rc, line) \
    do { \
        printf("%40.40s:", desc); \
        rc = call; \
        if (rc test) { \
            printf(" [PASS]\n"); \
        } else { \
            printf(" [FAIL] (line %d)\n", line); \
        } \
    } while(0)

#define DEFAULT_DEVICE "/dev/ttyUSB0"

int main(int argc, char*argv[])
{
    scooter_uart_handle_t hConn;
    int32_t rc;
    uint32_t i, j;
    static char pBuf_low[COBS_MAX_BLOCK];
    static char pBuf_up[COBS_MAX_BLOCK];
    static uint8_t pBuf_resp[COBS_MAX_BLOCK];
    size_t len;
    uint8_t status;
    int32_t dev;
    int32_t txlen = 5, err_len = 0, err_crc = 0;
    char *pDev = DEFAULT_DEVICE;
    static char pTestStr[40];

    if((argc >= 2) && (0 == strcmp(argv[1], "-C")))
    {
        pDev = argv[2];
        fprintf(stderr, "Using device '%s'\n", pDev);
    }
    memset(pBuf_low, 0xab, COBS_MAX_BLOCK);
    memset(pBuf_up, 0xcd, COBS_MAX_BLOCK);
    memset(pBuf_resp, 0xef, COBS_MAX_BLOCK);

    TEST("Create Connection", UartConnectionNew(&hConn), >= 0, rc, __LINE__);
    TEST("Open Connection", UartDeviceOpen(hConn, pDev), >= 0, rc, __LINE__);
    TEST("Set Timeout", UartTimeoutReadSet(hConn, 500), >= 0, rc, __LINE__);
    TEST("Set debug level", UartDebugSet(hConn, 0x1), >= 0, rc, __LINE__);


    //
    // COBS packets can not be 1 or 3 bytes long
    //
    for(i = 1; i < COBS_MAX_BLOCK - 1; i++)
    {
        txlen = i;
        TEST("Set Timeout", UartTimeoutReadSet(hConn, 100 + (10 * i)), >= 0, rc, __LINE__);
        snprintf(pTestStr, 40, "UartWrite len = %d", i);
        for(j = 0; j < txlen; j++)
        {
            pBuf_low[j] = 'a' + (j % 26);
            pBuf_up[j]  = 'A' + (j % 26);
        }
        pBuf_low[j] = 0;
        pBuf_up[j] = 0;
        j++;

        snprintf(pTestStr, 40, "UartWrite len = %d\n", j);
        TEST(pTestStr, UartWrite(hConn, pBuf_low, j, ePLC_INTF_ECHO, -1), >= 0, rc, __LINE__);
        if (rc < 0) goto fail;

        TEST("UartFlush", UartFlush(hConn), >= 0, rc, __LINE__);

        snprintf(pTestStr, 40, "UartRead \n");
        TEST(pTestStr, UartRead(hConn, pBuf_resp, &len, &dev, &status), >= 0, rc, __LINE__);
        pBuf_resp[len] = 0;

        // if (rc < 0) goto fail;
        if (len != j)
        {
            err_len++;
            fprintf(stderr, "Unexpected response length (%d vs %ld)\n", i+1, len);
        }
        if (dev != ePLC_INTF_ECHO)
        {
            fprintf(stderr, "Unexpected responding device (%d)\n", dev);
        }
        else
        {
            if (strncmp(pBuf_resp, pBuf_up, i) != 0)
            {
                pBuf_resp[len] = 0;
                fprintf(stderr, "Response differs.  RXed: '%s'\n", pBuf_resp);
            }
        }
    }

    fprintf(stderr,"err_len = %d, g_errcrc = %d\n", err_len, g_errcrc);

    TEST("Closing connection", UartClose(hConn), >= 0, rc, __LINE__);
    TEST("Deleting connection", UartConnectionDelete(hConn), >= 0, rc, __LINE__);
    return(0);

    fail:
    TEST("Closing connection (fail-path)", UartClose(hConn), >= 0, rc, __LINE__);
    return(1);
}
#endif



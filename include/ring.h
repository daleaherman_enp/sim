/* Copyright (c) 2006-2008 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _ring_h_
#define _ring_h_

/** @file ring.h
 *  @brief Rings
 *
 *  This file @c ring.h provides the Rings structure used for some
 *  inter-level communications.
 *
 */

/** @page ringHPg Header: ring.h Rings

@section ringHName ring.h Rings Interface

    ring.h -- Rings Interface

@section ringHSynop ring.h SYNOPSIS

    This file provides the Rings interface.

@section ringHDesc ring.h DESCRIPTION

    This file, @c ring.h provides the Rings interface.

    This implementation is based on the power of two ring structure, similar
    to that defined in the pow2buf.h header. However this abstraction is
    designed to provide a bit more infrastructure and encapsulation than that
    simply provided by use of just pow2buf.h.

*/

#include "include/enphase.h"

/**
 * @defgroup ringHGroups ring Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup ringHDefines ring Header Defines
 */
/*@{*/

#define cRingNameLen                24
#define mRingBufsSz(_p2Sz,_recSz)   ((1 << (_p2Sz)) * (_recSz))

/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs -------------------------- */

/**
 * @defgroup ringHTypedefs ring Header Typedefs
 */
/*@{*/

typedef enum _eRingHandles
{
     cRingHandleTxAppMsg = 0,
     cRingHandleTxL2Frame,
     cRingHandleRxL2Frame,
     cRingNumberOfHandles
} eRingHandles;

typedef char                    tRingH;

/*! Defined @c tRingH, the ring handle type. */
typedef char                    tRingInt;
typedef unsigned char           tRingUInt;

typedef struct _tRingStats {
    int   maxDepth;           // max number of entries used at one time
    int   enqueues;           // count of buffers ever used
    int   timesFull;          // count of times ring fill detected
    int   timesNearFull;      // count of times when ring is slow to empty
    int   overflowAttempts;   // count of time when no entries are available
} tRingStats;

typedef struct _tRingDescriptor {
    tRingUInt   rbufP2Sz;
    tRingUInt   rbufSz;
    tRingUInt   rbufMask;
    u_int8_t    rIdx;

    tRingUInt   rbufWIdx;
    tRingUInt   rbufRIdx;
    u_int16_t   rbufRecSz;

    u_int8_t _keil_xdata       * rbuf;
} tRingDescriptor;

typedef struct _tRing {
    tRingDescriptor   r;
    u_int8_t          fill1;
    tRingStats        stats;
    u_int8_t          fill2;
} tRing;


/*@}*/  /* END of ring Header Typedef Group */

/* ------------------------------------ Extern Declarations ---------- */

/**
 * @defgroup ringHExtdata ring Header Extern Data Definitions
 */
/*@{*/

extern tRing _keil_xdata        rings[cRingNumberOfHandles];

/*@}*/  /* END of ring Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup ringHGlobdata ring Header Global Data Defined
 */
/*@{*/

/*@}*/  /* END of ring Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup ringHStatdata ring Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of ring Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup ringHProto ring Header Prototypes
 */
/*@{*/

/** ringCreate - Create and initialize a ring. */
extern void
ringCreate(u_int8_t                 ringIdx,
           tRingInt                 p2Sz,
           int                      recSz,
           u_int8_t _keil_xdata   * preallocatedRingBuf);

/** ringSizeRoom - Return size of room available. */
extern int
ringSizeRoom(tRingH         ringHandle);

/** ringIncWt - Increment write index. */
extern void
ringIncWt(tRingH            ringHandle);

/** ringIncRd - Increment read index. */
extern void
ringIncRd(tRingH            ringHandle);

/** ringRead - Return current read record address. */
extern void _keil_xdata *
ringRead(tRingH         ringHandle);

/** ringWrite - Return current write record address. */
extern void _keil_xdata *
ringWrite(tRingH            ringHandle);

/** ringInitModule - initialize ring module. */
extern int
ringInitModule(void);


/*@}*/  /* END of ring Header Prototypes Group */

/*@}*/  /* END ring Header Definitions and Declarations */


#endif /* _ring_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of ring.h */



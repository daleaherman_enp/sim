/*
 * MMap.hh
 *
 *  Created on: May 13, 2015
 *      Author: miles
 */

#ifndef MMAP_HH_
#define MMAP_HH_

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <string>

#include "sbt/log.h"

class MMap
{
private:
    int filedesc;
    int _length;
    void *pbuf;
    struct stat stat_buf;
    std::string _path;
    MMap();
    void _mmap(const std::string *p)
    {
        int rc;
        _path = *p;
        rc = stat(_path.c_str(), &stat_buf);
        if(rc == -1)
        {
            LOG_ERROR("Unable to stat file '%s'\n", _path.c_str());
            throw(-errno);
        }
        _length = stat_buf.st_size;

        filedesc = open(_path.c_str(), O_RDONLY);
        if(filedesc == -1)
        {
            LOG_ERROR("Unable to open file '%s'\n", _path.c_str());
            throw(-errno);
        }

        pbuf = mmap(NULL, _length, PROT_READ, MAP_PRIVATE, filedesc, 0);
        if(!pbuf)
        {
            LOG_ERROR("Unable to mmap() file\n");
            close(filedesc);
            throw(-errno);
        }
    }

public:
    MMap(std::string *p)
    {
        _path = *p;
        _mmap(p);
    }

    MMap(const char *p)
    {
        _path = std::string(p);
        _mmap(&_path);
    }

    ~MMap()
    {
        assert(pbuf);
        munmap(pbuf, _length);
        close(filedesc);
    }

    const char* path() { return _path.c_str(); }
    const void* buf() { return pbuf; }
    int length() { return _length; }
};





#endif /* MMAP_HH_ */

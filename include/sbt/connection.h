/*!****************************************************************************
* @file connection.h
*  A module for abstracting underlying comms connections
*
* @copyright
*  Copyright (c) 2014 SolarBridge Technologies.  All rights reserved.
*
* @created 20-Oct-2014
*
* @author M. Bintz <m.bintz@solarbridgetech.com>
*
****************************************************************************/

#ifndef CONNECTION_H_
#define CONNECTION_H_

#ifdef __cplusplus
extern "C" {
#endif

// This is the largest payload we can/will send or receive through sidecar
#define CONNECTION_MAX_PACKET       256

//! \addtogroup connection_api
//! @{

struct _connection;
typedef struct _connection *connection_handle_t;

typedef enum
{
    CONNECTION_TYPE_INVALID,
    CONNECTION_TYPE_UART,
    CONNECTION_TYPE_UDP,
    CONNECTION_TYPE_MIME
} eCONNECTION_TYPE;

#define CONNECTION_DEBUG_VERBOSE    TRACE_FLAG_VERBOSE
#define CONNECTION_DEBUG_INFO       TRACE_FLAG_INFO
#define CONNECTION_DEBUG_WARNING    TRACE_FLAG_WARNING
#define CONNECTION_DEBUG_ERROR      TRACE_FLAG_ERROR

typedef int32_t (*pfnConnCB_t)(void *pCookie, uint8_t *pBuf, const size_t len);

//! @brief Open the connection specified by pPath
extern int32_t ConnectionOpen(const char *pPath, connection_handle_t *pConn);

//! @brief Close the connection specified in the provided handle
extern int32_t ConnectionClose(connection_handle_t hConn);

//! @brief register a callback to be called if the client would like to
//! override the connection's default behavior
extern int32_t ConnectionRegisterCallback(connection_handle_t hConn,
                                       pfnConnCB_t pfn, void *pCookie,
                                       pfnConnCB_t *oldpfn, void **ppOldcookie);

//! @brief Set flags in the provided connection handle
extern int32_t ConnectionDebugSet(connection_handle_t hConn,
                                  uint32_t ui32Flags);

//! @brief set the timeout in ms to wait when reading a byte from the connection
extern int32_t ConnectionTimeoutReadSet(connection_handle_t hConn,
                                        uint32_t timeout);

//! @brief set the timeout in ms to wait when reading a byte from the connection
extern int32_t ConnectionTimeoutReadGet(connection_handle_t hConn,
                                        uint32_t *timeout);

//! @brief Flush out any pending characters
extern int32_t ConnectionFlush(connection_handle_t hConn);

//! @brief Read a packet from the connection
extern int32_t ConnectionRead(connection_handle_t hConn, uint8_t *packet,
                              size_t *length, int32_t *device, int32_t *lun);

//! @brief Write a packet to the connection
extern int32_t ConnectionWrite(connection_handle_t hConn, const uint8_t *packet,
                               size_t length, int32_t device, int32_t lun);

extern int32_t ConnectionType(connection_handle_t hConn);

//! @brief Write a packet to L2 w/ no added L3 processing
extern int32_t ConnectionWriteL2(connection_handle_t hConn,
                                const uint8_t *packet, size_t length);

//! @brief Read a packet from L2 w/ no added L3 processing
extern int32_t ConnectionReadL2(connection_handle_t hConn, uint8_t *packet,
                                size_t *length, uint32_t timeout);

//! @}


#ifdef __cplusplus
}
#endif

#endif /* CONNECTION_H_ */

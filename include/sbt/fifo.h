/*!
 * @copyright
 * Copyright (c) 2015, SunPower Corp.  All rights reserved.
 *
 * @file
 * fifo.c
 *
 * @date
 * Jun 30, 2015
 *
 * @author
 * Miles Bintz <mbintz@sunpower.com>
 */
#ifndef _FIFO_H_
#define _FIFO_H_

typedef struct sFIFO_t* hFifo;

extern int32_t fifoCreate(hFifo *fifo, uint32_t nMsgs, uint32_t msgSize);
extern int32_t fifoDestroy(hFifo fifo);
extern int32_t fifoAdd(hFifo fifo, void *pItem, uint32_t len);
extern int32_t fifoRemove(hFifo fifo, void *ppItem, size_t *pLen, int32_t timeoutms);
extern int32_t fifoFlush(hFifo fifo);

#endif /* _FIFO_H_ */

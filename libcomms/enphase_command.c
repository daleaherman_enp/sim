/*!
 * @copyright
 * Copyright 2018, SunPower Corporation
 * All rights reserved.
 *
 * @file enphase_command.c
 * 	Enphase protocol packet builders
 *
 * @date July 8, 2018
 * @author  Dale Herman
 */

#include <memory.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include "sbt/portable_endian.h"
#include "sbt/conversions.h"
#include "enphase/mcp_enphase.h"
#include "agf.h"
#include "mcp_sunpower.h"
#include "enphase/enphase_packet.h"
#include "enphase/enphase_command.h"

#include "sbt/errno.h"
#include "sbt/log.h"
#include "sbt/portable_endian.h"
#include <time.h>

#define ENPH_MODULE_ADDRESS_MASK 0x3FFF
#define ENPH_ADDRESSING_MODE_MASK 0xC000
#define ENPH_ADDRESS_MODE_UNICAST 0x0000           // L2 directed
#define ENPH_ADDRESS_MODE_BCAST_DIRECTED 0x4000    // L2 McastMod
#define ENPH_ADDRESS_MODE_BCAST_MODZERO 0x8000     // L2 BcastCoord
#define ENPH_ADDRESS_MODE_BCAST_DOMAINZERO 0xC000  // L2 BcastAll

#define V6_MDE_BITMAP_SIZE (64)
#pragma pack(push, 1)
typedef struct _tMcpAddressHdr
{
    uint8_t domainAddr[6];
    uint16_t modAddr;
} tMcpAddressHdr;
typedef struct
{
    uint8_t msg_id;
    uint8_t mde_bitmap[V6_MDE_BITMAP_SIZE];
} tPollCmdRequestV6;

#pragma pack(pop)


static uint8_t zeroDomain[6] = {0,0,0,0,0,0};


/*
 * Enphase message checksum code
 */
static uint16_t mcpUtilFletcher8BitCsum(uint8_t *m, int len)
{
    uint8_t a, b;
    int i;

    /* mcpUtilFletcher8BitCsum */

    a = b = 0;

    for(i = 0; i < len; i++)
    {
        a += *(m + i);
        b += a;
    }
    return (uint16_t)((a << 8) + b);

} /* END of mcpUtilFletcher8BitCsum */

static uint16_t mcpUtilMcpMsgCsum(tMcpMsgHdr *m)
{
    int csum;
    int len;

    /* mcpUtilMcpMsgCsum */

    len  = (be16toh(m->msgLen) & 0xFFFF) - 2;
    csum = mcpUtilFletcher8BitCsum((uint8_t *) &m->msgLen, len);

    return htobe16(csum);

} /* END of mcpUtilMcpMsgCsum */

const char *enphase_response_status_string(uint8_t s)
{
    static const char *enpRstStatusStrings[] = {
        "AGFConfig",  "AGFUpdated", "AGFChecked",      "AGFMatch", "AGFLoadErr",
        "AGFBadType", "UnexpRec",   "ConditionChange", 0};

    return enpRstStatusStrings[s];
}

/* This formats a search/discover/idall packet All unassociated, unsquelched
 * PCUs send their devInfo in a ~3second (up to 24 devices) window with some
 * automatic collision avoidance Assumes a header to the E_ASIC of
 * tMcpAddressHdr that is used to format the Enphase L2 header */
int32_t enphase_pack_poll_command_search(uint8_t *packet, uint8_t *pDomain,
                                         uint16_t logAddr)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr      = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpPollCmdTlv *pMcpPollCmd = (tMcpPollCmdTlv *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after

    tMcpPollCmdSendPollDataTlv *pPollCmdSendPollDataTlv =
        (tMcpPollCmdSendPollDataTlv *) &pMcpPollCmd
            ->tlvData[0];  // variable length Tlv Cmd Data

    if(pDomain)
    {
        memcpy(pMcpAddr->domainAddr, pDomain, 6);
        // setup Message header to E_ASIC to format L2 frame header -
    }
    else
    {
        memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    }

    // Unassociated will respond to DOMAIN ZERO broadcast
    pMcpAddr->modAddr = htobe16((logAddr & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);

    // setup the tMcpPollCmdTlv structure
    pMcpPollCmd->msgTime      = htobe32(time(0));
    pMcpPollCmd->pollReqFlags = htobe16(
        cMcpPollFlagDeviceInfo);  // only request this one
    pMcpPollCmd->sequenceNumber = 1;
    pMcpPollCmd->tlvCnt         = 1;  // Only one TLV command requested

    // setup the tMcpPollCmdSendPollDataTlv structure
    pPollCmdSendPollDataTlv->tlvHdr.length = htobe16(
        sizeof(tMcpPollCmdSendPollDataTlv));
    pPollCmdSendPollDataTlv->tlvHdr.type = cMcpPollDiscover;

    memset(&pPollCmdSendPollDataTlv->devSerialNum, 0,
           sizeof(pPollCmdSendPollDataTlv->devSerialNum));

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdPollCmdTlv;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpPollCmdTlv) +
        sizeof(tMcpPollCmdSendPollDataTlv) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;  // TODO: Does the E_ASIC MAC do this ???
    pMcpMsgHdr->ackdSeqNum = 1;  // TODO: what when where
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpPollCmdTlv) + sizeof(tMcpPollCmdSendPollDataTlv) -
            1);  // unused tlvData[1] ??
}
// send a squelch command to a single PCU. BC to it with the embedded
// SERIAL_NUMBER
int32_t enphase_pack_squelch(uint8_t *packet, tSerialNumber *devSerialNum,
                             uint16_t squelchTimeInSecs)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr

    tMcpAssociationTlv
        *pAssocHdr = (tMcpAssociationTlv *) &packet[sizeof(tMcpAddressHdr) +
                                                    sizeof(tMcpMsgHdr)];
    tMcpAssociationSquelchTlv
        *pMcpAssocSquelch = (tMcpAssociationSquelchTlv *) &pAssocHdr
                                ->tlvData[0];  // message DATA comes after
    tMcpAssociationSquelchData
        *pCmdAssocSquelchTlv = (tMcpAssociationSquelchData *) &pMcpAssocSquelch
                                   ->squelch;  // variable length Tlv Cmd Data

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DIRECTED);

    // setup the tMcpAssociationTlv structure
    pMcpAssocSquelch->tlvHdr.type   = cMcpAssocCmdSquelch;
    pMcpAssocSquelch->tlvHdr.length = htobe16(
        sizeof(tMcpAssociationSquelchData));
    pAssocHdr->tlvCnt = 1;

    // setup the tMcpAssociationSquelch structure
    pCmdAssocSquelchTlv->seconds = htobe16(squelchTimeInSecs);
    memcpy(&pCmdAssocSquelchTlv->devSerialNum, devSerialNum,
           sizeof(pCmdAssocSquelchTlv->devSerialNum));

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdAssociation;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpAssociationTlv) +
        sizeof(tMcpAssociationSquelchTlv) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpAssociationTlv) + sizeof(tMcpAssociationSquelchTlv) -
            1);  // unused tlvData[1] ??
}

// associate a PCU to a domain assigning a LA and a groupMask, unicast, matches
// devSerial and return devInfo
int32_t enphase_pack_v6_telemetry_1(uint8_t *packet, uint8_t *domainAddr,
                                    uint16_t modAddr)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tPollCmdRequestV6
        *pPollRqst = (tPollCmdRequestV6 *) &packet[sizeof(tMcpAddressHdr) +
                                                   4];  // the McpMsgHdr

    // setup Message header to E_ASIC to format L2 frame header -
    memcpy(pMcpAddr->domainAddr, domainAddr, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((modAddr & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_UNICAST);

    pPollRqst->msg_id = 50;
    memset(pPollRqst->mde_bitmap, 0, sizeof(pPollRqst->mde_bitmap));
    pPollRqst->mde_bitmap[1] = 0x40;

    return (sizeof(tMcpAddressHdr) +
            sizeof(tPollCmdRequestV6));  // unused tlvData[1] ??
}
// associate a PCU to a domain assigning a LA and a groupMask, unicast, matches
// devSerial and return devInfo
int32_t enphase_pack_association(uint8_t *packet, uint8_t *domainAddr,
                                 tSerialNumber *devSerialNum, uint16_t modAddr)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpAssociationTlv
        *pAssocHdr = (tMcpAssociationTlv *) &packet[sizeof(tMcpAddressHdr) +
                                                    sizeof(tMcpMsgHdr)];
    tMcpAssociationLoadTlv
        *pMcpAssoc = (tMcpAssociationLoadTlv *) &pAssocHdr
                         ->tlvData[0];  // message DATA comes after

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DIRECTED);

    // setup the tMcpAssociationTlv structure

    pMcpAssoc->tlvHdr.type   = cMcpAssocCmdLoad;
    pMcpAssoc->tlvHdr.length = htobe16(sizeof(tMcpAssociationLoadData));

    pAssocHdr->tlvCnt = 1;

    // setup the tMcpAssociationDiscover structure
    memcpy(&pMcpAssoc->load.domainAddr, domainAddr, cL2DomainAddrLen);
    memcpy(&pMcpAssoc->load.devSerialNum.byteArr[0], devSerialNum,
           sizeof(pMcpAssoc->load.devSerialNum.byteArr));
    pMcpAssoc->load.devSerialNum.fillb1 = 0;
    pMcpAssoc->load.devSerialNum.fillb2 = 0;
    pMcpAssoc->load.modAddr             = htobe16(modAddr);

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdAssociation;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpAssociationTlv) +
        sizeof(tMcpAssociationLoadTlv) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpAssociationTlv) + sizeof(tMcpAssociationLoadTlv) -
            1);  // unused tlvData[1] ??
}
int32_t enphase_pack_imginfo(uint8_t *packet, tMcpImageInfoMsg *pUGInfoMsg)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpImageInfoMsg
        *pImgInfo = (tMcpImageInfoMsg *) &packet[sizeof(tMcpAddressHdr) +
                                                 sizeof(tMcpMsgHdr)];

    memcpy(pImgInfo, pUGInfoMsg, sizeof(tMcpImageInfoMsg));

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdImgInfo;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen = htobe16(sizeof(tMcpMsgHdr) + sizeof(tMcpImageInfoMsg));
    pMcpMsgHdr->msgSeq = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpImageInfoMsg));  // unused tlvData[1] ??
}

int32_t enphase_pack_vrise_bc(uint8_t *packet, uint8_t *domainAddr, uint32_t groupMask, 
							  uint32_t mvRmsLL1, uint32_t mvRmsLL2, uint32_t mvRmsLL3)
{
	tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
		packet;  // beginning of packet has header just used by E_ASIC MAC layer
	tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
		tMcpAddressHdr)];  // the McpMsgHdr
	tMcpSysConfigTlv *pConfigTlv;
	tMcpSysConfigVriseData *pVriseData;
	tMcpSysConfigIe
		*pSysConfigIe = (tMcpSysConfigIe *) &packet[sizeof(tMcpAddressHdr) +
												 sizeof(tMcpMsgHdr)];
	memcpy(pMcpAddr->domainAddr, domainAddr, sizeof(pMcpAddr->domainAddr));
	pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
							 ENPH_ADDRESS_MODE_BCAST_DIRECTED);



	pSysConfigIe->grpMask = htobe32(groupMask);
	pSysConfigIe->ieHdr.version = cMspIeHdrVer_1;
	pSysConfigIe->ieHdr.rsvd_ie = 0;
	pSysConfigIe->ieHdr.len = htobe16(sizeof(tMcpSysConfigVriseData));
	pSysConfigIe->tlvCnt = 1;
	pConfigTlv = (tMcpSysConfigTlv *)&pSysConfigIe->tlvData[0];
	pVriseData = (tMcpSysConfigVriseData *)&pSysConfigIe->tlvData[sizeof(tMcpSysConfigTlv)];
	pVriseData->dataType = cMcpVriseDataTypeLL;
	pVriseData->phaseAcMv[0][0] = (mvRmsLL1 >> 16) & 0xFF;
	pVriseData->phaseAcMv[0][1] = (mvRmsLL1 >> 8) & 0xFF;
	pVriseData->phaseAcMv[0][2] = mvRmsLL1 & 0xFF;
	pVriseData->phaseAcMv[1][0] = (mvRmsLL2 >> 16) & 0xFF;
	pVriseData->phaseAcMv[1][1] = (mvRmsLL2 >> 8) & 0xFF;
	pVriseData->phaseAcMv[1][2] = mvRmsLL2 & 0xFF;
	pVriseData->phaseAcMv[2][0] = (mvRmsLL3 >> 16) & 0xFF;
	pVriseData->phaseAcMv[2][1] = (mvRmsLL3 >> 8) & 0xFF;
	pVriseData->phaseAcMv[2][2] = mvRmsLL3 & 0xFF;

	pConfigTlv->type = cMcpSysConfigVrise;
	pConfigTlv->length = sizeof(tMcpSysConfigVriseData);

	pMcpMsgHdr->msgId               = cMcpMsgIdSysConfig;
	pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionFour;
	pMcpMsgHdr->msgLen = htobe16(sizeof(tMcpMsgHdr) + sizeof(tMcpImageInfoMsg) + sizeof(tMcpSysConfigIe)  + sizeof(tMcpSysConfigTlv) + sizeof(tMcpSysConfigVriseData) - cMcpSysConfigDataSize);
	pMcpMsgHdr->msgSeq = 1;
	pMcpMsgHdr->ackdSeqNum = 1;
	pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
		(tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);


	return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) + sizeof(tMcpImageInfoMsg) + sizeof(tMcpSysConfigIe)  + sizeof(tMcpSysConfigTlv) + sizeof(tMcpSysConfigVriseData) - cMcpSysConfigDataSize);
}

int32_t enphase_pack_imgrspinfo(uint8_t *packet, tMcpImageRspMsg *pImgRspMsg)
{
    uint16_t messageLen;
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpImageInfoMsg
        *pImgInfo = (tMcpImageInfoMsg *) &packet[sizeof(tMcpAddressHdr) +
                                                 sizeof(tMcpMsgHdr)];

    messageLen = sizeof(tMcpImageRspMsg) + be16toh(pImgRspMsg->count);

    memcpy(pImgInfo, pImgRspMsg, messageLen);
    // printf("messageLen=%d\n", messageLen);

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdImgRsp;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(sizeof(tMcpMsgHdr) + messageLen);
    pMcpMsgHdr->msgSeq              = 1;
    pMcpMsgHdr->ackdSeqNum          = 1;
    pMcpMsgHdr->msgCsum             = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) + messageLen);
}
int32_t enphase_pack_clear_agf(uint8_t *packet, tSerialNumber *devSerialNum)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpMfgTlv *pMcpMfg =
        (tMcpMfgTlv *) &packet[sizeof(tMcpAddressHdr) +
                               sizeof(tMcpMsgHdr)];  // message DATA comes after

    tMcpMfgGenericData
        *pCmdStartData = (tMcpMfgGenericData *) &pMcpMfg
                             ->tlvData[0];  // variable length Tlv Cmd Data

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_UNICAST);

    // setup the tMcpMfgTlv structure
    memcpy(&pMcpMfg->devSerialNum, devSerialNum, sizeof(pMcpMfg->devSerialNum));
    pMcpMfg->tlvCnt = 1;

    // setup the tMcpMfgStartData structure
    pCmdStartData->tlvHdr.type   = cMcpMfgCmdClearAgfRecords;
    pCmdStartData->tlvHdr.length = htobe16(sizeof(tMcpMfgGenericData));

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdMfg;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpMfgTlv) + sizeof(tMcpMfgGenericData) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) + sizeof(tMcpMfgTlv) +
            sizeof(tMcpMfgGenericData) - 1);  // unused tlvData[1] ??
}

// Manuf command that either starts or stops power, stopping erases LTE
int32_t enphase_pack_start_stop_power_now(uint8_t *packet,
                                          tSerialNumber *devSerialNum,
                                          uint32_t bStart)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpMfgTlv *pMcpMfg =
        (tMcpMfgTlv *) &packet[sizeof(tMcpAddressHdr) +
                               sizeof(tMcpMsgHdr)];  // message DATA comes after

    tMcpMfgGenericData
        *pCmdStartData = (tMcpMfgGenericData *) &pMcpMfg
                             ->tlvData[0];  // variable length Tlv Cmd Data

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_UNICAST);

    // setup the tMcpMfgTlv structure
    memcpy(&pMcpMfg->devSerialNum, devSerialNum, sizeof(pMcpMfg->devSerialNum));
    pMcpMfg->tlvCnt = 1;

    // setup the tMcpMfgStartData structure
    pCmdStartData->tlvHdr.type   = bStart ? cMcpMfgCmdStart : cMcpMfgCmdStop;
    pCmdStartData->tlvHdr.length = htobe16(sizeof(tMcpMfgGenericData));

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdMfg;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpMfgTlv) + sizeof(tMcpMfgGenericData) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) + sizeof(tMcpMfgTlv) +
            sizeof(tMcpMfgGenericData) - 1);  // unused tlvData[1] ??
}
// issue a request to send poll data to a single PCU using
// unicast/domain/logical address can be one or more of cMcpPollFlagDeviceInfo,
// cMcpPollFlagImageInfo, cMcpPollFlagAgfInfo, cMcpPollFlagInterval,
// cMcpPollFlagCondition, cMcpPollFlagCounters, cMcpPollFlagPhaseAware, etc
int32_t enphase_pack_poll_request_data_tlv(uint8_t *packet, uint8_t *domainAddr,
                                           uint16_t logicalAddr,
                                           uint16_t reqFlags,
                                           tSerialNumber *devSerialNum, uint8_t addressingMode)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr      = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpPollCmdTlv *pMcpPollCmd = (tMcpPollCmdTlv *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after

    tMcpPollCmdSendPollDataTlv *pPollCmdSendPollDataTlv =
        (tMcpPollCmdSendPollDataTlv *) &pMcpPollCmd
            ->tlvData[0];  // variable length Tlv Cmd Data

	uint16_t eMode = 0;
	uint8_t *pDomain = zeroDomain;
	uint16_t useLA = 0;
	switch (addressingMode) {
	case cAddressModeTypeDirected:
		eMode = ENPH_ADDRESS_MODE_BCAST_DIRECTED;
		memcpy(&pPollCmdSendPollDataTlv->devSerialNum, devSerialNum,
			   sizeof(pPollCmdSendPollDataTlv->devSerialNum));

		break;
	case cAddressModeTypeUnicast:
		eMode = ENPH_ADDRESS_MODE_UNICAST;
		pDomain = domainAddr;
		useLA = logicalAddr;
		break;
	case cAddressModeTypeModzero:
		eMode = ENPH_ADDRESS_MODE_BCAST_MODZERO;
		pDomain = domainAddr;
		break;
	case cAddressModeTypeDomainZero:
		eMode = ENPH_ADDRESS_MODE_BCAST_DOMAINZERO;
		break;
	default:
		break;
	}

   if (pDomain) {
	   memcpy(pMcpAddr->domainAddr, pDomain, sizeof(pMcpAddr->domainAddr));
	}
	else {
		printf("YIKES\n");
		return 0;
	}
	pMcpAddr->modAddr = htobe16((useLA & ENPH_MODULE_ADDRESS_MASK) | eMode);  //
    

    // setup the tMcpPollCmdTlv structure
    pMcpPollCmd->msgTime        = htobe32(time(0));
    pMcpPollCmd->pollReqFlags   = htobe16(reqFlags);  // only request this one
    pMcpPollCmd->sequenceNumber = 1;
    pMcpPollCmd->tlvCnt         = 1;

    // setup the tMcpPollCmdSendPollDataTlv structure
    pPollCmdSendPollDataTlv->tlvHdr.length = htobe16(
        sizeof(tMcpPollCmdSendPollDataTlv) - sizeof(tMcpTlvHeader));
    pPollCmdSendPollDataTlv->tlvHdr.type = cMcpPollSendPollData;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdPollCmdTlv;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpPollCmdTlv) +
        sizeof(tMcpPollCmdSendPollDataTlv) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

#if 0
    {
        volatile int x = 0;
        x = sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) + sizeof(tMcpPollCmdTlv) + sizeof(tMcpPollCmdSendPollDataTlv) - 1;
        fprintf(stderr, "x = %d\n", x);
    }
#endif
    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpPollCmdTlv) + sizeof(tMcpPollCmdSendPollDataTlv) -
            1);  // unused tlvData[1] ??
}
// Mfg command to load module PV data during EOL
int32_t enphase_pack_load_pv_data(uint8_t *packet, tSerialNumber *devSerialNum,
                                  tEEPROM_PV_MODULE_ENTRY *moduleData)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpMfgTlv *pMcpMfg =
        (tMcpMfgTlv *) &packet[sizeof(tMcpAddressHdr) +
                               sizeof(tMcpMsgHdr)];  // message DATA comes after

    tMcpMfgLoadData
        *pCmdLoadData = (tMcpMfgLoadData *) &pMcpMfg
                            ->tlvData[0];  // variable length Tlv Cmd Data

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DIRECTED);

    // setup the tMcpMfgTlv structure
    memcpy(&pMcpMfg->devSerialNum, devSerialNum, sizeof(pMcpMfg->devSerialNum));
    pMcpMfg->tlvCnt = 1;

    // setup the tMcpMfgStartData structure
    pCmdLoadData->tlvHdr.type   = cMcpMfgCmdLoad;
    pCmdLoadData->tlvHdr.length = htobe16(sizeof(tMcpMfgLoadData));
    memcpy(&pCmdLoadData->moduleEntry, moduleData,
           sizeof(pCmdLoadData->moduleEntry));

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdMfg;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpMfgTlv) + sizeof(tMcpMfgLoadData) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum((tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)]);  // TODO: What is the algorithm Do I need to fill out

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) + sizeof(tMcpMfgTlv) +
            sizeof(tMcpMfgLoadData) - 1);  // unused tlvData[1] ??
}

// unpack a tMcpPollRspDevInfoTlv response.
// What will the LIB actually received from the E_ASIC with a response???
// Does the LIB/MIME need to the domainAddr or the modAddr if used???
// for now, assume we start with a tMcpMsgHdr
// any checking of the data packet (CRC/CHKSUM) will have been done higher up...
// we are only doing one TLV per request for now
int32_t enphase_unpack_deviceInfo_tlv(uint8_t *packet, uint32_t *msgTime,
                                      uint8_t *status,
                                      tMcpPollRspDevInfo *devInfoP)
{
    uint16_t tmpVar;
    tMcpPollRspTlv
        *pPollRspTlv = (tMcpPollRspTlv *) &packet[sizeof(tMcpMsgHdr)];
    tMcpPollRspDevInfoTlv *pRspDevInfoTlv = NULL;

    /* While there remain entries in the TLV, look at the header of the current
     * entry. If the type of the entry is cMcpPollDataDeviceInfo, assign the
     * entry to pRspDevInfoTlv. Otherwise, advance the entry pointer by
     * sizeof(tlvHdr) + tlvHdr.length bytes. */
    uint8_t *tlvData = pPollRspTlv->tlvData;
    for(int i = 0; i < pPollRspTlv->tlvCnt; i++)
    {
        tMcpPollRspDevInfoTlv *tlvEntry = (tMcpPollRspDevInfoTlv *) tlvData;

        if(tlvEntry == NULL)
        {
            /* tlvCnt was wrong; we can't recover here. */
            return -1;
        }
        if(tlvEntry->tlvHdr.type == cMcpPollDataDeviceInfo)
        {
            pRspDevInfoTlv = tlvEntry;
            break;
        }
        else
        {
            tlvData += sizeof(tMcpTlvHeader) + be16toh(tlvEntry->tlvHdr.length);
        }
    }

    if(pRspDevInfoTlv == NULL)
    {
        return -1;
    }

    // first copy everything then flip whats needed
    memcpy(devInfoP, &pRspDevInfoTlv->devInfo, sizeof(tMcpPollRspDevInfo));
    devInfoP->modAddr  = be16toh(devInfoP->modAddr);
    devInfoP->grpMask  = be32toh(devInfoP->grpMask);
    devInfoP->rxEmuSsi = be16toh(devInfoP->rxEmuSsi);
    devInfoP->flags    = be16toh(devInfoP->flags);
    *msgTime           = be32toh(pPollRspTlv->msgTime);
    tmpVar             = be16toh(pPollRspTlv->pollRspStatus);
    status[1]          = tmpVar >> 8;
    status[0]          = tmpVar & 0xff;

    // TODO: all other fields are BCD type data, assume we don't need to change
    // byte order? Use library to convert as required
    return 0;
}

int32_t enphase_unpack_imageInfo_tlv(uint8_t *packet, uint32_t *msgTime,
                                     uint8_t *status, tImageMetaData *imgInfoP)
{
    tMcpPollRspTlv
        *pPollRspTlv = (tMcpPollRspTlv *) &packet[sizeof(tMcpMsgHdr)];
    tMcpPollRspImageInfoTlv *pRspImgInfoTlv = NULL;

    uint8_t *tlvData = pPollRspTlv->tlvData;
    for(int i = 0; i < pPollRspTlv->tlvCnt; i++)
    {
        tMcpPollRspImageInfoTlv *tlvEntry = (tMcpPollRspImageInfoTlv *) tlvData;

        if(tlvEntry == NULL)
        {
            /* tlvCnt was wrong; we can't recover here. */
            return -1;
        }
        if(tlvEntry->tlvHdr.type == cMcpPollDataImageInfo)
        {
            pRspImgInfoTlv = tlvEntry;
            break;
        }
        else
        {
            tlvData += sizeof(tMcpTlvHeader) + be16toh(tlvEntry->tlvHdr.length);
        }
    }

    if(pRspImgInfoTlv == NULL)
    {
        return -1;
    }

    memcpy(imgInfoP, &pRspImgInfoTlv->imd, sizeof(tImageMetaData));

    return 0;
}
int32_t enphase_unpack_skip_counters(uint8_t *packet, tMcpDebugSkipDataHeron *pSkip)
{
	tMcpDebugSkipDataHeron *pMcpDebugSkip = (tMcpDebugSkipDataHeron *) &packet[sizeof(tMcpMsgHdr)];
	if ((pMcpDebugSkip->debugReq & 0x7F) != cMcpDebugReqDbgSkip)
	{
		return -1;
	}
	memcpy(pSkip, pMcpDebugSkip, sizeof(tMcpDebugSkipDataHeron));
	pSkip->islandCntr = be16toh(pSkip->islandCntr);
	pSkip->suspensionDbgCntr = be16toh(pSkip->suspensionDbgCntr);
	return 0;
}
// unpack a tMcpPollRspAgfRecListTlv response.
// What will the LIB actually received from the E_ASIC with a response???
// Does the LIB/MIME need to the domainAddr or the modAddr if used???
// for now, assume we start with a tMcpMsgHdr
// any checking of the data packet (CRC/CHKSUM) will have been done higher up...
// we are only doing one TLV per request for now
int32_t enphase_unpack_agfInfo_tlv(uint8_t *packet, uint32_t *msgTime,
                                   uint8_t *status,
                                   tMcpAgfLoadedRecStatus *agfRecStatus)
{
    uint16_t tmpVar;
    uint8_t length         = 0;
    size_t i               = 0;
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[0];  // the McpMsgHdr
    tMcpPollRspTlv
        *pPollRspTlv = (tMcpPollRspTlv *) &packet[sizeof(tMcpMsgHdr)];
    tMcpPollRspAgfRecListTlv *pRspAgfRecListTlv =
        (tMcpPollRspAgfRecListTlv *) &pPollRspTlv->tlvData[0];

    // should only have one response
    if((pMcpMsgHdr->msgId != cMcpMsgIdPollRspTlv) ||
       (be16toh((pPollRspTlv->pollRspFlags)) != cMcpPollFlagAgfInfo) ||
       (pRspAgfRecListTlv->tlvHdr.type != cMcpPollDataAgfInfo))
    {
        return -1;
    }

    length = be16toh(pRspAgfRecListTlv->tlvHdr.length) /
             sizeof(tMcpAgfLoadedRecStatus);
    // printf("enphase_unpack_agfInfo_tlv: length = %d\n", length);

    *msgTime  = be32toh(pPollRspTlv->msgTime);
    tmpVar    = be16toh(pPollRspTlv->pollRspStatus);
    status[1] = tmpVar >> 8;
    status[0] = tmpVar & 0xff;

    for(i = 0; i < length; i++)
    {
        // first copy everything then flip whats needed
        // memcpy((uint8_t *)&agfRecStatus[i], (uint8_t
        // *)&pRspAgfRecListTlv->recList[i], sizeof(tMcpAgfLoadedRecStatus));
        agfRecStatus[i].recordHeader.type = pRspAgfRecListTlv->recList[i]
                                                .recordHeader.type;
        agfRecStatus[i].recordHeader.version = pRspAgfRecListTlv->recList[i]
                                                   .recordHeader.version;
        agfRecStatus[i].recordHeader.key = be16toh(
            pRspAgfRecListTlv->recList[i].recordHeader.key);
        agfRecStatus[i].recordHeader.recCsum = be16toh(
            pRspAgfRecListTlv->recList[i].recordHeader.recCsum);
        agfRecStatus[i].recordHeader.recLength = be16toh(
            pRspAgfRecListTlv->recList[i].recordHeader.recLength);
        agfRecStatus[i].recStatus = be16toh(
            pRspAgfRecListTlv->recList[i].recStatus);
    }

    return length;
}

// Unpack interval PollRspTlv
int32_t enphase_unpack_interval_data_tlv(uint8_t *packet, uint32_t *msgTime,
                                         uint8_t *status,
                                         tMcpPcuPwrIntervalData *intervalDataP,
                                         tIntervalLongData *intervalLongP)
{
    uint16_t tmpVar;
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[0];  // the McpMsgHdr
    tMcpPollRspTlv
        *pPollRspTlv = (tMcpPollRspTlv *) &packet[sizeof(tMcpMsgHdr)];
    tMcpPollRspIntervalTlv
        *pRspIntervalTlv = (tMcpPollRspIntervalTlv *) &pPollRspTlv->tlvData[0];

    // check response, we are only doing one TLV per for now
    if((pMcpMsgHdr->msgId != cMcpMsgIdPollRspTlv) ||
       (be16toh((pPollRspTlv->pollRspFlags)) != cMcpPollFlagInterval) ||
       (pRspIntervalTlv->tlvHdr.type != cMcpPollDataInterval))
    {
        return -1;
    }

    *msgTime  = be32toh(pPollRspTlv->msgTime);
    tmpVar    = be16toh(pPollRspTlv->pollRspStatus);
    status[1] = tmpVar >> 8;
    status[0] = tmpVar & 0xff;

    intervalDataP->temperature = pRspIntervalTlv->intervalData.temperature;
    intervalDataP->flags       = pRspIntervalTlv->intervalData.flags;
    intervalDataP->pwr.pwrConvErrSecs = be16toh(
        pRspIntervalTlv->intervalData.pwr.pwrConvErrSecs);
    intervalDataP->pwr.pwrConvMaxErrCycles = be16toh(
        pRspIntervalTlv->intervalData.pwr.pwrConvMaxErrCycles);
    intervalDataP->pwr.acVoltageINmV = be32toh(
        pRspIntervalTlv->intervalData.pwr.acVoltageINmV);
    intervalDataP->pwr.acFrequencyINClkCycles = be32toh(
        pRspIntervalTlv->intervalData.pwr.acFrequencyINClkCycles);
    intervalDataP->pwr.dcVoltageINmV = be32toh(
        pRspIntervalTlv->intervalData.pwr.dcVoltageINmV);
    intervalDataP->pwr.dcCurrentINmA = be32toh(
        pRspIntervalTlv->intervalData.pwr.dcCurrentINmA);

    // printf( "**** %04x%04x%04x\n",
    // pRspIntervalTlv->intervalData.pwr.producedMilliJoules[0],
    // pRspIntervalTlv->intervalData.pwr.producedMilliJoules[1],
    // pRspIntervalTlv->intervalData.pwr.producedMilliJoules[2] );
    intervalLongP->joulesProduced = be16toh(
        pRspIntervalTlv->intervalData.pwr.producedMilliJoules[0]);
    intervalLongP->joulesProduced = (intervalLongP->joulesProduced << 16) +
                                    be16toh(pRspIntervalTlv->intervalData.pwr
                                                .producedMilliJoules[1]);
    intervalLongP->joulesProduced = (intervalLongP->joulesProduced << 16) +
                                    be16toh(pRspIntervalTlv->intervalData.pwr
                                                .producedMilliJoules[2]);

    intervalLongP->joulesUsed = be16toh(
        pRspIntervalTlv->intervalData.pwr.usedMilliJoules[0]);
    intervalLongP->joulesUsed = (intervalLongP->joulesUsed << 16) +
                                be16toh(pRspIntervalTlv->intervalData.pwr
                                            .usedMilliJoules[1]);
    intervalLongP->joulesUsed = (intervalLongP->joulesUsed << 16) +
                                be16toh(pRspIntervalTlv->intervalData.pwr
                                            .usedMilliJoules[2]);

    intervalLongP->sumLeadingVAr = be16toh(
        pRspIntervalTlv->intervalData.pwr.sumMilliLeadingVAr[0]);
    intervalLongP->sumLeadingVAr = (intervalLongP->sumLeadingVAr << 16) +
                                   be16toh(pRspIntervalTlv->intervalData.pwr
                                               .sumMilliLeadingVAr[1]);
    intervalLongP->sumLeadingVAr = (intervalLongP->sumLeadingVAr << 16) +
                                   be16toh(pRspIntervalTlv->intervalData.pwr
                                               .sumMilliLeadingVAr[2]);

    intervalLongP->sumLaggingVAr = be16toh(
        pRspIntervalTlv->intervalData.pwr.sumMilliLaggingVAr[0]);
    intervalLongP->sumLaggingVAr = (intervalLongP->sumLaggingVAr << 16) +
                                   be16toh(pRspIntervalTlv->intervalData.pwr
                                               .sumMilliLaggingVAr[1]);
    intervalLongP->sumLaggingVAr = (intervalLongP->sumLaggingVAr << 16) +
                                   be16toh(pRspIntervalTlv->intervalData.pwr
                                               .sumMilliLaggingVAr[2]);

    intervalDataP->onTime = be32toh(pRspIntervalTlv->intervalData.onTime);
    return 0;
}
// Unpack counter data poll response
int32_t enphase_unpack_counter_data_tlv(uint8_t *packet, uint32_t *msgTime,
                                        uint8_t *status,
                                        tMcpCounterData *counterDataP)
{
    uint16_t tmpVar;
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[0];  // the McpMsgHdr
    tMcpPollRspTlv
        *pPollRspTlv = (tMcpPollRspTlv *) &packet[sizeof(tMcpMsgHdr)];
    tMcpPollRspCounterTlv
        *pRspCounterTlv = (tMcpPollRspCounterTlv *) &pPollRspTlv->tlvData[0];

    uint16_t *pWordIn, *pWordOut;
    uint32_t *pLongIn, *pLongOut;
    int i;
    // check response, we are only doing one TLV per for now
    if((pMcpMsgHdr->msgId != cMcpMsgIdPollRspTlv) ||
       (be16toh((pPollRspTlv->pollRspFlags)) != cMcpPollFlagCounters) ||
       (pRspCounterTlv->tlvHdr.type != cMcpPollDataCounters))
    {
        return -1;
    }

    *msgTime  = be32toh(pPollRspTlv->msgTime);
    tmpVar    = be16toh(pPollRspTlv->pollRspStatus);
    status[1] = tmpVar >> 8;
    status[0] = tmpVar & 0xff;

    pWordIn  = (uint16_t *) &pRspCounterTlv->counters.ssiMeasures;
    pWordOut = (uint16_t *) &counterDataP->ssiMeasures;
    // first convert all the 16 bit values
    for(i = 0; i < (sizeof(counterDataP->ssiMeasures) / sizeof(uint16_t)); i++)
    {
        *pWordOut = be16toh(*pWordIn);
        ++pWordIn;
        ++pWordOut;
    }
    pWordIn  = (uint16_t *) &pRspCounterTlv->counters.frameCounters;
    pWordOut = (uint16_t *) &counterDataP->frameCounters;
    for(i = 0; i < (sizeof(counterDataP->frameCounters) / sizeof(uint16_t));
        i++)
    {
        *pWordOut = be16toh(*pWordIn);
        ++pWordIn;
        ++pWordOut;
    }
    pWordIn  = (uint16_t *) &pRspCounterTlv->counters.msgCounter;
    pWordOut = (uint16_t *) &counterDataP->msgCounter;
    for(i = 0; i < (sizeof(counterDataP->msgCounter) / sizeof(uint16_t)); i++)
    {
        *pWordOut = be16toh(*pWordIn);
        ++pWordIn;
        ++pWordOut;
    }
    pLongIn  = (uint32_t *) &pRspCounterTlv->counters.modemCounters;
    pLongOut = (uint32_t *) &counterDataP->modemCounters;
    for(i = 0; i < 5; i++)
    {
        *pLongOut = be32toh(*pLongIn);
        ++pLongIn;
        ++pLongOut;
    }
    pWordIn = (uint16_t *) &pRspCounterTlv->counters.modemCounters
                  .cryptKey0PktCnt;
    pWordOut = (uint16_t *) &counterDataP->modemCounters.cryptKey0PktCnt;
    for(i = 0; i < 14; i++)
    {
        *pWordOut = be16toh(*pWordIn);
        ++pWordIn;
        ++pWordOut;
    }

    return 0;
}

int32_t enphase_unpack_conditionData_tlv(uint8_t *packet, uint32_t *msgTime,
                                         uint8_t *status,
                                         tMcpConditionData *conditionDataP)
{
    uint16_t tmpVar;
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[0];  // the McpMsgHdr
    tMcpPollRspTlv
        *pPollRspTlv = (tMcpPollRspTlv *) &packet[sizeof(tMcpMsgHdr)];
    tMcpPollRspConditionTlv
        *pRspCondDataTlv = (tMcpPollRspConditionTlv *) &pPollRspTlv->tlvData[0];

    // should only have one response
    if((pMcpMsgHdr->msgId != cMcpMsgIdPollRspTlv) ||
       (be16toh((pPollRspTlv->pollRspFlags)) != cMcpPollFlagCondition) ||
       (pRspCondDataTlv->tlvHdr.type != cMcpPollDataCondition))
    {
        return -1;
    }

    // first copy everything then flip whats needed
    memcpy(conditionDataP, &(pRspCondDataTlv->conditionData),
           sizeof(tMcpConditionData));
    conditionDataP->lastMsgTime          = be32toh(conditionDataP->lastMsgTime);
    conditionDataP->latchedConditions[0] = be32toh(
        conditionDataP->latchedConditions[0]);
    conditionDataP->latchedConditions[1] = be32toh(
        conditionDataP->latchedConditions[1]);
    conditionDataP
        ->unlatchedConditions = be32toh(conditionDataP->unlatchedConditions) &
                                C_MCP_PCU_UNLATCHED_FLAG_VALID_MASK;
    *msgTime  = be32toh(pPollRspTlv->msgTime);
    tmpVar    = be16toh(pPollRspTlv->pollRspStatus);
    status[1] = tmpVar >> 8;
    status[0] = tmpVar & 0xff;
    return 0;
}

int32_t enphase_pack_set_groupMask(uint8_t *packet, uint8_t *domainAddr,
                                   uint16_t logicalAddr,
                                   tSerialNumber *devSerialNum,
                                   uint32_t groupMask)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr      = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpPollCmdTlv *pMcpPollCmd = (tMcpPollCmdTlv *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after

    tMcpPollCmdSendGroupMaskTlv *pPollCmdSendGroupDataTlv =
        (tMcpPollCmdSendGroupMaskTlv *) &pMcpPollCmd
            ->tlvData[0];  // variable length Tlv Cmd Data

#if 0
   if (devSerialNum == NULL)
   {
      memcpy(pMcpAddr->domainAddr, domainAddr, sizeof(pMcpAddr->domainAddr));
      pMcpAddr->modAddr = htobe16((logicalAddr & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST);
      memset(&pPollCmdSendGroupDataTlv->devSerialNum, 0, sizeof(pPollCmdSendGroupDataTlv->devSerialNum));
   }
   else
   {
      memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
      pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST); //
      memcpy(&pPollCmdSendGroupDataTlv->devSerialNum, devSerialNum, sizeof(pPollCmdSendGroupDataTlv->devSerialNum));
   }
#else

    memcpy(pMcpAddr->domainAddr, domainAddr, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((logicalAddr & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_UNICAST);
    memset(&pPollCmdSendGroupDataTlv->devSerialNum, 0,
           sizeof(pPollCmdSendGroupDataTlv->devSerialNum));

#endif

    // setup the tMcpPollCmdTlv structure
    pMcpPollCmd->msgTime      = htobe32(time(0));
    pMcpPollCmd->pollReqFlags = htobe16(
        cMcpPollFlagDeviceInfo);  // only request this one
    pMcpPollCmd->sequenceNumber = 1;
    pMcpPollCmd->tlvCnt         = 1;

    // setup the tMcpPollCmdSendGroupMaskTlv structure
    pPollCmdSendGroupDataTlv->tlvHdr.length = htobe16(
        sizeof(tMcpPollCmdSendGroupMaskTlv) - sizeof(tMcpTlvHeader));
    pPollCmdSendGroupDataTlv->tlvHdr.type = cMcpPollSetGroupMask;
    pPollCmdSendGroupDataTlv->grpMask     = htobe32(groupMask);

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdPollCmdTlv;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen = htobe16(sizeof(tMcpMsgHdr) + sizeof(tMcpPollCmdTlv) +
                                 sizeof(tMcpPollCmdSendGroupMaskTlv) - 1);
    pMcpMsgHdr->msgSeq = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpPollCmdTlv) + sizeof(tMcpPollCmdSendGroupMaskTlv) - 1);
}

int32_t enphase_pack_unassociate_all(uint8_t *packet)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr

    tMcpAssociationTlv
        *pAssocHdr = (tMcpAssociationTlv *) &packet[sizeof(tMcpAddressHdr) +
                                                    sizeof(tMcpMsgHdr)];
    tMcpAssociationForgetAllTlv
        *pMcpAssocForgetAll = (tMcpAssociationForgetAllTlv *) &pAssocHdr
                                  ->tlvData[0];  // message DATA comes after

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);

    // setup the tMcpAssociationTlv structure
    pMcpAssocForgetAll->tlvHdr.type   = cMcpAssocCmdForgetAll;
    pMcpAssocForgetAll->tlvHdr.length = 0;  // No data
    pAssocHdr->tlvCnt                 = 1;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdAssociation;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpAssociationTlv) +
        sizeof(tMcpAssociationForgetAllTlv) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpAssociationTlv) + sizeof(tMcpAssociationForgetAllTlv) -
            1);  // unused tlvData[1] ??
}
int32_t enphase_pack_unassociate_specific(uint8_t *packet, uint8_t *domain)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr

    tMcpAssociationTlv
        *pAssocHdr = (tMcpAssociationTlv *) &packet[sizeof(tMcpAddressHdr) +
                                                    sizeof(tMcpMsgHdr)];
    tMcpAssociationForgetTlv
        *pMcpAssocForgetDataTlv = (tMcpAssociationForgetTlv *) &pAssocHdr
                                      ->tlvData[0];  // message DATA comes after

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);

    // setup the tMcpAssociationTlv structure
    pMcpAssocForgetDataTlv->tlvHdr.type   = cMcpAssocCmdForgetSpecific;
    pMcpAssocForgetDataTlv->tlvHdr.length = sizeof(
        tMcpAssociationForgetData);  // No data
    pAssocHdr->tlvCnt = 1;
    memcpy(pMcpAssocForgetDataTlv->forget.domainAddr, domain,
           sizeof(pMcpAssocForgetDataTlv->forget.domainAddr));

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdAssociation;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpAssociationTlv) +
        sizeof(tMcpAssociationForgetTlv) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpAssociationTlv) + sizeof(tMcpAssociationForgetTlv) -
            1);  // unused tlvData[1] ??
}

int32_t enphase_pack_tlv_agf(uint8_t *packet, uint8_t *domainAddr,
                             uint16_t logicalAddress, uint32_t groupMask,
                             tMcpAgfRecord *pAgfRecord)
{

	static int ack = 0;

	tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr      = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpAgfRecords *pAgfRecords = (tMcpAgfRecords *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after
    tMcpAgfRecord *pAgfRecordData = (tMcpAgfRecord *) &pAgfRecords->value[0];

    if(domainAddr == NULL)
    {
        // setup Message header to E_ASIC to format L2 frame header -
        memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
        // Unassociated will respond to DOMAIN ZERO broadcast
        pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                    ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);  //
    }
    else
    {
        memcpy(pMcpAddr->domainAddr, domainAddr, sizeof(pMcpAddr->domainAddr));
        pMcpAddr->modAddr = htobe16(
            (logicalAddress & ENPH_MODULE_ADDRESS_MASK) |
            ENPH_ADDRESS_MODE_UNICAST);  //
    }

    pAgfRecords->count   = 1;
    pAgfRecords->grpMask = htobe32(groupMask);
    pAgfRecords->msgTime = htobe32(time(0));

    pAgfRecordData->recordHeader.type    = pAgfRecord->recordHeader.type;
    pAgfRecordData->recordHeader.version = pAgfRecord->recordHeader.version;
    pAgfRecordData->recordHeader.key = htobe16(pAgfRecord->recordHeader.key);
    pAgfRecordData->recordHeader.recLength = htobe16(
        pAgfRecord->recordHeader.recLength);

    memcpy(pAgfRecordData->recordData, pAgfRecord->recordData,
           pAgfRecord->recordHeader.recLength);
    pAgfRecordData->recordHeader.recCsum = htobe16(
        pAgfRecord->recordHeader.recCsum);

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdAgfRecords;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpAgfRecords) + sizeof(tMcpAgfRecHeader) +
        pAgfRecord->recordHeader.recLength -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = ack;
    pMcpMsgHdr->ackdSeqNum = ack++;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpAgfRecords) + sizeof(tMcpAgfRecHeader) +
            pAgfRecord->recordHeader.recLength - 1);  // unused tlvData[1] ??
}

int32_t enphase_pack_exp_tlv_agf(uint8_t *packet, uint8_t *domainAddr,
                                 uint16_t logicalAddress, uint32_t groupMask,
                                 tMcpAgfRecHeader *pAgfRecordHeaders)
{
    size_t i                 = 0;
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr                  = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpAgfExpRecordsTlv *pAgfExpRecordsTlv = (tMcpAgfExpRecordsTlv *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after
    tMcpAgfRecHeader *pAgfRecordHeader = (tMcpAgfRecHeader *) &pAgfExpRecordsTlv
                                             ->value[0];

    if(domainAddr == NULL)
    {
        // setup Message header to E_ASIC to format L2 frame header -
        memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
        // Unassociated will respond to DOMAIN ZERO broadcast
        pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                    ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);  //
    }
    else
    {
        memcpy(pMcpAddr->domainAddr, domainAddr, sizeof(pMcpAddr->domainAddr));
        pMcpAddr->modAddr = htobe16(
            (logicalAddress & ENPH_MODULE_ADDRESS_MASK) |
            ENPH_ADDRESS_MODE_UNICAST);  //
    }

    pAgfExpRecordsTlv->count   = cAgfFunctionTypeNumOf;
    pAgfExpRecordsTlv->grpMask = htobe32(groupMask);
    pAgfExpRecordsTlv->msgTime = htobe32(time(0));

    for(i = 0; i < (size_t) cAgfFunctionTypeNumOf; i++)
    {
        pAgfRecordHeader[i].type      = pAgfRecordHeaders[i].type;
        pAgfRecordHeader[i].version   = pAgfRecordHeaders[i].version;
        pAgfRecordHeader[i].key       = htobe16(pAgfRecordHeaders[i].key);
        pAgfRecordHeader[i].recLength = htobe16(pAgfRecordHeaders[i].recLength);
        pAgfRecordHeader[i].recCsum   = htobe16(
            pAgfRecordHeaders[i].recCsum);  // checksum of just record??
    }

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdExpAgfRecordTlv;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpAgfExpRecordsTlv) +
        cAgfFunctionTypeNumOf * sizeof(tMcpAgfRecHeader) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpAgfExpRecordsTlv) +
            cAgfFunctionTypeNumOf * sizeof(tMcpAgfRecHeader) -
            1);  // unused tlvData[1] ??
}

int32_t enphase_pack_condition_poll_ack(uint8_t *packet, uint8_t seqNum,
                                        uint8_t *domain, uint16_t modAddr)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr      = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpPollCmdTlv *pMcpPollCmd = (tMcpPollCmdTlv *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after

    tMcpPollCmdConditionAcknowledgeTlv
        *pConditionAck = (tMcpPollCmdConditionAcknowledgeTlv *) &pMcpPollCmd
                             ->tlvData[0];  // variable length Tlv Cmd Data

    memcpy(pMcpAddr->domainAddr, domain, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((modAddr & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_UNICAST);  //
    // memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    // pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
    // ENPH_ADDRESS_MODE_BCAST_DOMAINZERO); //

    // setup the tMcpPollCmdTlv structure
    pMcpPollCmd->msgTime      = htobe32(time(0));
    pMcpPollCmd->pollReqFlags = htobe16(
        cMcpPollFlagCondition);  // only request this one
    pMcpPollCmd->sequenceNumber = 1;
    pMcpPollCmd->tlvCnt         = 1;

    pConditionAck->tlvHdr.length = htobe16(
        sizeof(tMcpPollCmdConditionAcknowledgeTlv) - sizeof(tMcpTlvHeader));
    pConditionAck->tlvHdr.type    = cMcpPollConditionAcknowledge;
    pConditionAck->sequenceNumber = seqNum;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdPollCmdTlv;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpPollCmdTlv) +
        sizeof(tMcpPollCmdConditionAcknowledgeTlv) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpPollCmdTlv) +
            sizeof(tMcpPollCmdConditionAcknowledgeTlv) -
            1);  // unused tlvData[1] ??
}
/* This formats a search/discover/idall packet All unassociated, unsquelched
 * PCUs send their devInfo in a ~3second (up to 24 devices) window with some
 * automatic collision avoidance Assumes a header to the E_ASIC of
 * tMcpAddressHdr that is used to format the Enphase L2 header */
int32_t enphase_pack_poll_control(uint8_t *packet, uint8_t *domain,
                                  uint16_t modAddr, uint16_t flags)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr      = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpPollCmdTlv *pMcpPollCmd = (tMcpPollCmdTlv *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after

    tMcpPollCmdControlFlagsTlv *pPollCmdControlFlagsTlv =
        (tMcpPollCmdControlFlagsTlv *) &pMcpPollCmd
            ->tlvData[0];  // variable length Tlv Cmd Data

#if 0
   // setup Message header to E_ASIC to format L2 frame header -
   memcpy(pMcpAddr->domainAddr, domain, sizeof(pMcpAddr->domainAddr));

   // Unassociated will respond to DOMAIN ZERO broadcast
   pMcpAddr->modAddr = htobe16((modAddr & ENPH_MODULE_ADDRESS_MASK) | ENPH_ADDRESS_MODE_UNICAST);
#else

    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);  //
#endif
    // setup the tMcpPollCmdTlv structure
    pMcpPollCmd->msgTime      = htobe32(time(0));
    pMcpPollCmd->pollReqFlags = htobe16(
        cMcpPollFlagDeviceInfo);  // only request this one
    pMcpPollCmd->sequenceNumber = 1;
    pMcpPollCmd->tlvCnt         = 1;  // Only one TLV command requested

    // setup the tMcpPollCmdSendPollDataTlv structure
    pPollCmdControlFlagsTlv->tlvHdr.length = htobe16(
        sizeof(tMcpPollCmdControlFlagsTlv));
    pPollCmdControlFlagsTlv->tlvHdr.type = cMcpPollCmdControlFlags;

    pPollCmdControlFlagsTlv->controlFlags = htobe16(flags);
    pPollCmdControlFlagsTlv->periodInSecs = 2;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdPollCmdTlv;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpPollCmdTlv) +
        sizeof(tMcpPollCmdControlFlagsTlv) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;  // TODO: Does the E_ASIC MAC do this ???
    pMcpMsgHdr->ackdSeqNum = 1;  // TODO: what when where
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpPollCmdTlv) + sizeof(tMcpPollCmdControlFlagsTlv) -
            1);  // unused tlvData[1] ??
}
int32_t enphase_pack_data_rd(uint8_t *packet,
                                  char *devSerialNum,
                                  uint16_t logicalAddress, uint32_t mem_addr, uint16_t len, uint8_t cmd_type, uint8_t data_type)
{
	static int ack = 0;

	tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpDebugCmd
        *pDebugCmd = (tMcpDebugCmd
                        *) &packet[sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr)];

    memset(pDebugCmd, 0, sizeof(tMcpDebugCmd));

    // setup Message header to E_ASIC to format L2 frame header -
    memcpy(pMcpAddr->domainAddr, devSerialNum, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((logicalAddress & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_UNICAST);

    
	pDebugCmd->debugReq = cmd_type;
	pDebugCmd->cmdParm  = data_type; 
	pDebugCmd->count = htobe16(len);
	pDebugCmd->addr = htobe32(mem_addr);

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdDebug;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionFour;
    pMcpMsgHdr->msgLen              = htobe16(sizeof(tMcpMsgHdr) +
                                 sizeof(tMcpDebugCmd) - cMcpDbgCmdCdataLen);
    pMcpMsgHdr->msgSeq              = ack;
    pMcpMsgHdr->ackdSeqNum          = ack++;
    pMcpMsgHdr->msgCsum             = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpDebugCmd) - cMcpDbgCmdCdataLen);
}


int32_t enphase_pack_ack_dev_info(uint8_t *packet, uint8_t *domainAddr,
                                  tSerialNumber *devSerialNum,
                                  uint16_t logicalAddress)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpAckDevInfoMsg_Ver3
        *pAckDev = (tMcpAckDevInfoMsg_Ver3
                        *) &packet[sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr)];

    memset(pAckDev, 0, sizeof(tMcpAckDevInfoMsg_Ver3));

    // setup Message header to E_ASIC to format L2 frame header -
    memcpy(pMcpAddr->domainAddr, domainAddr, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((0 & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_BCAST_DIRECTED);

    // setup the tMcpAckDevInfoMsg_Ver3 structure

    memcpy((void *) &pAckDev->devSerialNum, devSerialNum,
           sizeof(pAckDev->devSerialNum));
    pAckDev->modId             = htobe16(logicalAddress);
    pAckDev->assocStatus       = cMcpDomainMembershipStatusValid;
    pAckDev->ackStatus         = cMcpAckStatusOk;
    pAckDev->ackdDevInfoSeqNum = logicalAddress & 0xFF;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdAckDevInfo;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionFour;
    pMcpMsgHdr->msgLen              = htobe16(sizeof(tMcpMsgHdr) +
                                 sizeof(tMcpAckDevInfoMsg_Ver3));
    pMcpMsgHdr->msgSeq              = 1;
    pMcpMsgHdr->ackdSeqNum          = 1;
    pMcpMsgHdr->msgCsum             = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpAckDevInfoMsg_Ver3));
}



int32_t enphase_pack_poll_dev_info(uint8_t *packet, uint8_t *domainAddr,
                                   tSerialNumber *devSerialNum,
                                   uint16_t logicalAddress)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpPollMsg *pPoll     = (tMcpPollMsg *) &packet[sizeof(tMcpAddressHdr) +
                                                 sizeof(tMcpMsgHdr)];

    memset(pPoll, 0, sizeof(tMcpPollMsg));

    // setup Message header to E_ASIC to format L2 frame header -
    memcpy(pMcpAddr->domainAddr, domainAddr, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16((logicalAddress & ENPH_MODULE_ADDRESS_MASK) |
                                ENPH_ADDRESS_MODE_UNICAST);

    pPoll->pollType = cMcpPollTypeDirectedDevice;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdPoll;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionFour;
    pMcpMsgHdr->msgLen     = htobe16(sizeof(tMcpMsgHdr) + sizeof(tMcpPollMsg));
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) + sizeof(tMcpPollMsg));
}

int32_t enphase_pack_poll_envoy_unassociate_all(uint8_t *packet)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpDomainCtlMsg *pD = (tMcpDomainCtlMsg *) &packet[sizeof(tMcpAddressHdr) +
                                                        sizeof(tMcpMsgHdr)];

    memset(pD, 0, sizeof(tMcpDomainCtlMsg));

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16(0 | ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);

    pD->forgetAnyDms = 1;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdDomainCtl;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionFour;
    pMcpMsgHdr->msgLen = htobe16(sizeof(tMcpMsgHdr) + sizeof(tMcpDomainCtlMsg));
    pMcpMsgHdr->msgSeq = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpDomainCtlMsg));
}
int32_t enphase_pack_poll_envoy_unassociate_domain(uint8_t *packet,
                                                   uint8_t *domainAddr)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpDomainCtlMsg *pD = (tMcpDomainCtlMsg *) &packet[sizeof(tMcpAddressHdr) +
                                                        sizeof(tMcpMsgHdr)];

    memset(pD, 0, sizeof(tMcpDomainCtlMsg));

    // setup Message header to E_ASIC to format L2 frame header -
    memcpy(pMcpAddr->domainAddr, domainAddr, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16(0 | ENPH_ADDRESS_MODE_BCAST_DIRECTED);

    pD->forgetMatchingDms = 1;
    memcpy(pD->domainAddr, domainAddr, sizeof(pD->domainAddr));
    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdDomainCtl;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionFour;
    pMcpMsgHdr->msgLen = htobe16(sizeof(tMcpMsgHdr) + sizeof(tMcpDomainCtlMsg));
    pMcpMsgHdr->msgSeq = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpDomainCtlMsg));
}
int32_t enphase_pack_poll_new_device(uint8_t *packet)
{
    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpPollMsg *pPoll     = (tMcpPollMsg *) &packet[sizeof(tMcpAddressHdr) +
                                                 sizeof(tMcpMsgHdr)];

    memset(pPoll, 0, sizeof(tMcpPollMsg));

    // setup Message header to E_ASIC to format L2 frame header -
    memset(pMcpAddr->domainAddr, 0, sizeof(pMcpAddr->domainAddr));
    pMcpAddr->modAddr = htobe16(0 | ENPH_ADDRESS_MODE_BCAST_DOMAINZERO);

    pPoll->pollType = cMcpPollTypeNewDevice;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdPoll;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionFour;
    pMcpMsgHdr->msgLen     = htobe16(sizeof(tMcpMsgHdr) + sizeof(tMcpPollMsg));
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);

    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) + sizeof(tMcpPollMsg));
}
int32_t enphase_unpack_debug_memr_data(uint8_t *packet, uint32_t *mem, uint32_t *count, uint8_t**pBuf, uint8_t cmd_type, uint8_t data_type)
{
   tMcpDebugCmd
        *pMcpDebugCmd = (tMcpDebugCmd *) &packet[sizeof(tMcpMsgHdr)];
	if ((pMcpDebugCmd->debugReq != cmd_type) || (pMcpDebugCmd->cmdParm != data_type))
	{
		return -1;
	}
	*mem = be32toh(pMcpDebugCmd->addr);
	*count = be16toh(pMcpDebugCmd->count);
	*pBuf = &pMcpDebugCmd->cdata[0];
	return 0;
}
int32_t enphase_unpack_poll_dev_info(uint8_t *packet,
                                     tMcpDevInfoMsg_Ver3 *pPacket)
{
    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[0];  // the McpMsgHdr
    tMcpDevInfoMsg_Ver3
        *pDevInfo = (tMcpDevInfoMsg_Ver3 *) &packet[sizeof(tMcpMsgHdr)];

    // should only have one response
    if(pMcpMsgHdr->msgId != cMcpMsgIdDevInfo)
    {
        return -1;
    }
    memcpy(pPacket, pDevInfo, sizeof(tMcpDevInfoMsg_Ver3));

    return 0;
}
int32_t enphase_pack_set_offgrid_mode(uint8_t *packet, uint8_t *domainAddr, tSerialNumber *devSerialNum, uint16_t logicalAddress, uint16_t backup_timeout_secs, uint16_t mode)
{
	    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr      = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpOffGridCtrMsg *pMcpOffCmd = (tMcpOffGridCtrMsg *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after

    tMcpOffGridCtrlSetData *pPollCmdSetData =
        (tMcpOffGridCtrlSetData *) &pMcpOffCmd
		->msgData[0];  // variable length Cmd Data
	uint16_t eMode = 0;
	uint8_t *pDomain = zeroDomain;
	uint16_t useLA = 0;
	switch (mode) {
	case cAddressModeTypeDirected:
		eMode = ENPH_ADDRESS_MODE_BCAST_DIRECTED;
		memcpy(&pMcpOffCmd->devSerialNum, devSerialNum,
               sizeof(pMcpOffCmd->devSerialNum));

		break;
	case cAddressModeTypeUnicast:
		eMode = ENPH_ADDRESS_MODE_UNICAST;
		pDomain = domainAddr;
		useLA = logicalAddress;
		break;
	case cAddressModeTypeModzero:
		eMode = ENPH_ADDRESS_MODE_BCAST_MODZERO;
		pDomain = domainAddr;
		break;
	case cAddressModeTypeDomainZero:
		eMode = ENPH_ADDRESS_MODE_BCAST_DOMAINZERO;
		break;
	default:
		break;
	}

   if (pDomain) {
	   memcpy(pMcpAddr->domainAddr, pDomain, sizeof(pMcpAddr->domainAddr));
	}
	else
		return 0;
	pMcpAddr->modAddr = htobe16((useLA & ENPH_MODULE_ADDRESS_MASK) | eMode);  //
    
	pPollCmdSetData->backupTimeSeconds = htobe16(backup_timeout_secs);
    
	pMcpOffCmd->msgId = cMcpOffGridCtrlSetBackupMode;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdOffGridCtrl;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpOffGridCtrMsg) +
        sizeof(tMcpOffGridCtrlSetData) -
        1);  // subtract 1 since the varaible data has at least 1
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);


    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpOffGridCtrMsg) + sizeof(tMcpOffGridCtrlSetData) -
            1);  // unused tlvData[1] ??
}


static int32_t enphase_pack_offgrid_mode_common(uint8_t *packet, uint8_t *domainAddr, tSerialNumber *devSerialNum, uint16_t logicalAddress, uint8_t msgId, uint16_t mode)
{
		    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr      = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpOffGridCtrMsg *pMcpOffCmd = (tMcpOffGridCtrMsg *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after


	uint16_t eMode = 0;
	uint8_t *pDomain = zeroDomain;
	uint16_t useLA = 0;
	switch (mode) {
	case cAddressModeTypeDirected:
		eMode = ENPH_ADDRESS_MODE_BCAST_DIRECTED;
		memcpy(&pMcpOffCmd->devSerialNum, devSerialNum,
			   sizeof(pMcpOffCmd->devSerialNum));

		break;
	case cAddressModeTypeUnicast:
		eMode = ENPH_ADDRESS_MODE_UNICAST;
		pDomain = domainAddr;
		useLA = logicalAddress;
		break;
	case cAddressModeTypeModzero:
		eMode = ENPH_ADDRESS_MODE_BCAST_MODZERO;
		pDomain = domainAddr;
		break;
	case cAddressModeTypeDomainZero:
		eMode = ENPH_ADDRESS_MODE_BCAST_DOMAINZERO;
		break;
	default:
		break;
	}

   if (pDomain) {
	   memcpy(pMcpAddr->domainAddr, pDomain, sizeof(pMcpAddr->domainAddr));
	}
	else
		return 0;
	pMcpAddr->modAddr = htobe16((useLA & ENPH_MODULE_ADDRESS_MASK) | eMode);  //


	pMcpOffCmd->msgId = msgId;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdOffGridCtrl;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpOffGridCtrMsg));  
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);


    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpOffGridCtrMsg) );  
}
int32_t enphase_pack_reset_offgrid_mode(uint8_t *packet, uint8_t *domainAddr, tSerialNumber *devSerialNum, uint16_t logicalAddress, uint16_t mode)
{
	return enphase_pack_offgrid_mode_common(packet, domainAddr, devSerialNum, logicalAddress, (uint8_t)cMcpOffGridCtrlResetBackupMode, mode);
}
int32_t enphase_pack_query_offgrid_mode(uint8_t *packet, uint8_t *domainAddr, tSerialNumber *devSerialNum, uint16_t logicalAddress, uint16_t mode)
{
	return enphase_pack_offgrid_mode_common(packet, domainAddr, devSerialNum, logicalAddress, (uint8_t)cMcpOffGridCtrlQueryBackupMode, mode);
}
int32_t enphase_unpack_query_rsp_offgrid_mode(uint8_t *packet, uint8_t *mode, uint16_t *backupTime, uint16_t *timeLeft)
{

    tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *) &packet[0];  // the McpMsgHdr
	if (pMcpMsgHdr->msgId != cMcpMsgIdOffGridRsp)
	{
		return -1;
	}
	tMcpOffGridCtrlRspData *pMcpCmd = (tMcpOffGridCtrlRspData *)&packet[sizeof(tMcpMsgHdr)];
	*mode = pMcpCmd->bmMode;
	*backupTime = be16toh(pMcpCmd->timeSeconds);
	*timeLeft = be16toh(pMcpCmd->curBackupTimeSeconds);
	return 0;
}
int32_t enphase_pack_mps_mode(uint8_t *packet, uint8_t *domainAddr, tSerialNumber *devSerialNum, uint16_t logicalAddress, uint8_t enable, uint16_t mode)
{
		    tMcpAddressHdr *pMcpAddr = (tMcpAddressHdr *)
        packet;  // beginning of packet has header just used by E_ASIC MAC layer
    tMcpMsgHdr *pMcpMsgHdr      = (tMcpMsgHdr *) &packet[sizeof(
        tMcpAddressHdr)];  // the McpMsgHdr
    tMcpMPSGateDriveCtrMsg *pMcpMpsCmd = (tMcpMPSGateDriveCtrMsg *) &packet
        [sizeof(tMcpAddressHdr) +
         sizeof(tMcpMsgHdr)];  // message DATA comes after
    uint16_t eMode = 0;
    uint8_t *pDomain = zeroDomain;
	uint16_t useLA = 0;
	switch (mode) {
	case cAddressModeTypeDirected:
		eMode = ENPH_ADDRESS_MODE_BCAST_DIRECTED;
		memcpy(&pMcpMpsCmd->devSerialNum, devSerialNum,
               sizeof(pMcpMpsCmd->devSerialNum));
        
		break;
	case cAddressModeTypeUnicast:
		eMode = ENPH_ADDRESS_MODE_UNICAST;
		pDomain = domainAddr;
		useLA = logicalAddress;
		break;
	case cAddressModeTypeModzero:
		eMode = ENPH_ADDRESS_MODE_BCAST_MODZERO;
		pDomain = domainAddr;
		break;
	case cAddressModeTypeDomainZero:
		eMode = ENPH_ADDRESS_MODE_BCAST_DOMAINZERO;
		break;
	default:
		break;
	}
	
   if (pDomain) {
	   memcpy(pMcpAddr->domainAddr, pDomain, sizeof(pMcpAddr->domainAddr));
	}
	else {
		printf("YIKES\n");
		return 0;
	}
    pMcpAddr->modAddr = htobe16((useLA & ENPH_MODULE_ADDRESS_MASK) | eMode);  //
        
   
	
    
	pMcpMpsCmd->enableFlag = enable;

    // setup the tMcpMsgHdr structure
    pMcpMsgHdr->msgId               = cMcpMsgIdMPSFixCtrl;
    pMcpMsgHdr->ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
    pMcpMsgHdr->msgLen              = htobe16(
        sizeof(tMcpMsgHdr) + sizeof(tMcpMPSGateDriveCtrMsg));  
    pMcpMsgHdr->msgSeq     = 1;
    pMcpMsgHdr->ackdSeqNum = 1;
    pMcpMsgHdr->msgCsum    = mcpUtilMcpMsgCsum(
        (tMcpMsgHdr *) &packet[sizeof(tMcpAddressHdr)]);


    return (sizeof(tMcpAddressHdr) + sizeof(tMcpMsgHdr) +
            sizeof(tMcpMPSGateDriveCtrMsg) );  
}

/*!****************************************************************************
 * @file enph_xaction.c
 *  Functions for managing PLC transactions and ensuring that unicast
 *
 * @copyright
 *  Copyright (c) 2018 SunPower Corporation.  All rights reserved.
 *
 * @created July 6, 2018
 *
 * @author
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#ifndef WIN32
#include <unistd.h>
#include <sys/time.h>
#else
#define tsleep usleep
#endif  // !WIN32

#include <errno.h>
#include "sbt/log.h"
#include "sbt/scooterapi.h"
#include "enphase/enph_xaction.h"
#include "sbt/scooterProto.h"
#include "enphase/mcp_enphase.h"
#include "enphase/enphase_packet.h"

#include "sbt/scooter_uart.h"
#include "sbt/os.h"

//! \addtogroup xaction_config
//! @{
//! @}

typedef enum
{
    XACT_STATE_IDLE = 0,
    XACT_STATE_INPROGRESS,
    XACT_STATE_FAILED,
} tXACT_STATE;
static uint32_t g_ui32DefaultRetries = 2;
struct _e_transaction_t
{
    uint8_t cmdObj[ENPHASE_MAX_PACKET];
    uint8_t rspObj[ENPHASE_MAX_PACKET];
    uint16_t msgLen;

    uint8_t cmdType;

    //! the callback function provided by the client to call when a response
    //! is received
    e_xactCB callback;
    uint16_t v2flags;
    //! This value gets passed to the callback
    void *pvCookie;

    //! handle to the connection to use for this transaction
    hScooter_t hScooter;

    //! originating pm SN
    uint8_t gateWayMAC[6];  // this may be cached in E_ASIC

    //! processing time, in milliseconds, for an MI to chew on a command.  This
    //! is set by _transaction_build_request. e_transactionexecute will
    //! implement the delay if bQuick is set and proctime > 0
    uint32_t proctime;

    //! the current state of the transaction (idle or in progress)
    tXACT_STATE state;

    //! How many retries to start with when executing a transaction.  If set to
    //! -1 then we go with the xaction module default retries
    int8_t i8StartingRetries;

    //! Internal state tracking how many retries are left to execute this
    //! transaction
    uint8_t ui8Retries;
    uint8_t ui8Limit;

    //! Whether or not the client requested a pResponse to this pCommand
    bool bResponse;

    //! A one-shot transaction is one that is automatically free's its resources
    //! after a call toe_transactionexecute.
    bool bOneshot;

    //! buffer storing the out-going pCommand.  The xaction module uses the
    //! other fields in the structure to render the packet into this buffer.
    //! NOTE:  pCommand and pResponse only need to exist from the beginning(ish)
    //! ofe_transactionexeucte to the end ofe_transactionexecute.
    uint8_t *pCommand;

    //! A buffer holding the response from an MI
    uint8_t *pResponse;
};

static uint32_t debug;

//! \addtogroup xaction_api
//! @{

const char *const e_plccmd_strings[] = {

    "LAST",
};
/*!
 * @brief Transmit the request in command over hConn
 * This is a primitive for transmitting a request buffer.  Applications can
 * use this function directly but should use the higher level transaction
 * interfaces when possible
 *
 * @param hConn is a handle to a connection object
 * @param command is a pointer to a command buffer
 *
 * @return 0 for success
 */

int enphase_write_commandV2(hScooter_t hScooter, const uint8_t *command,
                            uint16_t v2Flags, uint16_t length)
{
    int32_t rc;
    uint8_t cmd[ENPHASE_MAX_PACKET];
    sSCCmdV2_t *pV2Cmd = (sSCCmdV2_t *) cmd;
    uint8_t rsp[ENPHASE_MAX_PACKET];
    size_t txlen;
    size_t rsplen = ENPHASE_MAX_PACKET;
    eSCLun_t lun;

    //
    // Package up the Enphase command into a V1 Scooter payload
    // TODO: length is not first byte for Enphase PLC packets
    //
    txlen = ScooterPackWriteTx(pV2Cmd->payload, PLC_FLAGS_SYNCHRONOUS_PLC,
                               command, length);

    //
    // Wrap the V1 scooter payload into a V2 payload
    //
    txlen = ScooterPackV2(cmd, ScooterLunGet(hScooter), v2Flags,
                          pV2Cmd->payload, txlen);

    //
    // Transmit the command and handle any special ACKing
    //
    rc = ScooterExecute(hScooter, cmd, txlen, rsp, &rsplen, &lun);

    return (rc);
}
// response may/should have a V2 header
int enphase_read_responseV2(hScooter_t hScooter, const uint8_t *command,
                            uint8_t *response, uint32_t *lenP)
{
    int device;
    int32_t rc;
    int done = 0;
    uint8_t *pTmpFill;
    uint8_t *pTransfer;
    uint32_t bytesInPayload = 0;
    uint32_t bytesTotal     = 0;
    uint8_t bufMax[ENPHASE_MAX_PACKET];
    uint8_t pBuf[256];

    sSCCmdV2_t *pV2Cmd = (sSCCmdV2_t *) pBuf;
    uint8_t seq=0;
    size_t length = 256;
    uint32_t retries;
    uint32_t segmentsLeft;

    uint32_t lastSeg = 0;
    retries          = 32;
    do
    {
        // get a packet, look at
        //
        // When reads are performed through this path in the stack the caller
        // must supply a ENPHASE_MAX_PACKET byte buffer
        //

        length = sizeof(pBuf);
        rc     = ScooterReadPLC(hScooter, pBuf, &length);

        if(rc != 0) return rc;

        // check for V2 header...
        if(pV2Cmd->header.dev != eSC_INTF_CMD_VERSION2 ||
           pV2Cmd->header.lun != SC_E_ASIC_PLC)
        {
            LOG_WARNING(
                "Expected PLC response from LUN6 but rxd dev=%d lun=%d\n",
                pV2Cmd->header.dev, pV2Cmd->header.lun);
            if(--retries == 0)
            {
                return -ETIMEDOUT;
            }
            continue;
        }
        if((pV2Cmd->header.sequence & SCCMDHEADERV2_SEQUENCE_START))
        {
            // have new message, previous may have been out of sync, so start
            // over make sure a TX/RX

            device = pBuf[sizeof(sSCCmdHeaderV2_t)];
            if(device != ePLC_INTF_WRITE_TX && device != eSC_INTF_READ_PLC)
            {
                LOG_WARNING("Expected PLC response but RXed from device %d\n",
                            device);
                if(--retries == 0)
                {
                    return -ETIMEDOUT;
                }
                continue;
            }
            segmentsLeft = pV2Cmd->header.sequence &
                           SCCMDHEADERV2_SEQUENCE_PACKETS_LEFT_MASK;
            lastSeg    = segmentsLeft;
            bytesTotal = 0;
            pTransfer  = pBuf + sizeof(sSCCmdHeaderV2_t) +
                        2;  // only want payload at this point, first has TX/RX
        }
        else
        {
            // make sure segment is expected
            seq = pV2Cmd->header.sequence &
                  SCCMDHEADERV2_SEQUENCE_PACKETS_LEFT_MASK;

            if(seq != (lastSeg - 1))
            {
                LOG_WARNING("Unexpected sequence.  %d != %d\n", seq, lastSeg - 1);
                bytesTotal = 0;
                if(--retries == 0)
                {
                    return -ETIMEDOUT;
                }
                continue;
            }
            lastSeg   = seq;
            pTransfer = pBuf + sizeof(sSCCmdHeaderV2_t) +
                        2;  // only want payload at this point, first has TX/RX
        }
        bytesInPayload = pV2Cmd->header.packetSize;
        LOG_VERBOSE_LO("Sequence %d rx %d bytes\n", seq, bytesInPayload);
        pTmpFill = (uint8_t *) &bufMax[bytesTotal];  // where to store next set

        memcpy(pTmpFill, pTransfer, bytesInPayload);
        bytesTotal += bytesInPayload;
        if(lastSeg == 0)
        {
            done = 1;
        }

    } while(!done);

    LOG_VERBOSE_LO("Rx'ed %d bytes\n", bytesTotal);
    memcpy(response, bufMax, bytesTotal);
    if(lenP)
    {
        *lenP = bytesTotal;
    }
    return 0;
}

/*!
 * @brief Execute an enphase command buffer using a callback function.
 * The request in the buffer pCommand is transmitted over hConn.  For every
 * response received the function callback() is called.  The callback takes as
 * its parameters a ptr to void (which is "context") and a pointer to a response
 * buffer. Will be used for search only. They have a fixed 4 second window.
 *
 * @param hConn is the connection handle to transmet the request on
 * @param hScooter is a handle to a scooter object
 * @param pCommand is a pointer to the request buffer to transmit
 * @param pResponse is a pointer to storage for the response.  The caller
 *        should not rely on the contents of pResponse to be meaningful after
 *        this function has executed.  The caller is allowed to pass the
 *        storage simply so that it can manage that memory to its liking BUT
 *        the caller MUST always provide a ENPHASE_MAX_PACKET byte buffer here.
 * @param callback is a pointer to a function to call when a response is RXed
 * @param pvContext is an application-specific blob pointer passed into callback
 * @param limit is the number of timeslots to wait
 *
 * @note only one of hConn or hScooter should be supplied.  Not both.
 *
 * @note For SEARCH MI, the value of limit must be equal to the value of
 * windowSize.
 * @note This function is synchronous even though it uses a callback.
 *
 * @return 0 for success, < 0 for error
 */
int enphase_execute_cb(void *hScooter, const uint8_t *pCommand,
                       uint8_t *pResponse,
                       int (*callback)(void *, uint8_t *, uint32_t),
                       void *pvContext, int limit, uint16_t flags,
                       uint16_t cmdLen)
{
    struct timeval now_time;
    struct timeval end_time;
    int response_time_ms;
    int total_time_ms;

    int32_t rc;
    uint32_t lenMsg;

    if(limit <= 0)
    {
        return -EINVAL;
    }
    // printf("in enphase_execute_cb\n");
    rc = enphase_write_commandV2(hScooter, pCommand, flags, cmdLen);

    if(rc != 0)
    {
        LOG_WARNING("enphase_write_command returned %d\n", rc);
        return (rc);
    }
    else
    {
        LOG_VERBOSE_HI("enphase_write_command returned OK\n");
    }
    response_time_ms = 150;
    /*
     * Extract the inter-packet delay from the command's addressing mode.
     */

    /*
     * we get ack's on the PLC TX completion so we don't have to wait for
     * sidecar's tx slot.  Our total wait time is reposnse time * limit.
     * The 5 ms is just a fudge factor based on nothing empirical.
     */
    total_time_ms = response_time_ms * limit + 5;

    LOG_VERBOSE_MED("tt = %d\n", total_time_ms);

    gettimeofday(&now_time, NULL);

    end_time = now_time;
    end_time.tv_sec += (total_time_ms / 1000);
    end_time.tv_usec += (total_time_ms % 1000) * 1000;

    /*
     * Now normalize the time.
     */
    if(end_time.tv_usec > 1000000)
    {
        end_time.tv_sec += end_time.tv_usec / 1000000;
        end_time.tv_usec %= 1000000;
    }
    LOG_VERBOSE_MED("nt = %d.%06d, et = %d.%06d\n", now_time.tv_sec,
                    now_time.tv_usec, end_time.tv_sec, end_time.tv_usec);

    while(gettimeofday(&now_time, NULL) == 0 &&
          ((now_time.tv_sec < end_time.tv_sec) ||
           ((now_time.tv_sec == end_time.tv_sec) &&
            (now_time.tv_usec < end_time.tv_usec))))
    {
        int rc;

        rc = enphase_read_responseV2(hScooter, pCommand, pResponse, &lenMsg);

        if(rc == 0)
        {
            // this callback returns 0 to indicate success.  We will keep
            // trying to read until total_time_ms has elapsed
            int rc2 = (*callback)(pvContext, pResponse, lenMsg);
            if(rc2 < 0)
            {
                // if our callback returns an error then take an early exit
                return rc2;
            }
            // we successfully processed a response so decrement how many we're
            // expecting
            limit--;

            // if we get all our expected responses then we exit indicating
            // success
            if(!limit) return 0;
        }
        else
        {
            // timeouts are not entirely unexpected here
            LOG_VERBOSE_MED("enphase_read_response returned %d\n", rc);
        }
    }
    return (0);
}

/*!
 * @brief Transmit a request and wait for a response if the response flag is
 * set. This function is similar to \ref enphase_execute_cb() except that it is
 * intended for unicast transactions.  This function will send one command and
 * return after the first response.
 *
 * @param hConn is a handle to a connection to use for communication
 * @param hScooter is a handle to a scooter object
 * @param pCommand is pointer to the request buffer
 * @param pResponse is a pointer to storage for a response.  The caller must
 *        ensure this buffer is large enough to contain ther response
 *
 * @note only one of hConn or hScooter should be supplied.  Not both.
 *
 * @return 0 for success, < 0 for failure
 */
int enphase_execute(void *hScooter, const uint8_t *pCommand, uint8_t *pResponse,
                    uint16_t flags, uint16_t cmdLen, bool resp,
                    uint32_t *lenMsg)
{
    int32_t rc;

    if(!pCommand)
    {
        return -EINVAL;
    }
#if 0
    bResp = enphase_response_set(pCommand);
    if (bResp && !pResponse){
        return -EINVAL;
    }
#endif

    LOG_VERBOSE_HI("call enphase_write_command\n");

    // printf("in enphase_execute\n");
    rc = enphase_write_commandV2(hScooter, pCommand, flags, cmdLen);

    if(rc != 0)
    {
        LOG_WARNING("enphase_write_command returned %d\n", rc);
        return (rc);
    }
    else
    {
        LOG_VERBOSE_HI("enphase_write_command returned OK\n");
    }

    if(resp)
    {
        LOG_VERBOSE_HI("call enphase_read_response\n");
        rc = enphase_read_responseV2(hScooter, pCommand, pResponse, lenMsg);

        LOG_VERBOSE_HI("enphase_read_response returned %d\n", rc);
        return rc;
    }
    return 0;
}

/*!
 * @brief Create a new transaction object
 * This function will allocate the storage for the transaction object.  The
 * caller should supply a pointer to transaction handle.
 *
 * @param phTrans is a pointer to a transaction handle.
 *
 * @return 0 for success, < 0 for error
 */
int32_t e_transaction_new(e_transaction_t *phTrans)
{
    struct _e_transaction_t *pTrans;

    if(!phTrans)
    {
        return -EINVAL;
    }
    pTrans = calloc(sizeof(*pTrans), 1);

    if(!pTrans)
    {
        return -ENOMEM;
    }
    pTrans->i8StartingRetries = -1;
    pTrans->ui8Retries        = g_ui32DefaultRetries;

    *phTrans = pTrans;

    return 0;
}

/*!
 * @brief free resources allocated duringe_transactionnew
 * @param hTrans is the handle to the transaction to free
 * @return 0 for success
 */
int32_t e_transaction_delete(e_transaction_t hTrans)
{
    if(!hTrans) return -EINVAL;

    // fill with pattern to help catch use-after-free bugs
    memset(hTrans, 0xc3, sizeof(struct _e_transaction_t));

    free(hTrans);
    return 0;
}

/*!
 * @brief Set the number of times an unacknowledged request will be
 * retransmitted
 *
 * @note Users must differentiate between a lack of response and an error
 * response.  The transaction module only retries requests with missing
 * responses but the interpretation of a response is left to the application.
 *
 * @param hTrans is a handle to a transaction object
 * @param retries is the number of retries to attempt before failing the
 *        transaction
 *
 * @returns 0 for success, < 0 for error
 */
int32_t e_transaction_retries_set(e_transaction_t hTrans, int32_t retries)
{
    if(!hTrans)
    {
        return -EINVAL;
    }
    hTrans->i8StartingRetries = retries;
    return 0;
}

/*!
 * @brief Set the default retry count for transactions that don't specify (or
 * specify -1) for their retry count
 *
 * @param retries is the number of retries that a transaction instance will use
 * by default
 *
 * @returns 0 for success
 */
int32_t e_transaction_default_retries_set(uint32_t retries)
{
    g_ui32DefaultRetries = retries;
    return 0;
}

/*!
 * @brief Get the default retry count
 *
 * @returns retry count
 */
int32_t e_transaction_default_retries_get() { return g_ui32DefaultRetries; }

/*!
 * @brief Set the command for this transaction
 * If the command has any associated parameters/payload the store them as well.
 *
 * @param hTrans is a handle to the transaction
 * @param cmdtype sets which command type this trasnaction will be
 * @param pvCmdData points to data associated with the command.  May be NULL if
 *        there is no assoicated data.
 *
 * @return 0 for success, < 0 for error
 */
int32_t e_transaction_command_set(e_transaction_t hTrans, uint8_t cmdtype,
                                  void *pvCmdData, uint32_t size)
{
    if(!hTrans)
    {
        return -EINVAL;
    }
    if(hTrans->state == XACT_STATE_INPROGRESS)
    {
        return -EINPROGRESS;
    }

    // remember what type of command the client is requesting
    hTrans->cmdType = cmdtype;

    // and the command parameters if they provided them
    if(pvCmdData)
    {
        memcpy(hTrans->cmdObj, pvCmdData, size);
    }
    hTrans->msgLen    = size;
    hTrans->pCommand  = hTrans->cmdObj;
    hTrans->pResponse = hTrans->rspObj;
    //! @todo correlate the command type with those commands that need
    //! parameters and return an error if the client didn't provide enough data
    return 0;
}

/*!
 * @brief Set which callback this trasnaction should call upon completion
 * If no callback is set then the transaction will infer that no response to
 * a request is desired.
 *
 * @param hTrans is a handle to the transaction object
 * @param pfnCb is a pointer to an e_xactCB function.  Set to NULL to clear
 *        a previously registered callback and disable responses.
 * @param pvCookie is an application supplied pointer to something app specific
 *
 * @return  0 for success, < 0 for error
 */
int32_t e_transaction_callback_set(e_transaction_t hTrans, e_xactCB pfnCb,
                                   void *pvCookie)
{
    if(!hTrans)
    {
        return -EINVAL;
    }
    if(hTrans->state == XACT_STATE_INPROGRESS)
    {
        return -EINPROGRESS;
    }

    hTrans->callback = pfnCb;
    if(pfnCb)
    {
        hTrans->bResponse = true;
    }
    else
    {
        hTrans->bResponse = false;
    }
    hTrans->pvCookie = pvCookie;

    return 0;
}

/*!
 * @brief set the OneShot flag on this trasnaction.
 *
 * @param hTrans is a handle to a transaction object
 * @param bOneshot should be set to true ife_transactionexecute should free
 * all resources including the transaction object itself.
 *
 * @return 0 for success, < 0 for error
 */
int32_t e_transaction_oneshot_set(e_transaction_t hTrans, bool bOneshot)
{
    if(!hTrans)
    {
        return -EINVAL;
    }
    if(hTrans->state == XACT_STATE_INPROGRESS)
    {
        return -EINPROGRESS;
    }

    hTrans->bOneshot = bOneshot;
    return 0;
}

/*!
 * @brief Specify how many time slots to wait for a group response
 * If a multi-cast to a group requires less than 32 timeslots to gather all
 * responses then the limit can be reduced so less time is spent waiting.
 *
 * @param hTrans is a handle to a transaction object
 * @param ui8Limit is the highest timeslot to wait for.  If ui8Limit is set to
 *        0 then the associated group object will dictate ui8Limit
 *
 * @return 0 for success, < 0 for error
 */
int32_t e_transaction_limit_set(e_transaction_t hTrans, uint8_t ui8Limit)
{
    if(!hTrans)
    {
        return -EINVAL;
    }
    if(ui8Limit > 24)
    {
        return -EINVAL;
    }
    hTrans->ui8Limit = ui8Limit;
    return 0;
}
/*!
 * @brief Specify how many time slots to wait for a group response
 * If a multi-cast to a group requires less than 32 timeslots to gather all
 * responses then the limit can be reduced so less time is spent waiting.
 *
 * @param hTrans is a handle to a transaction object
 * @param ui8Limit is the highest timeslot to wait for.  If ui8Limit is set to
 *        0 then the associated group object will dictate ui8Limit
 *
 * @return 0 for success, < 0 for error
 */
int32_t e_transaction_flags_set(e_transaction_t hTrans, uint16_t flags)
{
    if(!hTrans)
    {
        return -EINVAL;
    }

    hTrans->v2flags = flags;
    return 0;
}

/*!
 * @brief set the PM's serial number in the transaction object
 * Multi-cast requests must specify the MI group and the originating PM SSN.
 * This function is used to set the SSN in the multi-cast address header.
 *
 * @param hTrans is a handle to the trasnaction object
 * @param addr is the PM's short serial
 *
 * @return 0 for success, < 0 for error
 */
int32_t e_transaction_pmserial_set(e_transaction_t hTrans, uint8_t *gateWayMAC)
{
    if(!hTrans) return -EINVAL;
    memcpy(hTrans->gateWayMAC, gateWayMAC, 6);
    return 0;
}

static int32_t _e_transaction_build_request(uint8_t *pCmd,
                                            e_transaction_t hTrans)
{
    int32_t rc;
    int timeout = 3000;

    // LOG_VERBOSE_MED("Setting read timeout to %d\n", timeout);
    rc = ScooterTimeoutSet(hTrans->hScooter, timeout);

    assert(rc == 0);

    return 0;
}

// internal callback used withe_transactionexecute.  This callback function
// is passed to enphase_execute_cb()
static int multicast_callback(void *pvCookie, uint8_t *pResp, uint32_t len)
{
    e_transaction_t hTrans = (e_transaction_t) pvCookie;
    int32_t rc;

    rc = hTrans->callback(pResp, len, hTrans->pvCookie);

    return rc;
}

/*!
 * @brief Execute the transaction on the specified connection
 *
 * @param hTrans is a handle to the transaction
 * @param hConn is a handle to an open connection
 * @param hScooter is a handle to a scooter object.
 *
 *
 * @return 0 for success, < 0 for error
 *
 * @note only one of hScooter or hConn should be set
 * @note An error frome_transactionexecute is an indication that either:
 *   1. the request did not get transmitted
 *  OR
 *   2. Not all expected MIs responded
 */
static int32_t _xaction_execute(e_transaction_t hTrans, hScooter_t hScooter,
                                uint32_t *rspMsgLen)
{
    int32_t rc = -EINVAL;

    bool bAllocCmd = false, bAllocRsp = false;

    if(!hTrans)
    {
        return rc;
    }

    if(hTrans->state == XACT_STATE_INPROGRESS)
    {
        return -EINPROGRESS;
    }

    //! @todo - make this a semaphore if we'd like this to be multi-threaded
    hTrans->state = XACT_STATE_INPROGRESS;

    hTrans->hScooter = hScooter;

    if(hTrans->i8StartingRetries == -1)
    {
        hTrans->ui8Retries = g_ui32DefaultRetries;
        // LOG_VERBOSE_LO("%d\n",Trans->ui8Retries);
    }
    else
    {
        hTrans->ui8Retries = hTrans->i8StartingRetries;
        // LOG_VERBOSE_LO("%d\n",Trans->ui8Retries);
    }

    // If the caller didn't supply us with a buffer then we need to allocate
    // our own.  The problem with doing it here is more over-head and we're
    // going to thrash the heap a bit.  If there is a thread that serializes
    // access to the UART then the command and response buffer could be shared
    // across all transactions.
    if(hTrans->pCommand == NULL)
    {
        hTrans->pCommand = calloc(1, ENPHASE_MAX_PACKET);
        bAllocCmd        = true;
    }

    //
    // Here is where all the heavy lifting is done in the transaction
    // module
    //
    _e_transaction_build_request(hTrans->pCommand, hTrans);

    //
    // If this transaction doesn't need a response then we don't need to wait
    // regardless of whether we're uni or multicasting
    //
    if(!hTrans->bResponse)
    {
        LOG_VERBOSE_MED("Initiating one-way command\n");
        do
        {
            // we don't log transaction activity until enphase_execute leads us
            // to believe we actually transmitted the packet
            rc = enphase_execute(hTrans->hScooter, hTrans->pCommand,
                                 hTrans->pResponse, hTrans->v2flags,
                                 hTrans->msgLen, hTrans->bResponse, rspMsgLen);

            if(rc)
            {
                LOG_WARNING(
                    "Failed to TX one-way command.  rc = %d, Retries = %d\n",
                    rc, hTrans->ui8Retries);
            }
            else
            {
                // transmit success, no response...nothing left to do...
                hTrans->state = XACT_STATE_IDLE;
                goto exit;
            }
        } while(rc && hTrans->ui8Retries--);

        hTrans->state = XACT_STATE_IDLE;
        if(hTrans->ui8Retries == 0)
        {
            uint32_t r;
            if(hTrans->i8StartingRetries == -1)
            {
                r = g_ui32DefaultRetries;
            }
            else
            {
                r = hTrans->i8StartingRetries;
            }
            LOG_WARNING("Failed to TX one-way command within %d retries\n", r);
        }

        goto exit;
    }

    // we need a response so we're going to need a response buffer...
    if(hTrans->pResponse == NULL)
    {
        hTrans->pResponse = calloc(1, ENPHASE_MAX_PACKET);
        bAllocRsp         = true;
    }

    if(hTrans->ui8Limit > 1)
    {
        uint32_t limit = 24;

        rc = enphase_execute_cb(hTrans->hScooter, hTrans->pCommand,
                                hTrans->pResponse, multicast_callback, hTrans,
                                limit, hTrans->v2flags, hTrans->msgLen);

        LOG_VERBOSE_MED(
            "Multicast transaction @ protolevel %d (dlmask = 0x%08x) returns "
            "rc %d\n",
            7, 0, rc);

        //
        // If execute callback fails then exit out of the transaction
        //
        if(rc < 0)
        {
            LOG_VERBOSE_LO("\n");
            hTrans->state = XACT_STATE_FAILED;
            goto exit;
        }

        //
        // if the client didn't supply a group handle then we can't do
        // any error checking.  Take an early exit
        //

        LOG_VERBOSE_LO("\n");
        hTrans->state = XACT_STATE_IDLE;
        rc            = 0;
        goto exit;
    }
    else if(hTrans->ui8Limit == 1)
    {
        do
        {
            rc = enphase_execute(hTrans->hScooter, hTrans->pCommand,
                                 hTrans->pResponse, hTrans->v2flags,
                                 hTrans->msgLen, hTrans->bResponse, rspMsgLen);
            LOG_VERBOSE_MED("Unicast transaction (retries = %d) returns %d\n",
                            hTrans->ui8Retries, rc);

            hTrans->state = XACT_STATE_IDLE;

            if(!rc)
            {
                rc = hTrans->callback(hTrans->pResponse, *rspMsgLen,
                                      hTrans->pvCookie);
                goto exit;
            }
            else
            {
                hTrans->state = XACT_STATE_FAILED;
                LOG_WARNING(
                    "Failed to complete unicast transaction, rc = %d, retries "
                    "= %d\n",
                    rc, hTrans->ui8Retries);
            }
        } while(rc && hTrans->ui8Retries--);
        goto exit;
    }
    else
    {
        LOG_ERROR("ERROR: Can't handle requested addr_mode\n");
    }
    LOG_ERROR("Probably shouldn't get here!\n");

exit:
    if(bAllocCmd)
    {
        free(hTrans->pCommand);
        hTrans->pCommand = NULL;
    }
    if(bAllocRsp)
    {
        free(hTrans->pResponse);
        hTrans->pResponse = NULL;
    }
    if((hTrans->proctime > 0))
    {
        LOG_VERBOSE_LO("quick transaction, sleep for %d ms\n",
                       hTrans->proctime);
        tsleep(1000 * hTrans->proctime);
    }
    if(hTrans->bOneshot)
    {
        e_transaction_delete(hTrans);
    }

    return rc;
}

/*!
 * @brief Execute the transaction on the specified connection
 *
 * @param hTrans is a handle to the transaction
 * @param hSooter is a handle a ScooterAPI instance
 *
 * @return 0 for success, < 0 for error
 *
 * @note An error frome_transactionexecute is an indication that either:
 *   1. the request did not get transmitted
 *  OR
 *   2. Not all expected MIs responded
 */
int32_t e_transaction_execute(e_transaction_t hTrans, hScooter_t hScooter,
                              uint32_t *msgRespLen)
{
    if(!hScooter) return -EINVAL;

    return _xaction_execute(hTrans, hScooter, msgRespLen);
}

/*!
 * @brief set the global debug flags for the transaction module
 *
 * @param ui32Flags can be LOG_LEVEL_CRIT, LOG_LEVEL_WARNING, LOG_LEVEL_INFO,
 *        or LOG_LEVEL_VERBOSE
 *
 * @return 0 for success
 */
int32_t e_transactiondebugflags_set(uint32_t ui32Flags)
{
    debug = ui32Flags;
    return 0;
}

//! @}

#ifdef INCLUDE_TESTS

/*!
 * \addtogroup tests
 * @{
 */

/*!
 * \brief Some white and black box tests for SunSpec block storage functions.
 */
void test_SSBlockStorageFree()
{
    /*
     * \details These white and black box tests do all sorts of things.
     */
}

/*! Close the doxygen group
 * @}
 */
#endif

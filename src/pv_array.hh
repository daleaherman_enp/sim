// Copyright (c) 2018-2019 Enphase Energy, Inc. All rights reserved.
//
// This file contains reference encoder functions for converting
// a SunPower AGF Profile into Enphase PCU AGF Records
//

#ifndef _PV_ARRAY_HH_
#define _PV_ARRAY_HH_
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#include <sys/time.h>
#endif // !WIN32
#include <assert.h>
#include <list>
//#include "plc_tool.h"
#include "sbt/log.h"
#include "sbt/errno.h"
#include "sbt/connection.h"
//#include "sbt/xaction.h"
#include "sbt/scooter_uart.h"

#include <list>
#include "enphase/enph_xaction.h"


class cPV_ARRAY
{
public:
	cPV_ARRAY(const char *connection, char *name);
	~cPV_ARRAY();
	int OpenConnection(uint32_t timeout_ms);
	int WaitPLCMessage(uint32_t timeout_ms);
	int ConfigurationMessage();
	int AddPCU(cPCUSim *pcu);
	int Ticker(int num);
	int SetOperating(float freq, float acvRms, float dcmv);
	// 

private:
    uint8_t  m_msgBuf[1024];
	uint8_t m_outBuf[1024];
    char *m_connection;
	char *m_name;
	connection_handle_t m_hConn;
	e_transaction_t m_hTrans;
	hScooter_t m_hScooter;
	// last PMI, only change when needed...
	tPmiDataConfigData lastPMIData;
	std::list<cPCUSim*> m_cPCUList;
};







#endif   // _PV_ARRAY_HH_


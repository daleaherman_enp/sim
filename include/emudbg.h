/* Copyright (c) 2006-2008 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _emudbg_h_
#define _emudbg_h_


/** @file emudbg.h
 *  @brief EMU Debug support
 *
 *  This file @c emudbg.h provides various EMU debug support.
 *
 */

/** @page emudbgHPg Header: emudbg.h EMU debug support

@section emudbgHName emudbg.h HEADER NAME

    emudbg.h -- EMU debug support

@section emudbgHSynop emudbg.h SYNOPSIS

    This file provides the EMU debug support

@section emudbgHDesc emudbg.h DESCRIPTION

    This file, @c emudbg.h provides EMU debug support.

*/


/* system headers */
#include <stddef.h>
#include <stdlib.h>
#include <stdarg.h>

#include "include/enphase.h"

/**
 * @defgroup emudbgHGroups emudbg Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup emudbgHDefines emudbg Header Defines
 */
/*@{*/


#define mEmuAssert(e)
#define mEmuDbgChk(_dbglvl)   (0)
#define mEmuErrDumpCore
#define mEmuErrSysExit
#define mEmuErrSysRet
#define mEmuErr
#define mEmuErrQuit
#define mEmuPrt
#define mEmuDbg
#define mEmuVerbose

#define mXEmuErrDumpCore
#define mXEmuErrSysExit
#define mXEmuErrSysRet
#define mXEmuErr
#define mXEmuErrQuit
#define mXEmuPrt
#define mXEmuDbg
#define mXEmuVerbose
#define mEmuSysCmd

#define mDbgBitSet(_idx,_bnum)
#define mDbgBitClr(_idx,_bnum)
#define mDbgCntInc(_idx)
#define mDbgCntDec(_idx)
#define mDbgCntSet(_idx,_val)
#define mDbgCntClt(_idx)


/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs -------------------------- */

/**
 * @defgroup emudbgHTypedefs emudbg Header Typedefs
 */
/*@{*/

/** Remember that you can add your own grouping of definitions as you see fit.
 *
 */

/*@}*/  /* END of emudbg Header Typedef Group */

/* ------------------------------------ Extern Declarations ---------- */

/**
 * @defgroup emudbgHExtdata emudbg Header Extern Data Definitions
 */
/*@{*/


/*@}*/  /* END of emudbg Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup emudbgHGlobdata emudbg Header Global Data Defined
 */
/*@{*/

/*@}*/  /* END of emudbg Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup emudbgHStatdata emudbg Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of emudbg Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup emudbgHProto emudbg Header Prototypes
 */
/*@{*/



/*@}*/  /* END of emudbg Header Prototypes Group */

/*@}*/  /* END emudbg Header Definitions and Declarations */


#endif /* _emudbg_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of emudbg.h */



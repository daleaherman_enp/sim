/*!****************************************************************************
* @file log.h
*  Simple logging macros
*
* @copyright
*  Copyright (c) 2014 SolarBridge Technologies.  All rights reserved.
*
* @created Sep 3, 2014
*
* @author J. Q. Public <j.public@solarbridgetech.com>
*
****************************************************************************/

#ifndef LOG_H_
#define LOG_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "cJSON.h"
#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \addtogroup log_api
 * @{
 */
#define TRACE_RX 0x10
#define TRACE_TX 0x20

#define TRACE_FLAG_VERBOSE    0x08
#define TRACE_FLAG_INFO       0x04
#define TRACE_FLAG_WARNING    0x02
#define TRACE_FLAG_ERROR      0x01

#define LOG_LEVEL_VERBOSE_HI  6
#define LOG_LEVEL_VERBOSE_MED 5
#define LOG_LEVEL_VERBOSE_LO  4
#define LOG_LEVEL_INFO        3
#define LOG_LEVEL_WARNING     2
#define LOG_LEVEL_ERROR       1
#define LOG_LEVEL_CRIT        0

#define TRACE_VERBOSE(flags, ...) do{if(flags & TRACE_FLAG_VERBOSE) fprintf(stderr, __VA_ARGS__); } while(0)
#define TRACE_INFO(flags, ...) do{if(flags & TRACE_FLAG_INFO) fprintf(stderr, __VA_ARGS__); } while(0)

extern int32_t LogInit(FILE *fout, FILE *fconsole);
extern void LogDeinit();
extern int32_t LogLevelSet(const char *pModule, int32_t i32Level);
extern int32_t LogConsoleSet(FILE *f);
extern int32_t LogFileSet(FILE *f);
extern cJSON* LogConfigGet();

extern void LogMessage(uint32_t ui32Level, const char *pModule,
                       const char *pFunc, uint32_t line,
                       const char *pFmt, ...);

#ifndef WIN32
#define LOG_VERBOSE_HI(...)  do{ LogMessage(LOG_LEVEL_VERBOSE_HI,  __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_VERBOSE_MED(...) do{ LogMessage(LOG_LEVEL_VERBOSE_MED, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_VERBOSE_LO(...)  do{ LogMessage(LOG_LEVEL_VERBOSE_LO,  __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_INFO(...)        do{ LogMessage(LOG_LEVEL_INFO,        __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_WARNING(...)     do{ LogMessage(LOG_LEVEL_WARNING,     __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_ERROR(...)       do{ LogMessage(LOG_LEVEL_ERROR,       __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_CRIT(...)        do{ LogMessage(LOG_LEVEL_CRIT,        __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#else
#ifndef WINPLCTOOLS
#define LOG_VERBOSE_HI(...)
#define LOG_VERBOSE_MED(...)
#define LOG_VERBOSE_LO(...)
#define LOG_INFO(...)
#define LOG_WARNING(...)
#define LOG_ERROR(...)
#define LOG_CRIT(...)
#else
#define LOG_VERBOSE_HI(...)  do{ LogMessage(LOG_LEVEL_VERBOSE_HI,  __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_VERBOSE_MED(...) do{ LogMessage(LOG_LEVEL_VERBOSE_MED, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_VERBOSE_LO(...)  do{ LogMessage(LOG_LEVEL_VERBOSE_LO,  __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_INFO(...)        do{ LogMessage(LOG_LEVEL_INFO,        __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_WARNING(...)     do{ LogMessage(LOG_LEVEL_WARNING,     __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_ERROR(...)       do{ LogMessage(LOG_LEVEL_ERROR,       __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#define LOG_CRIT(...)        do{ LogMessage(LOG_LEVEL_CRIT,        __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); } while(0)
#endif
#endif

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif /* LOG_H_ */

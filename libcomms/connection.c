/*!****************************************************************************
* @file connection.c
*  A module for abstracting underlying comms connections
*
* @copyright
*  Copyright (c) 2014 SolarBridge Technologies.  All rights reserved.
*
* @created 20-Oct-2014
*
* @author M. Bintz <m.bintz@solarbridgetech.com>
*
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#ifndef CONNMIME
#define CONNMIME 0
#endif

#include "sbt/errno.h"
#include "sbt/log.h"
#include "sbt/connection.h"
#include "sbt/scooter_uart.h"
#include "sbt/udp.h"

#if CONNMIME
#include "sbt/conn_mime.h"
#endif


struct _connection
{
    eCONNECTION_TYPE type;
    void *pvHandle;

};
//! \addtogroup connection_config
//! @{

#define CONNECTION_DEFAULT_URI  "uart:/dev/ttyUSB0"

//! @}


//! \addtogroup connection_api
//! @{

/*!**************************************************************************
 *
 * Open the connection specified in pPath and place a handle in pConn.
 * If pPath is null then open the default device as configured in
 * \ref CONNECTION_DEFAULT_URI.
 *
 * @param pPath is pointer to a string with a URI to the desired device.  pPath
 *   can be NULL to use the default in CONNECTION_DEFAULT_URI.  Otherwise
 *   pPath should be a URI.  Supported schemes are:
 *      uart:<device>[,<baud>
 *       udp:<ip>:<port>
 *      mime:<mqpath>
 *
 * @param pConn is a pointer to a connection_handle_t which will be allocated
 *   and populated
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionOpen(const char *pPath, connection_handle_t *pConn)
{
    const char *pDevice;
    connection_handle_t hConn;

    if (!pConn)
    {
        return -EINVAL;
    }
    if (!pPath)
    {
        pPath = CONNECTION_DEFAULT_URI;
    }

    hConn = calloc(1, sizeof(struct _connection));
    if (!hConn)
    {
        return -ENOMEM;
    }
    *pConn = hConn;

    //
    // Handle "uart:" or "ftdi:" URI scheme
    // ftdi: has only a reference number instead of a full device path
    //
    if ((strncmp(pPath, "uart:", 5) == 0) ||
	(strncmp(pPath, "ftdi:", 5) == 0))
    {
        int32_t rc;

        if(0 == UartConnectionNew((scooter_uart_handle_t*)&hConn->pvHandle))
        {
            pDevice = pPath + 5;
            rc = UartDeviceOpen(hConn->pvHandle, pDevice);
            if (0 == rc)
            {
                hConn->type = CONNECTION_TYPE_UART;
            }
            return rc;
        }
    }

    


    //
    // Handle unknown URI scheme
    //
    else
    {
        LOG_ERROR("'%5.5s' is an unsupported URI scheme\n", pPath);
        return -EINVAL;
    }

    return 0;
}

/*!**************************************************************************
 *
 * Close the given connection handle
 *
 * @param hConn is the connection to close
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionClose(connection_handle_t hConn)
{
    int32_t rc = -EFAULT;

    if (!hConn)
    {
        LOG_WARNING("Attempting to close invalid connection\n");
        return -EINVAL;
    }

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            rc = UartClose(hConn->pvHandle);
            if(rc < 0) break;

            //
            // Free the resources
            //
            rc = UartConnectionDelete(hConn->pvHandle);
            break;

        
        default:
            fprintf(stderr, "Unknown connection type %d\n", hConn->type);
            assert(0);
    }

    memset(hConn, 0xfe, sizeof(struct _connection));
    free(hConn);
    return rc;
}

/*!**************************************************************************
 *
 * Set the specified debug flags on the given connection
 *
 * @param hConn is the handle to the connection to modify
 * @param ui32Flags are the flags to set and can be one of: \ref LOG_FLAG_VERBOSE,
 *   \ref LOG_FLAG_INFO, \ref LOG_FLAG_WARNING, \ref LOG_FLAG_ERROR
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionDebugSet(connection_handle_t hConn, uint32_t ui32Flags)
{
    if (!hConn)
    {
        return -EINVAL;
    }


    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            return UartDebugSet(hConn->pvHandle, ui32Flags);
		default:
            assert(0);
    }
    return -EFAULT;

}

/*!**************************************************************************
 *
 * Set the timeout when attempting to read a byte
 *
 * @param hConn is a handle to a connection to set the timeout on
 * @param timeout is in ms
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionTimeoutReadSet(connection_handle_t hConn, uint32_t timeout)
{
    if (!hConn)
    {
        return -EINVAL;
    }

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            return UartTimeoutReadSet(hConn->pvHandle, timeout);
		default:
            assert(0);
    }
    return -EFAULT;
}

/*!**************************************************************************
 *
 * Get the timeout when attempting to read a byte
 *
 * @param hConn is a handle to a connection to set the timeout on
 * @param timeout is in ms
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionTimeoutReadGet(connection_handle_t hConn, uint32_t *pTimeout)
{
    if (!hConn)
    {
        return -EINVAL;
    }

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            return UartTimeoutReadGet(hConn->pvHandle, pTimeout);
		
        default:
            assert(0);
    }
    return -EFAULT;
}

/*!**************************************************************************
 *
 * Flush out any pending characters
 *
 * @param hConn is a handle to a connection to flush
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionFlush(connection_handle_t hConn)
{
    if (!hConn)
    {
        return -EINVAL;
    }

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            return UartFlush(hConn->pvHandle);
        
        default:
            assert(0);
    }
    return -EFAULT;
}

/*!**************************************************************************
 *
 * Read into packet from hConn.
 *
 * @param hConn is a handle to the connection to read
 * @param packet is pointer to storage in which to place the packet.  packet
 *   should be able to store at least \ref COBS_MAX_BLOCK bytes
 * @param length returns the amount of data read
 * @param device returns the device the packet is from
 * @param lun returns the logical unit the packet was from
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionRead(connection_handle_t hConn, uint8_t *packet,
                       size_t *length, int32_t *device, int32_t *lun)
{
    int rc = -EFAULT;

    if (!hConn)
    {
        return -EINVAL;
    }

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            rc = UartRead(hConn->pvHandle, packet, length, device);
            break;
        
        default:
            assert(0);
    }
    return rc;

}

/*!**************************************************************************
 *
 * Write packet to hConn.
 *
 * @param hConn is a handle to the connection to read
 * @param packet is pointer to the packet to transmit
 * @param length is the length of packet
 * @param device specifies which logical device the packet is destined for
 * @param lun specifies the logical unit the packet is destined for
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionWrite(connection_handle_t hConn, const uint8_t *packet,
                        size_t length, int32_t device, int32_t lun)
{
    if (!hConn)
    {
        return -EINVAL;
    }

    // fprintf(stderr, "%s ====================\n", __FUNCTION__);
    // hexdump(packet, 16);

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            return UartWrite(hConn->pvHandle, packet, length, device);
        
        default:
            assert(0);
    }
    return -EFAULT;
}

int32_t ConnectionType(connection_handle_t hConn)
{
    return hConn->type;
}
/*!**************************************************************************
 *
 * Write packet to hConn without blocking.
 *
 * @param hConn is a handle to the connection to read
 * @param packet is pointer to the packet to transmit
 * @param length is the length of packet
 * @param device specifies which logical device the packet is destined for
 * @param lun specifies the logical unit the packet is destined for
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionRawWrite(connection_handle_t hConn, const uint8_t *packet,
                        size_t length, int32_t device, int32_t lun)
{
    if (!hConn)
    {
        return -EINVAL;
    }

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            return UartRawWrite(hConn->pvHandle, packet, length, device);
        
        default:
            assert(0);
    }
    return -EFAULT;
}

/*!**************************************************************************
 *
 * Write packet to hConn without blocking or any hints about the underlying
 * protocol or framing.  This is a "Layer 2" write only.  With "Layer 3"
 * protocol handling (like acks and retries) moving into the upper layer.
 *
 * @param hConn is a handle to the connection to read
 * @param packet is pointer to the packet to transmit
 * @param length is the length of packet
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionWriteL2(connection_handle_t hConn, const uint8_t *packet,
                        size_t length)
{
    if (!hConn)
    {
        return -EINVAL;
    }

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            return UartWriteL2(hConn->pvHandle, packet, length);
        
        default:
            assert(0);
    }
    return -EFAULT;
}

/*!**************************************************************************
 *
 * Read into packet from hConn without any hints about the underlying
 * protocol or framing.  This is a "Layer 2" read only.  With "Layer 3"
 * protocol handling (like acks and retries) moving into the upper layer.
 *
 * @param hConn is a handle to the connection to read
 * @param packet is pointer to storage in which to place the packet.  packet
 *   should be able to store at least \ref COBS_MAX_BLOCK bytes
 * @param length returns the amount of data read
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t ConnectionReadL2(connection_handle_t hConn, uint8_t *packet,
                         size_t *length, uint32_t timeout)
{
    int rc = -EFAULT;

    if (!hConn)
    {
        return -EINVAL;
    }

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            rc = UartReadL2(hConn->pvHandle, packet, length, timeout);
            break;
        
        default:
            assert(0);
    }
    return rc;

}

int32_t ConnectionRegisterCallback(connection_handle_t hConn,
                                       pfnConnCB_t pfn, void *pCookie,
                                       pfnConnCB_t *oldpfn, void **ppOldcookie)
{
    int rc = -EFAULT;

    if (!hConn)
    {
        return -EINVAL;
    }

    switch (hConn->type)
    {
        case CONNECTION_TYPE_UART:
            rc = UartRegisterCallback(hConn->pvHandle, pfn, pCookie, oldpfn, ppOldcookie);
            break;
        
        default:
        	LOG_ERROR("Invalid connection type: %d\n",hConn->type);
            assert(0);
    }
    return rc;
}

//! @}


#if defined SELFTEST
#include <stdio.h>
#include <string.h>

//! \addtogroup connection_tests
//! @{

#define TEST(desc, call, test, rc, line) \
    do { \
        printf("%40.40s:", desc); \
        rc = call; \
        if (rc test) { \
            printf(" [PASS]\n"); \
        } else { \
            printf(" [FAIL] (line %d)\n", line); \
            bFail = true; \
        } \
    } while(0)

int main(int argc, char*argv[])
{
    connection_handle_t hConn;
    int32_t rc;
    bool bFail = false;

    TEST("Open with bad parameters", ConnectionOpen(NULL, NULL), <= 0, rc, __LINE__);
    TEST("Open with good parameters, bad URI", ConnectionOpen("foo:/yuck", &hConn), <= 0, rc, __LINE__);
    TEST("Open with good parameters, bad device", ConnectionOpen("uart:/yuck", &hConn), <= 0, rc, __LINE__);

    TEST("Close with bad parameters", ConnectionClose(NULL), <= 0, rc, __LINE__);

    TEST("Open with good parameters, good device", ConnectionOpen("uart:/dev/ttyUSB0", &hConn), >= 0, rc, __LINE__);
    if(rc >= 0)
    {
        TEST("Set all logging", ConnectionDebugSet(hConn, 0xF), >= 0, rc, __LINE__);
        TEST("Close with good parameters", ConnectionClose(hConn), >= 0, rc, __LINE__);
    }

    TEST("Open with good parameters, no device", ConnectionOpen(NULL, &hConn), >= 0, rc, __LINE__);
    assert(0 == rc);

    printf("NOTE: read and write not tested here until there is a COBS echo\n");
    return bFail;

}
//! @}
#endif

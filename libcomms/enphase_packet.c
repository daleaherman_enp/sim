/*!
 * @copyright
 *  Copyright 2018, SunPower Corporation.  All rights reserved.
 *
 * @file enphase_packet.c
 *  enphase Packet Building Functions
 *
 * @date July 5, 2018
 * @author 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <memory.h>
#include <inttypes.h>
#include <assert.h>

// #include "sbt/log.h"

#include "enphase/enphase_packet.h"
#include "enphase/mcp_enphase.h"
#ifdef _WIN32
#include "sbt/portable_endian.h"
#endif
#include "sbt/errno.h"
#include "enphase/mcp_enphase.h"
#include "mcp_sunpower.h"
#ifndef DEPRECATED
#ifdef WIN32
#define DEPRECATED(func)
#else
#define DEPRECATED(func) func __attribute__ ((deprecated))
#endif
#endif

/*!
 * \addtogroup enphase_packet_api
 * @{
 */


#if !defined(USE_SLOW_CRC) || defined(TEST_CRC)
/*!
 * @brief A table of pre-computed CRC values using the polynomial
 *  x^16 + x^15 + x^2 + 1 and an initial CRC value of 0x0000.
 *
 * @details This table is computed in least-significant-bit-first order,
 *  so the value used is 0xA001, which is that polynomial, skipping
 *  the x^16 term, in reversed bit order.
 *
 * @note Replace the description above if the polynomial or initial
 *  value is changed.
 *
 * @note The CRC-16 value used for enphase and Modbus uses an initial value
 *  of 0xFFFF.  The CRC-16 value used for Pantheon uses an initial
 *  value of 0x0000.  The UART CRC, which is defined elsewhere, uses
 *  a different polynomial.
 */
uint16_t enphase_crc_table_values[256] =
{
    0x0000, 0xc0c1, 0xc181, 0x0140, 0xc301, 0x03c0, 0x0280, 0xc241,
    0xc601, 0x06c0, 0x0780, 0xc741, 0x0500, 0xc5c1, 0xc481, 0x0440,
    0xcc01, 0x0cc0, 0x0d80, 0xcd41, 0x0f00, 0xcfc1, 0xce81, 0x0e40,
    0x0a00, 0xcac1, 0xcb81, 0x0b40, 0xc901, 0x09c0, 0x0880, 0xc841,
    0xd801, 0x18c0, 0x1980, 0xd941, 0x1b00, 0xdbc1, 0xda81, 0x1a40,
    0x1e00, 0xdec1, 0xdf81, 0x1f40, 0xdd01, 0x1dc0, 0x1c80, 0xdc41,
    0x1400, 0xd4c1, 0xd581, 0x1540, 0xd701, 0x17c0, 0x1680, 0xd641,
    0xd201, 0x12c0, 0x1380, 0xd341, 0x1100, 0xd1c1, 0xd081, 0x1040,
    0xf001, 0x30c0, 0x3180, 0xf141, 0x3300, 0xf3c1, 0xf281, 0x3240,
    0x3600, 0xf6c1, 0xf781, 0x3740, 0xf501, 0x35c0, 0x3480, 0xf441,
    0x3c00, 0xfcc1, 0xfd81, 0x3d40, 0xff01, 0x3fc0, 0x3e80, 0xfe41,
    0xfa01, 0x3ac0, 0x3b80, 0xfb41, 0x3900, 0xf9c1, 0xf881, 0x3840,
    0x2800, 0xe8c1, 0xe981, 0x2940, 0xeb01, 0x2bc0, 0x2a80, 0xea41,
    0xee01, 0x2ec0, 0x2f80, 0xef41, 0x2d00, 0xedc1, 0xec81, 0x2c40,
    0xe401, 0x24c0, 0x2580, 0xe541, 0x2700, 0xe7c1, 0xe681, 0x2640,
    0x2200, 0xe2c1, 0xe381, 0x2340, 0xe101, 0x21c0, 0x2080, 0xe041,
    0xa001, 0x60c0, 0x6180, 0xa141, 0x6300, 0xa3c1, 0xa281, 0x6240,
    0x6600, 0xa6c1, 0xa781, 0x6740, 0xa501, 0x65c0, 0x6480, 0xa441,
    0x6c00, 0xacc1, 0xad81, 0x6d40, 0xaf01, 0x6fc0, 0x6e80, 0xae41,
    0xaa01, 0x6ac0, 0x6b80, 0xab41, 0x6900, 0xa9c1, 0xa881, 0x6840,
    0x7800, 0xb8c1, 0xb981, 0x7940, 0xbb01, 0x7bc0, 0x7a80, 0xba41,
    0xbe01, 0x7ec0, 0x7f80, 0xbf41, 0x7d00, 0xbdc1, 0xbc81, 0x7c40,
    0xb401, 0x74c0, 0x7580, 0xb541, 0x7700, 0xb7c1, 0xb681, 0x7640,
    0x7200, 0xb2c1, 0xb381, 0x7340, 0xb101, 0x71c0, 0x7080, 0xb041,
    0x5000, 0x90c1, 0x9181, 0x5140, 0x9301, 0x53c0, 0x5280, 0x9241,
    0x9601, 0x56c0, 0x5780, 0x9741, 0x5500, 0x95c1, 0x9481, 0x5440,
    0x9c01, 0x5cc0, 0x5d80, 0x9d41, 0x5f00, 0x9fc1, 0x9e81, 0x5e40,
    0x5a00, 0x9ac1, 0x9b81, 0x5b40, 0x9901, 0x59c0, 0x5880, 0x9841,
    0x8801, 0x48c0, 0x4980, 0x8941, 0x4b00, 0x8bc1, 0x8a81, 0x4a40,
    0x4e00, 0x8ec1, 0x8f81, 0x4f40, 0x8d01, 0x4dc0, 0x4c80, 0x8c41,
    0x4400, 0x84c1, 0x8581, 0x4540, 0x8701, 0x47c0, 0x4680, 0x8641,
    0x8201, 0x42c0, 0x4380, 0x8341, 0x4100, 0x81c1, 0x8081, 0x4040
};
#endif


#if defined(USE_SLOW_CRC) || defined(TEST_CRC) || defined(UNIT_TEST)
/*!
 * @details Compute the CRC-16 value of the data without a look-up table.
 *
 * @param data - The data used to compute the CRC-16.
 * @param cnt - The size of data in bytes.
 * @param init - Initial CRC value.
 *
 * @returns The CRC-16 computed over the specified data range.
 *
 * @note This is a "slow" algorithm which conserves data space by not
 *  using a 512 byte look-up table.
 * @note The CRC-16 value used for Modbus uses an initial value of 0xFFFF.
 *  The CRC-16 value used for Pantheon uses an initial value of 0x0000.
 */
static uint16_t calc_crc16_slow(uint8_t *data, size_t cnt, uint16_t init)
{
    uint8_t byte, bit;
    uint16_t result = 0;

    result = init;

    for (byte = 0; byte < cnt; byte++)
    {
        result ^= data[byte];

        for (bit = 0; bit < 8; bit++)
        {
            if (result & 0x0001)
            {
                result = (result >> 1) ^ 0xA001;
            }
            else
            {
                result = (result >> 1);
            }
        }
    }
    return result;
}
#endif


#if !defined(USE_SLOW_CRC) || defined(TEST_CRC) || defined(UNIT_TEST)
/*!
 * @details Compute the CRC-16 value of the data with a look-up table.
 *
 * @param data - The data used to compute the CRC-16.
 * @param size - The size of data in bytes.
 * @param init - The initial CRC value
 *
 * @returns The CRC-16 computed over the specified data range.
 *
 * @note This is a "fast" algorithm which uses a pre-computed lookup
 *  table and no bit-banging.
 */
static uint16_t calc_crc16_fast(const uint8_t *data, size_t size, uint16_t init)
{
    uint16_t crc = init;
    size_t i;

    for (i = 0; i < size; i++)
        crc = ((crc >> 8) & 0xFF)
              ^ enphase_crc_table_values[(crc ^ data[i]) & 0xFF];

    return crc;
}
#endif


/*!
 * @details Initialize an enphase PLC packet to the empty state by setting the
 * length of the packet to include only the combined packet header.
 *
 * @returns - EINVAL for null packet pointer otherwise SUCCESS
 * @param packet - A pointer to where the packet will be located.
 */
int32_t enphase_init_packet(uint8_t *packet)
{
	tMcpAddressHdr *pMsg = (tMcpAddressHdr *)packet;
	if (packet == NULL)
    {
        return -EINVAL;
    }

	pMsg->msgLen = sizeof(tMcpMsgrHdrCombined); // at least this big
    return SUCCESS;
}


/*!
 * @details Get the packet header as an unpacked structure, returning the actual
 *  structure itself.
 *
 * @returns A @ref tMcpAddressHdr containing the addressing mode information
 * or EINVAL for null packet pointer
 *
 * @param packet - A pointer to where the packet is located
 */
int32_t enphase_get_type(const uint8_t *packet, tMcpAddressHdr *result)
{
    if ((packet == NULL) || (result == NULL))
    {
        return -EINVAL;
    }

    memcpy(result, packet, sizeof(tMcpAddressHdr));

    return SUCCESS;
}

/*!
 * @details Return true if the referenced packet has a flag set requesting a
 *   response from the MI
 *
 * @param packet is a pointer to the packet in question
 *
 * @return TRUE if a response is expected, FALSE if not
 *
 */
int32_t enphase_response_set(const uint8_t *command)
{
    tMcpAddressHdr hdr;
    enphase_get_type(command, &hdr);

    //
    // We only want to wait for a response if the command had bRespond == TRUE
    // Note this relies on the union of mi_pm and pm_mi packet type falling in
    // the same location
    //
    
    if (hdr.bResponse)  return true;
    
    return false;

}
/*!**************************************************************************
 *
 * Set the address in packet to that of pAddr
 *
 * @param pPacket is a pointer to a packet in which to set the address
 * @param pAddr is the address structure to set in pPacket
 *
 * @return 0 for success.  Less than 0 for error.
 *
 * @note This function must be called after \ref enphase_set_type and before
 *       \ref enphase_set_cmd due to the way pPacket[0] is used to track
 *       packet length.
 ***************************************************************************/
int32_t enphase_set_address(uint8_t *pPacket, enphase_address_t *pAddr)
{
    tMcpMsgrHdrCombined *pHdr =
        (tMcpMsgrHdrCombined *) pPacket;
    uint16_t *pGM = (uint16_t *)&pPacket[sizeof(tMcpMsgrHdrCombined)];

    if ((pPacket == NULL) || (pAddr == NULL))
    {
        return -EINVAL;
    }
    // TODO: Should we always set the gateway MAC
    pHdr->mAddressHdr.gatewayMACID = pAddr->enphase_gateway_serial;
    switch (pAddr->addr_mode) 
    {
    case ENPH_MODE_MAC_SERIAL_NUMBER:
        // special case for association
        pHdr->mAddressHdr.bBroadcast = 1;
        pHdr->mAddressHdr.bResponse = 0;
        // allow message to define the rest...
        break;
    case ENPH_ADDR_MODE_LOGICAL_NUMBER:
        pHdr->mAddressHdr.bBroadcast = 0;
        pHdr->mAddressHdr.bResponse = pAddr->bResponseRequested;
        pHdr->mAddressHdr.logicalAddress = pAddr->addr_val.logicalAddress;
        break;
    case ENPH_ADDR_MODE_BROADCAST_GROUPMASK:
        pHdr->mAddressHdr.bBroadcast = 1;
        pHdr->mAddressHdr.bResponse = 0;
        *pGM = pAddr->addr_val.groupMask;  // TODO: do all groupmask messages start with the groupMask???
        break;
    }

    return SUCCESS;
}

/*!
 * @details Set the packet type and indicate if a response is expected.  A
 * response is required for commands which will have their results examined,
 * but is not required, even for commands where ignoring the response seems
 * pointless.
 *
 * @returns - -EINVAL for null packet pointer, -EINVAL for invalid type or SUCCESS
 *
 * @param packet - A pointer to where the packet will be located
 * @param type - A @ref ePACKET_TYPES value which defines the packet type.
 * @param respond - Indicates that a response is desired from the microinvert(s)
 */
int32_t enphase_set_ver(uint8_t *pPacket,
                          uint8_t ver)
{
    tMcpMsgrHdrCombined *pHdr =
        (tMcpMsgrHdrCombined *) pPacket;

    if (packet == NULL)
    {
        return -EINVAL;
    }
    
    pHdr->mMsgHdr.ctrlAndMcpProtoVers = ver;
    return SUCCESS;
}

/*!
 * @details Get the short serial number as an unpacked structure, returning
 *  the actual @ref tPLC_SHORT_SERIAL_ADDRESS structure.
 *
 * @param packet - A pointer to where the packet is located
 * @param result - A pointer to where the @ref tPLC_SHORT_SERIAL_ADDRESS short address should be stored
 * @returns EINVAL for null packet or result pointer.
 */
int32_t enphase_get_logical_address(const uint8_t *pPacket,
                                uint16_t *result)
{

    uint16_t address = 0;
    tMcpMsgrHdrCombined *pHdr =
        (tMcpMsgrHdrCombined *) pPacket;
    if ((packet == NULL) || (result == NULL))
    {
        return -EINVAL;
    }

    *result = pHdr->mAddressHdr.logicalAddress;
    return SUCCESS;
}

/*!
 * @details Returns a structure containing the address mode and the complete
 * address.
 *
 * @param packet - A pointer to the packet.
 * @param address is a pointer to storage provided by the caller in which to
 *        return the address
 *
 * @returns 0 for success, < 0 for error.
 *
 * @note Any 16 and 32 bit fields are stored in network byte order.
 */
int32_t enphase_get_address(const uint8_t *packet, tMcpAddressHdr *address)
{
    int32_t result;

    if ((packet == NULL) || (address == NULL))
    {
        return -EINVAL;
    }

 
    memcpy(address, packet, sizeof(tMcpAddressHdr));
          
    return SUCCESS;
}

//! comapre p1 to p2.  Use the same style is strcmp so this function could be
//! used with sort() if desired.
//!
//! @param p1 pointer to address 1
//! @param p2 pointer to address 2
//! @return 0 of p1 == p2, < 0 if p1 < p2, > 0 if p1 > p2
int32_t enphase_address_compare(tMcpAddressHdr *p1, tMcpAddressHdr *p2)
{
    if (p1->gatewayMACID != p2->gatewayMACID || p1->logicalAddress != p2->logicalAddress) 
    {
        return 1;
    }
            
	return 0;
    
}

/*!
 * @details Convert an addressing mode structure, which contains both the
 * addressing mode and the actual address, into a text string representing the
 * address.
 *
 * @param address - A pointer to an enphase_address_t containing the address
 * @param string - A pointer to a char array which will contain the textual
 * representation of address.
 */
int32_t enphase_format_address(const tMcpAddressHdr *address, char *string)
{
    if (address == NULL || string == NULL)
    {
        return -EINVAL;
    }

 

    /*
     * This can't be reached, but the compiler might be too stupid to figure that out.
     */
    return -EINVAL;
}


/*!
 * @details Match two different addresses
 *
 * @param addr_1 - The first address ...
 * @param addr_2 - The second address ...
 */
int32_t enphase_match_addresses(const tMcpAddressHdr *addr_1,
                               const tMcpAddressHdr *addr_2)
{
    if ((addr_1 == NULL) || (addr_2 == NULL))
    {
        return -EINVAL;
    }

 

    return SUCCESS;
}

/*!
 * @details Match two packets
 */
#include "sbt/log.h"
int32_t enphase_match_packets(const uint8_t *packet_1, const uint8_t *packet_2)
{
    int32_t result;
    enphase_address_t addr_1, addr_2;
    int c1, c2;

    if ((packet_1 == NULL) || (packet_2 == NULL))
    {
        LOG_WARNING("\n");
        return -EINVAL;
    }

    if ((result = enphase_get_address(packet_1, &addr_1)) < 0)
    {
         LOG_VERBOSE_MED("Invalid address packet1\n");
        return result;
    }

    

    if ((result = enphase_get_address(packet_2, &addr_2)) < 0)
    {
        LOG_WARNING("Invalid address packet2\n");
        return result;
    }

    result = enphase_address_compare(&addr_1, &addr_2);
    if (result < 0)
    {
       uint8_t * p1 = (uint8_t *) &addr_1;
       uint8_t * p2 = (uint8_t *) &addr_2;
       LOG_VERBOSE_LO("Address mismatch packet1 %0x2 %02x %02x packet2 %02x %02x %02x\n",
                      p1[0], p1[1], p1[2], p2[0], p2[1], p2[2]);
        return result;
    }
#if 0
    LOG_VERBOSE_HI("Address matches\n");
    c1 = enphase_get_cmd(packet_1);
    c2 = enphase_get_cmd(packet_2);

     // cmd/response are not matching so will need to map..
    

    return (c1 == c2 ? SUCCESS :
            -SBT_EMISMATCH);
#else
	return SUCCESS;
#endif
}

/*!
 * Sets the command value in the packet.
 *
 * @details The command resets the length of the packet to refer to the
 *  next byte immediately after the command value.
 *
 * @param packet - A pointer to where the packet will be located.
 * @param command - The @ref ePLC_COMMAND_TYPES command to be executed by the
 * microinverter.
 */
int32_t enphase_set_cmd(uint8_t *packet, eMcpMsgId command)
{

    tMcpMsgrHdrCombined *pHdr = (tMcpMsgrHdrCombined *)packet;
    if (packet == NULL)
    {
        return -EINVAL;
    }

    
    pHdr->mMsgHdr.msgId = command;

    
    return SUCCESS;
}

/*!
 * Gets the command value from the packet.
 *
 * @param packet - A pointer to where the packet will be located
 */
int32_t enphase_get_cmd(const uint8_t *packet)
{

    if (packet == NULL)
    {
        return -EINVAL;
    }
     tMcpMsgrHdrCombined *pHdr = (tMcpMsgrHdrCombined *)packet;
    return (eMcpMsgId) (pHdr->mMsgHdr.msgId);
}

/*!
 * Stores payload data in the packet.  The payload data must
 *  already be in network byte order.
 *
 * @param packet - A pointer to where the packet will be located.
 * @param payload - The data to be copied into the packet.
 * @param length - The length of the payload data in bytes.
 */
int32_t enphase_set_payload(uint8_t *packet, uint8_t *payload, size_t length)
{
    if ((packet == NULL) || (payload == NULL))
    {
        return -EINVAL;
    }

    int payload_offset = sizeof(tMcpMsgrHdrCombined);

    if ((payload_offset + length + sizeof(uint16_t)) >= ENPHASE_MAX_PACKET)
    {
        return -EINVAL;
    }

    memcpy(packet + payload_offset, payload, length);
   

    return SUCCESS;
}
#if 0
/*!
 * Calculate the CRC-16 of the data in the packet, using the algorith which was
 *  configured with this library.
 *
 * @param packet - A pointer to the data for the CRC calculation.
 *
 * @returns the CRC-16 of the data
 */
uint16_t enphase_calc_crc(const uint8_t *packet)
{
    #ifdef USE_SLOW_CRC
    return calc_crc16_slow(packet, packet[0] - sizeof(uint16_t), 0xFFFF);
    #else
    return calc_crc16_fast(packet, packet[0] - sizeof(uint16_t), 0xFFFF);
    #endif
}

/*!
 * Compute the CRC-16 of the data in packet, then append that value in
 * network byte order to the packet.
 *
 * @param packet - A pointer to the data for the CRC calculation.
 *
 * @returns the CRC-16 of the data
 */
uint16_t enphase_set_crc(uint8_t *packet)
{
    uint16_t crc;
    uint8_t *packet_end = &packet[packet[0]];
    int length = packet[0];
    tPLC_PAYLOAD_HDR *header = (tPLC_PAYLOAD_HDR *) &packet[1];

    /*
     * The CRC will be appended to the end, so the length BYTE needs to be
     * changed to its final value, but the length VALUE needs to be the
     * value without the two CRC bytes appended.
     */
    packet[0] = packet[0] + sizeof(uint16_t);
    header->parity = enphase_count_bits(packet[0]) - 1;

    #ifdef USE_SLOW_CRC
    crc = calc_crc16_slow(packet, (size_t) length, 0xFFFF);
    #else
    crc = calc_crc16_fast(packet, (size_t) length, 0xFFFF);
    #endif

    *packet_end++ = (uint8_t) crc;
    *packet_end++ = (uint8_t) (crc >> 8);

    return crc;
}

/*!
 * Retrieve the CRC from the end of a packet.
 *
 * @param packet - A pointer to the data for the CRC.
 *
 * @returns the CRC-16 of the data
 */
int32_t enphase_get_crc(const uint8_t *packet, uint16_t *crc)
{
    if ((packet == NULL) || (packet[0] < 6))
    {
        return -EINVAL;
    }

    *crc = packet[packet[0] - 2] | (packet[packet[0] - 1] << 8);

    return SUCCESS;
}

/*!
 * Re-compute the CRC-16 of the data in packet, then replace the
 * previous value in network byte order.
 *
 * @param packet - A pointer to the data for the CRC calculation.
 *
 * @returns the CRC-16 of the data
 *
 * @todo Need time and space efficient algorithms
 */
int32_t enphase_reset_crc(uint8_t *packet)
{
    if (packet == NULL)
    {
        return -EINVAL;
    }

    if (packet[0] > ENPHASE_GET_COMMAND_OFFSET(packet) + 3)
    {
        packet[0] -= sizeof(uint16_t);
        return enphase_set_crc(packet);
    }
    else
    {
        return -EINVAL;
    }
}

/*!
 * @details Check the CRC in a packet
 *
 * @param packet - A pointer to the packet to be validated
 *
 * @returns 1 = CRC matches, 0 = CRC does not match.
 */
int32_t enphase_check_packet(const uint8_t *packet)
{
    uint16_t packet_crc;
    uint16_t data_crc;

    if (packet == NULL)
    {
        return -EINVAL;
    }

    /*
     * The payload header includes a field, "parity", which is the count of the
     * number of 1 bits in the length field.  If these fields don't match, the
     * length is considered to be wrong and the packet is considered to be
     * invalid.  The "parity" field could actually be invalid, but the packet
     * is still invalid in that case.
     */
    if (packet[0] < 6)
    {
        return -EINVAL;
    }

    if (((tPLC_PAYLOAD_HDR *) (packet + 1))->parity != enphase_count_bits(
                packet[0]) - 1)
    {
        return -EINVAL;
    }

    (void) enphase_get_crc(packet, &packet_crc);
    data_crc = enphase_calc_crc(packet);

    return (packet_crc == data_crc ? SUCCESS : -SBT_EMISMATCH);
}
#endif
/*!
 * @}
 */

#ifdef UNIT_TEST

/*!
 * \addtogroup tests
 * @{
 */
#include <stdio.h>

/*!
 * Create CRC table as C data structure and test correctness of
 *  "fast" and "slow" results.
 */
void create_crc_table_test()
{
    int i;
    char c[1];
    uint16_t crc;

    printf("uint16_t enphase_crc_table_values[256] = {\n\t");
    for (i = 0; i < 256; i++)
    {
        if (i > 0 && i < 256)
        {
            putchar(',');
        }

        if (i != 0 && (i % 8) == 0)
        {
            putchar('\n');
            putchar('\t');
        }
        c[0] = i;
        crc = calc_crc16_slow(c, 1, 0x0000);
        printf(" 0x%04x", crc);
        #ifdef TEST_CRC
        if (crc != enphase_crc_table_values[i])
        {
            fprintf(stderr, "Invalid table value: %d\n", i);
        }
        #endif

        crc = calc_crc16_slow(c, 1, 0xFFFF);

        #ifdef TEST_CRC
        if (crc != calc_crc16_fast(c, 1, 0xFFFF))
            fprintf(stderr, "Invalid fast result %d: expected %04x, got %04x\n",
                    i, crc, calc_crc16_fast(c, 1, 0xFFFF));
        #endif
    }
    printf("\n};\n");
}

/*!
 * @skip main
 */
void main(int argc, char **argv)
{
    create_crc_table_test();
}

/*!
 * @}
 */
#endif

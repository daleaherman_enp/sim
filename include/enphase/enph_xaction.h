/*!****************************************************************************
 * @file xaction.h
 *  prototypes for the transaction object
 *
 * @copyright
 *  Copyright (c) 2014 SolarBridge Technologies.  All rights reserved.
 *
 * @created Sep 3, 2014
 *
 * @author J. Q. Public <j.public@solarbridgetech.com>
 *
 ****************************************************************************/

#ifndef E_XACTION_H_
#define E_XACTION_H_

#include <stdint.h>



#include "sbt/connection.h"
#include "sbt/scooterapi.h"
#include "agf.h"
#include "enphase/mcp_enphase.h"
//#include "mcp_sunpower.h"

#ifdef __cplusplus
extern "C" {
#endif

extern const char* const plccmd_strings[];
//! \addtogroup xaction_api
//! @{

//! Find First Set Bit -- return a 1-based index of first set bit or 0 if no set bits


extern const char* const error_text[];
extern const int n_error_texts;

struct _e_transaction_t;
typedef struct _e_transaction_t *e_transaction_t;

typedef int (*enphCB)(void *, uint8_t *, uint32_t msgLen);

// typedef int32_t (*migrcb_t)(arcpac_address_t *pAddr, void *pvCookie);
typedef int32_t (*e_xactCB)(uint8_t *pResp, uint32_t msgLen, void *pvCookie);

int enphase_write_commandV2(hScooter_t hScooter, const uint8_t *command, uint16_t v2Flags, uint16_t length);
int enphase_read_responseV2(hScooter_t hScooter, const uint8_t *command,
                         uint8_t *response, uint32_t *lenP);
int enphase_execute_cb(void* hScooter, const uint8_t *pCommand, uint8_t *pResponse,
                      int (*callback)(void *, uint8_t *, uint32_t rspLen),
                      void *pvContext, int limit, uint16_t flags, uint16_t cmdLen);
int enphase_execute(void* hScooter,
                     const uint8_t *pCommand, uint8_t *pResponse, uint16_t flags, uint16_t cmdLen, bool resp, uint32_t *rspLen);

int32_t e_transaction_new(e_transaction_t *phTrans);
int32_t e_transaction_delete(e_transaction_t hTrans);
int32_t e_transaction_retries_set(e_transaction_t hTrans, int32_t retries);
int32_t e_transaction_default_retries_set(uint32_t retries);
int32_t e_transaction_default_retries_get();
int32_t e_transaction_command_set(e_transaction_t hTrans,
                                uint8_t cmdtype, void *pvCmdData, uint32_t size);
int32_t e_transaction_callback_set(e_transaction_t hTrans, e_xactCB pfnCb,
                                 void *pvCookie);
int32_t e_transaction_oneshot_set(e_transaction_t hTrans,  bool bOneshot);
int32_t e_transaction_limit_set(e_transaction_t hTrans, uint8_t ui8Limit);
int32_t e_transaction_pmserial_set(e_transaction_t hTrans, uint8_t *gateWayMAC);
int32_t e_transaction_execute(e_transaction_t hTrans, hScooter_t hScooter, uint32_t *lenP);
int32_t e_transactiondebugflags_set(uint32_t ui32Flags);
int32_t e_transaction_flags_set(e_transaction_t hTrans, uint16_t flags);
#ifdef __cplusplus
}
#endif


//! @}
#endif /* XACTION_H_ */

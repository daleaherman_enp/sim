/*
 * @copyright
 * Copyright 2015, Sunpower Corp.
 * All rights reserved.
 *
 * @file os.c
 *  OS abstraction/helper functions
 *
 * @date Apr 9, 2015
 * @author Miles Bintz <mbintz@sunpower.com>
 *
 */

/*!
 * Sleep the calling THREAD (not the whole process) for usec microseconds.
 * If the sleep is interrupted by a signal it will resume sleeping for the
 * remainder of specified time.
 *
 * @param usec is the number of microseconds to sleep for
 *
 * @returns 0 for success, < 0 (-errno) to indicate cause of failure
 */

#ifdef __cplusplus
extern "C" {
#endif


extern int tsleep(uint32_t usec);
extern char* GetChildOutput(const char *argv[], uint32_t timeout);

#ifdef __cplusplus
}
#endif


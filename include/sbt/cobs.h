/*
 * @copyright
 * Copyright 2014, SolarBridge Technologies, LLC
 * All rights reserved.
 *
 * @file cobs.h
 *  Consistent Overhead Byte Stuffing function declarations
 *
 * @date Sep 2, 2014
 *
 * @author Julie Haugh <j.haugh@solarbridgetech.com>
 */

#ifndef __PVSLIB_COBS_H
#define __PVSLIB_COBS_H

#include <sys/types.h>

//! \addtogroup cobs_api
//! @{

//! @brief Maximum run of bytes that can be encoded
#define COBS_MAX_BLOCK  254

//! @brief Compute the required space to store a string of length x.
// #define cobs_get_required_size(x) ((x) + (((x) + COBS_MAX_BLOCK - 1) / COBS_MAX_BLOCK))
// MFB : How is this not always 2?
//#define cobs_get_required_size(x) ((x) + 2)
// Above line changed by Sergio Davila on 8/6/2015.  Buffer requires space to store the leading zero
// the trailing zero AND the COBS consistent additional byte.
#define cobs_get_required_size(x) ((x) + 3)

//! @brief Decode a COBS encoded block of data into the same location as the encoded data.
extern int cobs_decode_in_place(uint8_t *packet, size_t size);

//! @brief Decode a COBS encoded block of data into a user-provided location.
extern int cobs_decode(const uint8_t *in_packet, size_t size,
                       uint8_t *out_packet);

//! @brief COBS encode a block of data into a user-provided location.
extern int cobs_encode(const uint8_t *in_packet, size_t size,
                       uint8_t *out_packet);

//! @}
#endif // __PVSLIB_COBS_H

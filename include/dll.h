/* Copyright (c) 2007-2011 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _dll_h_
#define _dll_h_


/** @file dll.h
 *  @brief Data Link Layer (DLL)
 *
 *  This file @c dll.h provides the Data Link Layer (DLL)
 *
 */

/** @page dllHPg Header: dll.h Data Link Layer (DLL)

@section dllHName dll.h HEADER NAME

    dll.h -- Data Link Layer (DLL)

@section dllHSynop dll.h SYNOPSIS

    This file provides the EMU's Something.

@section dllHDesc dll.h DESCRIPTION

    This file, @c dll.h provides the Data Link Layer (DLL).

*/


/* system headers */

#include "include/enphase.h"
#include "include/pmi.h"
#include "include/ring.h"
#include "include/timer.h"

/**
 * @defgroup dllHGroups dll Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup dllHDefines dll Header Defines
 */
/*@{*/

// In mcpsecmsg.h there is a compile-time check to make sure that
// cAppMsgLenWPadIV is equal to cMcpSecMsgSzForRingElem.
// This size is used to dimension MCP message area available in
// msg rings by virtue of the size of tL2FrmAppMsg which is used to
// size MCP message ring elements.
#define cAppMsgLenWPadIV        512 // MUST be same cMcpSecMsgSzForRingElem


#define DEFINE_BIG_FRAMES

#define cDllTxAppMsgRingSzP2    2
#define cDllTxL2RingSzP2        4
#define cDllRxL2RingSzP2        4


#define cDllRingNameLen            32

#define cDllL2FrameHdrLen          12

// Legacy L2 Frame Length (L2 protoVer (1 and 2))
#define cDllLegMaxL2FrameLen       62
#define cDllLegL2FrameSegDataLen   (cDllLegMaxL2FrameLen-cDllL2FrameHdrLen)

#define cXXTeaPadLen               4
#define cDllBigMaxL2FrameLen       124 // Includes cXXTeaPadLen
#define cDllBigMaxL2FrameLenNoPad  120 // Excludes cXXTeaPadLen

/*
 * A note regarding the 'lit' frame lengths, they are defined and only useful
 * if we are supporting use of the software implementation of XXTea.  This
 * has been implemented and unit tested.  Then requirements were clarified
 * indicating no requirement to support a software implementation of
 * XXTea in legacy hardware or with the Corona modem.  That said we do not
 * yet want to lose the capability to support it.
 */

// For Corona with software XXTea implementation
#define cDllLitMaxL2FrameLen       60
#define cDllLitMaxL2FrameLenNoPad  56
#define cDllBigL2FrameSegDataLen   (cDllBigMaxL2FrameLenNoPad-cDllL2FrameHdrLen)
#define cDllLitL2FrameSegDataLen   (cDllLitMaxL2FrameLenNoPad-cDllL2FrameHdrLen)

#if defined(DEFINE_BIG_FRAMES)
#define cDllMaxL2FrameSegDataLen   cDllBigL2FrameSegDataLen
#else
#define cDllMaxL2FrameSegDataLen   cDllLegL2FrameSegDataLen
#endif /* #if defined(DEFINE_BIG_FRAMES) */

#define cDllBigMaxL2FrameLenSizing 128
#define cDllLitMaxL2FrameLenSizing 64

#if defined(DEFINE_BIG_FRAMES)
#define cDllMaxL2FrameLenSizing    cDllBigMaxL2FrameLenSizing
#else /* !DEFINE_BIG_FRAMES */
#define cDllMaxL2FrameLenSizing    cDllLitMaxL2FrameLenSizing
#endif /* if defined(DEFINE_BIG_FRAMES) */



// L2 domain address is based on, i.e. uses, the EMU serial number.
#define cL2DomainAddrStrLen     13 // 12 chars plus nul char

#define cL2PktCtlPktRsvdMask    0x3   // reserved bits mask
#define cL2PktCtlPktTypeShift   6
#define cL2PktCtlPktTypeMask    0x3


#define cL2PktCtlPktTypeAckOkRsp    0
#define cL2PktCtlPktTypeAckFailRsp  1
#define cL2PktCtlPktTypeUnAckReq    2
#define cL2PktCtlPktTypeAckReq      3

// For L2 Protocol version 2 these bits are MSBs for module address.
#define cL2PktCtlPktMsbsShift   0
#define cL2PktCtlPktMsbsMask    0x0f

// Experimental: encryption indication
#define cL2PktCtlPktCryptShift   4
#define cL2PktCtlPktCryptMask    0x3
#define cL2PktCtlPktCryptNone    0    // normal no encryption encoding
#define cL2PktCtlPktCryptMsgPsk  1    // msg data pre-shared key

#define cL2ProtoIdInvalid       0
#define cL2ProtoIdAriane        1
#define cL2ProtoIdEnphase       0x7
#define cL2ProtoIdEnphaseFp     0x8   // protocol for fastpath frames
#define cL2ProtoIdEnphasePa     0x9   // protocol for phase aware frames

#define cL2AddrInfoMsgSrcShift  7
#define cL2AddrInfoMsgSrcMask   1
#define cL2AddrInfoMsgSrcEmu    1
#define cL2AddrInfoMsgSrcMod    0

#define cL2AddrInfoScopeShift   5
#define cL2AddrInfoScopeMask    0x3
#define cL2AddrInfoScopeDirected 0
#define cL2AddrInfoScopeBcastMod 1
#define cL2AddrInfoScopeBcastEmu 2
#define cL2AddrInfoScopeBcastAll 3


#define cL2AddrInfoProtoVerShift 2
#define cL2AddrInfoProtoVerMask  0x7

#define cL2ProtoVerZero          0 // invalid

/*! cL2ProtoVerOne: This is the legacy 62 byte maximum frame length L2 frame
  that supports only 10-bit modules addresses. Also, no hardware or software
  XXTea is supported with this frame / L2 protocol version.
*/
#define cL2ProtoVerOne           1


/*! cL2ProtoVerTwo: This is the legacy 62 byte maximum frame length L2 frame
  that supports extended module 14-bit addresses. Also, no hardware or
  software XXTea is supported with this frame / L2 protocol version.
*/
#define cL2ProtoVerTwo           2

/*! cL2ProtoVerThree: Think of this one as reserved, not really using ... This
  is the 60,56 byte maximum frame length l2 frame that supports extended
  module 14-bit addresses. With this version, software XXTea encryption is
  supportable. But since the cL2ProtoVerFour and associated L2 frame structure
  could support both hardware and software XXTea implementations, it seems
  that there is little advantage to using this particular frame structure.
  Nevertheless its reserved for now.

*/
#define cL2ProtoVerThree         3

/*! cL2ProtoVerFour: The is the 120,124 maximum frame length extended, 14-bit,
  module addressing.  This L2 frame structure is capable of support XXTea
  hardware and software implementations.

 */
#define cL2ProtoVerFour          4

typedef struct _tL2ProtoVerSpec {
    u_int8_t                    valid;
    u_int8_t                    extModAddr;
    u_int8_t                    cryptCapable;
    u_int8_t                    maxSegDataLen;
    u_int8_t                    maxFrmLen;
    u_int8_t                    maxFrmLenNoPad;
} tL2ProtoVerSpec;

#if defined(DEFINE_L2_PROTOVER_SPEC)
const tL2ProtoVerSpec _keil_code
l2ProtoVerSpecs[cL2AddrInfoProtoVerMask+1] = {
    /* 0 */ {0,0,0,0,0,0},
    /* 1 */ {1,0,0,
             cDllLegL2FrameSegDataLen,
             cDllLegMaxL2FrameLen,cDllLegMaxL2FrameLen},
    /* 2 */ {1,1,0,
             cDllLegL2FrameSegDataLen,
             cDllLegMaxL2FrameLen,cDllLegMaxL2FrameLen},
    // For now, since though code in is in place to support software
    // encryption we are not really trying to support this feature
    // so treat this L2 protocol version as invalid.
    /* 3 */ {0,1,1,
             cDllLitL2FrameSegDataLen,
             cDllLitMaxL2FrameLen,cDllLitMaxL2FrameLenNoPad},
    // For now, we can only support big frames with the raven modem
    // due to a bug in the Raven0a ASIC.  Later when (if) we port
    // big frames to IcaurusII it can be supported there as well.
    /* 4 */ {
        1,
        1,1,
        cDllBigL2FrameSegDataLen,
        cDllBigMaxL2FrameLen,cDllBigMaxL2FrameLenNoPad},
    /* 5 */ {0,0,0,0,0,0},
    /* 6 */ {0,0,0,0,0,0},
    /* 7 */ {0,0,0,0,0,0}
};
#else
extern const tL2ProtoVerSpec _keil_code
l2ProtoVerSpecs[cL2AddrInfoProtoVerMask+1];
#endif /* if defined(DEFINE_L2_PROTOVER_SPEC) */

#define mL2FrmProtoVer(_l2frm)                                          \
    (((_l2frm)->l2hdr.addrInfo >>                                       \
      cL2AddrInfoProtoVerShift)&cL2AddrInfoProtoVerMask)

#define mL2UnpFrmProtoVer(_l2unpfrm) ((_l2unpfrm)->protoVer)

#define mL2FrmFirstSeg(_l2hdr)                                              \
    (((_l2hdr)->segInfo >> cL2SegInfoFirstSegShift) & cL2SegInfoFirstSegMask)

#define mL2FrmSegsRemain(_l2hdr)                                            \
    (((_l2hdr)->segInfo >> cL2SegInfoSegIdShift) & cL2SegInfoSegIdMask)


#define cL2AddrInfoModAddrHiShift       0
#define cL2AddrInfoModAddrHiMask        0x3

#define cL2SegInfoFirstSegShift         7
#define cL2SegInfoFirstSegMask          0x1
#define cL2SegInfoFirstSegMaskShifted   0x80
#define cL2SegInfoSegIdShift            0
#define cL2SegInfoSegIdMask             0x7f

// msd - this needs to change at some point.
#define cMaxPlcModAddrL2ProtoVerOne     1022
#define cMaxPlcModAddrExtended          16382

#define cDllNumReassContexts            2



/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs -------------------------- */

/**
 * @defgroup dllHTypedefs dll Header Typedefs
 */
/*@{*/



// msd -- update document for structure change
typedef struct tL2FrameHdr {
    u_int8_t                    pktCtl;
    u_int8_t                    protocolId;
    u_int8_t                    addrInfo;
    u_int8_t                    moduleAddrLo;
    u_int8_t                    domainAddr[cL2DomainAddrLen];
    u_int8_t                    segInfo;
    u_int8_t                    segMsgDataLen;
} tL2FrameHdr;

typedef struct tL2Frame {
    tL2FrameHdr                 l2hdr;
    u_int8_t                    segData[cDllBigL2FrameSegDataLen];
    u_int8_t                    notpartofframebyte0;
    u_int8_t                    notpartofframebyte1;
} tL2Frame;

typedef struct tL2FrameHdrUnpacked {
    u_int8_t                    packetType;
    u_int8_t                    protocolId;
    u_int8_t                    scopeInd;
    u_int8_t                    segMsgDataLen;
    u_int8_t                    protoVer;
    u_int8_t                    crypt;
    u_int8_t                    firstSeg;
    u_int8_t                    segsRemaining;

    // leave these three at end of structure
    u_int8_t                    msgSrc;
    u_int16_t                   modAddr;
    u_int8_t                    domainAddr[cL2DomainAddrLen+2];
} tL2FrameHdrUnpacked;

typedef struct _tL2FrmAppMsg {
    tPmiConfigData              pmi;
    tL2FrameHdrUnpacked         l2Unp;
    u_int8_t                    mb[cAppMsgLenWPadIV];
} tL2FrmAppMsg;

typedef struct _tL2FrmEntry {
    tPmiConfigData              pmi;
    tL2Frame                    l2;
} tL2FrmEntry;

typedef struct _tDllReassemblyCtx {
    u_int8_t                    rctxIdx;
    u_int8_t                    timerUserId;
    tL2FrmAppMsg _keil_xdata  * l2AppMsg;
    u_int8_t                    totFrags;
    u_int16_t                   modAddr;
    u_int8_t                    fragsRemaining;
    u_int16_t                   bufUsed;
    tL2FrameHdrUnpacked         unpkdL2Hdr;
} tDllReassemblyCtx;


typedef struct _tDllCtl {
    u_int8_t                    flushCtxCnt;
    u_int8_t                    newestCtxIdx;

    tDllReassemblyCtx           reass[cDllNumReassContexts];
    u_int8_t                    rxMsgBuf[cDllNumReassContexts][sizeof(tL2FrmAppMsg)];

    u_int8_t                    txAppRing[(1 << cDllTxAppMsgRingSzP2)][
                                          sizeof(tL2FrmAppMsg)];
    u_int8_t                    txL2Ring[(1 << cDllTxL2RingSzP2)][
                                          sizeof(tPmiConfigData) +
                                          cDllMaxL2FrameLenSizing];
    u_int8_t                    rxL2Ring[(1 << cDllRxL2RingSzP2)][
                                          sizeof(tPmiConfigData) +
                                         cDllMaxL2FrameLenSizing];

} tDllCtl;


/*@}*/  /* END of dll Header Typedef Group */

/* ------------------------------------ Extern Declarations --------------- */

/**
 * @defgroup dllHExtdata dll Header Extern Data Definitions
 */
/*@{*/


/*@}*/  /* END of dll Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup dllHGlobdata dll Header Global Data Defined
 */
/*@{*/


/*@}*/  /* END of dll Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup dllHStatdata dll Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of dll Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup dllHProto dll Header Prototypes
 */
/*@{*/

/** dllL2RxFrameDeq - dequeue a L2 frame as input to dll. */
extern bool
dllL2RxFrameDeq(tL2FrmEntry _keil_xdata    * l2entry);

/** dllL2TxFrameWrite - enqueue a L2 frame as output from dll. */
extern tL2FrmEntry _keil_xdata    *
dllL2TxFrameWrite(void);

/** dllL2TxFrameWriteComplete - enqueue a L2 frame as output from dll. */
extern void
dllL2TxFrameWriteComplete();

/** dllAppRxMsgWrite - get write address for msg to app. */
extern tL2FrmAppMsg _keil_xdata *
dllAppRxMsgWrite(void);

/** dllAppRxMsgWriteComplete - complete write of message to app rx ring. */
extern void
dllAppRxMsgWriteComplete(void);

/** dllAppRxMsgRead - read a msg for app. */
extern tL2FrmAppMsg _keil_xdata *
dllAppRxMsgRead(void);

/** dllAppRxMsgReadComplete - complete read a msg for app. */
extern void
dllAppRxMsgReadComplete(void);

/** dllAppTxMsgWrite - write a tx msg from app to dll (for disassembly). */
extern tL2FrmAppMsg _keil_xdata *
dllAppTxMsgWrite(void);

/** dllAppTxMsgWriteComplete - cause enqueue app tx msg. */
extern void
dllAppTxMsgWriteComplete(void);

/** dllAppTxMsgRead - read a app message for input to disassembler. */
extern tL2FrmAppMsg _keil_xdata *
dllAppTxMsgRead(void);

/** dllAppTxMsgReadComplete - complete read a msg for app. */
extern void
dllAppTxMsgReadComplete(void);

/** dllMsgReassemblerRxFrame - reassembler receives input L2 frame. */
extern u_int8_t
dllMsgReassemblerRxFrame(tL2FrmEntry _keil_xdata * l2entry);


/** dllMsgDisassembler - the dll message disassembler function. */
extern u_int8_t
dllMsgDisassembler(tL2FrmAppMsg _keil_xdata * l2AppMsg);

/** dllInitModule - initialize dll module. */
extern u_int8_t
dllInitModule(void);

/*@}*/  /* END of dll Header Prototypes Group */

/*@}*/  /* END dll Header Definitions and Declarations */


#endif /* _dll_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of dll.h */



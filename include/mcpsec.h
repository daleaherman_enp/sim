/* Copyright (c) 2006-2014 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _mcpsec_h_
#define _mcpsec_h_


/** @file mcpsec.h
 *  @brief MCP Security Feature Core APIs.
 *
 *  This file @c mcpsec.h provides the interface definitions for
 *  MCP Security Feature Core APIs.
 *
 */

/** @page mcpsecHPg Header: mcpsec.h MCP Security Feature Core APIs

@section mcpsecHName mcpsec.h HEADER NAME

    mcpsec.h -- MCP Security Feature Core APIs.

@section mcpsecHSynop mcpsec.h SYNOPSIS

    This file provides the interface definitions for
    MCP Security Feature Core APIs.

@section mcpsecHDesc mcpsec.h DESCRIPTION

    This file, @c mcpsec.h provides the interface definitions for
    MCP Security Feature Core APIs.

*/

/* system headers */
#include <stddef.h>
#include <stdlib.h>

#include "include/enphase.h"
#include "include/emudbg.h"

/**
 * @defgroup mcpsecHGroups mcpsec Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines -------------------------- */

/**
 * @defgroup mcpsecHDefines mcpsec Header Defines
 */
/*@{*/

#define cMcpSecFPGAMasterKeyIdx              0
#define cMcpSecHarrier1AsicMasterKeyIdx      1
#define cMcpSecNumberOfPredefinedMKIs        2

#if defined(HERON_FPGA)
#define cMcpSecDefaultMasterKeyIdx  cMcpSecFPGAMasterKeyIdx
#else
#define cMcpSecDefaultMasterKeyIdx  cMcpSecHarrier1AsicMasterKeyIdx
#endif

#define cMcpSecActiveSessionY       true
#define cMcpSecActiveSessionN       false

#define cMcpSecDspPkgHashLen        32
#define cMcpSecCryptBlkSz           16
#define cMcpSecDspLen               128
#define cMcpSecNonce0Len            4
#define cMcpSecNonce0StrBufLen      ((cMcpSecNonce0Len * 2)+1)
#define cMcpSecNonce1Len            8
#define cMcpSecNonce1StrBufLen      ((cMcpSecNonce1Len * 2)+2)
#define cMcpSecHash0Len             32
#define cMcpSecHash0StrBufLen       ((cMcpSecHash0Len * 2)+10)
#define cMcpSecHash1Len             32
#define cMcpSecHash1StrBufLen       ((cMcpSecHash1Len * 2)+10)
#define cMcpSecIVLen                4    // CBC initialization vector in message

#define mMcpSecPadLen(_msgLen)                                  \
    ((((_msgLen) % cMcpSecCryptBlkSz) == 0) ? 0 :               \
        (cMcpSecCryptBlkSz - ((_msgLen) % cMcpSecCryptBlkSz)))

#define mMcpSecCntInc(_counter) mcpsecCnts._counter++

/*
 * Define some macros related to inclusion / exclusion of MCPSEC in various
 * PCU targets (see above note).
 */

#if defined(MCPSEC) && (MCPSEC == 1)
#define DEFINE_INCLUDE_MCPSEC_FEATURE
#else
#undef DEFINE_INCLUDE_MCPSEC_FEATURE
#endif /* if defined(MCPSEC) && (MCPSEC == 1) */

/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs ------------------------- */

/**
 * @defgroup mcpsecHTypedefs mcpsec Header Typedefs
 */
/*@{*/


// The type for a master key index:

typedef u_int16_t                   tMcpSecMasterKeyIndex;

// An opaque type for the Domain Security Package:

typedef u_int8_t                    tMcpSecDspOpaque[cMcpSecDspLen];

// A type for Nounce0:

typedef u_int8_t                    tMcpSecNonce0[cMcpSecNonce0Len];

// A type for Nounce1:

typedef u_int8_t                    tMcpSecNonce1[cMcpSecNonce1Len];

// A type for Hash0:

typedef u_int8_t                    tMcpSecHash0[cMcpSecHash0Len];

// A type for Hash1:

typedef u_int8_t                    tMcpSecHash1[cMcpSecHash1Len];

// A type for message IV:

typedef u_int8_t                  * tMcpSecMsgIVPtr; // length cMcpSecIVLen
typedef u_int8_t                    tMcpSecMsgIVArr[cMcpSecIVLen];

// A type for key length:

typedef enum _eMcpSecKeyLen {
    cMcpSecKeyLen128,
    cMcpSecKeyLen256,
    cMcpSecKeyLenNumOf
} eMcpSecKeyLen;

/** The tMcpSecSystemSettings structure holds various PLC security
    feature, system-wide settings.

    TODO: What should we do, for persistence of these settings.

*/
typedef struct _tMcpSecSystemSettings {
    bool                             disablePlcSec; // default false
    // Defaults to true in system initialization.
    bool                             systemIsInSecureMode; // default false
    // For use256BitKeyLen: default is false
    bool                             use256BitKeyLen; // default false
} tMcpSecSystemSettings;

/** \defgroup sec-mode-info ``The Secure Mode Info Structure'' */
/* @{ */

/** Define the body of the `tMcpSecModeInfo` structure.
    This structure is used in the `ackSecInfo` message:

    The `tMcpSecModeInfo` structure is based on the message contents in the
    *Secure Mode Message* section of [sec-design].  In this document some
    naming and designations have be slightly modified to bring some further
    alignment with MCP conventions.

    Any endianess aspect in terms of handling this structure will be dealt
    with by the security feature core interface, by agreement, the security
    PLC messaging side of the interface does not have to deal with endianess
    when handling the contents of this structure.

*/
typedef struct              _tMcpSecModeInfo {
    tMcpSecMasterKeyIndex           masterKeyIndex;
    u_int8_t                        systemIsInSecureMode;
    u_int8_t                        keyLen; // use eMcpSecKeyLen
    u_int8_t                        activateSecureMode;
    u_int8_t                        fill2;
    u_int8_t                        fill1;
    u_int8_t                        fill0;
    tMcpSecDspOpaque                dsp;
    tMcpSecNonce0                   nonce0;
    tMcpSecNonce1                   nonce1;
    tMcpSecHash0                    hash0;
} tMcpSecModeInfo;

/* @} */

/** \defgroup sec-status-info ``The Secure Status Info Structure'' */
/* @{ */

/** Define the body of the `tMcpSecStatusInfo` structure. This structure is
    used in the `secInfo` message sent from a security aware PLD, e.g.
    a Frigate PCU, to the Envoy in response to a `secInfo` poll message.

    The `tMcpSecStatusInfo` structure is based on the message contents in the
    *Query Response Message* section of [sec-design].  In this document some
    naming and designations have be slightly modified to bring some further
    alignment with MCP conventions.

    Any endianess aspect in terms of handling this structure will be dealt
    with by the security feature core interface, by agreement, the security
    PLC messaging side of the interface does not have to deal with endianess
    when handling the contents of this structure.
*/
typedef struct              _tMcpSecStatusInfo {
    tMcpSecMasterKeyIndex           masterKeyIndex;
    u_int8_t                        systemIsInSecureMode;
    u_int8_t                        sessionIsActive;

    u_int8_t                        keyLen; // use eMcpSecKeyLen
    u_int8_t                        fill2;
    u_int8_t                        fill1;
    u_int8_t                        fill0;

    tMcpSecNonce0                   nonce0;
    tMcpSecNonce1                   nonce1;
    tMcpSecHash0                    hash0;
    tMcpSecHash1                    hash1;
} tMcpSecStatusInfo;

/* @} */

/** \defgroup sec-cnts ``The Secure Counts Structure'' */
/* @{ */

/** Define the body of the `tMcpSecCnts` structure.
    This structure is used in the `secInfo` message sent from
    a security aware PLD, e.g. a Frigate PCU,
    to the Envoy in response to a `secInfo` poll message:

    The `secCnts` counters will be accumulated in ram, in a manner similar to
    the way aurora detailed counters are.  Also a new REST interface will be
    provided to retrieve these counters.  At some future date, these counters
    may be added to some database schema for persistence.
*/
typedef struct              _tMcpSecCnts {
    u_int16_t                       secInfoPolls;
    u_int16_t                       enteredSecMode;
    u_int16_t                       exitedSecMode;
    u_int16_t                       secBadDecryptMsgs;
    u_int16_t                       encryptMsgsRxed;
    u_int16_t                       secSecureIgnoredUnencryptMsgs;
    u_int16_t                       directedAckSecInfoMsgsRxed;
    u_int16_t                       bcastAckSecInfoMsgsRxed;
    u_int16_t                       encryptMsgsTxed;
    u_int16_t                       secModeVerifyFailures;
    u_int16_t                       dspInvalids;
    u_int16_t                       dspDomainIdMismatches;
    u_int16_t                       noSessionErrors;
    u_int16_t                       encryptErrors;
    u_int16_t                       decryptErrors;

} tMcpSecCnts;

/* @} */

/** \defgroup sec-info-msg ``The Secure Info Message Structure'' */
/* @{ */

/** The Secure Info message is sent by security aware PLD, e.g.
    a Frigate PCU, in response to a `secInfo` poll message.

    Define the body of the Security Info Message:

    If the PLD is in an active secure session, this message may be sent
    encrypted. Also if the PLD is in an active secure session as understood by
    the Envoy, the poll message may be sent encrypted.

    The `tMcpSecInfoMsg` structure is based on the message definition in the
    *Query Response Message* section of [sec-design].  In this document some
    naming and designations have be slightly modified to bring some further
    alignment with MCP conventions.
*/
typedef struct              _tMcpSecInfoMsg {
    u_int8_t                        lastAckdSecStatusSeqNum;
    u_int8_t                        fill2;
    u_int8_t                        fill1;
    u_int8_t                        fill0;
    tMcpSecStatusInfo               secStatus;
    tMcpSecCnts                     secCnts;
} tMcpSecInfoMsg;

/* @} */

/** \defgroup ack-sec-info-msg ``The Ack Secure Info Message Structure'' */
/* @{ */

/** The `ackSecInfo` message is sent from the Envoy either broadcast or
    directed modes.  It is sent broadcast in order to more quickly
    transition all security aware PLD to secure mode.  Its also used as
    acknowledgment to a `secInfo` message from a specific device.

    Define the body of the Acknowledge Security Info Message:

    If this message is being sent directed, as part of a `secInfo` poll
    sequence, and `secStatus` that this message is an acknowledgment to
    matches, and the PLD is in an active secure session as understood by the
    Envoy, then this `ackSecInfo` message may be sent encrypted[^msg-encrypt],
    otherwise it must be sent unencrypted (though some `dsp` components may be
    encrypted).

    The `tMcpAckSecInfoMsg` structure is based on the message definition in
    the *Secure Mode Message* section of [sec-design].  In this document some
    naming and designations have be slightly modified to bring some further
    alignment with MCP conventions.

    [^msg-encrypt]: We are talking here about message encryption, not
    `secMode` component encryption, e.g. `dsp` field of `secMode`, encryption.
    Encryption within the `secMode` field is a function of the security
    feature core which is outside the scope of this document, see [sec-design].
*/
typedef struct              _tMcpAckSecInfoMsg {
    u_int8_t                        ackStatus; // use eMcpAckStatus
    u_int8_t                        ackedSecStatusSeqNum;
    u_int8_t                        fill1;
    u_int8_t                        fill0;
    tMcpSecModeInfo                 secMode;
} tMcpAckSecInfoMsg;

/* @} */

/*@}*/  /* END of mcpsec Header Typedef Group */

/* ------------------------------------ Extern Declarations -------------- */

/**
 * @defgroup mcpsecHExtdata mcpsec Header Extern Data Definitions
 */
/*@{*/


/*@}*/  /* END of mcpsec Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined -------------- */

/**
 * @defgroup mcpsecHGlobdata mcpsec Header Global Data Defined
 */
/*@{*/

// On the PLD (PCU) we have a local for secMode and secStatus.

// mcpsecSystemInSecureMode is set in mcpsecmsgAckSecInfoReceive
extern bool                         mcpsecSystemInSecureMode;
extern tMcpSecStatusInfo          * pSessSecStatusInfo;
extern tMcpSecModeInfo            * pSessSecModeInfo;


// For the PLD a global to hold current security mode settings.
extern tMcpSecModeInfo              mcpsecMode;

// For the PLD a global to hold current (last updated) security status.
extern tMcpSecStatusInfo            mcpsecStatus;

extern tMcpSecCnts                  mcpsecCnts;
extern tMcpSecCnts                  mcpsecCntsReported;

/*@}*/  /* END of mcpsec Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined -------------- */

/**
 * @defgroup mcpsecHStatdata mcpsec Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of mcpsec Header Static Data Defined Group */

/* ------------------------------------ Prototypes ----------------------- */

/**
 * @defgroup mcpsecHProto mcpsec Header Prototypes
 */
/*@{*/

/*

    With respect to PLC Security message handling there are two different
    contexts related to the security feature:

    - Interfaces for encryption and decryption of MCP application messages.

    - Interfaces for security-related message handling, the `secInfo`
      polling sequence, i.e. for handling of information from the `secInfo`
      message, and for support of formatting the `ackSecInfo` message.

    *For the interfaces documented in this section, with respect to the PCU
    implementation, at this time, it is not clear as to whether synchronous or
    asynchronous interfaces will work best.  Only further testing will
    determine this, so the current plan if to initially implement both.*

    *On the Envoy, we assume the interfaces can all be called synchronously.*

*/

/** \defgroup pld-mcpsec ``PLD mcpsec Interfaces'' */
/* @{ */

/** On Security Aware PLD: Interface for Transmit Support Formatting
    of `secStatus` in `secInfo` Message

    The API used on the security aware PLD for construction of the `secStatus`
    field of the `secInfo` message is shown below:

    This is the prototype for the synchronous interface:
*/
extern void mcpsecStatusGet(tMcpSecStatusInfo*  pSecStatus);

/** On Security Aware PLD: Interface for Receive Processing of `secMode`
    in `ackSecInfo` Message

    The API used on the security aware PLD for handling of the `secMode`
    field in the `ackSecInfo` message is:
*/
extern bool mcpsecModeSet(tMcpSecModeInfo* pSecMode);

/* @} */

/** mcpsecInitModule - initialize mcpsec module. */
extern int mcpsecInitModule(void);

/* @} */

/*@}*/  /* END of mcpsec Header Prototypes Group */

/*@}*/  /* END mcpsec Header Definitions and Declarations */

#endif /* _mcpsec_h_ */

/* ------------------------------------------------------------------------
   $Log:$
   ----------------------------------------------------------------------- */

/* END of mcpsec.h */

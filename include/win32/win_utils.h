
#ifdef WIN32
#ifndef WIN_UTILS_H
#define WIN_UTILS_H

#define CLOCK_MONOTONIC 1

#ifndef _TIMESPEC_DEFINED
#define _TIMESPEC_DEFINED
struct timespec { time_t tv_sec; long tv_nsec; };    //header part
#endif

#ifndef WIN_UTILS
#include <stdint.h>

extern int setWindowsEnvVars();
extern int __builtin_ffs(uint32_t i);
extern int usleep(int val);
extern int sleep(int val); 
extern char *basename(const char *path);
extern int gettimeofday(struct timeval *tp, struct timezone *tzp);
extern int dprintf(int fd, const char *format, ...);
extern int clock_gettime(int ignore, struct timespec *spec);     //C-file part

#if defined(_MSC_VER) && _MSC_VER < 1900

#define snprintf c99_snprintf
#define vsnprintf c99_vsnprintf
//
extern int c99_vsnprintf(char *outBuf, size_t size, const char *format, va_list ap);
extern int c99_snprintf(char *outBuf, size_t size, const char *format, ...);

#endif

#endif
#endif
#endif
/* Copyright (c) 2006-2011 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _mcp_enphase_h_
#define _mcp_enphase_h_

/** @file mcp_enphase.h
 *  @brief High Level enPhase Specific Definitions
 *
 *  This file @c enphase.h provides enPhase global definitions.
 */

/**
 * @defgroup enphaseHGroups enphase Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup enphaseHDefines enphase Header Defines
 */
/*@{*/

#ifndef false
#define false                           0
#endif

#ifndef true
#define true                            1
#endif

#define EXIT_SUCCESS                    0
#define EXIT_FAILURE                    1


#if defined(PCU)
typedef char                            int8_t;
typedef int                             int16_t;
typedef unsigned int                    u_int16_t;
typedef long                            int32_t;
typedef unsigned long                   u_int32_t;

typedef unsigned char                   u_int8_t;
typedef u_int8_t                        bool;
typedef u_int16_t                       in_port_t;
typedef u_int16_t                       ssize_t;
typedef u_int32_t                       time_t;

typedef u_int8_t                        uint8_t;
typedef u_int16_t                       uint16_t;
typedef u_int32_t                       uint32_t;

#elif defined(ENPHASE)
typedef u_int8_t                        uint8_t;
typedef u_int16_t                       uint16_t;
typedef u_int32_t                       uint32_t;
#else
#include <stdlib.h>
#include <stdbool.h>
typedef uint16_t                        u_int16_t;
typedef u_int16_t                       in_port_t;
#endif

// The cAppMsgLen value is based on 6 l2 frames (6 * 50 bytes) of app data,
// that number does not include the 12 bytes per frame used for the L2
// frame header.
#define cAppMsgLen              496

#define ENP_OK                  0
#define ENP_ERROR               (-1)

#define mMax(a,b)               (((a) > (b)) ? (a) : (b))
#define mMin(a,b)               (((a) < (b)) ? (a) : (b))


/* ------------------------------------ Typedefs -------------------------- */


/*@}*/  /* END of enphase Header Typedef Group */


#endif /* _mcp_enphase_h_ */


/* END of mcp_enphase.h */



/* Copyright (c) 2018 Enphase Energy, Inc. All rights reserved.
 *
 * Enphase Firmware Upgrade
 * This file defines the external API's of the Enphase Firmware upgrade
 * helper library.
 */


#ifndef _emu_fwupg_h_
#define _emu_fwupg_h_


#include "mcp_sunpower.h"
#include "enphase/emuutil.h"



/****** Error Codes ******/
typedef enum {
  SUP_OK = 0,
  SUP_BAD = -1,
  SUP_NOT_OPEN = -2,
  SUP_END_LIST = -3
} eSupError;

typedef enum {
  INV_OK = 0,
  INV_BAD = -1,
  INV_NOT_OPEN = -2
} eInvError;

typedef enum {
  MCP_OK = 0,
  MCP_BAD = -1,
  MCP_DEV_NOT_FOUND = -2,
  MCP_IMG_EOF = -3,
  MCP_IMG_NXT = -4
} eMcpError;

typedef struct t_img_m {
  int img_offset; //0-3
  int img_length; //4-7
  char img_type[24]; //8-31
  char img_part_num[64]; //32-95
  char img_file_ext[4]; // 96-99
  char file_md5_hash[32]; //100-131
  int img_compat; //134-137
  char img_file_name[80];
  bool img_validated_md5;
} t_img_meta;

/*
typedef struct _tDlImageCtl {
    bool                        imageInfoMsgValid;
    char                        imageInfoFilePath[256];
    tMcpImageInfoMsg            imageInfoMsg;

    tImageTargetMeta            meta;
    eMcpImageFormat             imageFormat;
    FILE                      * file;
    char                        filePath[256];
    long                        fileLenInBytes;
    uint8_t                  * binBuf;
    u_int32_t                   currRecOrByteOffset;
    size_t                      bytesRead;
    uint16_t                   reqCnt;
    u_int32_t                   reqOffset;
} tDlImageCtl;
*/

typedef  struct t_hardwarePN {
  char hw_pn[14];
} t_hardwarePN;


// inventory mgmt
char openInventory(const char * inventoryPath);

// sup mgmt
eSupError openSUP(const char * SUPpath);
int getImgCt(void);
eSupError  getNextImg(bool first, t_img_meta *devImg);
unsigned int getGenDate(void);

// mcp mgmgt
eMcpError getImgInfoMessage(tMcpImageInfoMsg *UGInfoMsg, t_img_meta imageData, const char * hwPN);
eMcpError getUpgRspMessage(tMcpImageRspMsg *UGrspMsg,t_img_meta imageData, const char * hwPN);

// upgrade mgmt - higher level api -

/* int upgSendImgInfoMsg(tL2FrmAppMsg *msg);
 * Build and transmit L2 frame containing imageInfoMsg
 *
 */

int upgSendImgInfoMsg(tL2FrmAppMsg *msg);

/* int upgSendImgRspMsg();
 * Build and transmit L2 frame containing next image chunk
 */
int upgSendImgRspMsg(tL2FrmAppMsg *msg);

/*
 * int upgSUP(const char * invPath, const char * SUPpath);
 * Parse SUP, and upgrade only devices matching the inventory file provided
 * this is the primary API for getting a device upgraded
 *
 */
int upgSUP(const char * invPath, const char * SUPpath);
char *GethwPartNo(void);
#endif /* _emu_fwupg_h_ */

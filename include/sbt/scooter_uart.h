/*!
 * @copyright
 * Copyright 2014, SolarBridge Technologies, LLC\n
 * All rights reserved.
 *
 * @file scooter_uart.h
 *  UART message framing from Scooter board.
 *
 * @date Sep 11, 2014
 * @author Julie <j.haugh@solarbridgetech.com>
 */

#ifndef SCOOTER_UART_H_
#define SCOOTER_UART_H_

#include <stddef.h>
//! @addtogroup uart_api
//! @{

#define SCOOTER_MAX_PACKET      256

struct scooter_uart_t;

typedef struct scooter_uart_t *scooter_uart_handle_t;

//! @brief Get Uart device name.
extern char * UartDeviceName(scooter_uart_handle_t hThis);

//! @breif A function pointer to a callback to receive decoded COBS frames
typedef int32_t (*pfnUartCB_t)(void *pCookie, uint8_t *pBuf, const size_t len);


//! @brief Set UART message debugging mode.
extern int32_t UartDebugSet(scooter_uart_handle_t This, uint32_t ui32Flags);

//! @brief Set UART message timeout in milliseconds
extern int32_t UartTimeoutReadSet(scooter_uart_handle_t This, uint32_t ms);

//! @brief Set UART message timeout in milliseconds
extern int32_t UartTimeoutReadGet(scooter_uart_handle_t This, uint32_t *ms);

//! @brief allocate and construct a new connection object
extern int32_t UartConnectionNew(scooter_uart_handle_t *pHandle);

//! @brief destruct and free a connection object
extern int32_t UartConnectionDelete(scooter_uart_handle_t This);

//! @brief Close the UART connection.
extern int32_t UartClose(scooter_uart_handle_t This);

//! @brief Flush the UART input.
extern int32_t UartFlush(scooter_uart_handle_t This);

//! @brief Open the UART device.
extern int32_t UartDeviceOpen(scooter_uart_handle_t This, const uint8_t *device);

//! @brief Register a callback to be called when the read thread has a valid COBS frame.
extern int32_t UartRegisterCallback(scooter_uart_handle_t hThis, pfnUartCB_t pfn,
                                       void *pCookie,
                                       pfnUartCB_t *oldpfn, void **ppOldcookie);

//! @brief Read a packet from the UART.
extern int32_t UartRead(scooter_uart_handle_t This, uint8_t *packet,
                        size_t *length, int32_t *device);

//! @brief Write a packet to the UART.
extern int32_t UartWrite(scooter_uart_handle_t This, const uint8_t *packet,
                         size_t length, int32_t device);

//! @brief Write a packet to the UART L2 handler.
extern int32_t UartWriteL2(scooter_uart_handle_t This, const uint8_t *packet,
                           size_t length);

//! @brief Read a packet from the UART L2 handler.
extern int32_t UartReadL2(scooter_uart_handle_t This, uint8_t *packet,
                           size_t *length, uint32_t timeout);

//! @brief Write a packet to the UART without requesting ACK
extern int32_t UartRawWrite(scooter_uart_handle_t hThis, const uint8_t *packet,
	size_t length, int32_t device);

//! @brief Send USB reset
extern int32_t UartReset(scooter_uart_handle_t This);


//! @}
#endif /* SCOOTER_UART_H_ */


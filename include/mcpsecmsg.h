/* Copyright (c) 2006-2014 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _mcpsecmsg_h_
#define _mcpsecmsg_h_


/** @file mcpsecmsg.h
 *  @brief MCP Security Message Support.
 *
 *  This file @c mcpsecmsg.h provides the interface definitions for
 *  MCP Security Message Support.
 *
 */

/** @page mcpsecmsgHPg Header: mcpsecmsg.h MCP Security Message Support

@section mcpsecmsgHName mcpsecmsg.h HEADER NAME

    mcpsecmsg.h -- MCP Security Message Support.

@section mcpsecmsgHSynop mcpsecmsg.h SYNOPSIS

    This file provides the interface definitions for
    MCP Security Message Support.

@section mcpsecmsgHDesc mcpsecmsg.h DESCRIPTION

    This file, @c mcpsecmsg.h provides the interface definitions for
    MCP Security Message Support.

*/


/* system headers */
#include <stddef.h>
#include <stdlib.h>

#include "include/enphase.h"
#include "include/mcp.h"
#include "include/mcpsec.h"
#include "include/dll.h"


/**
 * @defgroup mcpsecmsgHGroups mcpsecmsg Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines -------------------------- */

/**
 * @defgroup mcpsecmsgHDefines mcpsecmsg Header Defines
 */
/*@{*/

// For cMcpSecAttemptsBeforeFailure usage
// see mcpsecmsgSecInfoStatusAccounting.
#define cMcpSecAttemptsBeforeFailure 3
#define cMcpSecAttemptsMaxCnt        0xffff

#define cMcpSecMkiUnknownValue      0xffff

#define cMcpSecMsgIsEncryptedFlag   0x8000 // on msgHdr->msgLen
#define cMcpSecMsgLenMask           0x3fff // on msgHdr->msgLen
#define cMcpSecMsgMaxMsgBodyWPadLen 496 // (31 * cMcpSecCryptBlkSz) == (31 * 16) == 496
#define cMcpSecMsgMaxMsgWPadIVLen   508 // (31 * 16) + cMcpSecIVLen + sizeof(tMcpMsgHdr)
#define cMcpSecMsgSzForRingElem     512 // (32 * 16) == 512 ;; nice round

// In mcpsecmsg.h there is a compile-time check to make sure that
// cAppMsgLenWPadIV is equal to cMcpSecMsgSzForRingElem.
// The cAppMsgLenWPadIV value is defined in dll.h.
// This size is used to dimension MCP message area available in
// msg rings by virtue of the size of tL2FrmAppMsg which is used to
// size MCP message ring elements.

#if cAppMsgLenWPadIV != cMcpSecMsgSzForRingElem
#error cAppMsgLenWPadIV must equal cMcpSecMsgSzForRingElem
#endif /* if cAppMsgLenWPadIV != cMcpSecMsgSzForRingElem */


#define mMcpSecMsgUnPadLen(_msgHdr) \
    ((u_int16_t)((_msgHdr)->msgLen & cMcpSecMsgLenMask))

#define mMcpSecMsgUnPadLenN2H(_msgHdr) \
    ((u_int16_t)(ntohs((_msgHdr)->msgLen) & cMcpSecMsgLenMask))

#define mMcpSecMsgIsEncrypted(_msgHdr) \
    (((_msgHdr)->msgLen &          \
        (cMcpSecMsgIsEncryptedFlag)) ? true : false)

#define mMcpSecMsgIsEncryptedN2H(_msgHdr) \
    ((ntohs((_msgHdr)->msgLen) &          \
        (cMcpSecMsgIsEncryptedFlag)) ? true : false)

#define mMcpSecMsgIsEncrypted(_msgHdr) \
    (((_msgHdr)->msgLen &          \
        (cMcpSecMsgIsEncryptedFlag)) ? true : false)

#define mMcpSecMsgIsEncryptedN2H(_msgHdr) \
    ((ntohs((_msgHdr)->msgLen) &          \
        (cMcpSecMsgIsEncryptedFlag)) ? true : false)

#define mMcpSecMsgEncryptedMsgLenToXmt(_msgHdr) \
    (mMcpSecPadLen(((_msgHdr)->msgLen &             \
                    cMcpSecMsgLenMask) -            \
                   sizeof(tMcpMsgHdr)) +            \
     cMcpSecIVLen +                                 \
     ((_msgHdr)->msgLen & cMcpSecMsgLenMask))

#define mMcpSecMsgEncryptedMsgLenToXmtN2H(_msgHdr) \
    (mMcpSecPadLen((ntohs((_msgHdr)->msgLen) &      \
                    cMcpSecMsgLenMask) -            \
                   sizeof(tMcpMsgHdr)) +            \
     cMcpSecIVLen +                                 \
     (ntohs((_msgHdr)->msgLen) & cMcpSecMsgLenMask))


#define mMcpSecMsgMarkIsEncrypted(_msgHdr) \
    (_msgHdr)->msgLen |= cMcpSecMsgIsEncryptedFlag

#define mMcpSecMsgMarkIsEncryptedN2H(_msgHdr)              \
    (_msgHdr)->msgLen = htons(                            \
                               ntohs((_msgHdr)->msgLen) | \
                               cMcpSecMsgIsEncryptedFlag)

#define mMcpSecMsgUnMarkEncrypted(_msgHdr)              \
    (_msgHdr)->msgLen &= ~(cMcpSecMsgIsEncryptedFlag)

#define mMcpSecMsgUnMarkEncryptedN2H(_msgHdr)                \
    (_msgHdr)->msgLen = ntohs(                              \
                            (ntohs((_msgHdr)->msgLen) &     \
                             ~(cMcpSecMsgIsEncryptedFlag)))


/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs ------------------------- */

/**
 * @defgroup mcpsecmsgHTypedefs mcpsecmsg Header Typedefs
 */
/*@{*/



/*@}*/  /* END of mcpsecmsg Header Typedef Group */

/* ------------------------------------ Extern Declarations -------------- */

/**
 * @defgroup mcpsecmsgHExtdata mcpsecmsg Header Extern Data Definitions
 */
/*@{*/


/*@}*/  /* END of mcpsecmsg Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined -------------- */

/**
 * @defgroup mcpsecmsgHGlobdata mcpsecmsg Header Global Data Defined
 */
/*@{*/

/*@}*/  /* END of mcpsecmsg Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined -------------- */

/**
 * @defgroup mcpsecmsgHStatdata mcpsecmsg Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of mcpsecmsg Header Static Data Defined Group */

/* ------------------------------------ Prototypes ----------------------- */

/**
 * @defgroup mcpsecmsgHProto mcpsecmsg Header Prototypes
 */
/*@{*/

/** mcpsecmsgSecInfoMsgFormat - format a secInfo message. */
extern void
mcpsecmsgSecInfoMsgFormat(
    tMcpMsg                       * msg);

/** mcpsecmsgAckSecInfoReceive - On PLD receive ackSecInfo message. */
extern void
mcpsecmsgAckSecInfoReceive(
    tL2FrmAppMsg                  * l2AppMsg);


/** mcpsecmsgInitModule - initialize mcpsecmsg. */
extern int
mcpsecmsgInitModule(void);

/*@}*/  /* END of mcpsecmsg Header Prototypes Group */

/*@}*/  /* END mcpsecmsg Header Definitions and Declarations */


#endif /* _mcpsecmsg_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ----------------------------------------------------------------------- */

/* END of mcpsecmsg.h */

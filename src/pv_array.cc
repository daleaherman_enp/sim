
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#include <sys/time.h>
#endif // !WIN32
#include <assert.h>
//#include "include/mcp_sunpower.h"

#include "include/enphase.h"
#include "include/pmi.h"
#include "include/agf.h"
#include "include/dll.h"
#include "include/mcp_counters.h"
#include "include/mcp_debug.h"
#include "include/mcpsec.h"
#include "include/ring.h"
#include "include/timer.h"
#include "include/emuutl.h"
#include "include/emudbg.h"
#include "include/mcp.h"
#include "include/modmcp.h"
#include "enphase/emuutil.h"
#include "pcu.hh"
#include "pv_array.hh"



cPV_ARRAY::cPV_ARRAY(const char *connection, char *name) 
{
   m_connection = strdup(connection);
   if (name) {
	   m_name = strdup(name);
   }
   
}
int cPV_ARRAY::OpenConnection(uint32_t timeout_ms)
{
	int rc;
	rc = ConnectionOpen(m_connection, &m_hConn);
   if (rc < 0)
   {
      LOG_ERROR("Unable to open PLC device.\n");
      return ENODEV;
   }
   ConnectionDebugSet(m_hConn, 0);

   ScooterNew(&m_hScooter, m_hConn);

   ScooterLunSet(m_hScooter, SC_E_ASIC_PLC);
   ScooterTimeoutSet(m_hScooter, timeout_ms);
   return rc;
}
cPV_ARRAY::~cPV_ARRAY() 
{
}
int cPV_ARRAY::WaitPLCMessage(uint32_t timeout) 
{
	uint32_t msgLen=0;
	uint16_t outLen;
	tPmiDataConfigData *pPMIData;
	int rc;
    uint8_t buf[1024];
	ScooterTimeoutSet(m_hScooter, timeout);
	rc = enphase_read_responseV2(m_hScooter, NULL,
                                   m_msgBuf, &msgLen);
	
	if (rc == 0 && msgLen > 0)
	{
		
		for (std::list<cPCUSim*>::iterator it=m_cPCUList.begin(); it != m_cPCUList.end(); ++it)
		{
			memcpy(buf, m_msgBuf, msgLen);
			rc = (*it)->ProcessPLCMessage((const uint8_t *)buf, msgLen, m_outBuf, &outLen, &pPMIData);
			if (rc) {
				printf(" : %s: \n", m_name);
			}
		}
	}
	return rc;
}
int cPV_ARRAY::ConfigurationMessage() 
{
	int rc=0;
	return rc;
}
int cPV_ARRAY::AddPCU(cPCUSim *pcu)
{
	int rc = 0;
	m_cPCUList.push_back(pcu);
	return rc;
}


int cPV_ARRAY::Ticker(int num)
{
	int rc=0;
	
	for (std::list<cPCUSim*>::iterator it=m_cPCUList.begin(); it != m_cPCUList.end(); ++it)
	{
		
		(*it)->Ticker(num);
	}
	return rc;
}
int cPV_ARRAY::SetOperating(float freq, float acvRms, float dcmv)
{
	int rc=0;
	for (std::list<cPCUSim*>::iterator it=m_cPCUList.begin(); it != m_cPCUList.end(); ++it)
	{
			(*it)->SetOperatingCondition(freq, acvRms, dcmv);
	}
	return rc;
}


/*!
 * @copyright
 * Copyright 2013-2014, SolarBridge Technologies
 * All rights reserved.
 *
 * @errno.h
 *     Defined error codes
 *
 * Defines error codes to be used in sbt code.
 * Derived from linux 3.2.0 errno.h
 *
 * @author Hans Magnusson <h.magnusson@solarbridgetech.com>
 */
#ifndef _SBT_ERRNO_H
#define _SBT_ERRNO_H

#ifndef SUCCESS
#define SUCCESS          0
#endif

#include <errno.h>

/* SBT specific error messages */
#define SBT_EMISMATCH      200     /* Comparison detected mismatch between two objects */
#define SBT_EASSOCIATED    201     /* Already Associated */
#define SBT_EWRONGPM       202     /* Not associated with this PM */
#define SBT_EEPROMWFAIL    203     /* EEPROM write failed */
#define SBT_ESERIALKEY     204     /* Wrong serial key */
#define SBT_EVALUEKEY      205     /* Wrong value key */
#define SBT_ESNCHANGE      206     /* Cannot change serial number */
#define SBT_ERANGE         207     /* Value out of range */
#define SBT_EFLASHWFAIL    208     /* Flash write failed */
#define SBT_EIMAGECRC      209     /* Upgrade image CRC error */
#define SBT_EUARTREADFAIL  210     //! The write to the UART device failed.
#define SBT_EUARTWRITEFAIL 211     //! The write to the UART device failed.
#define SBT_EBADMODE       212     //! Invalid type of message
#define SBT_EBADVALUE      213     //! Invalid value in execution request
#define SBT_EBADDRESS      214     //! Invalid target address for command
#define SBT_ENOREPLY       215     //! No response from the target device.
#define SBT_ECOBSOVERRUN   216     //! COBS overrun error.
#define SBT_EBADCRC        217     //! Packet CRC error.
#define SBT_EPLCERROR      218     //! PLC Error.
#define SBT_ECANCELLED     219     //! The application took an early exit without doing any real work
#define SBT_EEMPTY         220     //! The resulting operation ended up producing an empty set
#define SBT_EPLCBUSY       221     //! we tried to initiate a PLC transaction before sidecar was finished with the last one
#define SBT_EMISSINGACK    222
#define SBT_EEXHAUSTED     223

#endif

/* Copyright (c) 2018 enPhase Energy, Inc. All rights reserved.
 *
 * This header defines the top level surrounding enPhase AGF Records.
 *
 */

#ifndef AGF_H_
#define AGF_H_

#if defined(SUNPOWER)
#pragma pack(push,1)
#endif

#include <stdint.h>

/**
 * @defgroup agfHGroups agf Header Definitions and Declarations
 */
/*@{*/

/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup agfHDefines agf Header Defines
 */
/*@{*/

#define PCU_FREQUENCY       50000000.0
#define LINE_FREQUENCY_NA         60.0
#define LINE_FREQUENCY_EU         50.0
#define LINE_HIGH_FREQUENCY      120.0
#define LINE_LOW_FREQUENCY        30.0
#define MAX_POWER                285.0

/*@}*//* END of agf Defines Group */

/* ------------------------------------ Typedefs -------------------------- */

/**
 * This is an enumeration of the supported AGF Functions.
 */
typedef enum _eAgfFunctionType
{
    cAgfFunctionTypeCookie = 0,
    cAgfFunctionTypeVVAR,
    cAgfFunctionTypeFRT,
    cAgfFunctionTypeVRT,
    cAgfFunctionTypeFPF,
    cAgfFunctionTypePRL,
    cAgfFunctionTypePLP,
    cAgfFunctionTypeVW,
    cAgfFunctionTypeINV2,
    cAgfFunctionTypeWP,
    cAgfFunctionTypeTV,
    cAgfFunctionTypeFW,
    cAgfFunctionTypeSS,
    cAgfFunctionTypeISLND,
    cAgfFunctionTypeNotUsed_1,
    cAgfFunctionTypeIAC,
    cAgfFunctionTypeVECT,
    cAgfFunctionTypeROCOF,
    cAgfFunctionTypeACAVE,
    cAgfFunctionTypeVW52,
    cAgfFunctionTypeFW22,
    cAgfFunctionTypeWVAR,
    cAgfFunctionTypeNumOf

} eAgfFunctionType;

#if defined(DEFINE_AGF_H_CONSTS)
const char *agfFuncTypeStrTbl[cAgfFunctionTypeNumOf] = {
  "cookie", "VVAR", "FRT", "VRT", "FPF", "PRL", "PLP", "VW",
  "INV2", "WP", "TV", "FW", "SS", "ISLND", "NotUsed_1",
  "IAC", "VECT", "ROCOF", "ACAVE", "VW52", "FW22", "WVAR"
};
#else
extern const char *agfFuncTypeStrTbl[cAgfFunctionTypeNumOf];
#endif // if defined(DEFINE_AGF_H_CONSTS)

typedef enum _eAgfCookieVariant
{
    cAgfCookieVariantNotUsed = 0,
    cAgfCookieVariantManifest,
    cAgfCookieVariantVVAR,
    cAgfCookieVariantFRT,
    cAgfCookieVariantVRT,
    cAgfCookieVariantVW,
    cAgfCookieVariantVW52,
    cAgfCookieVariantWVAR,
    cAgfCookieVariantNumOf

} eAgfCookieVariant;

#if defined(DEFINE_AGF_H_CONSTS)
const char *agfCookieVariantStrTbl[cAgfCookieVariantNumOf] = {
  "n/a", "Manifest", "VVAR", "FRT", "VRT", "VW51", "VW52", "WVAR"
};
#else
extern const char *agfCookieVariantStrTbl[cAgfCookieVariantNumOf];
#endif // if defined(DEFINE_AGF_H_CONSTS)


#define mAgfFuncNotUsed ((agfFunc == cAgfFunctionTypeNotUsed_1) || \
                         (agfFunc == cAgfFunctionTypeTV)     || \
                         (agfFunc >= cAgfFunctionTypeNumOf))

#define mAgfFuncTrxNotUsed ((agfFunc == cAgfFunctionTypeTV)     || \
                            (agfFunc == cAgfFunctionTypeNotUsed_1))


#define mAgfCookieNotUsed ((agfVersion == cAgfCookieVariantNotUsed) || \
                           (agfVersion >= cAgfCookieVariantNumOf))

#define cAgfUnsedFunctions   3
#define cAgfCookieSlotsUsed  (cAgfFunctionTypeNumOf + cAgfCookieVariantNumOf - cAgfUnsedFunctions)
#define cAgfFunctionVersion  1
#define cAgfFirstFunctionIdx  cAgfFunctionTypeVVAR
#define cAgfFirstCookieIdx    cAgfCookieVariantManifest
#define cAgfFirstCookiePtsIdx cAgfCookieVariantVVAR
#define cAgfDefaultVariantCnt 8

#define cAgfVvarVariantCnt    4
#define cAgfVvarVariant11     0
#define cAgfVvarVariant12     1
#define cAgfVvarVariant13     2
#define cAgfVvarVariant14     3

#define cAgfVwVariantCnt      1
#define cAgfVwVariant51       0

#define cAgfVw52VariantCnt      1
#define cAgfVw52Variant52       1

#define cAgfWpVariantCnt      1
#define cAgfWpVariant41       0

#define cAgfTvVariantCnt      1
#define cAgfTvVariant31       0

#define cAgfFwVariantCnt      1
#define cAgfFwVariant21       0

#define cAgfFw22VariantCnt      1
#define cAgfFw22Variant22       1

#define cAgfInvVariantCnt     2
#define cAgfInvVariant2       0
#define cAgfInvVariant4       1

#define cAgfRecordSuptPcu     0x01
#define cAgfRecordSuptAcb     0x02
#define cAgfRecordSuptNsrb    0x04



typedef struct _tAgfSupportedFuncData
{
    uint8_t           type;
    uint8_t           version;
    const char        *nameUpper;
    const char        *description;
    uint8_t           variantCount;
    uint8_t           variantStartIdx;
    const char        *variantNames[cAgfDefaultVariantCnt];
    uint8_t           deviceSupported;
    uint8_t           cookieVariantSupported;
} tAgfSupportedFuncData;

#define cAgfLdStringLen 16

#if defined(DEFINE_AGF_STR_TBL)
const char agfUpperFuncStrTbl[cAgfFunctionTypeNumOf][cAgfLdStringLen] = {
        "COOKIE",
        "VVAR",
        "FRT",
        "VRT",
        "FPF",
        "PRL",
        "PLP",
        "VW",
        "INV",
        "WP",
        "TV",
        "FW",
        "SS",
        "ISLND",
        "NOT_USED_1",
        "IAC",
        "VECT",
        "ROCOF",
        "ACAVE" ,
        "VW52",
        "FW22",
        "WVAR"
};

tAgfSupportedFuncData agfFuncSupport[cAgfFunctionTypeNumOf] = {
    { cAgfFunctionTypeCookie,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeCookie],
        "internal tracking value",
        cAgfCookieVariantNumOf,
        0,
        {"Not_used", "Manifest", "VVAR_pts", "FRT_pts", "VRT_pts", "VW_pts", "VW52_pts", "WVAR_pts"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantManifest
    },

    { cAgfFunctionTypeVVAR,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeVVAR],
        "volt-var",
        cAgfVvarVariantCnt,
        cAgfVvarVariant11,
        { "11", "12", "13", "14"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantVVAR
    },

    { cAgfFunctionTypeFRT,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeFRT],
        "frequency ride through",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantFRT
    },
    { cAgfFunctionTypeVRT,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeVRT],
        "voltage ride through",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantVRT
    },
    { cAgfFunctionTypeFPF,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeFPF],
        "fixed power factor",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },
    { cAgfFunctionTypePRL,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypePRL],
        "power rate limit",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },
    { cAgfFunctionTypePLP,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypePLP],
        "power limit parameters",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },
    { cAgfFunctionTypeVW,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeVW],
        "volt watt control",
        cAgfVwVariantCnt,
        cAgfVwVariant51,
        { "51" },
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantVW
    },

    { cAgfFunctionTypeINV2,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeINV2],
        "maximum power",
        cAgfInvVariantCnt,
        cAgfInvVariant2,
        {"2", "4"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeWP,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeWP],
        "watt power factor control",
        cAgfWpVariantCnt,
        cAgfWpVariant41,
        { "41" },
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeTV,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeTV],
        "dynamic reactive current support",
        cAgfTvVariantCnt,
        cAgfTvVariant31,
        { "31" },
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeFW,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeFW],
        "frequency watt control",
        cAgfFwVariantCnt,
        cAgfFwVariant21,
        { "21"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeSS,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeSS],
        "soft start",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeISLND,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeISLND],
        "islanding",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeNotUsed_1,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeNotUsed_1],
        "not used 1",
        0,
        0,
        {"NA"},
        0,
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeIAC,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeIAC],
        "iac",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeVECT,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeVECT],
        "vector shift",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeROCOF,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeROCOF],
        "Rate of Change of Frequency",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeACAVE,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeACAVE],
        "AC Average",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantNotUsed
    },

    { cAgfFunctionTypeVW52,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeVW52],
        "volt watt control for acb charging",
        cAgfVw52VariantCnt,
        cAgfVw52Variant52,
        { "52" },
        (cAgfRecordSuptAcb),
        cAgfCookieVariantVW52
    },

    { cAgfFunctionTypeFW22,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeFW22],
        "frequency watt control for acb charging",
        cAgfFw22VariantCnt,
        cAgfFw22Variant22,
        { "22"},
        (cAgfRecordSuptAcb),
        cAgfCookieVariantNotUsed
    },
        { cAgfFunctionTypeWVAR,
        1,
        agfUpperFuncStrTbl[cAgfFunctionTypeWVAR],
        "watt var",
        0,
        0,
        {"NA"},
        (cAgfRecordSuptPcu |
         cAgfRecordSuptAcb |
         cAgfRecordSuptNsrb),
        cAgfCookieVariantWVAR
    },
};

#else
extern const char agfUpperFuncStrTbl[cAgfFunctionTypeNumOf][cAgfLdStringLen];
extern tAgfSupportedFuncData agfFuncSupport[cAgfFunctionTypeNumOf];
#endif /* if defined(DEFINE_AGF_FUNCID_STR_TBL) */

#if defined(SUNPOWER)
#pragma pack(pop)
#endif

#endif /* AGF_H_ */
/* END of agf.h */

/*!
 * @copyright
 * Copyright 2015, SunPower Corp.
 * All rights reserved.
 *
 * @file conn_mime.h
 *  Send messages to scooter through MIME
 *
 * @date Jul 21, 2015
 * @author Miles Bintz <mbintz@sunpower.com>
 */

#ifndef CONN_MIME_H_
#define CONN_MIME_H_

#include <stddef.h>
//! @addtogroup conn_mime_api
//! @{

struct connmime_t;

typedef struct _connmime_t* hConnmime_t;

//! @breif A function pointer to a callback to receive decoded COBS frames
typedef int32_t (*pfnConnmimeCB_t)(void *pCookie, uint8_t *pBuf, const size_t len);

//! @brief Set pass-through message debugging mode.
extern int32_t ConnmimeDebugSet(hConnmime_t This, uint32_t ui32Flags);

//! @brief Set pass-through message timeout in milliseconds
extern int32_t ConnmimeTimeoutReadSet(hConnmime_t This, uint32_t ms);

//! @brief Set pass-through message timeout in milliseconds
extern int32_t ConnmimeTimeoutReadGet(hConnmime_t This, uint32_t *ms);

//! @brief allocate and construct a new connection object
extern int32_t ConnmimeConnectionNew(hConnmime_t *pHandle);

//! @brief destruct and free a connection object
extern int32_t ConnmimeConnectionDelete(hConnmime_t This);

//! @brief Close the pass-through connection.
extern int32_t ConnmimeClose(hConnmime_t This);

//! @brief Flush the pass-through input.
extern int32_t ConnmimeFlush(hConnmime_t This);

//! @brief Open the pass-through device.
extern int32_t ConnmimeDeviceOpen(hConnmime_t This, const uint8_t *device);

//! @brief Register a callback to be called when the read thread has a valid COBS frame.
extern int32_t ConnmimeRegisterCallback(hConnmime_t hThis,
                                        pfnConnmimeCB_t pfn, void *pCookie,
                                        pfnConnmimeCB_t *oldpfn, void **ppOldcookie);

//! @brief Read a packet from the pass-through.
extern int32_t ConnmimeRead(hConnmime_t This, uint8_t *packet,
                        size_t *length, int32_t *device);

//! @brief Write a packet to the pass-through.
extern int32_t ConnmimeWrite(hConnmime_t This, const uint8_t *packet,
                         size_t length, int32_t device);

//! @brief Write a packet to the pass-through L2 handler.
extern int32_t ConnmimeWriteL2(hConnmime_t This, const uint8_t *packet,
                           size_t length);

//! @brief Read a packet from the pass-through L2 handler.
extern int32_t ConnmimeReadL2(hConnmime_t This, uint8_t *packet,
                           size_t *length, uint32_t timeout);

//! @brief Write a packet to the pass-through without requesting ACK
extern int32_t ConnmimeRawWrite(hConnmime_t hThis, const uint8_t *packet,
	size_t length, int32_t device);

//! @brief Send USB reset
extern int32_t ConnmimeReset(hConnmime_t This);


//! @}
#endif /* CONN_MIME_H_ */


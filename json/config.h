#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <float.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#define HAVE_DECL_NAN 1
#define HAVE_DECL_INFINITY 1
#define HAVE_STRDUP 1
#define HAVE_SNPRINTF 1
#define HAVE_STDARG_H 1
#define HAVE_VSNPRINTF 1
#define HAVE_VASPRINTF 1
#define HAVE_STRNCASECMP 1
#define HAVE_STRCASECMP 1

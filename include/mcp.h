/* Copyright (c) 2006-2011 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _mcp_h_
#define _mcp_h_

/** @file mcp.h
 *  @brief Main Header for Module Control Protocol (MCP)
 *
 *  This file @c mcp.h provides MCP global definitions.
 */

#include "include/enphase.h"
#include "include/pmi.h"
#include "include/dll.h"
#include "include/mcpsec.h"
#include "include/agf.h"
#include "mcp_counters.h"
#include "mcp_debug.h"
//#include "mcp_sunpower.h"


// Valid ctrl indices can range from 0 - 7.
#define cMcpCAPVCtrlMask        0xe0
#define cMcpCAPVCtrlShft        5
#define cMcpCAPVProtoVersMask   0x1f
#define cMcpCAPVProtoVersShft   0




/**
 * @defgroup mcpHGroups mcp Header Definitions and Declarations
 */
/*@{*/

/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup mcpHDefines mcp Header Defines
 */
/*@{*/

#define cMcpProtocolVersionOne      1  // prior to manudata and debug changes.
#define cMcpProtocolVersionTwo      2
#define cMcpProtocolVersionThree    3
#define cMcpProtocolVersionFour     4
#define cMcpProtocolVersionFive     5

#define cMcpProtocolVersion         cMcpProtocolVersionFour  // default message protocol
#define cMcpMaxMsgLen               cAppMsgLen
#define cMcpFlagsByteSz             ((cMcpNumberOf/8)+1)

/*! Define eMcpAckReplyId */
/** This @c eMcpAckReplyId enumeration defines the values used in the
    "ackStatus" field of the MCP acknowledgment message.
*/
typedef enum _eMcpAckStatus {
    cMcpAckStatusNone,
    cMcpAckStatusOk,
    cMcpAckStatusUnableToComply,
    cMcpAckStatusFailure,
    cMcpAckStatusNumOf
} eMcpAckStatus;


#define cMcpImageTypesRealNumOf 5


typedef struct _tMcpImgInventoryMsg {
    tImageMetaData              imd;
} tMcpImgInventoryMsg;


typedef struct _tMcpAckIntervalMsg_Ver3 {
    uint8_t                    ackStatus;
    uint8_t                    fillb1;
    uint16_t                   ctlFlags;

    uint16_t                   audiblePeriodInSecs;
    uint8_t                    ackdIntervalSeqNum;
    uint8_t                    fillb2;
} tMcpAckIntervalMsg_Ver3;

typedef enum _eMcpDomainMembershipStatus {
    cMcpDomainMembershipStatusNone,
    cMcpDomainMembershipStatusDeleted,
    cMcpDomainMembershipStatusValid,
    cMcpDomainMembershipStatusVerified,
    cMcpDomainMembershipStatusReassignPending,
    cMcpDomainMembershipStatusNumOf
} eMcpDomainMembershipStatus;

typedef struct _tMcpAckDevInfoMsg_Ver3 {
    uint8_t                    ackStatus;
    uint8_t                    assocStatus; /*!< use dms enum */
    uint8_t                    devType;    /*!< use @c enPDeviceType. */
    uint8_t                    ackdDevInfoSeqNum;
    uint16_t                   modId;
    uint8_t                    fillb1;
    uint8_t                    ackDevInfoFlags;
    tPartNumber                 devPartNum;
    tSerialNumber               devSerialNum;
    uint32_t                   timestamp;
    tAsicId                     asicId;  //This might not be present, so should always be at the end
} tMcpAckDevInfoMsg_Ver3;


// Define a domain membership record used on the module side to store this
// information, though this record type, as defined here, is not directly
// used in a MCP message.

typedef struct _tMcpAgfAdcGroupInfo {
    uint32_t  mask;
} tMcpAgfGroupInfo;

typedef struct _tDmsInfo {
    uint8_t                    domainAddr[cL2DomainAddrLen+2];
    uint16_t                   modId;
    uint8_t                    assocStatus;  /*!< use dms enum */
    uint8_t                    newAssocStatus;
    tMcpAgfGroupInfo            agfGroup;
	uint8_t 					squelched;
} tDmsInfo;

typedef enum _eMcpPollType {
    cMcpPollTypeNone,
    cMcpPollTypeIntervalData,
    cMcpPollTypeDirectedDevice,
    cMcpPollTypeNewDevice,
    cMcpPollTypeImgInventory,
    cMcpPollTypePollSpecial,
    cMcpPollTypeTripPointInfo,
    cMcpPollTypeSecInfo,
    cMcpPollTypeIntervalIeData,
    cMcpPollTypeEventIeData,
    cMcpPollTypeAgf,         //not used
    cMcpPollTypeNumOf
} eMcpPollType;

typedef struct _tMcpPollMsg {
    uint8_t                    pollType; /*! use eMcpPoll */
    uint8_t                    fillb;
    uint16_t                   chanIndMask; /*<! MCPv4 for interval poll */
} tMcpPollMsg;

typedef enum _eMcpScanType {
    cMcpScanTypeUncontrolled,
    cMcpScanTypeControlled,
    cMcpScanTypeNumOf
} eMcpScanType;

typedef enum _eMcpScanTarget {
    cMcpScanTargetNewDevice,
    cMcpScanTargetDirectedDevice,
    cMcpScanTargetInterval,
    cMcpScanTargetImgInventory,
    cMcpScanTargetNumOf
} eMcpScanTarget;

typedef enum _eMcpScanMark {
    cMcpScanMarkBeginning,
    cMcpScanMarkEnd,
    cMcpScanMarkNumOf
} eMcpScanMark;

typedef struct _tMcpScanMsg {
    uint8_t                    scanType;   /*!< use eMcpScanType */
    uint8_t                    scanTarget; /*!< use eMcpScanTarget */
    uint8_t                    scanMark;   /*!< use eMcpScanMark */
    uint8_t                    newscanOpModulus;
} tMcpScanMsg;

typedef struct _tMcpDomainCtlMsg {
    uint8_t                    forgetMatchingDms;
    uint8_t                    forgetAnyDms;
    uint8_t                    fillb2;
    uint8_t                    fillb1;
    uint8_t                    domainAddr[cL2DomainAddrLen+2];
} tMcpDomainCtlMsg;

typedef union _uMcpModuleCounters {
    tPlcCounters                plc;
} uMcpModuleCounters;

typedef struct _tMcpSsiAndCounters {
    tMcpSsiMeasures             ssi;
    uMcpModuleCounters          counters;
} tMcpSsiAndCounters;


typedef struct _tMcpDevInfoMsg_Ver3 {
    tSerialNumber               devSerialNumber;
    tPartNumber                 devPartNumber;
    tPartNumber                 assemblyPartNumber;
    tPartNumber                 progParts[cManuNumProgParts];

    uint8_t                    devType;    /*!< use @c enPDeviceType. */
    uint8_t                    priorDevInfoMsgsWoAck;   /*!< normally zero */
    uint8_t                    lastAckdDevInfoSeqNum; /*!< zero only on
                                                          1st report */
    uint8_t                    capabilityFlags;

    tControllerCnts             controllerCnts;
    tMcpSsiAndCounters          countersAndSsi;

    // For corvus1 (raven1) based platforms:
    // Optionally present raven counters ..
    tPlcCounters                auroraPlmCnts;
    tAuroraDetailedCounters     auroraDetailedCnts;
} tMcpDevInfoMsg_Ver3;

#define cDevInfoNumProgParts       3


// PCUs that elect to send cMcpMsgIdDevInfoIe messages (rather than cMcpMsgIdDevInfo
// messages) supply a tMcpDevInfoMsgIe information element.
typedef struct _tMcpDevInfoMsgIe {              // msgId = cMcpMsgIdDevInfoIe
    tMcpIeHdr                   ieHdr;          // version = 0x01
    tMcpDevInfoMsg_Ver3         devInfo;
} tMcpDevInfoMsgIe;



typedef struct _tMcpPcuPwrIntervalIe {
    uint8_t                    chanTemp_v3;      // location of channel temp
                                                  // in v3 power data structure
    uint8_t                    chanTemp_heron;   // location of channel temp
                                                  // in heron power data
    uint16_t                   fillW1;

    uint8_t                    pwrConvErrSecs;
    uint8_t                    pwrConvMaxErrCycles;
    uint16_t                   mJoulesProduced[3];     /* 48-bit unsigned int */

    /* high byte reserved. */
    uint32_t                   reservedAndAcVoltageINmV;
    /* high byte reserved. */
    uint32_t                   reservedAndAcFrequencyINClkCycles;

    uint32_t                   dcVoltageINmV;
    uint32_t                   dcCurrentINmA;

    // The following 3 48 bits data elements were added for Heron
    uint16_t                   powerUsedmJ[3];     /* 48-bit unsigned int */
    uint16_t                   sumLeadingVAr[3];     /* 48-bit unsigned int */
    uint16_t                   sumLaggingVAr[3];     /* 48-bit unsigned int */
} tMcpPcuPwrIntervalIe;


#define cEventAddendumMax 40
#define cMcpPcuEventBitsSize  4

typedef struct _tMcpIntervalMsgIe {
    tMcpIeHdr                   ieHdr;
    uint8_t                    priorIntervalMsgsWoAck;   /*!< normally zero */
    uint8_t                    lastAckdIntervalSeqNum; /*!< zero only on 1st report */
    uint16_t                   fillw;
    uint32_t                   eventBits[cMcpPcuEventBitsSize];
    uint32_t                   prodInterval;
    uint32_t                   timestamp;
    tMcpPcuPwrIntervalIe        pwrIntrvl;
} tMcpIntervalMsgIe;

typedef struct _tMcpIntervalMsgAckIe {
    tMcpIeHdr                   ieHdr;
    uint8_t                    ackStatus;   /*!< normally zero */
    uint8_t                    ackdSeqNum; /*!< zero only on 1st report */
    uint16_t                   fillw;
} tMcpIntervalMsgAckIe;

typedef struct _tMcpPcuEventAddendum {
    uint8_t                    eventId;
    uint8_t                    fillB;
    uint16_t                   firstSetDelta;
    uint16_t                   lastClearDelta;
    uint16_t                   setDuration;
} tMcpPcuEventAddendum;

typedef struct _tMcpEventMsgIe {
    tMcpIeHdr                   ieHdr;          // version = 0x01
    uint8_t                    priorEventMsgsWoAck;   /*!< normally zero */
    uint8_t                    lastAckdEventSeqNum; /*!< zero only on 1st report */
    uint8_t                    fillb;
    uint8_t                    count;
    uint32_t                   eventBits[cMcpPcuEventBitsSize];
    uint32_t                   msgTimestamp;
    uint32_t                   baseTimestamp;
    tMcpPcuEventAddendum        addendum[cEventAddendumMax];   // leave at end of structure
} tMcpEventMsgIe;

typedef struct _tMcpEventMsgAckIe {
    tMcpIeHdr                   ieHdr;
    uint8_t                    ackStatus;   /*!< normally zero */
    uint8_t                    ackdSeqNum; /*!< zero only on 1st report */
    uint8_t                    fillB[2];
    uint16_t                   ctlFlags;     // send control flags back to PCU
    uint16_t                   audiblePeriodInSecs;
} tMcpEventMsgAckIe;

#define cMspIeHdrVer_0      0x0
#define cMspIeHdrVer_1      0x1

typedef struct _tMcpPlcCfgMsg {
    tPmiData            pmi;
} tMcpPlcCfgMsg;


// *****  START ADC DATA
#define cAdcDynPcStatusDynRecord    0x40

#define cAdcDynCommStatusUnknown    0x01
#define cAdcDynCommStatusNotLoaded  0x02
#define cAdcDynCommStatusNotFound   0x04

#define cAdcFixedStatusPcuFactoryData    0x80
#define cAdcFixedStatusPcuDynRamRecord   0x40

#define cAdcFixedStatusOpaqueSize    8

// Meta data header for record
typedef struct _tAdcRecHeaderMeta {
    uint16_t csum;          // checksum over HeaderFixed and RecData
    uint16_t len;           // data length (not part of checksum)
} tAdcRecHeaderMeta;

// Dynamic record data constructed by PCU
typedef struct _tAdcRecHeaderDyn {
    uint32_t dataAddr;        // flash address of record, set by PCU
    uint8_t  pcFlags;         // flags set by PCU power control code - see cAdcDynPcStatus...
    uint8_t  commFlags;       // flags set by PCU comm code - see cAdcDynCommStatus...
    uint16_t dynSeqNum;       // return dynamic record sequence number set by Envoy
} tAdcRecHeaderDyn;

typedef struct _tPcuAdcRecHeaderDyn {
    uint8_t  flashAddr[3];       // flash address of record, set by PCU (only 3 bytes)
    uint8_t  pcFlags;         // flags set by PCU power control code - see cAdcDynPcStatus...
    uint8_t  commFlags;       // flags set by PCU comm code - see cAdcDynCommStatus...
    uint16_t dynSeqNum;       // return dynamic record sequence number set by Envoy
} tPcuAdcRecHeaderDyn;

//  timeValHi - signed upper dword ... has value 0
//              until 2116-02-17 06:28:16.
//  timeValLo - on unix time_t signed 32-bit epoch value, but treat as
//              unsigned with respect to this data.

// Record header data from Envoy
typedef struct _tAdcRecHeaderFixed {
                              // type is first entry to simplify PCU code.
    uint8_t  type;           // record type - see eAgfFunctionType
    uint8_t  version;        // record version
    uint16_t recLen;         // used by envoy when reading data from flash
    int32_t   timeValHi;
    uint32_t timeValLo;
    uint8_t  metaVersion;    // record version, and header version - see cAdcStatus..
    uint8_t  envoyFlags;     // flags set by Envoy see  cAdcFixedStatusPcu..
    uint16_t dynSeqNum;      // dynamic record sequence number set by Envoy
    uint8_t  opaqueData[cAdcFixedStatusOpaqueSize];  // opaque data set by Envoy
    uint8_t  fillByte2[2];   // fill bytes
    uint16_t csum;           // header checksum, check on envoy
} tAdcRecHeaderFixed;


// Record header stored in flash
typedef struct _tAdcRecHeaderFixed_V2 {
    tMcpAgfRecHeader shortMeta;
    uint32_t        timeStamp;
    uint8_t         metaVersion;     // meta data version
    uint8_t         reserved[3];     // do not use as they overlap with V1
    uint8_t         fillByte2[10];   // fill bytes
    uint16_t        csum;            // header checksum, check on envoy
} tAdcRecHeaderFixed_V2;


typedef struct _tAgfRecStatus {
    tMcpAgfLoadedRecStatus  recLoadedStatus;   // type is first entry to simplify PCU code.
    uint16_t               expLength;         // expected length of record, 0 not checked
    uint16_t               dynSeqNum;         // dynamic record sequence number set by Envoy
} tAgfRecStatus;

typedef union _uAdcRecords {
    tAgfCookieMsg           agfCookie;    // agf_structs.h
    tAgfVVARMsg             agfVVAR;    // agf_structs.h
    tAgfFRTMsg              agfFRT;
    tAgfVRTMsg              agfVRT;
    tAgfFPFMsg              agfFPF;     // fixed power factor
    tAgfPRLMsg              agfPRL;     // power rate limiting
    tAgfSSMsg               agfSS;      // soft start
    tAgfVWMsg               agfVW;    // voltage watts 51
    tAgfINV2Msg             agfINV2;
    tAgfWPMsg               agfWP;
    tAgfTVMsg               agfTV;
    tAgfFWMsg               agfFW;
    tAgfPLPMsg              agfPLP;
    tAgfISLNDMsg            agfISLND;
    tAgfIACMsg              agfIAC;
    tAgfVECTMsg             agfVECT;
    tAgfROCOFMsg            agfROCOF;
    tAgfACAVEMsg            agfACAVE;
    tAgfVW52Msg             agfVW52;
    tAgfFW22Msg             agfFW22;
    tAgfWVARMsg             agfWVAR;
} uAdcRecords;

typedef union _uAdcRecHeaderFixed {
    tAdcRecHeaderFixed    v1;
    tAdcRecHeaderFixed_V2 v2;
} uAdcRecHeaderFixed;
// Layout of record data

typedef struct _tAgfPldRecord {
    tAdcRecHeaderMeta  meta;
    uAdcRecHeaderFixed uRecHeader;
    uAdcRecords        uRecData;
} tAgfPldRecord;

typedef struct _tAdcRecord {
    tAdcRecHeaderMeta  meta;
    tAdcRecHeaderFixed recHeader;
    uAdcRecords        recData;
} tAdcRecord;

// Layout of record status data
// order of data makes coding easier on PCU
typedef struct _tAdcStatus {
    tAdcRecHeaderDyn   recDyn;
    tAdcRecHeaderFixed recHeader;
} tAdcStatus;

typedef struct _tPcuAdcStatus {
    tPcuAdcRecHeaderDyn   pcuRecDyn;
    tAdcRecHeaderFixed    pcuRecHeader;
} tPcuAdcStatus;

typedef struct _tPcuAdcStatus_V2 {
    tPcuAdcRecHeaderDyn   pcuRecDyn;
    tAdcRecHeaderFixed_V2 pcuRecHeader_V2;
} tPcuAdcStatus_V2;

typedef struct _tMcpAgfAdcDataMsg {
    tMcpIeHdr  ieHdr;
    uint32_t  grpMask;
    uint16_t  fillW;
    uint16_t  csum;     // Fletcher8BitCsum over record data
    tAdcRecord record;
} tMcpAgfAdcDataMsg;


#define cAdcInfoStatusUpdateInProgress    0x01
#define cAdcInfoStatusInputError          0x02

#define cAdcInfoMaxCount            10
#define cAdcInfoMaxCap              40

typedef enum _eMcpAgfAdcReqId {
    cMcpAgfAdcReqIdNone,
    cMcpAgfAdcReqCap,
    cMcpAgfAdcReqMeta,
    cMcpAgfAdcSetMask,
    cMcpAgfAdcReqIdNumOf
} eMcpAgfAdcReqId;


typedef struct _tMcpAgfAdcMetaRsp {
    uint16_t  fillW;
    uint8_t   fillb;
    uint8_t   count;     // total records
    tAdcStatus recStatus[cAdcInfoMaxCount];  // array of record status elements
} tMcpAgfAdcMetaRsp;

#define cAdcMetaRspPad (sizeof(tMcpAgfAdcMetaRsp) - \
                        (sizeof(tAdcStatus) * cAdcInfoMaxCount))

typedef struct _tMcpAgfAdcCapData {
    uint8_t  type;
    uint8_t  version;
    uint16_t headerAddr;
    uint16_t dataAddr;
} tMcpAgfAdcCapData;

typedef struct _tMcpAgfAdcCapRsp {
    uint8_t          count;     // total records returned
    uint8_t          fillB;
    uint8_t          numRecord; // number record supported on PCU
    uint8_t          maxRecord; // max record supported on PCU
    tMcpAgfAdcCapData capData[cAdcInfoMaxCap];
} tMcpAgfAdcCapRsp;

#define cAdcCapRspPad (sizeof(tMcpAgfAdcCapRsp) - \
                       (sizeof(tMcpAgfAdcCapData) * cAdcInfoMaxCap))

typedef union _uMcpAgfAdcRsp {
    tMcpAgfAdcCapRsp   rspCap;
    tMcpAgfAdcMetaRsp  rspMeta;
} uMcpAgfAdcRsp;

typedef struct _tMcpAgfAdcInfoMsg {
    tMcpIeHdr     ieHdr;
    uint32_t     grpMask;
    uint8_t      reqId;     // eMcpAgfAdcReqId
    uint8_t      flags;     // status flags - see cAdcInfoStatus...
    uint16_t     fillW;
    uMcpAgfAdcRsp rsp;
} tMcpAgfAdcInfoMsg;

#define cAdcInfoMsgPad (sizeof(tMcpAgfAdcInfoMsg) - \
                        sizeof(uMcpAgfAdcRsp) - \
                        sizeof(tMcpIeHdr))

typedef struct _tMcpAgfAdcMetaReqInfo {
    uint8_t   type;
    uint8_t   version;
} tMcpAgfAdcMetaReqInfo;

typedef struct _tMcpAgfAdcMetaReq {
    uint8_t   count;
    uint8_t   fillB;
    tMcpAgfAdcMetaReqInfo info[cAdcInfoMaxCount];
} tMcpAgfAdcMetaReq;

typedef struct _tMcpAgfAdcCapReq {
    uint8_t   start;      // start type to send
    uint8_t   end;        // end type to send
    uint16_t  fillW;
} tMcpAgfAdcCapReq;

#define cAdcMaskPhase   0x00000007
#define cAdcMaskGroups  0xFFFFFFF8
#define cAdcMaskDefault 0x00000000
#define cAdcPhaseNotSet 0
#define cAdcPhaseIdx1   0
#define cAdcPhaseIdx2   1
#define cAdcPhaseIdx3   2

typedef union _uMcpAgfAdcReq {
    tMcpAgfAdcCapReq      reqCap;
    tMcpAgfAdcMetaReq     reqMeta;
    tMcpAgfGroupInfo      setgroup;
} uMcpAgfAdcReq;

typedef struct _tMcpAgfAdcReqMsg {
    tMcpIeHdr     ieHdr;
    uint8_t      reqId;      // eMcpAgfAdcReqId
    uint8_t      fillB;
    uint16_t     fillW;
    uMcpAgfAdcReq req;
} tMcpAgfAdcReqMsg;

typedef struct _tMcpFpAgfINV2Record {
    tAdcRecHeaderMeta  meta;
    tAdcRecHeaderFixed recHeader;
    tAgfINV2Msg        agfINV2;
} tMcpFpAgfINV2Record;

// *****  END ADC DATA

typedef enum _eMcpNonSecureId {
    cMcpNonSecureNone,
    cMcpNonSecureGroupInfo,
    cMcpNonSecureNumberOf,
} eMcpNonSecureId;

typedef union _uMcpNonSecureMsg {
    tMcpAgfGroupInfo      setgroup;
} uMcpNonSecureMsg;

typedef struct _tMcpNonSecureMsg {
    tMcpIeHdr        ieHdr;
    uint8_t         nsId;      // eMcpNonSecureId
    uint8_t         fillB;
    uint16_t        fillW;
    uMcpNonSecureMsg nsMsg;
} tMcpNonSecureMsg;


typedef struct _tPaInfoDriver {
	uint16_t regRxEnvoyPhase;
	uint16_t regLocalPhase;
	uint16_t regTestRxPhase;
	uint16_t regPhaseDelta;
	uint8_t  regPhaseGroup;
} tPaInfoDriver;

#define cMcpPaMinSeqNum 1
#define cMcpPaMaxSeqNum 0xff
#define cMcpPaCaptureMsgLen 0x20

typedef struct _tMcpPaCaptureMsg {
    uint8_t      fillB[3];
    uint8_t      phaseAwarenessSeqNum;
} tMcpPaCaptureMsg;

typedef struct _tMcpPaInfoMsg {
    tMcpIeHdr     ieHdr;
    tPaInfo       paInfo;
} tMcpPaInfoMsg;

typedef enum _eMcpSysConfigTlvId {
    cMcpSysConfigNone = 0,
    cMcpSysConfigVrise,
    cMcpSysConfigNumberOf,
} eMcpSysConfigId;

typedef enum _eMcpVriseDataType {
    cMcpVriseDataTypeNone = 0,      // No Data, disable feature
    cMcpVriseDataTypeLN,            // Line-to-Neutral data from Envoy
    cMcpVriseDataTypeLL,            // Line-to-Line data from Envoy
    cMcpVriseDataTypeNumberOf,
} eMcpVriseDataType;

typedef struct _tMcpSysConfigVriseData {
    uint8_t      dataType;          // eMcpVriseDataType
    uint8_t      fill[2];           // filler to get to 12 bytes
    uint8_t      phaseAcMv[3][3];   // phase L-to-L millivolts
} tMcpSysConfigVriseData;

typedef struct _tMcpSysConfigTlv {
    uint8_t      type;   //eMcpSysConfigId
    uint8_t      length;
} tMcpSysConfigTlv;

#define cMcpSysConfigDataSize (cDllBigMaxL2FrameLenNoPad -  \
                               cDllL2FrameHdrLen -          \
                               sizeof(tMcpMsgHdr) -         \
                               sizeof(tMcpIeHdr) -          \
                               8)                                 // grpMask + fill + tlvCnt

typedef struct _tMcpSysConfigIe {
    tMcpIeHdr     ieHdr;
    uint32_t     grpMask;
    uint8_t      fill[3];
    uint8_t      tlvCnt;
    uint8_t      tlvData[cMcpSysConfigDataSize];
} tMcpSysConfigIe;

#define cSysConfigVriseLen (sizeof(tMcpSysConfigVriseData))
#define cSysConfigMaxOffset (cMcpSysConfigDataSize - (sizeof(tMcpSysConfigTlv)) - 4)


// **********************
// Mcp Message Union
// **********************

typedef struct _tMcpMsg {
    tMcpMsgHdr                  hdr;
    union {
        tMcpAckIntervalMsg_Ver3 ackIntervalV3;
        tMcpAckDevInfoMsg_Ver3  ackDevInfoV3;
        tMcpAckSimpleMsg        ackSimple;
        tMcpPollMsg             poll;
        tMcpScanMsg             scan;
        tMcpDomainCtlMsg        domainCtl;
        tMcpDevInfoMsg_Ver3     devInfoV3;
        tMcpImgInventoryMsg     imgInventory;
        tMcpImageInfoMsg        imageInfo;
        tMcpImageReqMsg         imageReq;
        tMcpImageRspMsg         imageRsp;
        tMcpDebugMsg            debug;
        tMcpPlcCfgMsg           plcCfg;
        tMcpSecInfoMsg          secInfo;    // see mcpsec.h
        tMcpAckSecInfoMsg       ackSecInfo; // see mcpsec.h
        tMcpAgfAdcDataMsg       agfadcData;
        tMcpAgfAdcInfoMsg       agfadcInfo;
        tMcpAgfAdcReqMsg        agfadcReq;
        tMcpIntervalMsgIe       intervalIe;
        tMcpIntervalMsgAckIe    ackIntervalIe;
        tMcpEventMsgIe          eventIe;
        tMcpEventMsgAckIe       ackEventIe;
        tMcpFastpathIe          fastpathIe;
        tMcpPaCaptureMsg        paCapture;
        tMcpPaInfoMsg           paInfoIe;
        tMcpNonSecureMsg        nsReqIe;
        tMcpSysConfigIe         sysConfigIe;
        tMcpPollCmdTlv          pollCmdTlv;
        tMcpPollRspTlv          pollRspTlv;
        tMcpAgfRecords          agfRecords;
        tMcpAgfExpRecordsTlv    agfExpRecordsTlv;
        tMcpAssociationTlv      assocationTlv;
        tMcpMfgTlv              mfgTlv;
        tMcpMPSGateDriveCtrMsg  mpsGateMsg;
    } m;
} tMcpMsg;

typedef struct _tL2FrmAndMcpMsg {
    tL2FrameHdr                 l2FrmHdr;  /// this is the packed l2 frame
                                           /// hdr.
    tMcpMsg                     mcpMsg;
} tL2FrmAndMcpMsg;


///////////////////////////////////////////////////////////
//   P C U  C T R L  C O N D I T I O N  F L A G S
///////////////////////////////////////////////////////////

#define cMcpPcuCtrlCondFlagUndef_0x00000001    0x00000001
#define cMcpPcuCtrlCondFlagUndef_0x00000002    0x00000002
#define cMcpPcuCtrlCondFlagUndef_0x00000004    0x00000004
#define cMcpPcuCtrlCondFlagUndef_0x00000008    0x00000008
#define cMcpPcuCtrlCondFlagUndef_0x00000010    0x00000010
#define cMcpPcuCtrlCondFlagUndef_0x00000020    0x00000020
#define cMcpPcuCtrlCondFlagUndef_0x00000040    0x00000040
#define cMcpPcuCtrlCondFlagUndef_0x00000080    0x00000080
#define cMcpPcuCtrlCondFlagUndef_0x00000100    0x00000100
#define cMcpPcuCtrlCondFlagForcedPwrProdOff    0x00000200
#define cMcpPcuCtrlCondFlagCriticalTemp        0x00000400
#define cMcpPcuCtrlCondFlagOverTemp            0x00000800
#define cMcpPcuCtrlCondFlagAlertActive         0x00001000
#define cMcpPcuCtrlCondFlagRunningOnAC         0x00002000
#define cMcpPcuCtrlCondFlagManuTestMode        0x00004000
#define cMcpPcuCtrlCondFlagBadFlashImage       0x00008000
#define cMcpPcuCtrlCondFlagUnexpectedReset     0x00010000
#define cMcpPcuCtrlCondFlagCommandedReset      0x00020000
#define cMcpPcuCtrlCondFlagPowerOnReset        0x00040000
// (r3.1.0) was ctrl watchdog reset, not used at ctrl, at chan is HardwareError
#define cMcpPcuCtrlCondFlagUndef_0x00080000    0x00080000
#define cMcpPcuCtrlCondFlagUndef_0x00100000    0x00100000
#define cMcpPcuCtrlCondFlagDCPowerTooLow       0x00200000 // may lose power soon
#define cMcpPcuCtrlCondFlagBridgeFault         0x00400000
#define cMcpPcuCtrlCondFlagNSync               0x00800000
#define cMcpPcuCtrlCondFlagAltVf               0x01000000
#define cMcpPcuCtrlCondFlagUndef_0x02000000    0x02000000
#define cMcpPcuCtrlCondFlagUndef_0x04000000    0x04000000
#define cMcpPcuCtrlCondFlagIupLinkProblem      0x08000000
#define cMcpPcuCtrlCondFlagUndef_0x10000000    0x10000000
#define cMcpPcuCtrlCondFlagUndef_0x20000000    0x20000000
#define cMcpPcuCtrlCondFlagTPMtest             0x40000000   //TPM Test
#define cMcpPcuCtrlCondFlagTalkingBrick        0x80000000   //EU Brick
#define cMcpPcuCtrlCondFlagMask                0xc027be00 // MCPv3 flags
#define cMcpPcuCtrlCondFlagNumOf               32

#define cMcpPcuCtrlCondMaskNotSleepMaskable (cMcpPcuCtrlCondFlagBadFlashImage | \
                                             cMcpPcuCtrlCondFlagForcePwrProdOff | \
                                             cMcpPcuCtrlCondFlagDCPowerTooLow | \
                                             cMcpPcuCtrlCondFlagAltVf )
#define cMcpPcuCtrlCondResetFlagsMask (cMcpPcuCtrlCondFlagUnexpectedReset | \
                                       cMcpPcuCtrlCondFlagCommandedReset | \
                                       cMcpPcuCtrlCondFlagPowerOnReset)

///////////////////////////////////////////////////////////
//   P C U  C H A N  C O N D I T I O N  F L A G S
///////////////////////////////////////////////////////////

/*

    For historical purposes the MCP version <=3 condition flags are shown
    below:

    cMcpCondFlagAcVoltageOOSP1      0x00000001
    cMcpCondFlagAcVoltageOOSP2      0x00000002
    cMcpCondFlagAcVoltageOOSP3      0x00000004
    cMcpCondFlagAcFreqOOR           0x00000008
    cMcpCondFlagAntiIslanding       0x00000010
    cMcpCondFlagDCVoltageTooLow     0x00000020
    cMcpCondFlagDCVoltageTooHigh    0x00000040
    cMcpCondFlagSkippedCycles       0x00000080
    cMcpCondFlagGFITripped          0x00000100
    cMcpCondFlagForcePwrProdOff     0x00000200
    cMcpCondFlagCriticalTemp        0x00000400
    cMcpCondFlagOverTemp            0x00000800
    cMcpCondFlagAudibleActive       0x00001000
    cMcpCondFlagRunningOnAC         0x00002000
    cMcpCondFlagGridGone            0x00004000
    cMcpCondFlagBadFlashImage       0x00008000
    cMcpCondFlagUnexpectedReset     0x00010000
    cMcpCondFlagCommandedReset      0x00020000
    cMcpCondFlagPowerOnReset        0x00040000
    cMcpCondFlagWatchDogReset       0x00080000 // not used any more uses unexpected
    cMcpCondFlagVrefError           0x00100000
    cMcpCondFlagDCPowerTooLow       0x00200000

    For MCP version 4, with the introduction of the EMU's

        Device --> Controller --> Channel model

        We split the pre-version 4 set of condition flags into two sets:

        1) One set for channel level things and,

        2) The other set for the (device/controller) level.

    In an ideal world if powerline communication bandwidth were plentiful we
    would poll each level (device,controller,channel) with a separate set of
    messages. But powerline communications bandwidth is precious so as a
    compromise the polling for device and controller things is done with (a
    single set of) MCP device-info messages. Channel level things are polled
    with MCP interval report messages.

*/


#define cMcpPcuChanCondFlagAcVoltageOOSP1      0x00000001
#define cMcpPcuChanCondFlagAcVoltageOOSP2      0x00000002
#define cMcpPcuChanCondFlagAcVoltageOOSP3      0x00000004
#define cMcpPcuChanCondFlagAcFreqOOR           0x00000008
#define cMcpPcuChanCondFlagGridInstability     0x00000010
#define cMcpPcuChanCondFlagDCVoltageTooLow     0x00000020
#define cMcpPcuChanCondFlagDCVoltageTooHigh    0x00000040
#define cMcpPcuChanCondFlagSkippedCycles       0x00000080
#define cMcpPcuChanCondFlagGFITripped          0x00000100
#define cMcpPcuChanCondFlagUndef_0x00000200    0x00000200
#define cMcpPcuChanCondFlagUndef_0x00000400    0x00000400
#define cMcpPcuChanCondFlagUndef_0x00000800    0x00000800
#define cMcpPcuChanCondFlagUndef_0x00001000    0x00001000
#define cMcpPcuChanCondFlagUndef_0x00002000    0x00002000
#define cMcpPcuChanCondFlagGridGone            0x00004000
#define cMcpPcuChanCondFlagUndef_0x00008000    0x00008000
#define cMcpPcuChanCondFlagUndef_0x00010000    0x00010000
#define cMcpPcuChanCondFlagUndef_0x00020000    0x00020000
#define cMcpPcuChanCondFlagUndef_0x00040000    0x00040000
#define cMcpPcuChanCondFlagHardwareError       0x00080000 // (r3.1)
#define cMcpPcuChanCondFlagHardwareWarning     0x00100000 // (r3.1) was VrefError
#define cMcpPcuChanCondFlagUndef_0x00200000    0x00200000
#define cMcpPcuChanCondFlagHighSkipRate        0x00400000
// The cMcpPcuChanCondFlagMpptUnlocked is not translated to event, as most
// condition flags are: this one is applied as a flags bit in the associated
// performance interval (raw) record.
#define cMcpPcuChanCondFlagMpptUnlocked        0x00800000 // (r3.1)
#define cMcpPcuChanCondFlagGridDCILo           0x01000000    //GridDCILo
#define cMcpPcuChanCondFlagGridDCIHi           0x02000000    //GridDCIHi
#define cMcpPcuChanCondFlagdfdt                0x04000000    //ROCOF
#define cMcpPcuChanCondFlagACVAvg              0x08000000    //ACVAve
#define cMcpPcuChanCondFlagDCRLow              0x10000000    //DCRLow
#define cMcpPcuChanCondAcMonitorErr            0x20000000 // (r3.1)
#define cMcpPcuChanCondFlagUndef_0x40000000    0x40000000
#define cMcpPcuChanCondFlagUndef_0x80000000    0x80000000
#define cMcpPcuChanCondFlagMask                0x3fd841ff // MCPv3 flags
#define cMcpPcuChanCondFlagNumOf               32

// Define a mask of conditions that are NOT cleared at the
// point that sleep is declared.
#define cMcpPcuChanCondMaskNotSleepMaskable (       \
    cMcpPcuChanCondFlagGFITripped      |            \
    cMcpPcuChanCondFlagVrefError       |            \
    cMcpPcuChanCondFlagBadFlashImage)




///////////////////////////////////////////////////////////
//   P C U  C T R L  C A P A B I L I T Y  F L A G S
///////////////////////////////////////////////////////////
/*
The upper nibble is reserved for TPM's region encoding.
*/
#define cMcpPcuCtrlCapaFlagUndef_0x01           0x01
#define cMcpPcuCtrlCapaFlagDontNotUse           0x02
#define cMcpPcuCtrlCapaFlagSmartImgInfo         0x04
#define cMcpPcuCtrlCapaFlagAgf                  0x08
#define cMcpPcuCtrlCapaFlagTpmNa                0x10
#define cMcpPcuCtrlCapaFlagTpmEu                0x20
#define cMcpPcuCtrlCapaFlagTpmUndef_0x40        0x40
#define cMcpPcuCtrlCapaFlagTpmUndef_0x80        0x80
#define cMcpPcuCtrlCapaFlagNumOf                8
#define cMcpPcuCtrlCapaFlagTpmMask              0xf0


/*@}*/  /* END of mcp Header Typedef Group */

/* ------------------------------------ Extern Declarations ---------- */

/**
 * @defgroup mcpHExtdata mcp Header Extern Data Definitions
 */
/*@{*/

/*@}*/  /* END of mcp Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup mcpHGlobdata mcp Header Global Data Defined
 */
/*@{*/

/*@}*/  /* END of mcp Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup mcpHStatdata mcp Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of mcp Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup mcpHProto mcp Header Prototypes
 */
/*@{*/

/*@}*/  /* END of mcp Header Prototypes Group */


/*@}*/  /* END mcp Header Definitions and Declarations */


#endif /* _mcp_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of mcp.h */



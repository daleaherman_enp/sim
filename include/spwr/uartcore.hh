/**
 * @copyright
 * Copyright 2018, SunPower Corp
 * All rights reserved.
 *
 * @file uartcore.hh
 * UART abstraction layer.  This layer is what contains the OS
 * specific bits.  If you need to port between Windows and Linux
 * then pivot on this file.
 *
 * @date 2018-Jun-25
 * @author Miles Bintz <mbintz@sunpower.com>
 */

#ifndef _UARTCORE_HH_
#define _UARTCORE_HH_

#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
// #include <thread>

#include "spwr/transcieverif.hh"

//! @addtogroup libcomm2_api
//! @{

//! The max length of the string to hold the device name
#define UART_DEVNAME_LENGTH     128

//! A purely virtual class to define an OS-agnostic UART interface
class UARTCore : public TranscieverIF
{
    private:
        int32_t m_uart_fd;
        uint32_t m_uart_debug;
        uint32_t m_uart_read_timeout;
        char m_devname[UART_DEVNAME_LENGTH];

        // track the thread ID of the read thread for this UART
        // std::thread *m_thrd;
        pthread_t m_thrd;

        int32_t m_threadexit;
        volatile bool m_bRunning;
        CallbackIF *m_cb;

        void read_thread();
        static void* thread_helper(void *pThis);

    public:
        UARTCore();
        ~UARTCore();

        //! Ask the OS to open the UART device as described in pDevStr
        void Open(const char* pDevStr);

        //! Close the UART device at the OS layer
        void Close();

        //! Register a callback with the UART driver
        void CallbackSet(CallbackIF *cb);

        //! Set the read timeout where applicable
        void ReadTimeoutSet(uint32_t to);

        //! return the read timeout
        uint32_t ReadTimeoutGet();

        //! Write the buffer to the UART
        int32_t Write(uint8_t *pBuf, size_t length);

        //! Flush all unread characters from the UART input
        void Flush();

        //! Set debugging parameters for this module
        void DebugSet(uint32_t ui32Flags);

        //! Return the device name used to open this UART
        char* DeviceName();
};

//! @}


#endif

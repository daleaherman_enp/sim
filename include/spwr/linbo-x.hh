/**
 * @copyright
 * Copyright 2018, SunPower Corp
 * All rights reserved.
 *
 * @file linbo.hh
 * Line Interface Board Transciever definition
 *
 * @date 2018-Jul-1
 * @author Miles Bintz <mbintz@sunpower.com>
 */

#ifndef _LINBOX_HH_
#define _LINBOX_HH_

#include <stdlib.h>
#include <stdint.h>

#include "spwr/transcieverif.hh"
#include "spwr/fifo.hh"
#include "spwr/cobsuart.hh"
#include "sbt/scooterProto.h"


// /////////////////////////////////////////////////////////////
//
// Define the handler for Line Interface Board transcieving
//
// /////////////////////////////////////////////////////////////

class AckNak
{
    public:
        uint8_t ackdev;
        uint8_t reason;
};

//! Line Interface board transciever
class LInBoXcvr : public TranscieverIF, public CallbackIF
{
    private:
        TranscieverIF *m_pXcvr;
        Fifo m_acks;
        Fifo m_pkts;

    public:
        LInBoXcvr();
        ~LInBoXcvr();

        //! Open the requested device based on pDevStr
        void Open(const char* pDevStr);

        //! Ask the COBS layer to wrap the UARTCore object in pCore
        void Open(TranscieverIF *pCore);

        //! Close the device
        void Close();

        //! Register a callback with the transciever interface
        void CallbackSet(CallbackIF *cb);

        //! Set the read timeout where applicable
        void ReadTimeoutSet(uint32_t to);

        //! return the read timeout
        uint32_t ReadTimeoutGet();

        //! Write the buffer to the transmitter portion of the object
        int32_t Write(uint8_t *pBuf, size_t length);

        //! Flush all unread characters from the input side
        void Flush();

        //! Set debugging parameters for this module
        void DebugSet(uint32_t ui32Flags);

        //! Return the device name used to open this object
        char* DeviceName();

        //! Call this function when a packet arrives from the LInBo
        int cbifCallbackFunction(uint8_t *pBuf, size_t len);

        void dump(uint8_t *pBuf, uint32_t len);

};

#endif // __LINBOX_HH__

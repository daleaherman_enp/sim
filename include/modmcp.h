/* Copyright (c) 2006-2008 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */


#ifndef _modmcp_h_
#define _modmcp_h_


/** @file modmcp.h
 *  @brief EMU Thread Interface
 *
 *  This file @c modmcp.h provides the module side of the MCP.
 *
 */

/** @page modmcpHPg Header: modmcp.h Module MCP Implementation

@section modmcpHName modmcp.h HEADER NAME

    modmcp.h -- Module MCP Implementation.

@section modmcpHSynop modmcp.h SYNOPSIS

    This file provides the module side of the MCP.

@section modmcpHDesc modmcp.h DESCRIPTION

    This file, @c modmcp.h provides the module side of the MCP.

*/

#if 0
/* system headers */

#include "include/enphase.h"

#if defined(ICARUS)
#include "include/plc.h"
#endif /* if defined(ICARUS) */

#if defined(PCU)
#include "include/pcu.h"
#endif /* if defined(PCU) */

#include "include/mcp.h"
#include "include/ring.h"
#include "include/timer.h"
#include "include/dll.h"
#include "include/plc_dl.h"
#include "include/dll.h"

#endif
#include "mcp.h"
/**
 * @defgroup modmcpHGroups modmcp Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup modmcpHDefines modmcp Header Defines
 */
/*@{*/

#define cpcuMaxScratchBytes     512

#define cpcuOrphanTimerSecs     30
#define cpcuOrphanTimerTicks    (cpcuOrphanTimerSecs * cTimerTicksPerSec)

// wait about 1 hour
#define cpcuOrphanMinCryCount   120                       // cpcuOrphanMinCryCount * cpcuOrphanTimerSecs seconds before first unsolicited devinfo msgs go out when unassoc.
#define cpcuOrphanMaxCryCount   cpcuOrphanMinCryCount + 7 // total time until timer expiry


#define cNumPcuChans                    1
#define cSzIntervalData     (offsetof(tMcpIntervalMsg,pwr) + \
                             (cNumPcuChans * sizeof(tMcpPcuPwrData)))


/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs -------------------------- */

/**
 * @defgroup modmcpHTypedefs modmcp Header Typedefs
 */
/*@{*/

typedef enum _epcuDlStatus {
    cpcuDlStatusNone,
    cpcuDlStatusStart,
    cpcuDlStatusInProgress,
    cpcuDlStatusCompleted,
    cpcuDlStatusFailed,
    cpcuDlStatusTimeOut,
    cpcuDlStatusNumOf
} epcuDlStatus;



typedef struct _tIcarusDownloadCtl {
    epcuDlStatus                dlStatus;
    tMcpImageInfoMsg            ii;
    uint16_t                   count;
    uint32_t                   offset;
    uint8_t                    timeouts;
    uint16_t                   crc;
} tIcarusDownloadCtl;

typedef struct _tmodmcpModCtl {
    uint8_t                    rxIsBcast;
    uint8_t                    rxScopeInd;
    uint8_t                    rxMyModAddr;
    uint8_t                    rxMyDomain;
    uint8_t                    pktForMe;
    uint8_t                    packetType;
    uint8_t                    rxModAddrZero;
    uint8_t                    rxDomainZero;
    uint8_t                    lastRxDomainAddr[cL2DomainAddrLen];
    /* Data associated with MCP header msgSeq and ackdSeqNum values. */

    uint8_t                    txSeqNum;
    /*! @c hdrTxLastSeqNum - updated with sent hdr->msgSeq on tx.
                             used on rx to check:
                                error if (hdr->ackdSeqNum != hdrTxLastSeqNum)
                             ** Another similar check, could do both:
                                error if (hdr->ackdSeqNum !=
                                          (hdrRxLastAckdSeqNum+1))
    */
    uint8_t                    hdrTxLastSeqNum;
    /*! @c hdrRxLastSeqNum - set by rxed hdr->msgSeq on rx,
                             used to set hdr->ackdSeqNum on tx. */
    uint8_t                    hdrRxLastSeqNum;
    /*! @c hdrTxLastAckdSeqNum - set with hdrRxLastSeqNum on tx. */
    uint8_t                    hdrTxLastAckdSeqNum;
    /*! @c hdrRxLastAckdSeqNum - set with hdr->ackdSeqNum on rx. */
    uint8_t                    hdrRxLastAckdSeqNum;


    tMcpMsgHdr                  lastRxHdr;

    /* Data associated with Ack received-message information. */
    eMcpAckStatus               lastDevInfoAckAckStatus;
    eMcpAckStatus               lastSimpleAckAckStatus;

    /* Data associated with Debug received-message information. */
    eMcpDebugReq                debugRequest;
    uint16_t                   ctMsgCnt;
    uint16_t                   ctCDataLen;
    uint16_t                   ctTxCnt;
    uint16_t                   ctRxCnt;
    tMcpDebugCommCtl            ct;

    uint8_t                    devRand;
} tmodmcpModCtl;

typedef enum _eModDlState {
    cModDlStateIdle,
    cModDlStateImgRspWait,
    cModDlStateImgMcFailedEofWait,
    cModDlStateNumOf
} eModDlState;


typedef enum _eModDlEvent {
    cModDlEventNone,
    cModDlEventImgInfo,
    cModDlEventImgRsp,
    cModDlEventImgRspTimeout,
    cModDlEventMcMissedImgRsp,
    cModDlEventImgEof,
    cModDlEventNumOf
} eModDlEvent;

typedef struct _tModDlEventData {
    eModDlEvent                 event;
    union {
        struct {
            uint16_t                           imgDataLen;
            tMcpImageRspMsg        * m;
        } imgRsp;
    } u;
} tModDlEventData;

typedef struct _tModDlCtl {
    bool                        dlIsBcast;
    u_int8_t                    repCnt;
    eModDlEvent                 lastEvent;
    u_int8_t                    imgDirIdx;
    eModDlState                 state;
    uint16_t                   missedAt;
    uint16_t                   calen;
    uint16_t                   count;
} tModDlCtl;


typedef struct _tPlcAppCtl {
    uint32_t                   intervalTickCount;
    uint8_t                    ticksOnSecond;
    uint8_t                    gwL2ProtoVer;

    tImageMetaData  * imd;

    tControllerCnts             cnts;

    tmodmcpModCtl               mcp;
    tIcarusDownloadCtl          dl;
    tModDlCtl                   modDl;

    /* Set to indicate that PCU is in discovery mode. This is done
     * when a new device poll is received and all the conditions for
     * PCU to respond to the new device poll are satisfied.
     */
    bool                        inDiscoveryMode;
    bool                        discoveryTxAborted;
    bool                        discoveryTxFlushCmplt;
    uint8_t                    discoveryRetryCnt;
    uint8_t                    discoveryRetryOK;
} tPlcAppCtl;

typedef struct _tEventInfoHdr {
    uint8_t    count;         // count of events in addendum
    uint8_t    fillb[3];      // fill
    uint32_t   baseTimeStamp; //
} tEventInfoHdr;

typedef struct _tEventInfo {
  tEventInfoHdr        eventHdr;
  tMcpPcuEventAddendum addendum[cEventAddendumMax];
} tEventInfo;



/*@}*/  /* END of modmcp Header Typedef Group */

/* ------------------------------------ Extern Declarations ---------- */

/**
 * @defgroup modmcpHExtdata modmcp Header Extern Data Definitions
 */
/*@{*/

extern tEventInfo            EventInfo;
extern uint32_t             plcIntervalSecCount;
extern uint32_t             UnackdIntervalSecs;

extern tMcpAgfGroupInfo      agfGroup;
extern tMcpDevInfoMsg_Ver3   devInfo;

/*@}*/  /* END of modmcp Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup modmcpHGlobdata modmcp Header Global Data Defined
 */
/*@{*/

extern uint8_t              modmcpDeclareAsOrphan;

extern uint8_t              lastRxMsgOnModemType;


/*@}*/  /* END of modmcp Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup modmcpHStatdata modmcp Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of modmcp Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup modmcpHProto modmcp Header Prototypes
 */
/*@{*/

extern tL2FrmAppMsg  *
modmcpMsgPrepSend( tMcpMsg  ** ppMsg);

extern void
modmcpMsgAckFormat(void    * bufp,
                   eMcpAckStatus        ackStatus);

extern void
modmcpMsgStatusFormat(void  * bufp);

extern void
modmcpMsgImgReqFormat(tMcpMsg   * bufp,
                        tImageIMeta  * imeta,
                        uint16_t           count,
                        uint32_t           imgOffset);

extern int
modmcpMsgDebugFormat(void  * bufp,
                     eMcpDebugReq       dbgRequest,
                     uint16_t          cdataLen);

/** modmcpMsgDevInfoFormat - Format a dev-info message. */
extern void
modmcpMsgDevInfoFormat(
                      void   * bufp);
/** modmcpMsgAckDevInfoReceive - Ack dev-info receive processing. */
extern void
modmcpMsgAckDevInfoReceive(tL2FrmAppMsg           * l2AppMsg);

extern void
modmcpMsgIntervalFormatPcu(void   * bufp);

extern void
modmcpMsgAckIntervalReceivePcu( tL2FrmAppMsg  * l2AppMsg);

extern void
modmcpMsgIntervalFormatPcuIe(void   * bufp);

extern void
modmcpMsgAckIntervalReceivePcuIe( tL2FrmAppMsg  * l2AppMsg);

extern void
modmcpMsgEventPcuIe(void   * bufp);

extern void
modmcpMsgAckEventPcuIe( tL2FrmAppMsg  * l2AppMsg);

extern int
modmcpInit(void);

/** modmcpCondFlagClr - clear alarm information for status report. */
extern void
modmcpCondFlagClr(uint32_t           conditionFlag );

/** modmcpCondFlagSet - set alarm information for status report. */
extern void
modmcpCondFlagSet(uint32_t           conditionFlag );

/** modmcpRxMsg - process pcu mcp receive message. */
extern void
modmcpRxMsg(tL2FrmAppMsg           * l2AppMsg);


/** modmcpTxMsg - transmit a MCP message. */
extern void
modmcpTxMsg(tL2FrmAppMsg     * l2AppMsg);

/** modmcpOrphanDeclare - Declare ourselves as orphaned. */
extern void
modmcpOrphanDeclare(void);

/** modmcpInitIntervalData - initialize interval data. */
extern void
modmcpInitIntervalData(void);

/** modmcpMsgTripPointInfoFormat - Format a trip point info message. */
extern void
modmcpMsgTripPointInfoFormat(void    *bufp);

/** modmcpMsgAckTripPointInfoReceive - Ack trip point info receive processing. */
extern void
modmcpMsgAckTripPointInfoReceive(void);

extern void
modmcpMsgFastpath(tMcpFastpathIe  *fpMsg);

//extern void
//pcuSetPhase(uint8_t  phaseIdx);

//extern void
//pcuUpdateVrise(void *phaseAcMv);

extern tMcpSysConfigVriseData  *phaseVoltages;
extern uint8_t  emuVRiseValue[9];

extern uint8_t  phaseIdxAsm;
extern uint8_t  GotVRiseEvent;

extern uint8_t  debugVar[5];

/*! modmcpIsACB - Determines whether the parameter table asserts that this PCU is an AC Battery  */
#define modmcpIsACB() (*((uint8_t  *) PARAM_MISC_FLAGS) & PARAM_MISC_FLAGS_ACB)


/*@}*/  /* END of modmcp Header Prototypes Group */

/*@}*/  /* END modmcp Header Definitions and Declarations */


#endif /* _modmcp_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of modmcp.h */



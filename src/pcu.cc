
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#include <sys/time.h>
#endif // !WIN32
#include <assert.h>
#include "include/enphase.h"
#include "include/mcp_sunpower.h"

#include "include/pmi.h"
#include "include/agf.h"
#include "include/dll.h"

#include "include/mcp_debug.h"
#include "include/mcpsec.h"
#include "include/ring.h"
#include "include/timer.h"
#include "include/emuutl.h"
#include "include/emudbg.h"
#include "include/mcp.h"
#include "include/modmcp.h"
 
#include "pcu.hh"
#define AssMemCpy memcpy
bool
plcUtilDomainAddrIsZero(u_int8_t _keil_xdata * domainAddr)
{
    /* plcUtilDomainAddrIsZero */
    return (domainAddr[0] == 0) &&
        (domainAddr[1] == 0) &&
        (domainAddr[2] == 0) &&
        (domainAddr[3] == 0) &&
        (domainAddr[4] == 0) &&
        (domainAddr[5] == 0);

} /* END of plcUtilDomainAddrIsZero */


bool
plcUtilSerialNumMatch(u_int8_t _keil_xdata * sn1,
                      u_int8_t _keil_xdata * sn2) {
    return ((memcmp(sn1, sn2, cSerialNumBytesLen) == 0));
}

cPCUSim::cPCUSim(tSerialNumber *serialNumber)
{
	memcpy(&plcAppManuData.serialNumber, serialNumber, sizeof(plcAppManuData.serialNumber));
    LoadNonVolatileData();
}
cPCUSim::~cPCUSim()
{
}
int cPCUSim::LoadNonVolatileData()
{
// Using serial number open file, read in runtime data and keep for "flash" operations
	return 0;
}

bool
plcUtilDomainAddrIsZero(const uint8_t  * domainAddr)
{
    /* plcUtilDomainAddrIsZero */
    return (domainAddr[0] == 0) &&
        (domainAddr[1] == 0) &&
        (domainAddr[2] == 0) &&
        (domainAddr[3] == 0) &&
        (domainAddr[4] == 0) &&
        (domainAddr[5] == 0);

} /* END of plcUtilDomainAddrIsZero */

int cPCUSim::PacketFilter(const tL2TxFrameInfo *pL2)
{

	int process = 1;

   // check if message is from Envoy
   if (pL2->reassPattern.msgSrcEmu != 0)
   {
	   printf("bad EMU\n");
	   return 0;
   }


   if ( ((be16toh(pL2->reassPattern.modAddr) == 0x3FF) &&
		  ((plcUtilDomainAddrIsZero(pL2->reassPattern.domainAddr) != 0) || (memcmp(pL2->reassPattern.domainAddr, plcAppManuData.serialNumber.byteArr,6) == 0))))
	  {
		 // allow message through if for sunpower and special modAddr (0x3FF) and either serial number matches 'domain' or domain is 0
		 // if domain is 0 and modAddr is 0x3FF, should be broadcast type message used for find all inverters regardless of association
		 // if domain matches serial number, then a way to match on SN when associated and don't know or remember what it is!
		 // ONLY used for SunPower messages
		 // The rest of the messages will require matching domain!!!
		 
		 

	  }
	  else
	  {
		 if ((Dms.modId != 0) &&
			 (be16toh(pL2->reassPattern.modAddr) != 0) &&
			 (Dms.modId != be16toh(pL2->reassPattern.modAddr)))
		 {
			return 0;
		 }
	  }

   

	switch (pL2->scopeInd) {
	case (int)eL2ScopeCode::Directed:
		// can be UNICAST or BCAST DIRECTED

	if ((memcmp(pL2->reassPattern.domainAddr, Dms.domainAddr, 6) == 0 && be16toh(pL2->reassPattern.modAddr) == Dms.modId) ||
		(be16toh(pL2->reassPattern.modAddr) == 0x3FF && memcmp(pL2->reassPattern.domainAddr, plcAppManuData.serialNumber.byteArr,6) == 0)) {
       m_sendResponseToPacket = 1;

		return 1;
	}
	else
	{
		 //printf("modAddr: %d %02X %02X \n", be16toh(pL2->reassPattern.modAddr), pL2->reassPattern.domainAddr[0], m_serialNumber.byteArr[0]);
		return 0;
	}
	break;
	case (int)eL2ScopeCode::McastMod:
		break;
	case (int)eL2ScopeCode::BcastCoord:

		break;
	case (int)eL2ScopeCode::BcastAll:
		break;

	}

	return process;
	

}

void
cPCUSim::modmcpSendPollRsp(u_int16_t _keil_xdata pollReqFlags,
                  u_int8_t  _keil_xdata cmdSeqNum, uint8_t *OutMsg, uint16_t *OutLen)
{
   tL2FrmAppMsg    _keil_xdata *txL2AppMsg;
   tMcpMsg         _keil_xdata *txMsg;
   tMcpPollRspTlv  _keil_xdata *pRsp;
   u_int8_t        _keil_xdata *pNextTvlData;
   u_int16_t       _keil_xdata  msgSizeCnt = sizeof(tMcpMsgHdr) + sizeof(tMcpPollRspTlv);
   u_int8_t        _keil_xdata  loop;
   u_int16_t       _keil_xdata  offset = 0;
   u_int16_t       _keil_xdata  loopFlags;
   tMcpSsiMeasures _keil_xdata  ssiMeasures;

   txL2AppMsg = (tL2FrmAppMsg *)OutMsg;
   AssMemCpy(&txL2AppMsg->pmi, &pmiData.cfg, sizeof(pmiData.cfg));
   

   txMsg = (tMcpMsg _keil_xdata *)&txL2AppMsg->mb[0];
   txMsg->hdr.msgId = cMcpMsgIdPollRspTlv;
   txMsg->hdr.ctrlAndMcpProtoVers = cMcpProtocolVersionSeven;
   pRsp = &txMsg->m.pollRspTlv;

   pNextTvlData =  pRsp->tlvData;
   pRsp->tlvCnt = 0;
   pRsp->pollRspFlags = 0;

   printf("HERE 1\n");
   for (loop = 0; loop < cMcpPollDataNumberOf; loop++)
   {
      offset = 0;
      loopFlags = ((1 << loop) & pollReqFlags);

      if (loopFlags & cMcpPollFlagDeviceInfo)
      {
         tMcpPollRspDevInfoTlv  _keil_xdata *pRspDevInfo;

         pRspDevInfo = (tMcpPollRspDevInfoTlv *)pNextTvlData;
         pRspDevInfo->tlvHdr.type = cMcpPollDataDeviceInfo;
         pRspDevInfo->tlvHdr.length = htobe16(sizeof(tMcpPollRspDevInfo));

         AssMemCpy(&pRspDevInfo->devInfo.devSerialNum,   &devInfo.devSerialNumber, sizeof(tSerialNumber));
         AssMemCpy(&pRspDevInfo->devInfo.devPartNum,     &devInfo.devPartNumber, sizeof(tPartNumber));
         AssMemCpy(&pRspDevInfo->devInfo.devAssemblyNum, &devInfo.assemblyPartNumber, sizeof(tPartNumber));
         AssMemCpy(&pRspDevInfo->devInfo.asicId, &uniqueAsicIdNum, sizeof(uniqueAsicIdNum));
         // TODO: DAH getSunPowerManu((u_int8_t _keil_xdata *)&pRspDevInfo->devInfo.moduleEntry);
         AssMemCpy(&pRspDevInfo->devInfo.domainAddr, &Dms.domainAddr, cL2DomainAddrLen);
         pRspDevInfo->devInfo.devType = devInfo.devType;
         pRspDevInfo->devInfo.modAddr = htobe16(Dms.modId);
         pRspDevInfo->devInfo.grpMask = htobe32(Dms.agfGroup.mask);
         pRspDevInfo->devInfo.flags = Dms.newAssocStatus;
         pRspDevInfo->devInfo.flags |= (plcAppCtl.inDiscoveryMode) ? cDevInfoFlagsMaskDiscover : 0;
         pRspDevInfo->devInfo.flags |= (cMcpProtocolVersionSeven << cDevInfoFlagsShiftProtocol);
		 pRspDevInfo->devInfo.flags = htobe16(pRspDevInfo->devInfo.flags);
		 printf("HERE 2\n");

         // TODO: DAH ssiReadingsGet(&ssiMeasures);
		 ssiMeasures.rxEmuSsi = 116;
         pRspDevInfo->devInfo.rxEmuSsi = htobe16(ssiMeasures.rxEmuSsi);

         offset = sizeof(tMcpPollRspDevInfoTlv);
      }

      if (loopFlags & cMcpPollFlagImageInfo)
      {
         if ((msgSizeCnt + sizeof(tMcpPollRspImageInfoTlv)) < cAppMsgLenWPadIV)
         {
            tMcpPollRspImageInfoTlv _keil_xdata *pRspImageData;

            pRspImageData = (tMcpPollRspImageInfoTlv *)pNextTvlData;
            pRspImageData->tlvHdr.type = cMcpPollDataImageInfo;
            pRspImageData->tlvHdr.length = htobe16(sizeof(tImageMetaData));
            AssMemCpy(&pRspImageData->imd,  &imgInventory, sizeof(tImageMetaData));
            // TODO: DAH byte swap
            offset = sizeof(tMcpPollRspImageInfoTlv);
         }
      }

      if (loopFlags & cMcpPollFlagAgfInfo)
      {
         if (msgSizeCnt + sizeof(tMcpPollRspAgfRecListTlv) +
             (sizeof(tMcpAgfLoadedRecStatus) * (cAgfFunctionTypeNumOf - 3)) < cAppMsgLenWPadIV)
         {
            tMcpPollRspAgfRecListTlv  _keil_xdata *pRspAgfRec;
            u_int8_t                  _keil_xdata  count = 0;

            pRspAgfRec = (tMcpPollRspAgfRecListTlv *)pNextTvlData;
            pRspAgfRec->tlvHdr.type = cMcpPollDataAgfInfo;
            pRspAgfRec->tlvHdr.length = htobe16(sizeof(tMcpAgfLoadedRecStatus));

            // kick dog before flash read
            // TODO: DAH KickTheDog();

            for (count = cAgfFunctionTypeCookie; count < cAgfFunctionTypeNumOf; count++)
            {

               // TODO: DAH modmcpReadAgfRecMeta(count);

               AssMemCpy(&pRspAgfRec->recList[count],
                         &agfRecStatus[count].recLoadedStatus,
                         sizeof(tMcpAgfLoadedRecStatus));
            }

            pRspAgfRec->tlvHdr.length = htobe16((sizeof(tMcpAgfLoadedRecStatus) * count));
            offset = sizeof(tMcpTlvHeader) + be16toh(pRspAgfRec->tlvHdr.length);
         }
      }

      if (loopFlags & cMcpPollFlagInterval)
      {
         if ((msgSizeCnt + sizeof(tMcpPollRspIntervalTlv)) < cAppMsgLenWPadIV)
         {
            tMcpPollRspIntervalTlv  _keil_xdata *pRspInterval;
            tMcpPcuPwrIntervalData  _keil_xdata *pIntervalData;
            u_int32_t               _keil_xdata  deltatIntervalSecs = 0;

            pRspInterval = (tMcpPollRspIntervalTlv *)pNextTvlData;
            pRspInterval->tlvHdr.type = cMcpPollDataInterval;
            pRspInterval->tlvHdr.length = htobe16(sizeof(tMcpPcuPwrIntervalData));

            pIntervalData = &pRspInterval->intervalData;
            memset((u_int8_t *)pIntervalData, 0, sizeof(tMcpPcuPwrIntervalData));

            // TODO: DAH PowerDataGet((tMcpPcuPwrIntervalIe _keil_xdata *)&powerData);
            AssMemCpy(&pIntervalData->pwr, &powerData.pwrConvErrSecs, sizeof(tMcpPcuData));

            // load in the odometer data
            AssMemCpy((u_int8_t _keil_xdata *)pIntervalData->pwr.producedMilliJoules, CurrentOdometer, 6);

            pIntervalData->temperature = powerData.chanTemp_heron;

            // only unlock and skipped cycles are sent to Envoy in interval message.
            if (bMPPTunlocked != 0)
            {
               pIntervalData->flags |= cMcpPwrFlagMpptUnlocked;
            }

            // If errored seconds is greater than 5% of interval then
            // Raise "Skipped Cycles" condition flag.
            if ((prevIntervalSecs > 0) && (plcIntervalSecCount > prevIntervalSecs))
            {
               deltatIntervalSecs = plcIntervalSecCount - prevIntervalSecs;
               if (powerData.pwrConvErrSecs > (deltatIntervalSecs / 20))
               {
                  pIntervalData->flags |= cMcpPwrFlagSkipCycles;
               }
            }
            
            pIntervalData->onTime = htobe32(plcIntervalSecCount);
            prevIntervalSecs = plcIntervalSecCount;
            ErroredCycles = 0;

            offset = sizeof(tMcpPollRspIntervalTlv);
         }
      }

      if (loopFlags & cMcpPollFlagCondition)
      {
         if ((msgSizeCnt + sizeof(tMcpPollRspConditionTlv)) < cAppMsgLenWPadIV)
         {
            tMcpPollRspConditionTlv  _keil_xdata *pRspCond;
            tMcpConditionData        _keil_xdata *pConditionData;


            pRspCond = (tMcpPollRspConditionTlv *)pNextTvlData;
            memset((u_int8_t *)pRspCond, 0, sizeof(tMcpPollRspConditionTlv));
            pConditionData = &pRspCond->conditionData;

            pRspCond->tlvHdr.type = cMcpPollDataCondition;
            pRspCond->tlvHdr.length = htobe16(sizeof(tMcpConditionData));

            if (priorEventMsgsWoAck == 0)
            {
               lastCmdCondSeqNum = cmdSeqNum;

               memset(&eventBits[0], 0, (sizeof(eventBits[0]) * cMcpPcuEventBitsSize));
               // TODO: DAH IcarusConditionFlagsHist((u_int32_t _keil_xdata *)&eventBits[0]);
			   

               // save time of last condition message
               eventDataTimestamp = TimeStamp;
            }
			eventBits[3] &= 0xFFFF0000;
			eventBits[3] |= PcuStatusBits2;
            priorEventMsgsWoAck++;
            pConditionData->cmdSequenceNumber = lastCmdCondSeqNum;
            pConditionData->lastMsgTime = htobe32(eventDataTimestamp);
			// TODO: DAH swap all event bits
            AssMemCpy(&pConditionData->latchedConditions[0], eventBits, sizeof(eventBits));
            offset = sizeof(tMcpPollRspConditionTlv);
         }
      }

      if (loopFlags & cMcpPollFlagCounters)
      {
         if ((msgSizeCnt + sizeof(tMcpPollRspCounterTlv)) < cAppMsgLenWPadIV)
         {
            tMcpPollRspCounterTlv  _keil_xdata *pRspCounters;

            pRspCounters = (tMcpPollRspCounterTlv *)pNextTvlData;
            pRspCounters->tlvHdr.type = cMcpPollDataCounters;
            pRspCounters->tlvHdr.length = htobe16(sizeof(tMcpCounterData));

            // TODO: DAH plcCtlControllerCntsRead(&pRspCounters->counters.msgCounter);
            // TODO: DAH ravPlcRptPlcCnts(&pRspCounters->counters.frameCounters, &pRspCounters->counters.modemCounters);

            // TODO: DAH ssiReadingsGet(&pRspCounters->counters.ssiMeasures);
            pRspCounters->counters.ssiMeasures.flagsAndPlcIdx = htobe16(((cMcpSsiFlagInDbuV |
                                                                  cMcpSsiFlagFsk4Msk |
                                                                  cMcpSsiFlagImgUpgEnh1 |
                                                                  cMcpSsiFlagPlcSec) << cMcpSsiFlagsShf) | cMcpSsiPlcIdxHeron2Asic);

            // clear the counters
            // TODO: DAH plcCtlSsiAndCountersAck();
            // TODO: DAH plcCtlControllerCntsAck();

            offset = sizeof(tMcpPollRspCounterTlv);
         }
      }

      if (loopFlags & cMcpPollFlagPhaseAware)
      {
         if ((msgSizeCnt + sizeof(tMcpPollRspPhaseAwareTlv)) < cAppMsgLenWPadIV)
         {
            tMcpPollRspPhaseAwareTlv  _keil_xdata *pRspPhase;

            pRspPhase = (tMcpPollRspPhaseAwareTlv *)pNextTvlData;
            pRspPhase->tlvHdr.type = cMcpPollDataPhaseAware;
            pRspPhase->tlvHdr.length = htobe16(sizeof(tPaInfo));

            pRspPhase->paData.phaseDelta = htobe16(paInfoData.phaseDelta);
            pRspPhase->paData.phaseBin = paInfoData.phaseBin;
            pRspPhase->paData.phaseAwarenessSeqNum = paInfoData.phaseAwarenessSeqNum;

            offset = sizeof(tMcpPollRspPhaseAwareTlv);
         }
      }
      // TODO: DAH Stopped endianess here
      if (loopFlags & cMcpPollFlagSecurity)
      {
         if ((msgSizeCnt + sizeof(tMcpPollRspSecurityTlv)) < cAppMsgLenWPadIV)
         {
            tMcpPollRspSecurityTlv  _keil_xdata *pRspSecData;
            tMcpMsg                 _keil_xdata *pSecMsg;

            pRspSecData = (tMcpPollRspSecurityTlv *)pNextTvlData;
            pRspSecData->tlvHdr.type = cMcpPollDataSecurity;
            pRspSecData->tlvHdr.length = htobe16(sizeof(tSecurityData));

            // the blob is sized to be big enough to contain a security message
            pSecMsg = (tMcpMsg _keil_xdata *)&pRspSecData->secData.blob;
            pSecMsg->hdr.msgSeq = cmdSeqNum;

            // data loded by function
            // need to send cMcpMsgIdAckSecInfo in reply
            // TODO: DAH mcpsecmsgSecInfoMsgFormat(pSecMsg);

            offset = sizeof(tMcpPollRspSecurityTlv);
         }
      }


      if (offset > 0)
      {
         pRsp->pollRspFlags |= 1 << loop;
         msgSizeCnt += offset;
         pNextTvlData += offset;
         pRsp->tlvCnt++;
      }
   }
   pRsp->pollRspFlags = htobe16(pRsp->pollRspFlags);
   pRsp->msgTime = htobe32(TimeStamp);
   pRsp->pollRspStatus = htobe16(pollRspStatus);
   pRsp->sequenceNumber = cmdSeqNum;
   txMsg->hdr.msgLen = htobe16(msgSizeCnt);

   // TODO: DAH modmcpTxMsg(txL2AppMsg);
}

int cPCUSim::HandleMcpMsgIdPollCmdTlv(tMcpPollCmdTlv *cmdMsg, uint8_t *OutMsg, uint16_t *OutLen)
{


	uint8_t                     cnt;
   uint16_t                    offset = 0;
   tMcpTlvHeader               *tlvHdr;
   bool                         sendRsp = true;

   

   TimeStamp = be32toh(cmdMsg->msgTime);


   // cleared when conditions are ack'd
   if (memcmp(LastMcpCondFlags, McpCondFlags, sizeof(LastMcpCondFlags)) != 0)
   {
      pollRspStatus |= cMcpPollFlagRspStatusConditionsChanged;
   }

   AssMemCpy(LastMcpCondFlags, McpCondFlags, sizeof(LastMcpCondFlags));

   cmdMsg->pollReqFlags = be16toh(cmdMsg->pollReqFlags);
   for (cnt = 0; cnt < cmdMsg->tlvCnt; cnt++)
   {
      tlvHdr = (tMcpTlvHeader *)&cmdMsg->tlvData[offset];

      switch (tlvHdr->type)
      {
      case cMcpPollDiscover:
         {
            // in discover mode do not respond unless not associated or matches the domain or any
            cmdMsg->pollReqFlags = 0;
             m_sendResponseToPacket = 0;
            // three cases where we respond to a discover request
            //  1)  Not associated
            //  2)  Domain matches
            //  3)  modAddr is 0x3FF and domain sent is zero (all-over ride) - doesn't matter if associated or not
            // Added new flag to handle squelch mode instead of a separate newAssocStatus
            if (Dms.squelched == 0)
            {

               if ((Dms.newAssocStatus == cMcpNewAssocStatusNone) ||
                   (plcAppCtl.mcp.rxMyDomain) ||
                   ((lastModAddr == (uint16_t)0x3FF) && (plcAppCtl.mcp.rxDomainZero != 0)))
               {
                  // force Device Info response
                  cmdMsg->pollReqFlags = cMcpPollFlagDeviceInfo;
                   m_sendResponseToPacket = 1;

                  if (!plcAppCtl.inDiscoveryMode)
                  {
                     plcAppCtl.inDiscoveryMode = true;

                    // ravPlcCarrSensEnable(); // send to USM
                  }
                  else
                  {
                     /* Workaround for getting scheduled tx packets to go
                        out at intended times: toggle tx holdoff enable */
                    // ravPlcCarrSenseWorkaroundToggleTxHoldoff();
                  }

                  plcAppCtl.discoveryTxAborted = false;
                  plcAppCtl.discoveryTxFlushCmplt = false;
                  plcAppCtl.discoveryRetryCnt = 0;
                  plcAppCtl.discoveryRetryOK = true;
               }
            }
         }
         offset += sizeof(tMcpPollCmdDiscoverTlv);
         break;

      case cMcpPollCmdControlFlags:
         {
            tMcpPollCmdControlFlagsTlv  *pCmdCtrlFlags;

            pCmdCtrlFlags = (tMcpPollCmdControlFlagsTlv  *)tlvHdr;

            //IcarusCtlFlagsProcess(pCmdCtrlFlags->controlFlags);
            if (pCmdCtrlFlags->controlFlags & cMcpPcuCtlFlagCmdAlert)
            {
               //IcarusAudible(pCmdCtrlFlags->periodInSecs);
            }
         }

         offset += sizeof(tMcpPollCmdControlFlagsTlv);
         break;

      case cMcpPollConditionAcknowledge:
         {
            tMcpPollCmdConditionAcknowledgeTlv  *pCmdAckConditions;

            // don't process if can not respond
            if (sendRsp)
            {
               pCmdAckConditions = (tMcpPollCmdConditionAcknowledgeTlv  *)tlvHdr;

               if ((pCmdAckConditions->sequenceNumber == lastCmdCondSeqNum) ||
                   (pCmdAckConditions->sequenceNumber == 0))
               {
                  //PowerConditionAck();
                  priorEventMsgsWoAck = 0;

                  // clear status on Ack
                  pollRspStatus &= ~(cMcpPollFlagClearOnAck);
               }
            }
         }

         offset += sizeof(tMcpPollCmdConditionAcknowledgeTlv);
         break;

      case cMcpPollSetGroupMask:
         {
            tMcpPollCmdSendGroupMaskTlv  *pCmdSetGrpMask;
            if (plcAppCtl.mcp.pktForMe || plcAppCtl.mcp.rxModAddrZero)
            {
               pCmdSetGrpMask = (tMcpPollCmdSendGroupMaskTlv  *)tlvHdr;
			   m_grpMask = be32toh(pCmdSetGrpMask->grpMask);
               //modmcpMsgSetGroupMask(pCmdSetGrpMask->grpMask);
            }
         }

         offset += sizeof(tMcpPollCmdSendGroupMaskTlv);
         break;

      case cMcpPollSendPollData:
         {
            tMcpPollCmdSendPollDataTlv  *pCmdSendPollData;
            pCmdSendPollData = (tMcpPollCmdSendPollDataTlv  *)tlvHdr;
            printf("Flags: %04x\n", cmdMsg->pollReqFlags);
            // always send response if serial number matchs
            if (plcUtilSerialNumMatch((uint8_t  *)&plcAppManuData.serialNumber,
                                      (uint8_t  *)&pCmdSendPollData->devSerialNum))
            {
               m_sendResponseToPacket = 1;
			   //printf("Wooohoo\n");
            }
			else
			{
				//printf("struct %d\n", sizeof(tMcpTlvHeader));
				//printf("%02X %02X %02X %02X \n", plcAppManuData.serialNumber.byteArr[0], plcAppManuData.serialNumber.byteArr[1], pCmdSendPollData->devSerialNum.byteArr[0],pCmdSendPollData->devSerialNum.byteArr[1]);
			}

         }
         offset += sizeof(tMcpPollCmdSendPollDataTlv);
         break;
      }
   }

   if (m_sendResponseToPacket)
   {
      modmcpSendPollRsp(cmdMsg->pollReqFlags, cmdMsg->sequenceNumber, OutMsg, OutLen);
   }

return 0;
}

uint16_t mcpFletcher8BitCsum(uint8_t *m, int len)
{
    uint8_t a = 0, b = 0;
    int i = 0;
    for (; i < len; i++) {
        a += *(m + i);
        b += a;
    }
    return (a << 8) + b;
}
int
emuutlCheckMcpCsum(const tMcpMsg * m, uint32_t len)
{
    uint16_t                   csum;
    uint16_t                   mcsum;


    /* emuutlCheckMcpCsum */
    mcsum = be16toh(m->hdr.msgCsum);
	//printf("len: %d, chksum: %d\n", len, mcsum);
    csum = mcpFletcher8BitCsum(((uint8_t *)m) + 2, len-2);
	//printf("csum: %d\n", csum);
    return (csum == mcsum) ? ENP_OK : ENP_ERROR;
} /* END of emuutlCheckMcpCsum */

int cPCUSim::ProcessPLCMessage(const uint8_t *InMsg, uint32_t msgLen, uint8_t *OutMsg, uint16_t *OutLen, tPmiDataConfigData **pPmiData)
{
	// process PLC message - may not be for me...if I should respond, fill in out,outlen, and current PMI

	int rc=1;
	uint32_t mcpLen;
	tL2TxFrameInfo *pFrame;
	uint16_t modAddr;
	uint8_t msgId;
	tMcpMsgHdr *pMcpMsgHdr = (tMcpMsgHdr *)InMsg;
	msgId = pMcpMsgHdr->msgId;
   mcpLen = msgLen - sizeof(tL2TxFrameInfo) - 2;
   pFrame = (tL2TxFrameInfo *)&InMsg[mcpLen];

   m_sendResponseToPacket = 0;  // do not respons yet...
   if (mcpLen > cAppMsgLen) {
	   printf("Bad length\n");
	   return 0;
   }

   if (emuutlCheckMcpCsum((tMcpMsg *)InMsg, mcpLen) != 0)
	 {
	   printf("bad checksum\n");
		return 0;
	 }
   
	 // message length already checked by caller:
	 if ((mcpLen <= sizeof(tMcpMsgHdr)) || (msgId >= cMcpMsgIdNumOf))
	 {
		 printf("bad length or msgId\n");
		return 0;
	 }

   plcAppCtl.mcp.rxMyModAddr = false;
   plcAppCtl.mcp.rxMyDomain = false;
   plcAppCtl.mcp.pktForMe = false;
   plcAppCtl.mcp.rxIsBcast = (pFrame->ackCtrl == cL2PktCtlPktTypeUnAckReq);
   plcAppCtl.mcp.rxModAddrZero = (0 == pFrame->reassPattern.modAddr);
   plcAppCtl.mcp.rxDomainZero = plcUtilDomainAddrIsZero(&pFrame->reassPattern.domainAddr[0]);
   plcAppCtl.mcp.rxScopeInd = pFrame->scopeInd;
   plcAppCtl.mcp.packetType = pFrame->ackCtrl;

   if ((Dms.assocStatus >= cMcpDomainMembershipStatusValid) ||
       (Dms.newAssocStatus == cMcpNewAssocStatusAssociated))
   {
      plcAppCtl.mcp.rxMyModAddr = (Dms.modId == pFrame->reassPattern.modAddr);
      plcAppCtl.mcp.rxMyDomain = plcUtilSerialNumMatch(&pFrame->reassPattern.domainAddr[0], &Dms.domainAddr[0]);

      if (plcAppCtl.mcp.rxMyDomain && plcAppCtl.mcp.rxMyModAddr)
      {
         plcAppCtl.mcp.pktForMe = true;
      }
   }
   lastModAddr = 0;


   if (PacketFilter(pFrame) == 0)
   {
	   return 0;
   }

   switch (pMcpMsgHdr->msgId)
         {
         case 	cMcpMsgIdNone:              // 0
            printf("cMcpMsgIdNone: ");
            break;
         case 	cMcpMsgIdAckInterval:       // 1
            printf("cMcpMsgIdAckInterval: ");
            break;

         case 	cMcpMsgIdAckDevInfo:        // 2
            printf("cMcpMsgIdAckDevInfo: ");
            break;

         case 	cMcpMsgIdAckSimple:         // 3
            printf("cMcpMsgIdAckSimple: ");
            break;

         case 	cMcpMsgIdPoll:              // 4
            printf("cMcpMsgIdPoll: ");
            break;

         case 	cMcpMsgIdScan:              // 5
            printf("cMcpMsgIdScan: ");
            break; 

         case 	cMcpMsgIdDomainCtl:         // 6
            printf("cMcpMsgIdDomainCtl: ");
            break;

         case 	cMcpMsgIdInterval:          // 7
            printf("cMcpMsgIdInterval: ");
            break;

         case 	cMcpMsgIdDevInfo:           // 8
            printf("cMcpMsgIdDevInfo: ");
            break;

         case 	cMcpMsgIdImgInventory:      // 9
            printf("cMcpMsgIdImgInventory: ");
            break;

         case 	cMcpMsgIdImgInfo:           // 10
            printf("cMcpMsgIdImgInfo: ");
            break;

         case 	cMcpMsgIdImgReq:            // 11
            printf("cMcpMsgIdImgReq: ");
            break;

         case 	cMcpMsgIdImgRsp:            // 12
            printf("cMcpMsgIdImgRsp: ");
            break;

         case 	cMcpMsgIdDebug:             // 13
            printf("cMcpMsgIdDebug: ");
            break;

         case 	cMcpMsgIdTripPointInfo:     // 14
            printf("cMcpMsgIdTripPointInfo: ");
            break;

         case 	cMcpMsgIdAckTripPointInfo:  // 15
            printf("cMcpMsgIdAckTripPointInfo: ");
            break;

         case 	cMcpMsgIdPlcCfg:            // 16
            printf("cMcpMsgIdPlcCfg: ");
            break;

         case 	cMcpMsgIdSecInfo:           // 17
            printf("cMcpMsgIdSecInfo: ");
            break;

         case 	cMcpMsgIdAckSecInfo:        // 18
            printf("cMcpMsgIdAckSecInfo: ");
            break;

         case 	cMcpMsgIdMsgErr:            // 19
            printf("cMcpMsgIdMsgErr: ");
            break;

         case 	cMcpMsgIdAgfAdcData:        // 20
            printf("cMcpMsgIdAgfAdcData: ");
            break;

         case 	cMcpMsgIdAgfAdcInfo:        // 21
            printf("cMcpMsgIdAgfAdcInfo: ");
            break;

         case 	cMcpMsgIdAgfAdcReq:         // 22
            printf("cMcpMsgIdAgfAdcReq: ");
            break;

         case 	cMcpMsgIdIntervalIe:        // 23
            printf("cMcpMsgIdIntervalIe: ");
            break;

         case 	cMcpMsgIdAckIntervalIe:     // 24
            printf("cMcpMsgIdAckIntervalIe: ");
            break;

         case 	cMcpMsgIdEventIe:           // 25
            printf("cMcpMsgIdEventIe: ");
            break;

         case 	cMcpMsgIdAckEventIe:        // 26
            printf("cMcpMsgIdAckEventIe: ");
            break;

         case 	cMcpMsgIdMsgCapIe:          // 27
            printf("cMcpMsgIdMsgCapIe: ");
            break;

         case 	cMcpMsgIdFastpath:          // 28
            printf("cMcpMsgIdFastpath: ");
            break;

         case 	cMcpMsgIdPaCapture:         // 29
            printf("cMcpMsgIdPaCapture: ");
            break;

         case 	cMcpMsgIdPaInfo:            // 30
            printf("cMcpMsgIdPaInfo: ");
            break;

         case 	cMcpMsgIdDevInfoIe:         // 31
            printf("cMcpMsgIdDevInfoIe: ");
            break;

         case 	cMcpMsgIdNonSecure:         // 32
            printf("cMcpMsgIdNonSecure: ");
            break;

         case 	cMcpMsgIdSysConfig:         // 33
            printf("cMcpMsgIdSysConfig: ");
            break;

         case 	cMcpMsgIdPollCmdTlv:        // 34
            printf("cMcpMsgIdPollCmdTlv: ");
			HandleMcpMsgIdPollCmdTlv((tMcpPollCmdTlv *)&InMsg[sizeof(tMcpMsgHdr)], OutMsg, OutLen);
            break;

         case 	cMcpMsgIdPollRspTlv:        // 35
            printf("cMcpMsgIdPollRspTlv: ");
            break;

         case 	cMcpMsgIdAgfRecords:        // 36
            printf("cMcpMsgIdAgfRecords: ");
            break;

         case 	cMcpMsgIdExpAgfRecordTlv:   // 37
            printf("cMcpMsgIdExpAgfRecordTlv: ");
            break;

         case 	cMcpMsgIdAssociation:       // 38
            printf("cMcpMsgIdAssociation: ");
            break;

         case 	cMcpMsgIdMfg:               // 39
            printf("cMcpMsgIdMfg: ");
            break;

         default:
            printf("Unknown MCP Message: ");
            break;
         }
      //printf("\n");

	  pFrame = (tL2TxFrameInfo *)&InMsg[mcpLen];
	  modAddr = be16toh(pFrame->reassPattern.modAddr);

	  printf("Prot: %d: ", pFrame->protocolId);
      printf("ProtVer: %d: ", pFrame->protoVer);
      printf("Scope: %d: ", pFrame->scopeInd);
	  printf("AckCtl: %d: ", pFrame->ackCtrl);
      printf("Emu: %d: ", pFrame->reassPattern.msgSrcEmu);
      printf("Domain: %02X%02X%02X%02X%02X%02X: ", pFrame->reassPattern.domainAddr[0], pFrame->reassPattern.domainAddr[1], pFrame->reassPattern.domainAddr[2], pFrame->reassPattern.domainAddr[3], pFrame->reassPattern.domainAddr[4], pFrame->reassPattern.domainAddr[5]);
      printf("Modaddr: %4X", modAddr);
	  fflush(stdout);


	return  rc;
}
void cPCUSim::Ticker(int numTicks)
{
	 // process any internal ticks ticks are in cycles
	
}
void cPCUSim::SetOperatingCondition(float freq, float acvRms, float dcmv)
{
	 // for processing if implemented
}
void cPCUSim::ResetPCU(int type)
{
	 // force internal reset - reload from files...
}


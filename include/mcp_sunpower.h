/* Copyright (c) 2006-2011 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _mcp_sunpower_h_
#define _mcp_sunpower_h_

/** @file mcp_sunpower.h
 *  @brief Header for Module Control Protocol (MCP)
 *
 *  This file @c mcp_sunpower.h provides MCP global definitions.
 */


#include "mcp_counters.h"
#ifndef PACKING
#define PACKING __attribute__((__packed__))
#endif
/**
 * @defgroup mcpHGroups mcp Header Definitions and Declarations
 */
/*@{*/

/* ------------------------------------ Defines --------------------------- */

#define cPartNumIdLen           4
#define cPartNumInStrFmtLen     24

#define cSerialNumBytesLen      6
#define cSerialNumStrLen        25  // Allow for non-PLC serial numbers
#define cMacAddrStrLen          18
#define cIpAddrStrLen           16

#define cAsicIdSize              8
#define cAsicIdStrLen            25
#define cL2DomainAddrLen         6

#define cImageFormatTchLgBinMaxSz 131072  // Heron or later
#define cImageFormatScrRecLen   16
#define cImageFormatScrLgProcloadBinLen 126976 // 124 KB
#define cImageFormatScrProcloadBinLen   65536
#define cImageFormatScrProcloadNumRecs  (cImageFormatScrProcloadBinLen / \
                                         cImageFormatScrRecLen)


#if !defined(ENPHASE)
/*! Define enPDeviceType */
/** This @c enPDeviceType enumeration defines enPhase device type.
 *
 *  One of the key items beside the more obvious causes for distinction
 *  between device types is the characteristic of unique hierachy
 *  traversal w.r.t. the device/controller/channel
 *  equipment model.  Thus
 *
 */
typedef enum _enPDeviceType {
    cnPDevTypeNone,
    cnPDevTypePcu,
    cnPDevTypePcu2,     /*!< a.k.a. dually */
    cnPDevTypeCmu,
    cnPDevTypeEmu,
    cnPDevTypePmu,
    cnPDevTypeAmu,
    cnPDevTypeTherm,
    cnPDevTypeRgm,
    cnPDevTypeZbrptr,
    cnPDevTypeEim,
    cnPDevTypeAcb,
    cnPDevTypeNumOf
} enPDeviceType;


/*! Define tPartNumber */
/** This @c tPartNumber structure is used to encode a enPhase part number.
    enPhase part numbers are used to designate uniquely both device types,
    image types, and their revisions.

    Example part number:
        520     - System software
        00001   - Firmware, PCU Load
        00      - revision 00   (00-99)

        Display as 520-00001-r00 (see emuutlPartNumFmt)

        So we need a minimum of 5 bytes to BCD encode the ten decimal
        digits of a part number.

    In software, enPhase part numbers are coded in big-endian BCD.

    The "d" fields of the part number are used or can be used in the
    following ways:
        - The "d" fields can be zero .. indicating id-manuRev as primary
          discriminators.
        - For purposes of uniqueness id-manuRev should be considered
          sufficient.
        - The "d" fields can be non-zero indicating a correspondence
          with a software release development process, and a further
          discrimination.
*/


/*! Define tSerialNumber */
/** This @c tSerialNumber structure is used to encode a nP serial number.
    nP serial numbers are used to designate uniquely device instances.

    In software, nP serial numbers are code in big-endian BCD.
*/

#endif // #if !defined(ENPHASE)

/*! tMcpIeHdr define the common information element header. */
typedef struct PACKING _tMcp5IeHdr {
    uint8_t                         version; /**< use eMcp5Ie. */
    uint8_t                         rsvd_ie; /* reserved for future IE work */
    uint16_t                        len;     /**< length of ie to follow. */
} tMcpIeHdr;

typedef struct PACKING _tMcpMsgHdr {
    uint16_t                    msgCsum;
    uint16_t                    msgLen;
    uint8_t                     ctrlAndMcpProtoVers;
    uint8_t                     msgId;      /*!< use @c eMcpMsgId. */
    uint8_t                     msgSeq;
    uint8_t                     ackdSeqNum;
} tMcpMsgHdr;

/**
 * @defgroup mcpHDefines mcp Header Defines
 */
/*@{*/

#define cMcpProtocolVersionFour     4
#define cMcpProtocolVersionFive     5
#define cMcpProtocolVersionSix      6     // skipped
#define cMcpProtocolVersionSeven    7

/*! Define eMcpMsgId */
/** This @c eMcpMsdId enumeration defines the type of messages that are
    part of MCP.
*/
typedef enum _eMcpMsgId {
    cMcpMsgIdNone,              // 0
    cMcpMsgIdAckInterval,       // 1
    cMcpMsgIdAckDevInfo,        // 2
    cMcpMsgIdAckSimple,         // 3
    cMcpMsgIdPoll,              // 4
    cMcpMsgIdScan,              // 5
    cMcpMsgIdDomainCtl,         // 6
    cMcpMsgIdInterval,          // 7
    cMcpMsgIdDevInfo,           // 8
    cMcpMsgIdImgInventory,      // 9
    cMcpMsgIdImgInfo,           // 10
    cMcpMsgIdImgReq,            // 11
    cMcpMsgIdImgRsp,            // 12
    cMcpMsgIdDebug,             // 13
    cMcpMsgIdTripPointInfo,     // 14
    cMcpMsgIdAckTripPointInfo,  // 15
    cMcpMsgIdPlcCfg,            // 16
    cMcpMsgIdSecInfo,           // 17
    cMcpMsgIdAckSecInfo,        // 18
    cMcpMsgIdMsgErr,            // 19
    cMcpMsgIdAgfAdcData,        // 20
    cMcpMsgIdAgfAdcInfo,        // 21
    cMcpMsgIdAgfAdcReq,         // 22
    cMcpMsgIdIntervalIe,        // 23
    cMcpMsgIdAckIntervalIe,     // 24
    cMcpMsgIdEventIe,           // 25
    cMcpMsgIdAckEventIe,        // 26
    cMcpMsgIdMsgCapIe,          // 27
    cMcpMsgIdFastpath,          // 28
    cMcpMsgIdPaCapture,         // 29
    cMcpMsgIdPaInfo,            // 30
    cMcpMsgIdDevInfoIe,         // 31
    cMcpMsgIdNonSecure,         // 32
    cMcpMsgIdSysConfig,         // 33
    cMcpMsgIdPollCmdTlv,        // 34
    cMcpMsgIdPollRspTlv,        // 35
    cMcpMsgIdAgfRecords,        // 36
    cMcpMsgIdExpAgfRecordTlv,   // 37
    cMcpMsgIdAssociation,       // 38
    cMcpMsgIdMfg,               // 39
	cMcpMsgIdOffGridCtrl,       // 40
	cMcpMsgIdOffGridRsp,        // 41
    cMcpMsgIdNumOf
} eMcpMsgId;

#define cMcpNumberOf     cMcpMsgIdNumOf

// **********************
// tMcpAckSimpleMsg
// **********************

typedef struct PACKING _tMcpAckSimpleMsg {
    uint8_t                     ackStatus;
    uint8_t                     fillb3;
    uint8_t                     fillb2;
    uint8_t                     fillb1;
} tMcpAckSimpleMsg;


// *************************
// tMcpImage Update messages
// *************************

#define cFHashLen   16
#define cMD5DigestLen   16

typedef enum _eMcpImageType {
    cMcpImageTypeNone,
    cMcpImageTypeProcLoad,
    cMcpImageTypePwrCondTbl,
    cMcpImageTypeManuData,
    cMcpImageTypeParm,
    cMcpImageTypeParm0 = cMcpImageTypeParm,
    cMcpImageTypeParm1,
    cMcpImageTypeNumOf
} eMcpImageType;

typedef struct PACKING _tImageIMeta {
    tPartNumber                 imagePartNum;
    uint8_t                     imageType;   /*!< use @c eMcpImageType. */
    uint8_t                     compatNum;
    uint8_t                     fillb2;
    uint8_t                     fillb1;
    uint8_t                     imgfHash[cFHashLen];
} tImageIMeta;

typedef struct PACKING _tImageTMeta {
    uint32_t                    targetLen;
    uint16_t                    targetCrc;
    uint8_t                     fillb2;
    uint8_t                     fillb1;
} tImageTMeta;


// The tImageTargetMeta structure, is sent down in image info message.
// The image image message, in a since announces a download.
//
// It is intended that this structure be the one defined for the "image meta
// data" as it is contained in the target flash device.
//
//  fill bytes are always zero used for alignment purposes.
//  crcForMetadata - this is a placeholder for the crc over the metadata
//                   record.
//
//  loadTimeValHi - signed upper dword ... has value 0
//                            until 2116-02-17 06:28:16.
//  loadTimeValLo - on unix time_t signed 32-bit epoch value, but treat as
//                  unsigned with respect to this metadata.
//  fHash - is used for purposes of a "echo back", i.e. this value is
//          associated with the md5 hash on the image file and is in the
//          target meta data for tracability purpose.
typedef struct PACKING _tImageTargetMeta {
    uint16_t                    crcForMetadata;
    uint8_t                     fillb2;
    uint8_t                     fillb1;
    int32_t                     loadTimeValHi;
    uint32_t                    loadTimeValLo;
    tImageIMeta                 imeta;
    tImageTMeta                 tmeta;
} tImageTargetMeta;

typedef struct PACKING _tMcpImageReqMsg {
    tPartNumber                 devPartNum;
    uint32_t                    offset;
    uint16_t                    count; /*!< num records or bytes. */
    uint8_t                     devType;    /*!< use @c enPDeviceType. */
    uint8_t                     fillb1;
    tImageIMeta                 imeta;
} tMcpImageReqMsg;


typedef struct PACKING _tMcpImageInfoMsg {
    tPartNumber                 devPartNum;
    uint8_t                     devType;    /*!< use @c enPDeviceType. */
    uint8_t                     isBcastDl;
    uint8_t                     forceReload;
    uint8_t                     flagsAndRepCnt; /* new R3.0 */
    tImageTargetMeta            meta;
} tMcpImageInfoMsg;

#define cMcpImgInfoFlagsAllMatchingDevs  0x80
/*! For cMcpImgInfoFlagsMetadataUpgradeOnly see cMcpSsiFlagImgUpgEnh1. */
#define cMcpImgInfoFlagsMetadataUpgOnly  0x40 // In PCU ImageNewStart uses
#define cMcpImgInfoFlagsParmToOffline    0x20 // In PCU ImageNewStart uses
#define cMcpImgInfoFlagsImgRspShort      0x10
#define cMcpImgInfoRepCntMsk             0x03  // rep is +1 of this field

/*! Only used in PLD not Envoy but keep here in both source trees for
 * documentation and consistency.
 *
 * Keep tMcpImageDataToFlash consistent with offset and below of
 * tMcpImageRspMsg. Used in PCU in call to _ImgDataToFlash.
 * With image upgrade enhanced support, the fillb1 field has been
 * changed to imgDataOffsetLen2Add.  If using short versions of
 * image response data message are in use, then the imgDataOffsetLen2Add
 * will be set to false, if not it will be set to 16.  This field is the
 * length to add to sizeof(tMcpImageDataToFlashShort) to get the offset
 * of the actual image data to be written to flash by the
 * PCU _ImgDataToFlash function.
 */
typedef struct PACKING _tMcpImageDataToFlash {
    uint32_t                    offset;
    uint16_t                    count; /*!< num records or bytes. */
    uint8_t                     devType;    /*!< use @c enPDeviceType. */
    uint8_t                     imgDataOffsetLen2Add; // (old) 16 for lg, (new) 0 for sm
    tImageIMeta                 imeta;
} tMcpImageDataToFlash;

typedef struct PACKING _tImageIMetaShort {
    tPartNumber                 imagePartNum;
    uint8_t                     imageType;   /*!< use @c eMcpImageType. */
    uint8_t                     compatNum;
    uint8_t                     fillb2;
    uint8_t                     fillb1;
} tImageIMetaShort;

/*! Keep tMcpImageDataToFlashSort consistent with offset and below of
 * tMcpImageRspShortMsg. Used in PCU in call to _ImgDataToFlash
 * when image info flags contains cMcpImgInfoFlagsImgRspShort.
 */
typedef struct PACKING _tMcpImageDataToFlashShort {
    uint32_t                    offset;
    uint16_t                    count; /*!< num records or bytes. */
    uint8_t                     devType;    /*!< use @c enPDeviceType. */
    uint8_t                     imgDataOffsetLen2Add; // (old) 16 for lg, (new) 0 for sm
    tImageIMetaShort            imetaShort;
} tMcpImageDataToFlashShort;

typedef struct PACKING _tMcpImageRspMsg {
    tPartNumber                 devPartNum;
    uint32_t                    offset;
    uint16_t                    count; /*!< num records or bytes. */
    uint8_t                     devType;    /*!< use @c enPDeviceType. */
    uint8_t                     fillb1;
    tImageIMeta                 imeta;
    /* image data follows here. */
} tMcpImageRspMsg;

/*! Use of tMcpImageRspShortMsg requires cMcpSsiFlagImgUpgEnh1 support and
 *  having seen cMcpImgInfoFlagsImgRspShort in the flagsAndRepCnt field
 *  if the associated tMcpImageInfoMsg message.
 */
typedef struct PACKING _tMcpImageRspShortMsg {
    tPartNumber                 devPartNum;
    uint32_t                    offset;
    uint16_t                    count; /*!< num records or bytes. */
    uint8_t                     devType;    /*!< use @c enPDeviceType. */
    uint8_t                     fillb1;
    tImageIMetaShort            imetaShort;
    /* image data follows here. */
} tMcpImageRspShortMsg;


// **********************
// tMcpFastpathIe
// **********************

#define cL2FrameDataSize     108
#define cMcpFastpathDataSize (cL2FrameDataSize -    \
                              sizeof(tMcpMsgHdr) - \
                              sizeof(tMcpIeHdr) -  \
                              sizeof(uint32_t ) - 4)

typedef enum _eMcpFpId {
    cMcpFpIdNone = 0,          // 0
    cMcpFpIdAgfInvMsg,         // 1 - not used
    cMcpFpIdAgfInvTvlMsg,      // 2
    cMcpFpIdNumOf
} eMcpFpId;

// max count of TLV items for cMcpFpIdAgfInvTvlMsg
#define cMcpFpAgfInvTvlCnt 2

typedef enum _eMcpFpTlvId {
    cMcpFpTlvInvNone = 0,   // 0
    cMcpFpTlvInvPcu,        // 1
    cMcpFpTlvInvAcb,        // 2
    cMcpFpTlvInvNumOf
} eMcpFpTlvId;

typedef struct PACKING _tMcpFastpathTlv {
    uint8_t       type;   //eMcpFpTlvId
    uint8_t       length;
} tMcpFastpathTlv;

typedef struct PACKING _tMcpFastpathIe {
    tMcpIeHdr     ieHdr;
    uint32_t      grpMask;
    uint8_t       fpId;        // eMcpFpId
    uint8_t       tlvCnt;
    uint16_t      dynSeqNum;
    uint8_t       fpData[cMcpFastpathDataSize];
} tMcpFastpathIe;

typedef struct PACKING _tMcpTlvHeader {
    uint8_t       type;
    uint16_t      length;
} tMcpTlvHeader;

// **********************
// tMcpAgfExpRecordsTlv
// **********************

typedef struct PACKING _tMcpAgfRecHeader {
    uint8_t       type;
    uint8_t       version;
    uint16_t      recLength;
    uint16_t      key;
    uint16_t      recCsum;
} tMcpAgfRecHeader;


typedef struct PACKING _tMcpAgfExpRecordsTlv {
    uint32_t          msgTime;
    uint32_t          grpMask;
    uint8_t           count;
    uint8_t           value[1];   // array of tMcpAgfRecHeader data
} tMcpAgfExpRecordsTlv;

// **********************
// tMcpAgfRecordsTlv
// **********************

typedef struct PACKING _tMcpAgfRecord {
    tMcpAgfRecHeader  recordHeader;
    uint8_t           recordData[1];
} tMcpAgfRecord;

typedef struct PACKING _tMcpAgfRecords {
    uint32_t            msgTime;
    uint32_t            grpMask;
    uint8_t             count;
    uint8_t             value[1];     // tMcpAgfRecord data
} tMcpAgfRecords;

// **********************
// tMcpPollCmdTlv
// **********************

// The poll Data Element flags are in priority order
// The response data in the reply will be loaded in priority
// order until all are loaded or message is full.  The pollRspFlags
// and TLV type will indicate what data is in the response.
typedef enum _eMcpPollDataElements {
    cMcpPollDataDeviceInfo,
    cMcpPollDataImageInfo,
    cMcpPollDataAgfInfo,
    cMcpPollDataInterval,
    cMcpPollDataCondition,
    cMcpPollDataCounters,
    cMcpPollDataPhaseAware,
    cMcpPollDataSecurity,
    cMcpPollDataNumberOf
} eMcpPollDataElements;

#define cMcpPollFlagDeviceInfo     ((uint16_t ) (1 << cMcpPollDataDeviceInfo))
#define cMcpPollFlagImageInfo      ((uint16_t ) (1 << cMcpPollDataImageInfo))
#define cMcpPollFlagAgfInfo        ((uint16_t ) (1 << cMcpPollDataAgfInfo))
#define cMcpPollFlagInterval       ((uint16_t ) (1 << cMcpPollDataInterval))
#define cMcpPollFlagCondition      ((uint16_t ) (1 << cMcpPollDataCondition))
#define cMcpPollFlagCounters       ((uint16_t ) (1 << cMcpPollDataCounters))
#define cMcpPollFlagPhaseAware     ((uint16_t ) (1 << cMcpPollDataPhaseAware))
#define cMcpPollFlagSecurity       ((uint16_t ) (1 << cMcpPollDataSecurity))

typedef enum _eMcpPollCmdTypes {
    cMcpPollCmdNone,
    cMcpPollDiscover,              // If the PLD is not associated, it will do fast discovery
    cMcpPollCmdControlFlags,       // Gateway to PLD commands
    cMcpPollConditionAcknowledge,  // Sequence number of last Condition poll response
    cMcpPollSetGroupMask,          // sets the device group mask in ram buffer.
    cMcpPollSendPollData,          // If the PLD serial number matches, respond with requested poll data
    cMcpPollCmdNumberOf
} eMcpPollCmdTypes;


///////////////////////////////////////////////////////////
//   P C U   C O N T R O L  F L A G S
///////////////////////////////////////////////////////////

#define cMcpPcuCtlFlagUndef_0x0001   0x0001
#define cMcpPcuCtlFlagPwrProdOff     0x0002
#define cMcpPcuCtlFlagClrGfi         0x0004
#define cMcpPcuCtlFlagReboot         0x0008
#define cMcpPcuCtlFlagCmdAlert       0x0010 // command LED to blink red
#define cMcpPcuCtlFlagManuTestMode   0x0020
#define cMcpPcuCtlFlagNSyncOff       0x0040
#define cMcpPcuCtlFlagUndef_0x0080   0x0080
#define cMcpPcuCtlFlagUndef_0x0100   0x0100
#define cMcpPcuCtlFlagUndef_0x0200   0x0200
#define cMcpPcuCtlFlagUndef_0x0400   0x0400
#define cMcpPcuCtlFlagUndef_0x0800   0x0800
#define cMcpPcuCtlFlagUndef_0x1000   0x1000
#define cMcpPcuCtlFlagUndef_0x2000   0x2000
#define cMcpPcuCtlFlagUndef_0x4000   0x4000
#define cMcpPcuCtlFlagUndef_0x8000   0x8000
#define cMcpPcuCtlFlagNumOf          16

typedef struct PACKING _tMcpPollCmdControlFlagsTlv {
    tMcpTlvHeader tlvHdr;
    uint16_t      controlFlags;
    uint16_t      periodInSecs;
} tMcpPollCmdControlFlagsTlv;

typedef struct PACKING _tMcpPollCmdConditionAcknowledgeTlv {
    tMcpTlvHeader tlvHdr;
    uint8_t       sequenceNumber;
} tMcpPollCmdConditionAcknowledgeTlv;

typedef struct PACKING  _tMcpPollCmdSendPollDataTlv {
    tMcpTlvHeader tlvHdr;
    tSerialNumber devSerialNum;
} tMcpPollCmdSendPollDataTlv;

typedef struct PACKING _tMcpPollCmdSendGroupMaskTlv {
    tMcpTlvHeader tlvHdr;
    tSerialNumber devSerialNum;
    uint32_t      grpMask;
} tMcpPollCmdSendGroupMaskTlv;

typedef struct PACKING _tMcpPollCmdDiscoverTlv {
    tMcpTlvHeader tlvHdr;
    uint8_t       fill;
} tMcpPollCmdDiscoverTlv;

typedef struct PACKING _tMcpPollCmdTlv {
    uint32_t      msgTime;
    uint16_t      pollReqFlags;
    uint8_t       sequenceNumber;
    uint8_t       tlvCnt;
    uint8_t       tlvData[1];
} tMcpPollCmdTlv;

// **********************
// tMcpPollRspTlv
// **********************

typedef enum _eMcpPollRspStatusTypes {
    cMcpPollRspStatusAgfConfig,              // AGF data in flash
    cMcpPollRspStatusAgfUpdated,             // AGF data has been updated by the gateway
    cMcpPollRspStatusAgfChecked,             // AGF data compared to gateway
    cMcpPollRspStatusAgfMatch,               // AGF data matches gateway
    cMcpPollRspStatusAgfLoadErr,             // Error in record data to load
    cMcpPollRspStatusAgfBadType,             // Unknown AGF type
    cMcpPollRspStatusAgfUnExpRec,            // Unexpected record is loaded
    cMcpPollRspStatusConditionsChanged,      // Some condition changed since last condition poll
    cMcpPollRspStatusNumberOf
} eMcpPollRspStatusTypes;

#define cMcpPollFlagRspStatusAgfConfig          (1 << cMcpPollRspStatusAgfConfig)
#define cMcpPollFlagRspStatusAgfUpdated         (1 << cMcpPollRspStatusAgfUpdated)
#define cMcpPollFlagRspStatusAgfChecked         (1 << cMcpPollRspStatusAgfChecked)
#define cMcpPollFlagRspStatusAgfMatch           (1 << cMcpPollRspStatusAgfMatch)
#define cMcpPollFlagRspStatusAgfLoadErr         (1 << cMcpPollRspStatusAgfLoadErr)
#define cMcpPollFlagRspStatusAgfBadType         (1 << cMcpPollRspStatusAgfBadType)
#define cMcpPollFlagRspStatusAgfUnExpRec        (1 << cMcpPollRspStatusAgfUnExpRec)
#define cMcpPollFlagRspStatusConditionsChanged  (1 << cMcpPollRspStatusConditionsChanged)

#define cMcpPollFlagClearOnAck                  (cMcpPollFlagRspStatusAgfLoadErr        | \
                                                 cMcpPollFlagRspStatusAgfBadType        | \
                                                 cMcpPollFlagRspStatusAgfUnExpRec       | \
                                                 cMcpPollFlagRspStatusConditionsChanged)

typedef struct PACKING _tMcpPollRspTlv {
    uint32_t      msgTime;
    uint16_t      pollRspFlags;
    uint16_t      pollRspStatus;
    uint8_t       sequenceNumber;
    uint8_t       tlvCnt;
    uint8_t       tlvData[1];
} tMcpPollRspTlv;

//
//    cMcpPollDataDeviceInfo Data
//

#define cSunpowerPvNameLen   20
#define cSunpowerPvModelLen   8
#define cSunpowerPvManufLen   8
typedef struct PACKING s_pv_module_info
{
    uint8_t  pv_name[cSunpowerPvNameLen];
    uint8_t  pv_model[cSunpowerPvModelLen];
    uint8_t  pv_manuf[cSunpowerPvManufLen];
} tEEPROM_PV_MODULE_ENTRY;


typedef enum _eMcpNewAssocStatus {
    cMcpNewAssocStatusNone,
    cMcpNewAssocStatusSquelch,
    cMcpNewAssocStatusAssociated,
    cMcpNewAssocStatusNumOf
} eMcpNewAssocStatus;


#define cDevInfoFlagsMaskAssoc      0x0003
#define cDevInfoFlagsMaskDiscover   0x0004
#define cDevInfoFlagsMaskProtocol   0x00F0

#define cDevInfoFlagsShiftDiscoverMode  2
#define cDevInfoFlagsShiftProtocol      4

typedef struct PACKING _tMcpPollRspDevInfo {
    tSerialNumber               devSerialNum;
    tPartNumber                 devPartNum;
    tPartNumber                 devAssemblyNum;
    tAsicId                     asicId;
    tEEPROM_PV_MODULE_ENTRY     moduleEntry;
    uint8_t                     domainAddr[cL2DomainAddrLen];
    uint8_t                     devType;
    uint16_t                    modAddr;
    uint32_t                    grpMask;
    uint16_t                    rxEmuSsi;
    uint16_t                    flags;
} tMcpPollRspDevInfo;

typedef struct PACKING _tMcpPollRspDevInfoTlv {
    tMcpTlvHeader        tlvHdr;
    tMcpPollRspDevInfo   devInfo;
} tMcpPollRspDevInfoTlv;

//
//    cMcpPollDataImageInfo Data
//

typedef enum _eMcpImgDirIndex {
    cMcpImgDirIdxPwrCondTbl,
    cMcpImgDirIdxProcLoad0,
    cMcpImgDirIdxProcLoad1,
    cMcpImgDirIdxParm0,    // parameter table associated with procload 0
    cMcpImgDirIdxParm1,    // parameter table associated with procload 1
    cMcpImgDirIdxManuData,
    cMcpImgDirIdxNumOf
} eMcpImgDirIndex;

typedef enum _eMcpImgRunningInd {
    cMcpImgRunningImg0Img1Bad,
    cMcpImgRunningImg1Img0Bad,
    cMcpImgRunningImg0Img1Ok,
    cMcpImgRunningImg1Img0Ok,
    cMcpImgRunningIndNumOf
} eMcpImgRunningInd;

typedef struct PACKING _tImageMetaData {
    uint8_t                     procImgIdxRunning; /*! eMcpImgRunningInd */
    uint8_t                     metaBadBits;/*!<(1<<{eMcpImgDirIndex val}) */
    uint8_t                     imageBadBits;/*!<(1<<{eMcpImgDirIndex val}) */
    uint8_t                     imgCount; // 6 or 8
    tImageTargetMeta            meta[cMcpImgDirIdxNumOf];
} tImageMetaData;

typedef struct PACKING _tMcpPollRspImageInfoTlv {
    tMcpTlvHeader        tlvHdr;
    tImageMetaData       imd;
} tMcpPollRspImageInfoTlv;

//
//    cMcpPollDataAgfInfo Data
//

typedef enum _eMcpAgfRecStatus {
    cMcpAgfRecStatusSupported,
    cMcpAgfRecStatusLoaded,
    cMcpAgfRecStatusMetaV1,
    cMcpAgfRecStatusMetaV2,
    cMcpAgfRecStatusVersionMatch,
    cMcpAgfRecStatusKeyMatch,
    cMcpAgfRecStatusCsumMatch,
    cMcpAgfRecStatusLengthMatch,
    cMcpAgfRecStatus_Undef_0100,
    cMcpAgfRecStatus_Undef_0200,
    cMcpAgfRecStatus_Undef_0400,
    cMcpAgfRecStatus_Undef_0800,
    cMcpAgfRecStatusLoadError,
    cMcpAgfRecStatusBadRxSize,
    cMcpAgfRecStatusBadRxCsum,
    cMcpAgfRecStatusBadRxRec,
    cMcpAgfRecStatusNumberOf
} eMcpAgfRecStatus;

#define cMcpAgfRecStatusFlagSupported    ((uint16_t ) (1 << cMcpAgfRecStatusSupported))
#define cMcpAgfRecStatusFlagLoaded       ((uint16_t ) (1 << cMcpAgfRecStatusLoaded))
#define cMcpAgfRecStatusFlagMetaV1       ((uint16_t ) (1 << cMcpAgfRecStatusMetaV1))
#define cMcpAgfRecStatusFlagMetaV2       ((uint16_t ) (1 << cMcpAgfRecStatusMetaV2))
#define cMcpAgfRecStatusFlagVersionMatch ((uint16_t ) (1 << cMcpAgfRecStatusVersionMatch))
#define cMcpAgfRecStatusFlagKeyMatch     ((uint16_t ) (1 << cMcpAgfRecStatusKeyMatch))
#define cMcpAgfRecStatusFlagCsumMatch    ((uint16_t ) (1 << cMcpAgfRecStatusCsumMatch))
#define cMcpAgfRecStatusFlagLengthMatch  ((uint16_t ) (1 << cMcpAgfRecStatusLengthMatch))
#define cMcpAgfRecStatusFlag_Undef_0100  ((uint16_t ) (1 << cMcpAgfRecStatus_Undef_0100))
#define cMcpAgfRecStatusFlag_Undef_0200  ((uint16_t ) (1 << cMcpAgfRecStatus_Undef_0200))
#define cMcpAgfRecStatusFlag_Undef_0400  ((uint16_t ) (1 << cMcpAgfRecStatus_Undef_0400))
#define cMcpAgfRecStatusFlag_Undef_0800  ((uint16_t ) (1 << cMcpAgfRecStatus_Undef_0800))
#define cMcpAgfRecStatusFlagLoadError    ((uint16_t ) (1 << cMcpAgfRecStatusLoadError))
#define cMcpAgfRecStatusFlagBadRxSize    ((uint16_t ) (1 << cMcpAgfRecStatusBadRxSize))
#define cMcpAgfRecStatusFlagBadRxCsum    ((uint16_t ) (1 << cMcpAgfRecStatusBadRxCsum))
#define cMcpAgfRecStatusFlagBadRxRec     ((uint16_t ) (1 << cMcpAgfRecStatusBadRxRec))

#define cMcpAgfRecStatusFlagExpChecked  (cMcpAgfRecStatusFlagVersionMatch | \
                                         cMcpAgfRecStatusFlagKeyMatch     | \
                                         cMcpAgfRecStatusFlagCsumMatch    | \
                                         cMcpAgfRecStatusFlagLengthMatch)

#define cMcpAgfRecStatusFlagBadRxFlags  (cMcpAgfRecStatusFlagLoadError | \
                                         cMcpAgfRecStatusFlagBadRxSize | \
                                         cMcpAgfRecStatusFlagBadRxCsum | \
                                         cMcpAgfRecStatusFlagBadRxRec)


typedef enum _eMcpAgfMetaVer {
    cMcpAgfMeta_V1 = 1,
    cMcpAgfMeta_V2 = 2,
    cMcpAgfMetaNumOf,
} eMcpAgfMetaVer;


typedef enum _eAgfFunctionType
{
    cAgfFunctionTypeCookie = 0,
    cAgfFunctionTypeVVAR,
    cAgfFunctionTypeFRT,
    cAgfFunctionTypeVRT,
    cAgfFunctionTypeFPF,
    cAgfFunctionTypePRL,
    cAgfFunctionTypePLP,
    cAgfFunctionTypeVW,
    cAgfFunctionTypeINV2,
    cAgfFunctionTypeWP,
    cAgfFunctionTypeTV,
    cAgfFunctionTypeFW,
    cAgfFunctionTypeSS,
    cAgfFunctionTypeISLND,
    cAgfFunctionTypeNotUsed_1,
    cAgfFunctionTypeIAC,
    cAgfFunctionTypeVECT,
    cAgfFunctionTypeROCOF,
    cAgfFunctionTypeACAVE,
    cAgfFunctionTypeVW52,
    cAgfFunctionTypeFW22,
    cAgfFunctionTypeWVAR,
    cAgfFunctionTypeNumOf

} eAgfFunctionType;


typedef struct PACKING _tMcpAgfLoadedRecStatus {
    tMcpAgfRecHeader  recordHeader;
    uint16_t          recStatus;
} tMcpAgfLoadedRecStatus;

// Count of items in the list is tlvHdr.length / sizeof(tMcpAgfLoadedRecStatus)
typedef struct PACKING _tMcpPollRspAgfRecListTlv {
    tMcpTlvHeader           tlvHdr;
    tMcpAgfLoadedRecStatus  recList[1];
} tMcpPollRspAgfRecListTlv;


//
//    cMcpPollDataInterval Data
//

typedef enum _eMcpPwrFlags {
    cMcpPwrMpptUnlocked,
    cMcpPwrSkipCycles,
} eMcpPwrFlags;

#define cMcpPwrFlagMpptUnlocked      ((uint8_t ) (1 << cMcpPwrMpptUnlocked))
#define cMcpPwrFlagSkipCycles        ((uint8_t ) (1 << cMcpPwrSkipCycles))

typedef struct PACKING _tMcpPcuData {
    uint8_t                     pwrConvErrSecs;
    uint8_t                     pwrConvMaxErrCycles;
    uint16_t                    producedMilliJoules[3];       /* 48-bit unsigned int */
    uint32_t                    acVoltageINmV;
    uint32_t                    acFrequencyINClkCycles;
    uint32_t                    dcVoltageINmV;
    uint32_t                    dcCurrentINmA;
    uint16_t                    usedMilliJoules[3];      /* 48-bit unsigned int */
    uint16_t                    sumMilliLeadingVAr[3];   /* 48-bit unsigned int */
    uint16_t                    sumMilliLaggingVAr[3];   /* 48-bit unsigned int */
} tMcpPcuData;

typedef struct PACKING _tMcpPcuPwrIntervalData {
    int8_t                      temperature;
    uint8_t                     flags;
    uint32_t                    onTime;
    tMcpPcuData                 pwr;
} tMcpPcuPwrIntervalData;

typedef struct PACKING _tMcpPollRspIntervalTlv {
    tMcpTlvHeader            tlvHdr;
    tMcpPcuPwrIntervalData   intervalData;
} tMcpPollRspIntervalTlv;


//
//    cMcpPollDataCondition Data
//

typedef struct PACKING _tMcpConditionData {
    uint8_t          cmdSequenceNumber;
    uint32_t         lastMsgTime;
    uint32_t         latchedConditions[2];
    uint8_t          fill[4];
    uint32_t         unlatchedConditions;
} tMcpConditionData;

typedef struct PACKING tMcpPollRspConditionTlv {
    tMcpTlvHeader       tlvHdr;
    tMcpConditionData   conditionData;
} tMcpPollRspConditionTlv;

#define cMcpPcuCondFlag_0_AcVoltageOOSP1       0x00000001
#define cMcpPcuCondFlag_0_AcVoltageOOSP2       0x00000002
#define cMcpPcuCondFlag_0_AcVoltageOOSP3       0x00000004
#define cMcpPcuCondFlag_0_AcFreqOOR            0x00000008
#define cMcpPcuCondFlag_0_GridInstability      0x00000010
#define cMcpPcuCondFlag_0_DCVoltageTooLow      0x00000020
#define cMcpPcuCondFlag_0_DCVoltageTooHigh     0x00000040
#define cMcpPcuCondFlag_0_SkippedCycles        0x00000080
#define cMcpPcuCondFlag_0_GFITripped           0x00000100
#define cMcpPcuCondFlag_0_ForcedPwrProdOff     0x00000200
#define cMcpPcuCondFlag_0_CriticalTemp         0x00000400
#define cMcpPcuCondFlag_0_OverTemp             0x00000800
#define cMcpPcuCondFlag_0_AlertActive          0x00001000
#define cMcpPcuCondFlag_0_RunningOnAC          0x00002000
#define cMcpPcuCondFlag_0_GridGone             0x00004000
#define cMcpPcuCondFlag_0_BadFlashImage        0x00008000
#define cMcpPcuCondFlag_0_UnexpectedReset      0x00010000
#define cMcpPcuCondFlag_0_CommandedReset       0x00020000
#define cMcpPcuCondFlag_0_PowerOnReset         0x00040000
#define cMcpPcuCondFlag_0_HardwareError        0x00080000
#define cMcpPcuCondFlag_0_HardwareWarning      0x00100000
#define cMcpPcuCondFlag_0_DCPowerTooLow        0x00200000
#define cMcpPcuCondFlag_0_BridgeFault          0x00400000
#define cMcpPcuCondFlag_0_MpptUnlocked         0x00800000
#define cMcpPcuCondFlag_0_GridDCILo            0x01000000
#define cMcpPcuCondFlag_0_GridDCIHi            0x02000000
#define cMcpPcuCondFlag_0_ROCOF                0x04000000
#define cMcpPcuCondFlag_0_ACVAvg               0x08000000
#define cMcpPcuCondFlag_0_DCRLow               0x10000000
#define cMcpPcuCondFlag_0_AcMonitorErr         0x20000000
#define cMcpPcuCondFlag_0_ArcFaultTripped      0x40000000
#define cMcpPcuCondFlag_0_Undef_0x80000000     0x80000000

#define cMcpPcuCondFlag_1_Undef_0x00000001     0x00000001
#define cMcpPcuCondFlag_1_Undef_0x00000002     0x00000002
#define cMcpPcuCondFlag_1_Undef_0x00000004     0x00000004
#define cMcpPcuCondFlag_1_Undef_0x00000008     0x00000008
#define cMcpPcuCondFlag_1_Undef_0x00000010     0x00000010
#define cMcpPcuCondFlag_1_Undef_0x00000020     0x00000020
#define cMcpPcuCondFlag_1_Undef_0x00000040     0x00000040
#define cMcpPcuCondFlag_1_Undef_0x00000080     0x00000080
#define cMcpPcuCondFlag_1_Undef_0x00000100     0x00000100
#define cMcpPcuCondFlag_1_Undef_0x00000200     0x00000200
#define cMcpPcuCondFlag_1_Undef_0x00000400     0x00000400
#define cMcpPcuCondFlag_1_Undef_0x00000800     0x00000800
#define cMcpPcuCondFlag_1_Undef_0x00001000     0x00001000
#define cMcpPcuCondFlag_1_Undef_0x00002000     0x00002000
#define cMcpPcuCondFlag_1_Undef_0x00004000     0x00004000
#define cMcpPcuCondFlag_1_Undef_0x00008000     0x00008000
#define cMcpPcuCondFlag_1_Undef_0x00010000     0x00010000
#define cMcpPcuCondFlag_1_Undef_0x00020000     0x00020000
#define cMcpPcuCondFlag_1_Undef_0x00040000     0x00040000
#define cMcpPcuCondFlag_1_Undef_0x00080000     0x00080000
#define cMcpPcuCondFlag_1_Undef_0x00100000     0x00100000
#define cMcpPcuCondFlag_1_Undef_0x00200000     0x00200000
#define cMcpPcuCondFlag_1_Undef_0x00400000     0x00400000
#define cMcpPcuCondFlag_1_Undef_0x00800000     0x00800000
#define cMcpPcuCondFlag_1_Undef_0x01000000     0x01000000
#define cMcpPcuCondFlag_1_ACVoltageLow         0x02000000
#define cMcpPcuCondFlag_1_ACVoltageHigh        0x04000000
#define cMcpPcuCondFlag_1_ACFrequencyLow       0x08000000
#define cMcpPcuCondFlag_1_ACFrequencyHigh      0x10000000
#define cMcpPcuCondFlag_1_AGFPowerLimiting     0x20000000
#define cMcpPcuCondFlag_1_DcBridgeHwProtection 0x40000000
#define cMcpPcuCondFlag_1_Undef_0x80000000     0x80000000

#define cMcpPcuUnlatchedFlag_TempPwrLimit      0x00000001
#define cMcpPcuUnlatchedFlag_SoftStartPwrLimit 0x00000002
#define cMcpPcuUnlatchedFlag_FpfPwrLimit       0x00000004
#define cMcpPcuUnlatchedFlag_VrmsPwrLimit      0x00000008
#define cMcpPcuUnlatchedFlag_Inv2PwrLimit      0x00000010
#define cMcpPcuUnlatchedFlag_Fw21PwrLimit      0x00000020
#define cMcpPcuUnlatchedFlag_Vw51PwrLimit      0x00000040
#define cMcpPcuUnlatchedFlag_DcLowPwrLimit     0x00000080
#define cMcpPcuUnlatchedFlag_Wp41Enable        0x00000100
#define cMcpPcuUnlatchedFlag_Tv31Enable        0x00000200
#define cMcpPcuUnlatchedFlag_FpfEnable         0x00000400
#define cMcpPcuUnlatchedFlag_VvarEnable        0x00000800
#define cMcpPcuUnlatchedFlag_Inv2Enable        0x00001000
#define cMcpPcuUnlatchedFlag_Fw21Enable        0x00002000
#define cMcpPcuUnlatchedFlag_Vw51Enable        0x00004000
#define cMcpPcuUnlatchedFlag_Undef_0x00008000  0x00008000


//
//    cMcpPollDataCounters Data
//

typedef struct PACKING _tMcpCounterData {
    tAuroraDetailedCounters   modemCounters;
    tPlcCounters              frameCounters;
    tControllerCnts           msgCounter;
    tMcpSsiMeasures           ssiMeasures;
} tMcpCounterData;

typedef struct PACKING _tMcpPollRspCounterTlv {
    tMcpTlvHeader     tlvHdr;
    tMcpCounterData   counters;
} tMcpPollRspCounterTlv;

//
//    cMcpPollDataPhaseAware Data
//

typedef struct PACKING _tPaInfo {
	uint16_t  phaseDelta;
	uint8_t   phaseBin;
	uint8_t   phaseAwarenessSeqNum;
} tPaInfo;

typedef struct PACKING _tMcpPollRspPhaseAwareTlv {
    tMcpTlvHeader     tlvHdr;
    tPaInfo           paData;
} tMcpPollRspPhaseAwareTlv;


//
//    cMcpPollDataSecurity Data
//

#define cSecDataSize 128
typedef struct PACKING _tSecurityData {
	uint16_t  blob[cSecDataSize];
} tSecurityData;

typedef struct PACKING _tMcpPollRspSecurityTlv {
    tMcpTlvHeader     tlvHdr;
    tSecurityData     secData;
} tMcpPollRspSecurityTlv;

// **********************
// tMcpAssociation
// **********************

/*
cMcpAssocCmdLoad - Used at site commissioning time. Loads gateway domain address and
                       inverter logical address to flash.  The message should be unicast.
                       If unicast, the gateway will respond a tMcpPollRspTlv message
                       containing deviceInfo data.

cMcpAssocCmdForgetAll - Used to delete all data loaded with cMcpAssocCmdDiscover command.
                        The message can be sent unicast or broadcast. If unicast, the gateway
                        will respond a tMcpPollRspTlv message containing deviceInfo data.  If
                        broadcast there will be no response.

cMcpAssocCmdForgetSpecific -  Used to delete all data loaded with cMcpAssocCmdDiscover command
                              If the device is associated with the domain in the message.  The
                              message can be sent unicast or broadcast. If unicast, the gateway
                              will respond a tMcpPollRspTlv message containing deviceInfo data.
                              If broadcast there will be no response

*/
typedef enum _eMcpAssociationCmd {
    cMcpAssocCmdNone,
    cMcpAssocCmdSquelch,
    cMcpAssocCmdLoad,
    cMcpAssocCmdForgetAll,
    cMcpAssocCmdForgetSpecific,
    cMcpAssocCmdNumberOf
} eMcpAssociationCmd;

// used with  cMcpAssocCmdDiscover
typedef struct PACKING _tMcpAssociationLoadData {
    tSerialNumber     devSerialNum;                   // PLD serial number
    uint8_t           domainAddr[cL2DomainAddrLen];   // domain address of the gateway
    uint16_t          modAddr;                        // PLD logical address
} tMcpAssociationLoadData;

typedef struct PACKING _tMcpAssociationLoadTlv {
    tMcpTlvHeader               tlvHdr;
    tMcpAssociationLoadData     load;
} tMcpAssociationLoadTlv;

// used with  cMcpAssocCmdForgetAll and cMcpAssocCmdForgetSpecific
typedef struct PACKING _tMcpAssociationForgetData {
    uint8_t           domainAddr[cL2DomainAddrLen];   // domain address of the gateway to forget
} tMcpAssociationForgetData;

typedef struct PACKING _tMcpAssociationForgetTlv {
    tMcpTlvHeader                tlvHdr;
    tMcpAssociationForgetData    forget;
} tMcpAssociationForgetTlv;

// used with  cMcpAssocCmdSquelch
typedef struct PACKING _tMcpAssociationSquelchData {
    tSerialNumber     devSerialNum;                   // PLD serial number
    uint16_t          seconds;
} tMcpAssociationSquelchData;

typedef struct PACKING _tMcpAssociationSquelchTlv {
    tMcpTlvHeader                  tlvHdr;
    tMcpAssociationSquelchData     squelch;
} tMcpAssociationSquelchTlv;

typedef struct PACKING  _tMcpAssociationTlv {
    uint8_t           tlvCnt;
    uint8_t           tlvData[1];
} tMcpAssociationTlv;


// **********************
// tMcpModuleData
// **********************

typedef enum _eMcpMfgCmd {
    cMcpMfgCmdStart,
    cMcpMfgCmdLoad,
    cMcpMfgCmdStop,
    cMcpMfgCmdClearAgfRecords,
    cMcpMfgCmdNumberOf
} eMcpMfgCmd;

typedef struct PACKING _tMcpMfgGenericData {
    tMcpTlvHeader      tlvHdr;                  // eMcpMfgCmd command is the type
    uint8_t            fill[1];
} tMcpMfgGenericData;

typedef struct PACKING _tMcpMfgLoadData {
    tMcpTlvHeader               tlvHdr;        // eMcpMfgCmd command is the type
    tEEPROM_PV_MODULE_ENTRY     moduleEntry;
} tMcpMfgLoadData;

typedef struct PACKING _tMcpMfgTlv {
    tSerialNumber     devSerialNum;               // PLD serial number
    uint8_t           tlvCnt;
    uint8_t           tlvData[1];
} tMcpMfgTlv;

typedef enum _eMcpOffGridCtrlCmd {
    cMcpOffGridCtrlSetBackupMode,
	cMcpOffGridCtrlResetBackupMode,
    cMcpOffGridCtrlQueryBackupMode,
    cMcpOffGridCmdNumberOf
} eMcpOffGridCmd;

typedef struct PACKING  _tMcpOffGridCtrlSetData {
    uint16_t                    backupTimeCycles; //default value of BM_TIMER_INIT_VAL 0 is disable
} tMcpOffGridCtrlSetData;

typedef struct PACKING _tMcpOffGridCtrlQueryRspData {
	uint8_t                     bmMode;        // 0 is off
    uint16_t                    timeCycles;    // 0 means off
	uint16_t                    curBackupTimeCycles; // current value of BM_TIMER 0 means off         
} tMcpOffGridCtrlRspData;


typedef struct PACKING tMcpOffGridCtrMsg {
    tSerialNumber     devSerialNum;               // PLD serial number
	uint8_t           msgId;
    uint8_t           msgData[1];
} tMcpOffGridCtrMsg;

typedef struct PACKING _tMcpMPSGateDriveCtrMsg {
    tSerialNumber     devSerialNum;               // PLD serial number
	uint8_t           enableFlag;
} tMcpMPSGateDriveCtrMsg;
typedef struct PACKING _tPmiDataAuroraMode
{
    __extension__ uint8_t channel : 2;  // use ePmiChannel
    __extension__ uint8_t plan0 : 1;    // Bit0 of plan number
                                        // use ePmiChanPlan
    __extension__ uint8_t baud : 1;     // use ePmiBaud

    __extension__ uint8_t scheme : 2;  // use ePmiModScheme
    __extension__ uint8_t rs : 1;      // use ePmiReedSolomon
    __extension__ uint8_t plan1 : 1;   // Bit1 of plan number
} tPmiDataAuroraMode;

typedef union PACKING_uPmiDataAuroraMode
{
    uint8_t u8;
    tPmiDataAuroraMode f;
} uPmiDataAuroraMode;

/*! union of modem type configuration */
typedef struct PACKING _tPmiDataAuroraConfig
{
    uPmiDataAuroraMode mode; /* mode bitmap */
    uint8_t txLvl;           /* ePmiDataTxLevel (FA6C setting) */
    uint8_t flags;
    int8_t txGainDb; /* transmit gain in dB */

    uint8_t fillb1;
    uint8_t fillb2;
    uint8_t fillb3;
    uint8_t fillb4;
} tPmiDataAuroraConfig;

typedef union PACKING _uPmiDataModemConfig
{
    tPmiDataAuroraConfig aurora;
} uPmiDataModemConfig;

typedef struct PACKING _tPmiDataCryptMode
{
    __extension__ uint8_t type : 3;     // ePmiDataCryptType
    __extension__ uint8_t keyType : 3;  // ePmiDataCryptKeyType
    __extension__ uint8_t keyIdx : 2;   // key index (0/1)
} tPmiDataCryptMode;

typedef union PACKING _uPmiDataCryptMode
{
    uint8_t u8;
    tPmiDataCryptMode f;
} uPmiDataCryptMode;

typedef struct PACKING _tPmiDataConfigData
{
    uint8_t modem; /* ePmiDataModemType */
    uint8_t filla2;
    uint8_t filla3;
    uint8_t filla4;

    uint16_t cryptOverride;  /* 1-override value/0-use def/ept*/
    uint16_t configOverride; /* 1-override value/0-use def/ept*/

    uPmiDataModemConfig config; /* modem configuration */

    uPmiDataCryptMode crypt; /* crypto configuration */
    uint8_t fillb2;
    uint8_t fillb3;
    uint8_t fillb4;

    uint8_t fillc1;
    uint8_t fillc2;
    uint8_t fillc3;
    uint8_t fillc4;
} tPmiDataConfigData;


#endif /* _mcp_sunpower_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of mcp_sunpower.h */



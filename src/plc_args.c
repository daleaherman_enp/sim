/*!
 * @file     plc_args.c
 *
 * @copyright
 *  Copyright (c) 2014 SolarBridge Technologies.  All rights reserved.
 *
 * @created  Oct 20, 2014
 *
 * @author   M. Bintz <m.bintz@solarbridgetech.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#else
#include "win32/getopt.h"
#include <io.h>
#endif // !WIN32
#include <fcntl.h>

#include "plc_tool.h"
#include "sbt/errno.h"
#include "sbt/log.h"

// #define DEBUG_FLAGS     TRACE_FLAG_INFO
#define DEBUG_FLAGS     0

#define ENPHSE_ADDR_PREFIX_STR   "Exx"
#define ENPHSE_ADDR_PREFIX_LEN   3

//! \addtogroup plctool_api
//! @{

/*!**************************************************************************
 *
 * This is a function common to all the plc_tools for parsing the command
 * line arguments
 *
 * @param argc is the argument count as passed to main()
 * @param argv is an array of argument values as passed to main()
 * @param pArgStruct is a pointer to a plc_args_t structure into which the parsed
 *   command line will set flag/argument variables
 *
 * @return 0 for success, < 0 for error
 *
 ***************************************************************************/
int32_t PlctoolArgsParse(int argc, char *argv[], plc_args_t *pArgStruct)
{
    int opt;
    int farg;
    int snarglen = 0;
    char *pDevName;
    char *pCfgName;
    static char snarg[64];

    if (!pArgStruct)
        return -EINVAL;

    memset(pArgStruct, 0, sizeof(plc_args_t));

    //
    // Set non-zero defaults if they don't specify params on the command line
    //
    pArgStruct->group_limit = 32;
    pArgStruct->count = 1;
    pArgStruct->delay = 1000;
    pArgStruct->pm_serial = -1;
    pArgStruct->window = 50;
    pArgStruct->J_flag = false;
    pArgStruct->J_arg = NULL;
    pArgStruct->bXplore = false;
    pArgStruct->vl1 = pArgStruct->vl2 = 0.0;
    pArgStruct->vol = false;
	pArgStruct->H_arg = false;
	pArgStruct->addressingMode = 1;
    pDevName = getenv("PLCDEV");
    if (pDevName)
    {
        pArgStruct->connection_arg = pDevName;
    }
    pDevName = getenv("FORCE_ENPHASE");
    if (pDevName)
    {
        pArgStruct->e_flag = 1;
    }
    
    pCfgName = getenv("CONVCFG");
    if (pCfgName)
    {
        fprintf(stderr, "Using CONVCFG arctic configuration '%s'\n", pCfgName);
        pArgStruct->J_arg = pCfgName;
    }

    // attempt to open an existing serial number file.
    farg = open("lastsn", O_RDWR);
    if (farg > 0)
    {
        // We were able to open an existing file.  Read its contents.
        int rc = 0;
        // we were able to open the serial number file.  See if we can
        // extract an argument from it
        do
        {
            rc = read(farg, snarg + snarglen, 64 - snarglen);
            snarglen += rc;
        } while (rc > 0);
        lseek(farg, 0, SEEK_SET);
        
        if( strlen( snarg ) > ENPHSE_ADDR_PREFIX_LEN )
        {
           if( strncmp( snarg, ENPHSE_ADDR_PREFIX_STR, ENPHSE_ADDR_PREFIX_LEN ) == 0 )
           {
              memcpy( snarg, snarg + ENPHSE_ADDR_PREFIX_LEN, strlen(snarg) - ENPHSE_ADDR_PREFIX_LEN + 1 );   
              pArgStruct->e_flag = 1;
           }
           else
           {
              pArgStruct->e_flag = 0;
           }
        }

    }
    else
    {
        // the file didn't exist.  Create it.
        farg = open("lastsn", O_WRONLY | O_CREAT, 0644);
    }

    optind = 1;
    opt = getopt(argc, argv, PLCTOOL_OPTARGS);
    while (opt != -1)
    {
        switch (opt)
        {
        case '-':
            optind--;
            goto skip;

        case '1':
            pArgStruct->one_arg = optarg;
            break;

        case '2':
            pArgStruct->two_arg = optarg;
            break;

        case 'A':
            pArgStruct->A_arg = optarg;
            break;
        case 'a':
            //
            // -a address 32 bit for memory
            //
            pArgStruct->a_arg = optarg;
            if (optarg)
            {
                pArgStruct->address = strtoul(optarg, NULL, 0);
                ;
            }
            break;

        case 'b':
            pArgStruct->bBinary = 1;
            break;

        case 'c':
            //
            // -c <count> - number of pings
            //
            pArgStruct->c_arg = optarg;
            if (optarg)
            {
                pArgStruct->count = strtoul(optarg, NULL, 0);
                TRACE_INFO(DEBUG_FLAGS, "Found count = %d\n",
                        pArgStruct->count);
            }

            break;

        case 'C':
            //
            // -C <URI> - connection type
            //
            pArgStruct->connection_arg = optarg;
            if (optarg)
            {
                TRACE_INFO(DEBUG_FLAGS, "Found connection_arg = '%s'\n",
                        pArgStruct->connection_arg);
            }
            break;

        case 'd':
            //
            // -d <ms> - interpacket delay in milliseconds
            //
            pArgStruct->d_arg = optarg;
            if (optarg)
            {
                int d;
                d = atoi(optarg);
                if (d < 5)
                    d = 5;
                if (d > 10000)
                    d = 10000;
                pArgStruct->delay = d;
                TRACE_INFO(DEBUG_FLAGS, "Set delay to %d\n", pArgStruct->delay);
            }
            break;

        case 'D':
            //
            // -D <level> - sets debug level.
            //
            pArgStruct->D_flag = 1;
            pArgStruct->debug = strtoul(optarg, NULL, 0);
            LogLevelSet("*", pArgStruct->debug);
            break;
        case 'e':
            // no args. Sets enphase flag
            // -e 
            //
            pArgStruct->e_flag = 1;

            break;
        case 'f':
            //
            // -f <filename> pass in a data file
            //
            pArgStruct->f_arg = optarg;
            if (optarg)
            {
                TRACE_INFO(DEBUG_FLAGS, "Found data-in file '%s'\n", optarg);
            }
            break;

        case 'F':
            //
            // -F <filename> write out to a data file
            //
            pArgStruct->F_arg = optarg;
            if (optarg)
            {
                TRACE_INFO(DEBUG_FLAGS, "Found data-out file '%s'\n", optarg);
            }
            break;

        case 'g':
            //
            // -g <group> - sets group id to address
            //
            pArgStruct->g_arg = optarg;
            if (optarg)
            {
                pArgStruct->group = atoi(optarg);
                TRACE_INFO(DEBUG_FLAGS, "Found group = %d\n",
                        pArgStruct->group);
            }
            break;

        case 'G':
            //
            // -G <limit> - sets group limit
            //
            pArgStruct->G_arg = optarg;
            if (optarg)
            {
                pArgStruct->group_limit = atoi(optarg);
                TRACE_INFO(DEBUG_FLAGS, "Found group_limit = %d\n",
                        pArgStruct->group_limit);
            }
            break;

        case 'h':
            //
            // -h - Shows help
            //
            pArgStruct->h_flag = 1;
            pArgStruct->h_arg = optarg;

            break;

		case 'H':
			//
			// -H  hex string comming up
			//
			pArgStruct->H_flag = 1;
			pArgStruct->H_arg = optarg;

			break;


        case 'i':
            //
            // -i <mi_idx> - MI index in the group
            //
            pArgStruct->i_arg = optarg;
            if (optarg)
            {
                pArgStruct->index = atoi(optarg);
                TRACE_INFO(DEBUG_FLAGS, "Found index = %d\n",
                        pArgStruct->index);
            }
            break;

        case 'j':
            pArgStruct->bJsonOutput = true;
            fprintf(stderr,
                    "\nWARNING: -j is now depricated.  Redirect file descriptor 3 to get JSON output, like so:\n\n"
                            "To output JSON to a file:\n"
                            "      ./command   3>json.txt\n\n"
                            "To output JSON to stdout, and send the normal human-readable output to /dev/null:\n"
                            "      ./command   3>&1  >/dev/null\n");
            break;

        case 'J':
            // -J [<json config file>]
            pArgStruct->J_flag = true;
            pArgStruct->J_arg = optarg;
            if (optarg)
            {
                fprintf(stderr, "Using non-standard arctic configuration %s\n",
                        optarg);
            }
            break;

        case 'l':
            // -l [<logfile>]
            pArgStruct->l_flag = true;
            pArgStruct->l_arg = optarg;
            if (optarg)
            {
                FILE *fLog;

                fprintf(stderr, "Logging progress to '%s'\n", optarg);
                fLog = fopen(optarg, "a");
                LogFileSet(fLog);
                if (!fLog)
                    fprintf(stderr, "Error opening log file\n");
            }

            break;

        case 'L':
            pArgStruct->L_arg = optarg;
            if (optarg != NULL)
            {
                pArgStruct->length = atoi(optarg);
            }
            break;

        case 'm':
            pArgStruct->m_arg = optarg;
            break;

        case 'M':
            pArgStruct->M_arg = optarg;
            break;
		case 'n':
			pArgStruct->n_arg = optarg;
			if (optarg != NULL)
            {
                pArgStruct->addressingMode = atoi(optarg);
            }
			break;
        case 'o':
            pArgStruct->o_arg = optarg;
            if (optarg)
            {
                pArgStruct->old_pm_serial = strtoul(optarg, NULL, 10);
                TRACE_INFO(DEBUG_FLAGS, "old_pm_sn found = %d\n",
                        pArgStruct->old_pm_serial);
            }

            break;
        case 'O':
            pArgStruct->O_arg = optarg;
            if (optarg)
            {
                pArgStruct->processor = atoi(optarg);
            }
            break;
        case 'p':
            //
            // -p <pm_sn> - pm serial number
            //
            pArgStruct->p_arg = optarg;
            if (optarg)
            {
                pArgStruct->pm_serial = strtoul(optarg, NULL, 10);
                TRACE_INFO(DEBUG_FLAGS, "pm_sn found = %d\n",
                        pArgStruct->pm_serial);
            }
            break;

        case 'P':
            // -P <pf> - power factor as floating point
            pArgStruct->P_arg = optarg;
            if (optarg)
            {
                int rc;
                rc = sscanf(optarg, "%f", &pArgStruct->power_factor);
                if (!rc)
                {
                    fprintf(stderr,
                            "Error parsing power factor.  Defaulting to 1.0\n");
                    pArgStruct->power_factor = 1.0f;
                }
            }
            break;

        case 'q':
            pArgStruct->bQuiet = true;
            LogConsoleSet(NULL);
            break;

        case 'r':
            pArgStruct->r_arg = optarg;
            if (optarg)
            {
                pArgStruct->row = (uint8_t)strtoul(optarg, NULL, 10);
                TRACE_INFO(DEBUG_FLAGS, "Row %d\n", pArgStruct->row);
            }
            break;

        case 'R':
            pArgStruct->bReset = true;
            break;

        case 's':
            if( strlen( optarg ) > ENPHSE_ADDR_PREFIX_LEN )
            {
               if( strncmp( optarg, ENPHSE_ADDR_PREFIX_STR, ENPHSE_ADDR_PREFIX_LEN ) == 0 )
               {
                  optarg += ENPHSE_ADDR_PREFIX_LEN;   
                  pArgStruct->e_flag = 1;
               }
               else
               {
                  pArgStruct->e_flag = 0;
               }
            }

            pArgStruct->serial_arg = optarg;
            
            if (optarg)
            {  
               if( pArgStruct->e_flag == 1 )
               {
                  LOG_VERBOSE_LO("Enphase mi_sn found '%s'\n", pArgStruct->serial_arg);
               }
               else
               {
                  LOG_VERBOSE_LO("mi_sn found '%s'\n", pArgStruct->serial_arg);
               }
            }
            break;

        case 'S':
            pArgStruct->S_arg = optarg;
            break;

        case 'T':
            pArgStruct->T_arg = optarg;
            if (optarg)
            {
                pArgStruct->throttle = (uint16_t)strtoul(optarg, NULL, 10);
            }
            break;

        case 't':
            pArgStruct->t_arg = optarg;
            if (optarg)
            {
                pArgStruct->recontime = atoi(optarg);
            }
            break;

        case 'U':
            pArgStruct->U_arg = optarg;
            break;

        case 'v':
            pArgStruct->v_arg = optarg;
            if (optarg)
            {
                pArgStruct->version = atoi(optarg);
            }
            break;

        case 'V':
            pArgStruct->vol = true;
            break;

        case 'w':
            pArgStruct->w_arg = optarg;
            if (optarg)
            {
                pArgStruct->window = atoi(optarg);
            }
            break;

        case 'X':
            pArgStruct->bXplore = true;
            break;

        case 'Y':
            pArgStruct->Y_flag = true;
            break;

        case '?':
            if (argv[optind])
            {
                if ((argv[optind][0] == '-') && (argv[optind][1] == '-'))
                {
                    goto skip;
                }
            }
            break;

        default:
            fprintf(stderr, "Unknown argument: '%c'\n", opt);
            return -EINVAL;
        } // switch
        opt = getopt(argc, argv, PLCTOOL_OPTARGS);
    } // while

    skip:
    // at this point we can be in one of 4 states WRT serial number:
    // 1. A SN file existed and they specified a SN (use theirs, update file)
    // 2. A SN file existed and they didn't specify SN (use the file's)
    // 3. A SN file didn't exist and they specified one (update the sn file)
    // 4. Neither an SN file nor CLI option was provided.  Do nothing.
    if (pArgStruct->serial_arg)
    {
        int wlen, rc;
        if( pArgStruct->e_flag == 1)
        {
           rc = write(farg, ENPHSE_ADDR_PREFIX_STR, ENPHSE_ADDR_PREFIX_LEN);
           if (rc != ENPHSE_ADDR_PREFIX_LEN)
           {
               LOG_WARNING("Unable to write entire lastsn file\n");
           }
        }

        wlen = strlen(pArgStruct->serial_arg) + 1;
        rc = write(farg, pArgStruct->serial_arg, wlen);
        if (rc != wlen)
        {
            LOG_WARNING("Unable to write entire lastsn file\n");
        }
    }
    else if (snarglen > 0)
    {
        pArgStruct->serial_arg = snarg;
    }

#ifdef WIN32
    if (!pArgStruct->J_flag)
    {
        pArgStruct->J_arg = getenv("ArcticConfigurationFile");
        pArgStruct->J_flag = true;
    }
#endif

    close(farg);
    return 0;
}

//! @}

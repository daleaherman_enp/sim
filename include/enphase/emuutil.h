// Copyright (c) 2018 Enphase Energy, Inc. All rights reserved.
//
// This file contains reference encoder/decoder functions for
// use by the SunPower gateway interfacing to the Enphase Module
// Control Protocol message format.
//

#include "enphase.h"
//#include "mcp_enphase.h"
//#include "include/mcp_sunpower.h"

#ifdef __cplusplus
extern "C" {
#endif

/* emuutlBcdBytesToStr
 * Convert a BCD formated buffer to a string
 * Resulting string length should be twice the BCD field size.
 *
 * INPUT:
 *  bcd_ptr : pointer to BCD buffer
 *  bcd_len : length of BCD buffer in bytes
 *  str_len : maximum length of output string.
 *
 * OUTPUT:
 *  str_ptr : pointer to ascii character buffer of size str_len
 *
 * RETURNS:
 *   0 on success.
 *
 */
int
emuutlBcdBytesToStr(unsigned char * bcd_ptr,
                    unsigned char bcd_len,
                    char * str_ptr,
                    char str_len);



/* emuutlStrToBcdBytes
 * Given a decimal numeric string, output encoded BCD
 *
 * INPUT:
 *   str_ptr : pointer to decimal numeric string
 *   str_len : length of input string.
 *   bcd_len : maximum length of output encoded bytes.
 *
 * OUTPUT:
 *   bcd_ptr : pointer to BCD buffer.
 *
 * RETURNS:
 *   0 on success.
 *  -1 on failure.
 */
int
emuutilStrToBcdBytes(const char * str_ptr,
                     char str_len,
                     unsigned char * bcd_ptr,
                     unsigned char bcd_len);


/* emuutlPartNumToStr
 * Given an encoded part number, convert to a formatted string.
 *
 * INPUT:
 *   tPartNumber - binary part number for a device or firmware image
 *   str_len - maximum length of the output string.
 *
 * OUTPUT:
 *   str_ptr - pointer to character array for the part number.
 *             Minimum preallocated size of 24 bytes.
 *   str_len - length of the resulting string.
 *
 * RETURNS:
 *   0 on success.
 *  -1 on failure.
 */
int
emuutilPartNumToStr(tPartNumber part_num,
                    char * str_ptr,
                    char str_len);


/* emuutilStrToPartNum
 * Given an Enphase Part Number String, compress to binary encode
 *
 * INPUT:
 *   str_ptr - input XXX-YYYYY-rZZ string. Null terminated.
 *
 * OUTPUT:
 *   part_num - binary encode of the string for msg send
 *
 * RETURNS:
 *   0 on success
 *  -1 on failure
 */
int
emuutilStrToPartNum(const char * str_ptr,
                    tPartNumber * part_num);


/* emuutilSerialNumToStr
 * Given a compressed Enphase Serial Number, generate a serial number string.
 *
 * INPUT:
 *   serial_num - Serial number for an Enphase device, as sent in DevInfo poll
 *   str_len - maximum output string length.
 *
 * OUTPUT:
 *   str_ptr - Output formatted serial number string.
 *
 * RETURNS:
 *   0 on success.
 *   -1 on failure.
 */
int
emuutilSerialNumToStr(tSerialNumber serial_num,
                      char        * str_ptr,
                      int           str_len);


/* emumtilStrToSerialNum
 * Given an Enphase serial number string, generated a compressed serial num struct.
 *
 * INPUT:
 *   str_ptr - Serial number as a string, null terminated
 *
 * OUTPUT:
 *   serial_num - formatted serial number structure.
 *
 * RETURNS:
 *   0 on success.
 *  -1 on failure.
 */
int
emumtilStrToSerialNum(const char * str_ptr,
                      tSerialNumber * serial_num);



/* emuutlAsicIdToStr
 * Given an Enphase ASIC ID value, convert it to a string.
 *
 * INPUT:
 *   asic_id - identifier for an Enphase device.
 *   str_len - maximum length of output string.
 *
 * OUTPUT:
 *   str_ptr - asic_id, as represented by a string.
 *
 * RETURNS:
 *   0 on success.
 *  -1 on failure.
 */
int
emuutlAsicIdToStr(tAsicId asic_id,
                  char * str_ptr,
                  int str_len);

/* emuutlStrToAsicId
* Given an Enphase ASIC ID String, convert it to 8 byte compressed binary.
*
* INPUT:
*   str_ptr - Pointer to ASIC ID string.  Null terminated.
*
* OUTPUT:
*   asic_id - Output asic ID structure.
*
* RETURNS:
*   0 on success.
*  -1 on failure.
*/
int
emuutlStrToAsicId(const char * str_ptr,
                  tAsicId * asic_id);


/** emuutlCrc16 - Compute CRC-16 as used in target devices. */
extern uint16_t
emuutlCrc16(void            * pData,
            size_t            dataLen);

/** emuutlCrc16Update - update for running crc16 sum. */
extern void
emuutlCrc16Update(uint16_t     * sum,
                  void          * pData,
                  u_int32_t       dataLen);

/** emuutlCrc16TableGen - Code to generate crc16 table. */
extern void
emuutlCrc16TableGen(void);

/** emuutlCrcTblCheck - check crc table results. */
extern int
emuutlCrcTblCheck(void);

extern void
emuutlMD5DigestStrToHash(char       * hashStr,
                         uint8_t   * hash);

                         extern int
emuutlDigitToInt(int achar);

#ifdef __cplusplus
}
#endif

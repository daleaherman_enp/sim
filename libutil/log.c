/*!****************************************************************************
 * @file log.c
 *  Log API implementation
 *
 * @copyright
 *  Copyright (c) 2014 SolarBridge Technologies.  All rights reserved.
 *
 * @created Nov 4, 2014
 *
 * @author M. Bintz <m.bintz@solarbridgetech.com>
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <sys/types.h>

#ifndef WIN32
#include <unistd.h>
#include <libgen.h>
#else
// KLUDGE FOR WINDOWS -  Irrelevant anyway
#define CLOCK_REALTIME 3
#include <Windows.h>
#include "win32/win_utils.h"

//typedef struct _FILETIME
//{
//	uint32_t dwLowDateTime;
//	uint32_t dwHighDateTime;
//} FILETIME;


#endif
#include <time.h>
#include <errno.h>

#if defined(__linux__)
#define SHM_SUPPORT 1
#define SHM_SIZE (128*1024)
#else
#define SHM_SUPPORT 0
#endif

#if SHM_SUPPORT
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif

#include "sbt/errno.h"
#include "sbt/log.h"

/*!
 * \addtogroup log_config
 * @{
 */

/*!
 *  @}
 */

typedef struct _logcfg_t
{
    uint32_t hash;
    int32_t level;
    const char *pModuleName;
    struct _logcfg_t* pLeft;
    struct _logcfg_t* pRight;
} LogConfig_t;

#define N_MODULES 256

static LogConfig_t config[N_MODULES];
static LogConfig_t *pRoot = NULL;

static uint32_t g_ui32LogLevel;
static FILE *g_fLog, *g_fStream;
static bool bInit = 0;

#if SHM_SUPPORT
static int fdShm;
static uint32_t outdex;
static char *pShm;
#endif

// search our statically allocated block of config entries for a free entry
// and a return a pointer to it.  If no free entries are available, return 0
static LogConfig_t* logConfigGetFree()
{
    int i = 0;
    for (i = 0; i < N_MODULES; i++)
    {
        if(config[i].hash == 0) return config + i;
    }
    return 0;
}

// simple string hashing function to convert filenames into uint32_t
static uint32_t logHash(const char *str)
{
   uint32_t h = 0;
   const char *p;
   const uint32_t MULTIPLIER = 37;

   for (p = str; *p != '\0'; p++)
      h = MULTIPLIER * h + *p;
   return h;
}

// insert an entry into the tree that matches hash and set it's level to level
static int32_t logInsert(LogConfig_t **pNode, uint32_t hash, int32_t level, const char *pName)
{
    if(!pNode) return -EINVAL;

    if(*pNode == 0)
    {
        *pNode = logConfigGetFree();
        if(!*pNode) return -ENOMEM;
        (*pNode)->hash = hash;
        (*pNode)->level = level;
        (*pNode)->pModuleName = pName;
        (*pNode)->pLeft = 0;
        (*pNode)->pRight = 0;
        return 0;
    }
    else if(hash < (*pNode)->hash)
    {
        return logInsert(&((*pNode)->pLeft), hash, level, pName);
    }
    else if(hash > (*pNode)->hash)
    {
        return logInsert(&((*pNode)->pRight), hash, level, pName);
    }
    else if(hash == (*pNode)->hash)
    {
        (*pNode)->level = level;
        return 0;
    }
    // shouldn't get here...
    return -1;
}

// given a node of the log config tree, find a matching hash
static LogConfig_t* logSearch(LogConfig_t *pNode, uint32_t hash)
{
    if(!pNode) return 0;
    if(pNode->hash == hash) return pNode;
    if(hash < pNode->hash) return logSearch(pNode->pLeft, hash);
    if(hash > pNode->hash) return logSearch(pNode->pRight, hash);
    // shouldn't get here...
    return 0;
}

/*!
 * \addtogroup log_api
 * @{
 */

/*!**************************************************************************
 *
 * \brief Initialize logging subsystem
 * @param fLog is a file handle to the file descriptor used for logging
 * @param fStream is afile handle to stdout/stderr
 * @return 0 for success
 *
 ***************************************************************************/
int32_t LogInit(FILE *fLog, FILE *fStream)
{
    int i;
#if SHM_SUPPORT
	int rc;
#endif

    if(!bInit)
    {
        bInit = 1;
        // mark all module level as unused
        for(i = 0; i < N_MODULES; i++)
        {
            config[i].hash = 0;
            config[i].level = -1;
        }
#if SHM_SUPPORT
        outdex = 0;
        fdShm = shm_open("/mime", O_CREAT | O_RDWR, 0644);
        if(fdShm != -1)
        {
            rc = ftruncate(fdShm, SHM_SIZE);
            if(rc != 0)
            {
                fprintf(stderr, "Could not ftruncate() shared mem region.  errno=%d ('%s')\n",
                        errno, strerror(errno));

            }
            else
            {
                pShm = (char*)mmap(NULL, SHM_SIZE, PROT_WRITE, MAP_SHARED, fdShm, 0);
                if((long)pShm == -1)
                {
                    fprintf(stderr, "Could not mmap() shared mem region.  errno=%d ('%s')\n",
                            errno, strerror(errno));
                }
                else
                {
                    memset(pShm, 0, SHM_SIZE);
                    sprintf(pShm, "Hello world!\n");
                }
            }
        }
        else
        {
            fprintf(stderr,"%s[%d]: Unable to open shared memory pool\n",
                    __FUNCTION__, __LINE__);
        }
#endif
    }
    g_fLog = fLog;
    g_fStream = fStream;
    g_ui32LogLevel = LOG_LEVEL_INFO;
    return 0;
}

/*!**************************************************************************
 *
 * \brief tear down the logging system
 *
 * @param fLog is a file handle to the file descriptor used for logging
 * @param fStream is afile handle to stdout/stderr
 * @return 0 for success
 *
 ***************************************************************************/
void LogDeinit()
{
    if(bInit)
    {
        bInit = 0;
#if SHM_SUPPORT
        if(pShm)
        {
            munmap(pShm, SHM_SIZE);
            pShm = 0;
        }
        if(fdShm)
        {
            close(fdShm);
            shm_unlink("/mime");
        }
#endif
    }
}

/*!**************************************************************************
 *
 * \brief Get a cJSON representation of the current log level configuration
 *
 * @return pointer to a cJSON object which the caller must free
 *
 * NOTE:  This function will only return stanzas that record deviations from
 * the default log configuration.  (ie. if g_ui32LogLevel is INFO and every
 * module's level is INFO then this function will return NULL)
 *
 ***************************************************************************/
cJSON* LogConfigGet()
{
    int i;
    bool bDefault = true;
    cJSON *pArray = cJSON_CreateArray();
    cJSON *pObj;

    if (g_ui32LogLevel != LOG_LEVEL_INFO)
    {
        bDefault = false;
        pObj = cJSON_CreateObject();
        cJSON_AddItemToObject(pObj, "*", cJSON_CreateNumber(g_ui32LogLevel));
        cJSON_AddItemToArray(pArray, pObj);
    }

    for(i = 0; i < N_MODULES; i++)
    {
        if(config[i].hash == 0) continue;

        // a module with a log level of -1 defaults to the global log level
        // setting
        if(config[i].level == -1) continue;

        bDefault = false;

        pObj = cJSON_CreateObject();
        cJSON_AddItemToObject(pObj,
                              config[i].pModuleName,
                              cJSON_CreateNumber(config[i].level));

        cJSON_AddItemToArray(pArray, pObj);
    }

    if(bDefault)
    {
        free(pArray);
        return NULL;
    }
    return pArray;

}

/*!**************************************************************************
 *
 * \brief Set the file to which console output should be sent.
 * Default is stderr.
 *
 * @param f is a file handle to the file descriptor used for console output
 * Suggest stdout or stderr but any FILE will do.  Set to NULL to squelch
 * console output.
 *
 * @return 0 for success
 *
 ***************************************************************************/
int32_t LogConsoleSet(FILE *f)
{
    g_fStream = f;
    return 0;
}

/*!**************************************************************************
 *
 * \brief Set the FILE to which file-based logging output should be sent.
 *
 * @param f is a file handle to the file descriptor used for logfile output.
 * Set to NULL to squelch logfile output.
 *
 * @return 0 for success
 *
 ***************************************************************************/
int32_t LogFileSet(FILE *f)
{
    g_fLog = f;
    return 0;
}

/*!**************************************************************************
 *
 * \brief Set the log level for the specified module
 *
 * @param pModule is a path to C module (as set by __FILE__ in the calling function)
 * @paral lvl is the level to associate this module
 *
 * @note: If pModule is "*" then we set the global/default log level
 *
 * @return 0 for success
 *
 ***************************************************************************/
int32_t LogLevelGet(const char *pModule)
{
    uint32_t hash;
    LogConfig_t *pEntry;

    if(*pModule == '*')
    {
        return g_ui32LogLevel;
    }

    hash = logHash(pModule);
    pEntry = logSearch(pRoot, hash);

    if(pEntry == NULL) return -1;
    return(pEntry->level);
}

/*!**************************************************************************
 *
 * \brief Set the log level for the specified module
 *
 * @param pModule is a path to C module (as set by __FILE__ in the calling
 * function).  pModule should NOT point to a string on the stack UNLESS the
 * caller is 100% confident that LogLevelSet was already called for this
 * pModule using __FILE__ to set the level).
 * @paral lvl is the level to associate this module.  Set to -1 to use global
 * log level
 *
 * @note: If pModule is "*" then we set the global/default log level
 *
 * @return 0 for success
 *
 ***************************************************************************/
int32_t LogLevelSet(const char *pModule, int32_t lvl)
{
    int32_t rc;
    uint32_t hash;

    if(!pModule) return -EINVAL;
    if(*pModule == '*')
    {
        g_ui32LogLevel = lvl & 0xff;
    }
    else
    {
        hash = logHash(pModule);
        LOG_VERBOSE_MED("%s hashes to 0x%08x\n", pModule, hash);
        rc = logInsert(&pRoot, hash, lvl, pModule);
        if(rc != 0)
        {
            fprintf(stderr, "Unable to set logging level for module '%s'\n",
                    pModule);
        }
        return rc;
    }
    return 0;
}

/*!**************************************************************************
 *
 * \brief Send a message to the various log channels
 *
 * @param ui32Lvl is the verbosity level of this message.  Higher levels imply
 * higher verbosity (more noise).
 * @param pModule should be __FILE__ of the calling function
 * @param pFunc should be __FUNCTION__ of the calling function
 * @param line should be __LINE__ of the calling function
 * @param pFmt is the same as printf's formatting
 * @param ... var args, same as printf
 *
 * @note: This function should not be called directly.  It should be invoked
 * through the \ref LOG_ERROR, \ref LOG_WARNING, LOG_* etc. macros
 *
 ***************************************************************************/
void LogMessage(uint32_t ui32Lvl, const char *pModule, const char *pFunc,
                uint32_t line, const char *pFmt, ...)
{
    va_list args;
    char *pBasename;
    char tmpbuf[256];
    char *pTmpbuf = tmpbuf;
    int rc;

    static const char *pLevels[] = {"CRIT","ERR","WARN","INFO","VERL","VERM","VERH", "UNK"};
    const char *plevel;
    int32_t lvl;
    struct timespec ts;
	time_t s;
	uint32_t ms;

    if(pModule)
    {
        pBasename = basename((char*)pModule);
    }
    else
    {
        pBasename = "(nil)";
    }

    // get this modules log level
    lvl = LogLevelGet(pBasename);

    // if this module has no associated log level then fall back to the default
    // log level
    if(lvl == -1) lvl=g_ui32LogLevel;
    if(lvl <= LOG_LEVEL_VERBOSE_HI)
    {
        plevel = pLevels[ui32Lvl];
    }
    else
    {
        plevel = pLevels[LOG_LEVEL_VERBOSE_HI + 1];
    }

    // if the verbosity level exceeds the set level then we take an early exit
    if (ui32Lvl > (uint32_t)lvl) return;
    clock_gettime(CLOCK_REALTIME, &ts);
    s=ts.tv_sec;
    ms=ts.tv_nsec / 1000000;
    va_start(args, pFmt);

    rc = snprintf(pTmpbuf, sizeof(tmpbuf), "%d.%03d|%s|%s|%s[%d]| ", (uint32_t)s, ms, plevel, pBasename, pFunc, line);
    rc += vsnprintf(pTmpbuf + rc, sizeof(tmpbuf) - rc, pFmt, args);
    va_end(args);

    if (g_fStream)
    {
        fprintf(g_fStream, "%s", pTmpbuf);
        fflush(g_fStream);
    }

    if (g_fLog)
    {
        fprintf(g_fLog, "%s", pTmpbuf);
        fflush(g_fLog);
    }

#if SHM_SUPPORT
    if(pShm)
    {
        int writ;
        // write out formatted output buffer to pShm at offset "outdex"
        // we can write up to outdex - "len" bytes before reaching the end of
        // our "circular" shared memory buffer.
        // here rc is the length of the formatted output above
        writ = snprintf(pShm + outdex, SHM_SIZE - outdex, "%s", pTmpbuf);

        // note, we may not have written our entire output buffer.  Hold that thought
        // advance the outdex by the amount we just wrote.
        outdex = (outdex + writ) % SHM_SIZE;

        // now, if the amount we just wrote didn't equal the amount we tried to
        // write...
        if(writ != rc)
        {
            // write the rest.  outdex should now be 0 (assuming our wrap logic
            // is working).  And the amount left to write is rc (the lenght of
            // the original thing) minus what we just wrote.  Since rc can be
            // _at most_ sizeof(tmpbuf) (which should be MUCH less than SHM_SIZE)
            // we just write the rest.
            snprintf(pShm + outdex, rc - writ, "%s", pTmpbuf + writ);
        }
    }
#endif

}

/*!
 * Close the Doxygen group for the API
 * @}
 */

#if defined SELFTEST


/*!
 * \addtogroup log_tests
 * @{
 */
/*!
 * \brief Some white and black box tests for SunSpec block storage functions.
 */
int main()
{
    FILE *f = fopen("delme", "a");

    LogInit(f, stderr);
    LogLevelSet("*", LOG_LEVEL_INFO);
    LogLevelSet("foo", LOG_LEVEL_VERBOSE_MED);
    LogLevelSet("bar", LOG_LEVEL_ERROR);

    LogMessage(LOG_LEVEL_ERROR, "unk", __FUNCTION__, __LINE__, "You should see me\n");
    LogMessage(LOG_LEVEL_VERBOSE_LO, "unk", __FUNCTION__, __LINE__, "You should NOT see me\n");
    LogMessage(LOG_LEVEL_VERBOSE_LO, "foo", __FUNCTION__, __LINE__, "You should see me\n");
    LogMessage(LOG_LEVEL_VERBOSE_HI, "foo", __FUNCTION__, __LINE__, "You should NOT see me\n");
    LogMessage(LOG_LEVEL_INFO, "bar", __FUNCTION__, __LINE__, "You should NOT see me\n");
    LogMessage(LOG_LEVEL_ERROR, "bar", __FUNCTION__, __LINE__, "You should see me\n");
    return 0;
}
/*! Close the doxygen group
 * @}
 */
#endif

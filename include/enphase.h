/* Copyright (c) 2006-2011 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _enphase_h_
#define _enphase_h_


/** @file enphase.h
 *  @brief High Level enPhase Specific Definitions
 *
 *  This file @c enphase.h provides enPhase global definitions.
 */

/** @page enphaseHPg Header: enphase.h enPhase General Definitions

@section enphaseHName enphase.h HEADER NAME

    enphase.h -- enPhase General Definitions.

@section enphaseHSynop enphase.h SYNOPSIS

    This file, @c enphase.h, provides definitions of a general nature.

    Some part number assignments:

    800-00005       PCBA, PCU, 175-24-240-S
    800-00009       PCBA, PCU, 175-24-208-S
    800-00010       PCBA, RAPTOR PCU, 175-24-277-S
    800-00015       PCBA, PCU, M175-96-240
    800-00016       PCBA, PCU, M175-96-208
    800-00017       PCBA, PCU, M200-32-240
    800-00018       PCBA, PCU, M200-32-208
    800-00021       PCBA, PCU, Dually
    800-00022       PCBA, RAPTOR PCU, M175-72-208
    800-00023-r99   CMU, PCBA First Generation PCBA (prototype)

    541-00001       Config Table, Power Conditioning Table M175-24-240-S
    541-00002       Config Table, Power Conditioning Table M175-24-208-S
    541-00011       Power Conditioning Table, M200-32-240
    541-00012       Power Conditioning Table, M200-32-208
    541-00021       Power Conditioning Table, PCU, M175-72-240-S1
    541-00022       Power Conditioning Table, PCU, M175-72-208-S1
    541-00023       Power Conditioning Table, PCU, M175-72-277-S1

    520-00001       Processor Code Image, PCU
    520-00003       Processor Code Image, M200-32-240/208
    520-00006       Processor Load, PCU, M175-72-xxx-S1
    520-00080       Processor Load, CMU

    540-00002       Config Table, Manufacturing Data, PCU
    540-00004       Config table, Parameter Block, PCU, M175-24-240-S
    540-00005       Config table, Parameter Block, PCU, M175-24-208-S
    540-00014       Config table, Parameter Block, PCU, M200-96-240
    540-00015       Config table, Parameter Block, PCU, M200-96-208
    540-00021       Config table, Parameter Block, PCU, M175-72-240-S1
    540-00022       Config table, Parameter Block, PCU, M175-72-208-S1
    540-00023       Config table, Parameter Block, PCU, M175-72-277-S1
    540-00080       Config table, Parameter Block, CMU


    Current Serial Number Format:

        01      = vendor ID
        07      = year
        21      = work week
        999999  = unit number

      In BCD format it requires 6-bytes.  Messages provide room for
      8 bytes, the above format of 6-bytes should be left aligned in the
      8-byte field.



*/

/**
 * @defgroup enphaseHGroups enphase Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup enphaseHDefines enphase Header Defines
 */
/*@{*/

#define cMaxLine                8192
#define cBufSize                8192

#define STR(x) #x
#define XSTR(x) STR(x)

// disable the unknown pragma warning, ironically with another pragma
#define pragma WARNING DISABLE = 245


#define mPacked

#define ntohs htole16
#define ntohl htole32
#define htons be16toh
#define htonl be32toh

struct icarus_timeval {
    uint16_t   tv_sec;
    uint16_t   tv_msec;
};

#define mntohs
#define mntohl
#define mhtons
#define mhtonl

#define _keil_code                      
#define _keil_ddata                     
#define _keil_idata                     
#define _keil_bdata                     
#define _keil_xdata                     
#define _keil_far	                    
#define _keil_pdata                     
#define _keil_reentrant                 
#define _keil_large                     
#define _keil_bit						
#define _keil_sbit						
#define _keil_sfr						
#define _keil_sfr16						

#define cIsBigEndian            0

// The cAppMsgLen value is based on 6 l2 frames (6 * 50 bytes) of app data,
// that number does not include the 12 bytes per frame used for the L2
// frame header.
#define cAppMsgLen              496

#define ENP_OK                  0
#define ENP_ERROR               (-1)

#define mMax(a,b)               (((a) > (b)) ? (a) : (b))
#define mMin(a,b)               (((a) < (b)) ? (a) : (b))

#define cEmuPcuListenPort       28282

#define cPartNumIdLen           4

#define cPartNumInStrFmtLen     24

// Define enphase OUI.
#define cenPhaseMacOuiByte1     0x00
#define cenPhaseMacOuiByte2     0x1d
#define cenPhaseMacOuiByte3     0xc0

#define cSerialNumBytesLen      6
#define cSerialNumStrLen        25  // Allow for non-PLC serial numbers
#define cMacAddrStrLen          18
#define cIpAddrStrLen           16

#define cBit0                   (1 << 0)
#define cBit1                   (1 << 1)
#define cBit2                   (1 << 2)
#define cBit3                   (1 << 3)
#define cBit4                   (1 << 4)
#define cBit5                   (1 << 5)
#define cBit6                   (1 << 6)
#define cBit7                   (1 << 7)
#define cBit8                   (1 << 8)
#define cBit9                   (1 << 9)
#define cBit10                  (1 << 10)
#define cBit11                  (1 << 11)
#define cBit12                  (1 << 12)
#define cBit13                  (1 << 13)
#define cBit14                  (1 << 14)
#define cBit15                  (1 << 15)
#define cBit16                  (1 << 16)
#define cBit17                  (1 << 17)
#define cBit18                  (1 << 18)
#define cBit19                  (1 << 19)
#define cBit20                  (1 << 20)
#define cBit21                  (1 << 21)
#define cBit22                  (1 << 22)
#define cBit23                  (1 << 23)
#define cBit24                  (1 << 24)
#define cBit25                  (1 << 25)
#define cBit26                  (1 << 26)
#define cBit27                  (1 << 27)
#define cBit28                  (1 << 28)
#define cBit29                  (1 << 29)
#define cBit30                  (1 << 30)
#define cBit31                  (1 << 31)

// ASIC register definition mapping for use on Envoy (register info)
#define cEnvoyAsicAryMask       (0xFF000000)
#define cEnvoyAsicAryField      (24)
#define cEnvoyAsicSizeMask      (0x00FF0000)
#define cEnvoyAsicSizeField     (16)
#define cEnvoyAsicAddrMask      (0x0000FFFF)
#define cEnvoyAsicAddrField     (0)

#define cEnvoyAsicSize32        (4)
#define cEnvoyAsicSize16        (2)
#define cEnvoyAsicSize8         (1)
#define cMD5DigestLen   16


/*@}*/  /* END of Defines Group */


/* Define byte (offset 3) of progParts struct value -- for encoding
   portion of the ASIC part number */

// Heron3 ASIC
#define cAsicPnumStringByteOffset3  0x31

/* ------------------------------------ Typedefs -------------------------- */

/**
 * @defgroup enphaseHTypedefs enphase Header Typedefs
 */
/*@{*/

/** Remember that you can add your own grouping of definitions as you see fit.
 *
 */

// The constants defined below are all on a per device basis.  See related
// header file emueh.h

#if 0
#define cPcu2MaxControllers     1
#define cPcuMaxControllers      1
#define cCmuMaxControllers      1
#define cAmuMaxControllers      1
#define cThermMaxControllers    1

#define cPcu2MaxChannels        2
#define cPcuMaxChannels         1
#define cCmuMaxChannels         12
#define cAmuMaxChannels         2
#define cThermMaxChannels       1


typedef enum _enPChanType {
    cnPChanTypeNone,
    cnPChanTypeProductive,
    cnPChanTypeConsumptive,
    cnPChanTypeMonitoring,
    cnPChanTypeThermostat,
    cnPChanTypeNumOf
} enPChanType;
#endif


#define cManuDataTotal          64
#define cMacAddrLen             6
#define cMacAddrLenFill         (6+2)
#define cEkLen                  8
#define cFillLen                24
#define cManuNumProgParts       4
#define cAsicIdSize             8
#define cL2DomainAddrLen         6

typedef struct  _tSerialNumber {
    uint8_t                     byteArr[cSerialNumBytesLen];
    uint8_t                     fillb2;
    uint8_t                     fillb1;
} tSerialNumber;

typedef struct  _tPartNumber {
    uint8_t                     idb[cPartNumIdLen];
    uint8_t                     manuRev;
    uint8_t                     dMajor;
    uint8_t                     dMinor;
    uint8_t                     dMaint;
} tPartNumber;

/*! Define tAsicIdNumber */
/** This @c tAsicIdNumber structure is used to store the unique identifier
	present in the ASIC.

	This is used to uniquely identify the PCU during manufacturing runup to
	allow programming with the correct serial number from the Envoy.
*/


typedef struct  _tAsicId {
    uint8_t                     asicId[cAsicIdSize];
} tAsicId;
/** The structure defines the basic manufacturing data information.
    The "manuData" area is for further manufacturing purposes.

*/
typedef struct _tManuData_Ver1 {
    tPartNumber                 partNum;
    tSerialNumber               serialNum;
    uint8_t                    macAddr[cMacAddrLenFill];
    uint8_t                    fill[40];
} tManuData_Ver1;

// NOTE:  The difference between version 2 and version 3 of the manudata
//        is that the number of programmable parts in the progParts array
//        was reduced from 4 to 3 going from version 2 to 3.  Since the
//        extent of this array is not used currently this change could be
//        made in a transparent manner.
//        The reason for this change was to make for more bytes available to
//        in the dev-info MCP structure which also uses cManuNumProgParts.
//        This related MCP change was made in MCP version 4.
//        Note total manudata record size did not change due to this change,
//        the number of fill bytes at the end of the structure was change to
//        8 from 4.

/*

   manudata version 3:

      - converted fill to controller id
      - changed last fill from 4 bytes to 8 bytes

 */

#define cManuDataVersion        3
#define cManuDataSize           64
#define cManuDataCalibrationDataOffset 0x100
typedef u_int8_t uint8_t;
typedef u_int16_t uint16_t;
typedef u_int32_t uint32_t;
typedef struct _tManuData {
    tSerialNumber               serialNumber;
    tPartNumber                 boardPartNumber;
    tPartNumber                 assemblyPartNumber;
    uint8_t                    manuDataVer;
    uint8_t                    controllerId; // 1-2 (dually), else (0)
    uint8_t                    fillb1;
    uint8_t                    numProgrammableParts;
    tPartNumber                 progParts[cManuNumProgParts];
    uint8_t                    fill[4];  // calibration data at offset 0x100
} tManuData;

typedef enum _eRegion {
    cRegionNA,
    cRegionROW,
    cRegionNumOf
}eRegion;

typedef enum _ePlatform {
    cPlatformCorvusOrFrigate,
    cPlatformHornet,
    cPlatformNumOf
}ePlatform;


typedef struct _tGatewayAssoc {
    uint8_t                    domainAddr[cL2DomainAddrLen];
    uint16_t                   modAddr;
    uint32_t                   grpMask;
} tGatewayAssoc;

/*@}*/  /* END of enphase Header Typedef Group */

/* ------------------------------------ Extern Declarations --------------- */

/**
 * @defgroup enphaseHExtdata enphase Header Extern Data Definitions
 */
/*@{*/



/*@}*/  /* END of enphase Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup enphaseHGlobdata enphase Header Global Data Defined
 */
/*@{*/


/*@}*/  /* END of enphase Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup enphaseHStatdata enphase Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of enphase Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup enphaseHProto enphase Header Prototypes
 */
/*@{*/
/*@}*/  /* END of enphase Header Prototypes Group */


/*@}*/  /* END enphase Header Definitions and Declarations */


#endif /* _enphase_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of enphase.h */



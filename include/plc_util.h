/* Copyright (c) 2006-2010 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _plc_util_h_
#define _plc_util_h_


/** @file plc_util.h
 *  @brief PLC Utilities
 *
 *  This file @c plc_util.h provides the PLC Utilities
 *
 */

/** @page plc_utilHPg Header: plc_util.h PLC Utilities

@section plc_utilHName plc_util.h HEADER NAME

    plc_util.h -- PLC Utilities

@section plc_utilHSynop plc_util.h SYNOPSIS

    This file provides the EMU's Something.

@section plc_utilHDesc plc_util.h DESCRIPTION

    This file, @c plc_util.h provides the PLC Utilities.

    Specifically this interface defines powerline-communications (PLC) low
    level utilities that are not communications-driver specific, though the
    utilities in this module can be and are called by communications-driver
    specific contexts.

*/


/* system headers */
#include <stddef.h>
#include <stdlib.h>

#include "include/enphase.h"

/**
 * @defgroup plc_utilHGroups plc_util Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines -------------------------- */

/**
 * @defgroup plc_utilHDefines plc_util Header Defines
 */
/*@{*/

// used for Histogram construction and cluster detection
// in plcUtilHistAndCluster
#define cMaxLogLevel                 32    // 7-bit field in FA7F with max value of 31
#define cValidityThresholdPercent    8
#define cPreambleLength              64

/* Random number generator is reseeded every this many times raven
 * periodic processing is called
 */
#define cPlcUtilRandomNumberReseedPeriod            500

/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs ------------------------- */

/**
 * @defgroup plc_utilHTypedefs plc_util Header Typedefs
 */
/*@{*/

/*!
 * tPlcUtilHistClusterInfo - struct that maintains information and state
 *                           required for the histogram construction and
 *                           clustering used as part of the SSI computation
 *                           for adaptive squelch.
 */
typedef struct _tPlcUtilHistClusterInfo {
    u_int8_t                   workingArray[2*cMaxLogLevel];  // the size has been chosen keeping
                                                               // in mind that there will be zeroes
                                                               // padded before hist construction
    u_int8_t                    workingArraySize;
    u_int8_t                    numClusters;
    u_int8_t                    numSamplesProcessed;
} tPlcUtilHistClusterInfo;

/** Remember that you can add your own grouping of definitions as you see fit.
 *
 */


/*@}*/  /* END of plc_util Header Typedef Group */

/* ------------------------------------ Extern Declarations -------------- */

/**
 * @defgroup plc_utilHExtdata plc_util Header Extern Data Definitions
 */
/*@{*/

extern tPlcUtilHistClusterInfo _keil_xdata plcUtilHistClusterInfo;

/*@}*/  /* END of plc_util Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined -------------- */

/**
 * @defgroup plc_utilHGlobdata plc_util Header Global Data Defined
 */
/*@{*/

/*@}*/  /* END of plc_util Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined -------------- */

/**
 * @defgroup plc_utilHStatdata plc_util Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of plc_util Header Static Data Defined Group */

/* ------------------------------------ Prototypes ----------------------- */

/**
 * @defgroup plc_utilHProto plc_util Header Prototypes
 */
/*@{*/


/** plcUtilScramble - scramble / unscramble the buffer.

*/

//#if defined(INCLUDE_CORONA)
/** plcUtilScramble - scramble / unscramble the buffer. */
extern void
plcUtilScramble(u_int8_t _keil_xdata * buf);
//#endif /* if defined(INCLUDE_CORONA) */

//#if defined(INCLUDE_CORONA)
/** plcUtilDomainAddrIsZero - true if domain addr is zero. */
extern bool
plcUtilDomainAddrIsZero(u_int8_t _keil_xdata * domainAddr);
//#endif /* if defined(INCLUDE_CORONA) */

/** plcUtilDriverLvlPktFilter - perform driver level packet filtering. */
extern bool
plcUtilDriverLvlPktFilter(u_int8_t _keil_xdata     * buf,
                          tPlcCounters _keil_xdata * plccnts);

 /** plcUtilDriverLvlFastpathFilter - perform driver level packet filtering */
 /**                                  seraching for fastpath frames         */
extern bool
plcUtilDriverLvlFastpathFilter(u_int8_t _keil_xdata  *buf);

bool
plcUtilChkAddrGrp(u_int32_t _keil_xdata  grpMask);

/*!
 * plcUtilFindFirstClusterPeak - find the peak of the first cluster in the histogram
 *                               this value is the SSI of the signal whose samples
 *                               were used to make the histogram.
 * inputs:
 * u_int16_t* pHist - pointer to histogram
 * u_int16_t size - number of samples used to create histogram
 *
 * outputs:
 * u_int8_t* peak - reference argument to pass back value of first peak
 * int return - number of clusters found. -1 => no clusters found.
 */
extern int plcUtilFindFirstClusterPeak(u_int16_t* pHist, u_int16_t size, u_int8_t* peak);


extern void plcUtilGpioOn(u_int8_t pinNum);
extern void plcUtilGpioOff(u_int8_t pinNum);

/** plcReseedRand - Re-seeds random number generator */
extern void
plcUtilReseedRand(void);

/** plcGetRand - returns random number between specified range [min,max],
 *  inclusive of both values.
 */
extern u_int16_t
plcUtilGetRand(u_int16_t min, u_int16_t max);

extern void
plcUtilDumpMem(const char *str, u_int8_t _keil_xdata *dp, u_int16_t len);

extern void
plcUtilDumpMsg(bool rxDir, u_int8_t *msg);

extern bool
plcUtilSerialNumMatch(u_int8_t _keil_xdata *sn1,
                      u_int8_t _keil_xdata *sn2);

/*@}*/  /* END of plc_util Header Prototypes Group */

/*@}*/  /* END plc_util Header Definitions and Declarations */


#endif /* _plc_util_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ----------------------------------------------------------------------- */

/* END of plc_util.h */



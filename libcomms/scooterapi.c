/*!
 * @Copyright
 *  Copyright (c) 2015 Sunpower Corp.  All rights reserved.
 *
 * @file scooterapi.c
 *  A module for handling the communication details of when interacting with a
 *  PVS5 Sidecar
 *
 * @date
 *  Jun 17, 2015
 *
 * @author
 *  Miles Bintz <mbintz@sunpower.com>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#ifndef WIN32
#include <pthread.h>
#include <alloca.h>
#else
#include "win32/pthread.h"
#define alloca _alloca
#endif // !WIN32
#include <memory.h>
#include <assert.h>

#include <time.h>

#include "sbt/os.h"
#include "sbt/scooterProto.h"
#include "sbt/fifo.h"
#include "sbt/log.h"
#include "sbt/errno.h"
#include "sbt/scooter_uart.h"
#include "sbt/scooterapi.h"
#include "sbt/crc.h"
#include "sbt/portable_endian.h"

struct sScooter_t
{
    connection_handle_t hConn;
    scooterCB_t pfnCB;
    void *cbcookie;

    scooterMeterCB_t meterCB;
    void *metercookie;
    uint8_t lun;
    uint32_t readtimeout;
    bool bWaitingForAck;
    hFifo ackfifo;
    hFifo plcfifo;
    hFifo readfifo;
    pthread_mutex_t writemutex;
};

typedef struct
{
    uint8_t ackdev;
    uint8_t reason;
} acknak_t;

static uint8_t getCommand(uint8_t *pBuf, uint8_t **pRealCmd);
static int32_t readL2(hScooter_t handle, uint8_t *pRxBuf, size_t *rxbuflen, eSCLun_t *pLun,
        uint32_t timeout);
static uint32_t handleWritePlc(hScooter_t handle, uint8_t *pTxbuf,
        uint8_t *pRxBuf, size_t *rxbuflen);
static uint32_t handleRead(hScooter_t handle, uint8_t *pTxbuf,
        uint8_t *pRxBuf, size_t *rxbuflen, eSCLun_t *pLun);

static int32_t defaultscootercb(void *pCookie, uint32_t flags,
                                   uint32_t val0, uint32_t val1)
{
    hScooter_t handle = (hScooter_t)pCookie;

    sSCCmdResetStatusFlags_t rflags = { {eSC_INTF_RESET_STATUS_FLAGS}, flags };

    LOG_WARNING("Cookie = %p, flags = 0x%08x, val0 = %d, val1 = %d\n",
                 pCookie, flags, val0, val1);

    LOG_INFO("Clearing alerts\n");
    ScooterWrite(handle, (uint8_t*)&rflags, sizeof(rflags));

    return 0;
}

static void uart_packet_dump(uint8_t *buffer, int length)
{
    int i;

    fprintf(stderr, "Packet length %d: ",length);
    for (i = 0; i < length; i++)
    {
        if (i > 0 && i % 16 == 0)
        {
            fputc('\n', stderr);
        }

        fprintf(stderr, "%02x ", buffer[i]);
    }
    fputc('\n', stderr);
    fflush(stderr);
}


int32_t scooterReadCB(void *pCookie, uint8_t *pBuf, const size_t rxLen)
{
    hScooter_t handle = (hScooter_t)pCookie;
    uint8_t dev;
    sSCRsp_t *pRsp = (sSCRsp_t*)pBuf;
    int rc;
    sSCCmdHeaderV2_t *pV2;

    // we enter here in the context of the Connection's read thread.  pBuf
    // is a raw (Layer 2) scooterProto frame.  It exists only in pBuf for now.
    //
    // There is a window to the north and a door to the east.  There is a
    // night-stand with an open drawer on the south wall.  What would you like
    // to do?
    dev = pRsp->header.dev;
    if (pRsp->header.dev == eSC_INTF_CMD_VERSION2) {
        dev = pBuf[sizeof(sSCCmdHeaderV2_t)];
        pRsp = (sSCRsp_t *)&pBuf[sizeof(sSCCmdHeaderV2_t)];
        pV2 = (sSCCmdHeaderV2_t *)pBuf;
        if (pV2->lun == SC_E_ASIC_PLC && (pV2->sequence & SCCMDHEADERV2_SEQUENCE_CONT))
        {
            dev = eSC_INTF_WRITE_PLC;
            // force to plcfifo since this is a continuation packet and does not have a dev
        }
    }
    
    switch (dev)
    {
        case eSC_INTF_ACK:
        case eSC_INTF_NACK:
            // acks and nacks need to be signaled to the tx'ing thread
            if(handle->bWaitingForAck)
            {
                acknak_t an;
                an.ackdev = pRsp->header.dev;
                an.reason = pRsp->payload[0];
                rc = fifoAdd(handle->ackfifo, (uint8_t*)&an, sizeof(an));
                if(rc != 0)
                {
                    LOG_CRIT("Couldn't push ack to ackfifo %d\n", rc);
                }
            }
            else
            {
                LOG_CRIT("Recieved unexpected ack/nack = %d/%d\n", pRsp->header.dev, pBuf[1]);
                uart_packet_dump(pBuf, 32);
            }
            break;

        case eSC_INTF_READ_PLC:
        case eSC_INTF_WRITE_PLC:
            rc = fifoAdd(handle->plcfifo, pBuf, rxLen);
            if(rc != 0)
            {
                LOG_CRIT("Couldn't push buffer to PLC fifo %d\n", rc);
            }
            break;

        case eSC_INTF_WNPOLL:
        case eSC_INTF_WN_FAST_POWER:
            if(handle->meterCB == NULL)
            {
                LOG_VERBOSE_HI("Ignoring async meter report\n");
            }
            else
            {
                handle->meterCB(handle->metercookie, pRsp->header.dev, (uint8_t*)pRsp);
            }
            break;

        default:
        {
            rc = fifoAdd(handle->readfifo, pBuf, rxLen);
            if(rc != 0)
            {
                LOG_CRIT("Couldn't push buffer to read fifo %d\n", rc);
            }
            break;
        }
    }
    return 0;
}

/*!
 * \addtogroup sidecar_api Sidecar API
 * @{
 */

/*!
 * Allocate and initialize the opaque control structure for a ScooterAPI object
 *
 * @param pHandle is a pointer to a handle object which will be assigned to
 *        the new Scooter object
 * @param hConn is a handle to the connection that the scooter API should use
 *
 * @returns 0 for success, < 0 for error
 */
int32_t ScooterNew(hScooter_t *pHandle, connection_handle_t hConn)
{
    struct sScooter_t *pS;
    int rc;

    if(!pHandle) return -EINVAL;

    //
    // allocate room for this object's control block
    //
    pS = malloc(sizeof(struct sScooter_t));
    if(!pS) return -ENOMEM;

    // zero it out to be safe
    memset(pS, 0, sizeof(*pS));

    // and assign members
    pS->hConn = hConn;

    // assign a default callback to handle async scooter alerts.  If a client
    // doesn't register their own then this one will complain noisily on
    // stderr
    pS->pfnCB = defaultscootercb;
    pS->cbcookie = (void*)pS;
    pS->readtimeout = 500;
    pS->lun = SC_DEFAULT_LUN;
    *pHandle = pS;

    pthread_mutex_init(&pS->writemutex, NULL);

    rc = fifoCreate(&pS->readfifo, 8, 256);
    assert(rc == 0);

    rc = fifoCreate(&pS->plcfifo, 10, 256);
    assert(rc == 0);

    rc = fifoCreate(&pS->ackfifo, 8, sizeof(acknak_t));
    assert(rc == 0);

    //
    // with a connection in hand, we want to enhance the way we handle TX ACK
    // callbacks
    //
    ConnectionRegisterCallback(hConn, scooterReadCB, (void*)pS, 0, 0);
    return 0;
}

/*!
 * Deinitialize and free the control structure for the provided instance handle
 *
 * @param handle is the Scooter object handle to free
 *
 * @returns 0 for success, < 0 for error
 */
int32_t ScooterDelete(hScooter_t handle)
{
    if(!handle) return -EINVAL;

    pthread_mutex_destroy(&handle->writemutex);
    fifoDestroy(handle->ackfifo);
    fifoDestroy(handle->readfifo);
    fifoDestroy(handle->plcfifo);

    free(handle);

    return 0;
}

int32_t ScooterWrite(hScooter_t handle, uint8_t *pTxbuf, uint32_t txlen)
{
    int rc;

    // don't let more than one thread try to write at the same time
    pthread_mutex_lock(&handle->writemutex);

    //
    // Transmit the requested buffer
    //
    rc = ConnectionWriteL2(handle->hConn, pTxbuf, txlen);

    // note that this lock is also unlocked in handleWritePLC which may lead
    // to us doing a double-unlock but that should be ok.
    pthread_mutex_unlock(&handle->writemutex);

    return rc;
}

#define MIN(a,b) ((a) < (b) ? (a) : (b))

int32_t ScooterRead(hScooter_t handle, uint8_t *pRxbuf, size_t *rxlen, eSCLun_t *pLun)
{
    //
    // Wait to read a buffer. Timeout is whatever the connection's rcv timeout
    // was set to in another call
    //
    return readL2(handle, pRxbuf, rxlen, pLun, handle->readtimeout);
}

int32_t ScooterReadPLC(hScooter_t handle, uint8_t *pRxbuf, size_t *rxlen)
{
    int rc;

    //
    // Wait to read a buffer. Timeout is whatever the connection's rcv timeout
    // was set to in another call
    //
    rc = fifoRemove(handle->plcfifo, pRxbuf, rxlen, handle->readtimeout);
    if(rc != 0)
    {
        LOG_VERBOSE_HI("(timeout = %d) rc = %d\n", handle->readtimeout, rc);
    }
    return rc;
}

int32_t ScooterTimeoutSet(hScooter_t handle, uint32_t timeout)
{
    if(!handle) return -EINVAL;

    handle->readtimeout = timeout;
    return 0;
}

int32_t ScooterTimeoutGet(hScooter_t handle)
{
    if(!handle) return -EINVAL;
    return handle->readtimeout;
}
int32_t ScooterLunSet(hScooter_t handle, uint8_t lun)
{
    if(!handle) return -EINVAL;

    handle->lun = lun;
    return 0;
}
int32_t ScooterLunGet(hScooter_t handle)
{
    if(!handle) return -EINVAL;

    
    return handle->lun;
}
/*!
 * The heart of the ScooterAPI.  Given an object handle, output buffers
 * (commands), and input buffers (responses), send and receive the necessary L2
 * frames to make the transaction happen
 *
 * @param handle is the ScooterAPI object handle
 * @param pTxbuf is the pointer to the command buffer to transmit
 * @param txlen is the lenght of the packet in pTxbuf
 * @param pRxbuf is the pointer to the response buffer
 * @param rxbuflen is a bidirectional parameter.  On entry *rxbuflen is the
 *        size of pRxbuf.  On exit rxbuflen is the length of the actual
 *        response received.
 *
 * @note that the response to a PLC transmit is NOT a PLC response.  The
 * response to a PLC transmit (if any) is an ACK/NAK.  PLC responses are
 * actually received "unsolicited".
 *
 * @returns 0 on success, < 0 on error
 */
int32_t ScooterExecute(hScooter_t handle, uint8_t *pTxbuf, uint32_t txlen,
                     uint8_t *pRxbuf, size_t *rxbuflen, eSCLun_t *pLun)
{
    int rc;
    uint8_t *pNestedCmd;
    int cmd;
    uint8_t *pTmpbuf;
    size_t tmpsize;
    int ui32Retries = 2;
    sSCCmdHeaderV2_t *pV2Hdr, *pTmp;
    uint8_t buf[128 + sizeof(sSCCmdHeaderV2_t)];
    

    // @todo - is it reasonable to not provide rx buffers if they don't want
    // the reposnse?
    if(!handle || !pTxbuf || !txlen) 
    {
        printf("bad call to ScooterExecute  \n");
        return -EINVAL;
    }

    //
    // Handle layer-3isms.  If the command we just sent was a PLC command
    // and if it requested a TX ack then we need to wait for it.
    //
    cmd = getCommand(pTxbuf, &pNestedCmd);
    switch(cmd)
    {
        case eSC_INTF_WRITE_PLC:
            //
            // we're starting a write so flush all the acks...
            //
            fifoFlush(handle->ackfifo);

            //
            // And we don't care about any unread PLC messages at this point
            //
            fifoFlush(handle->plcfifo);

            //
            // I think we're ALWAYS wanting PLC TX acks.  Indicate that we'll
            // wait before we transmit the command
            //
            handle->bWaitingForAck = true;
            //
            // Transmit the requested buffer
            //

            // for Enphase, packets can be longer than fit in a single COBS message, so we need to break up.
            // 128 byte fragments 
            if ((*pTxbuf == eSC_INTF_CMD_VERSION2) && (pTxbuf[1] == SC_E_ASIC_PLC) && (txlen > 134)) 
            {
                int numSegs,bytesToSend;
                int first=1;
                int bytesLeft = txlen - sizeof(sSCCmdHeaderV2_t);
                numSegs = (bytesLeft + 127) / 128;
                
                pTmpbuf = &pTxbuf[sizeof(sSCCmdHeaderV2_t)];
                pV2Hdr = (sSCCmdHeaderV2_t *)buf;
                pTmp = (sSCCmdHeaderV2_t *)pTxbuf;
                pV2Hdr->sequence = numSegs - 1;
                pV2Hdr->flags = pTmp->flags;
                while (bytesLeft) 
                {
                    if (first)
                    {
                        first = 0;
                        pV2Hdr->sequence |= SCCMDHEADERV2_SEQUENCE_START;
                    }
                    else
                    {
                        pV2Hdr->sequence &= ~SCCMDHEADERV2_SEQUENCE_START;
                        pV2Hdr->sequence |= SCCMDHEADERV2_SEQUENCE_CONT;
                        pV2Hdr->flags = 0;
                        --pV2Hdr->sequence;
                    }
                    bytesToSend = MIN(128, bytesLeft);
                    pV2Hdr->packetSize = bytesToSend;
                    pV2Hdr->lun = SC_E_ASIC_PLC;
                    pV2Hdr->dev = eSC_INTF_CMD_VERSION2;
                    
                    memcpy(&buf[sizeof(sSCCmdHeaderV2_t)], pTmpbuf, bytesToSend);

                    rc = ScooterWrite(handle, buf, bytesToSend + sizeof(sSCCmdHeaderV2_t));
                    if(rc != 0) return rc;
                    pTmpbuf += bytesToSend;
                    bytesLeft -= bytesToSend;

                }

            }
            
            else
            {
                
                if (*pTxbuf == eSC_INTF_CMD_VERSION2) {
                    // enphase, need to make sure the packetization is correct.
                    pV2Hdr = (sSCCmdHeaderV2_t *)pTxbuf;
                    pV2Hdr->sequence = SCCMDHEADERV2_SEQUENCE_START;
                    pV2Hdr->packetSize = txlen - sizeof(sSCCmdHeaderV2_t);
                }
                rc = ScooterWrite(handle, pTxbuf, txlen);
                if(rc != 0) return rc;
            }
            if (handle->bWaitingForAck == true) {
                rc = handleWritePlc(handle, pNestedCmd, pRxbuf, rxbuflen);
            }
            else
                rc = 0;
            break;

        case eSC_INTF_VERSION:
        case eSC_INTF_ECHO:
        case eSC_INTF_WRITE_WHITELIST:
        case eSC_INTF_CLEAR_WHITELIST:
        case eSC_INTF_SET_FILTERING:
        case eSC_INTF_WNPOLL:
        case eSC_INTF_READ_SCSTATS:
        case eSC_INTF_READ_STATS:
        case eSC_INTF_READ_WNSTATS:
        case eSC_INTF_RESET_SCSTATS:
        case eSC_INTF_RESET_WNSTATS:
        case eSC_INTF_RESET_STATS:
        case eSC_INTF_RESET_STATUS_FLAGS:
        case eSC_INTF_ADATA:
        case eSC_INTF_GRID_DATA:
        case eSC_INTF_SET_CONFIG:
        case eSC_INTF_SET_WN_CT_CONFIG:
        case eSC_INTF_READ_CSMASTATS:
        case eSC_INTF_NULL_COMMAND:
        case eSC_INTF_READ_HISTOGRAM:
        case eSC_INTF_RESET:
        case eSC_INTF_UPGRD_DATA:
        case eSC_INTF_UPGRD_COMMIT:
        case eSC_INTF_START_HF_FFT:
        case eSC_INTF_READ_HF_FFT:
        case eSC_INTF_GET_E_ASIC_RSSI:
		case eSC_INTF_E_ASIC_SET_CONFIG:
            tmpsize = *rxbuflen + sizeof(sSCCmdHeaderV2_t);
            pTmpbuf = alloca(tmpsize);
            do
            {
                //
                // if we're starting a new scooter command, anything left pending
                // in the response queue is bogus
                //
                fifoFlush(handle->readfifo);

                rc = ScooterWrite(handle, pTxbuf, txlen);
                if(rc != 0)
                {
                    LOG_ERROR("ScooterWrite returned %d\n", rc);
                    return rc;
                }
                assert((*rxbuflen >= 0) && (*rxbuflen <= 256));

                rc = handleRead(handle, pNestedCmd, pTmpbuf, &tmpsize, pLun);
                if(rc != 0)
                {
                    LOG_ERROR("handleRead returned %d\n", rc);
                    return rc;
                }
                else
                {

                    if((getCommand(pTmpbuf, NULL) == cmd) ||
                        (((cmd == eSC_INTF_UPGRD_DATA) ||
                          (cmd == eSC_INTF_UPGRD_COMMIT)) &&
                              (getCommand(pTmpbuf, NULL) == eSC_INTF_UPGRD_STAT)))
                    {
                        memcpy(pRxbuf, pTmpbuf, tmpsize);
                        *rxbuflen = tmpsize;
                        break;
                    }
                    else
                    {
                        LOG_WARNING("Incoming sidecar response != outgoing command (cmd/rsp = %d/%d\n",
                                     cmd, pTmpbuf[0]);
                    }
                }
            } while(rc && ui32Retries--);
            if(ui32Retries == 0) return -SBT_EEXHAUSTED;
            break;

        default:
            //
            // @todo handle everything else
            //
            LOG_WARNING("Not handling command %d\n", cmd);
            rc = -EINVAL;
            break;

    } // switch getCommand()
    return rc;
}

/*!
 * Register the callback that will be called when the sidecar has alerts set
 *
 * @param handle is the handle to the ScooterAPI object
 * @param pfncb is a pointer to the callback function
 * @param pCookie is a value that is passed back to the callback
 *
 * @returns 0 for success, < 0 for errors
 */
int32_t ScooterCallbackSet(hScooter_t handle, scooterCB_t pfncb, void *pCookie)
{
    if(!handle || !pfncb) return -EINVAL;

    handle->pfnCB = pfncb;
    handle->cbcookie = pCookie;

    return 0;
}

/*!
 * Register the callback that will be called when the sidecar has alerts set
 *
 * @param handle is the handle to the ScooterAPI object
 * @param pfncb is a pointer to the callback function
 * @param pCookie is a value that is passed back to the callback
 *
 * @returns 0 for success, < 0 for errors
 */
int32_t ScooterRegisterMeterCallback(hScooter_t handle, scooterMeterCB_t pfncb, void *pCookie)
{
    if(!handle || !pfncb) return -EINVAL;

    handle->meterCB = pfncb;
    handle->metercookie = pCookie;

    return 0;
}

/*!
 * Pack a PLC command into a scooter packet.
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 * @param flags is the Sidecar flags which control how the transaction is
 *        executed.  Valid flags include:  \ref PLC_FLAGS_RQACK
 * @param pInbuf is the input buffer containing the PLC payload
 * @param inlen is the size of the input buffer payload
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackWriteTx(uint8_t *pOutbuf, uint32_t flags, const uint8_t *pInbuf,
                           uint32_t inlen)
{
    int i = 0;
    if(!pOutbuf || !pInbuf || !inlen) 
    {
        printf("Bad argumnets to ScooterPackWriteTx \n");
        return -EINVAL;
    }

    pOutbuf[i++] = eSC_INTF_WRITE_PLC;
    pOutbuf[i++] = flags & 0xff;
    memcpy(pOutbuf + i, pInbuf, inlen);
    i += inlen;

    return i;
}

int32_t ScooterPackEcho(uint8_t *pOutbuf, uint8_t *pInbuf, uint32_t inlen)
{
    int i = 0;

    pOutbuf[i] = eSC_INTF_ECHO;
    i++;
    memcpy(pOutbuf + i, pInbuf, inlen);
    return inlen + i;
}

// Temporary define
#define ASCII_HEX_MAX 248

int32_t ScooterPackAsciiHex(uint8_t *pOutbuf, const char *hexStr)
{
	int i = 0;
	int j = -1;
	for (i = 0; i < ASCII_HEX_MAX; i++)
	{
		if (i % 2 == 0)
		{
			j++;
			pOutbuf[j] = 0;
		}
		pOutbuf[j] <<= 4;
		if (hexStr[i] == 0) break;
		if (hexStr[i] >= '0' && hexStr[i] <= '9')
		{
			pOutbuf[j] |= hexStr[i] - '0';
			continue;
		}
		if (hexStr[i] >= 'a' && hexStr[i] <= 'f')
		{
			pOutbuf[j] |= hexStr[i] - 'a' + 10;
			continue;
		}
		if (hexStr[i] >= 'A' && hexStr[i] <= 'F')
		{
			pOutbuf[j] |= hexStr[i] - 'A' + 10;
			continue;
		}
		return -1;  // Not a hex digit
	}
	if (strlen(hexStr) % 2) return -1;  // No nibbles
	return j;
}


/*!
 * Form a packet to clear the specified bits
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 * @param flags is the Sidecar flags to clear
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackStatusFlagsReset(uint8_t *pOutbuf, uint32_t flags)
{
    sSCCmdResetStatusFlags_t *pCmd = (sSCCmdResetStatusFlags_t*)pOutbuf;
    pCmd->header.dev = eSC_INTF_RESET_STATUS_FLAGS;
    pCmd->resetFlagsMask = htole32(flags);
    return sizeof(*pCmd);
}

/*!
 * Encapsulate a sidecar "version 1" packet into a "version 2" capsule
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 * @param ui8LUN is the logical unit number to address with this packet
 * @param ui32flags are the Sidecar flags which control how the transaction is
 *        executed.  There are no flags defined yet.
 * @param pIn is the input buffer containing the payload
 * @param inlen is the size of the input buffer payload
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackV2(uint8_t *pOutbuf, uint8_t ui8LUN, uint16_t ui16Flags,
                        uint8_t *pIn, uint32_t inlen)
{
    int i = 0;
    sSCCmdV2_t *pCmd = (sSCCmdV2_t *)pOutbuf;
    if(!pOutbuf || !pIn || !inlen) 
    {
        printf("Bad argumnets to ScooterPackV2 \n");
        return -EINVAL;
    }

    pCmd->header.dev = eSC_INTF_CMD_VERSION2;
    pCmd->header.lun = ui8LUN;
    pCmd->header.flags = htole16(ui16Flags);
    i += sizeof(sSCCmdHeaderV2_t);

    // note that memmove works "in-place".  Thus, if pIn lined up correctly
    // within pOutbuf then this memmove should be a no-op (or, if the
    // if the implementation is dumb, should copy the data onto itself w/ no
    // side-effect other than wasted time).
    memmove(pCmd->payload, pIn, inlen);
    i += inlen;

    return i;
}

/*
 * Form a command for requesting histogram data from the sidecar
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackHistogramRead(uint8_t *pOutbuf, uint16_t index, uint8_t name)
{
    sSCCmdReadHistogram_t *pCmd = (sSCCmdReadHistogram_t*)pOutbuf;
    int len = 0;

    if(!pOutbuf) return -EINVAL;

    pCmd->dev = eSC_INTF_READ_HISTOGRAM;
    len += 1;

    pCmd->index = index;
    len += 2;

    pCmd->name = name;
    len += 1;

    return len;
}

int32_t ScooterUnpackHistogram(const uint8_t *pInBuf, sSCHistogram_t *pHistogram)
{
    sSCRspHeader_t *pRspHeader = (sSCRspHeader_t*)pInBuf;
    int i;
    uint16_t length;
    uint16_t *pLength = (uint16_t*)pHistogram;
    uint32_t *pIntval = (uint32_t*)(pHistogram->data);

    if(!pInBuf || !pHistogram) return -EINVAL;
    if(pRspHeader->dev != eSC_INTF_READ_HISTOGRAM) return -SBT_EMISMATCH;

    pInBuf += sizeof(sSCRspHeader_t);

    length = (pInBuf[1] << 8) | (pInBuf[0] << 0);
    *pLength = length;

    pInBuf += 2;

    for(i = 0; i < length; i++)
    {
        pIntval[i] = (pInBuf[3] << 24) | (pInBuf[2] << 16) |
                     (pInBuf[1] << 8)  | (pInBuf[0] << 0);
        pInBuf += 4;
    }

    return 0;
}

/*!
 * Form a command for requesting version from the sidecar
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackVersionGet(uint8_t *pOutbuf)
{
    if(!pOutbuf) return -EINVAL;

    *pOutbuf = eSC_INTF_VERSION;
    return 1;
}
int32_t ScooterPackCSMAStatsRead(uint8_t *pOutbuf)
{
    if(!pOutbuf) return -EINVAL;

    *pOutbuf = eSC_INTF_READ_CSMASTATS;
    return 1;
}

int32_t ScooterUnpackCSMAStatsRead(const uint8_t *pInBuf, sSCCSMAStats_t *pcsmaStats)
{
    sSCRspHeader_t *pRspHeader = (sSCRspHeader_t*)pInBuf;
    int i;
    uint32_t *pIntval = (uint32_t*)pcsmaStats;

    if(!pInBuf || !pcsmaStats) return -EINVAL;
    if(pRspHeader->dev != eSC_INTF_READ_CSMASTATS) return -SBT_EMISMATCH;

    pInBuf += sizeof(sSCRspHeader_t);

    for(i = 0; i < sizeof(sSCCSMAStats_t) / sizeof(uint32_t); i++)
    {
        pIntval[i] = (pInBuf[3] << 24) | (pInBuf[2] << 16) |
                     (pInBuf[1] << 8)  | (pInBuf[0] << 0);
        pInBuf += 4;
    }
    return 0;
}

/*!
 * Form a command for sending sidecar upgrade data
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 * @param address is the address to place the data at
 * @param nBytes is the number of bytes in the payload
 * @param pPayload is a pointer to the payload
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackSCUpgradeData(uint8_t *pOutbuf, uint32_t address,
                               uint32_t nBytes, uint8_t *pPayload)
{
    sSCCmdUpgrade_t *pCmd = (sSCCmdUpgrade_t*)pOutbuf;
    memset(pCmd, 0, sizeof(sSCCmdUpgrade_t));
    pCmd->dev = eSC_INTF_UPGRD_DATA;
    pCmd->data.address = htole32(address);
    pCmd->data.no_of_valid_bytes = htole32(nBytes);
    memcpy(pCmd->data.data_bytes, pPayload, nBytes);
    return sizeof(sSCCmdUpgrade_t);
}

/*!
 * Form a command for sending sidecar upgrade data
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large

 * @returns 1 which is the length of this payload
 *
 */
int32_t ScooterPackSCUpgradeCommand(uint8_t *pOutbuf)
{
    sSCCmdHeader_t *pHdr = (sSCCmdHeader_t*)pOutbuf;
    pHdr->dev = eSC_INTF_UPGRD_COMMIT;
    return 1;
}

/*!
 * Form a command for requesting sidecar comms stats
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackSCStatsRead(uint8_t *pOutbuf)
{
    if(!pOutbuf) return -EINVAL;

    *pOutbuf = eSC_INTF_READ_SCSTATS;
    return 1;
}

/*!
 * Form a command for requesting sidecar PLC stats
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackPLCStatsRead(uint8_t *pOutbuf)
{
    if(!pOutbuf) return -EINVAL;

    *pOutbuf = eSC_INTF_READ_STATS;
    return 1;
}

/*!
 * Form a command for requesting WattNode stats
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackWattnodeStatsRead(uint8_t *pOutbuf)
{
    if(!pOutbuf) return -EINVAL;

    *pOutbuf = eSC_INTF_READ_WNSTATS;
    return 1;
}

/*!
 * Form a command for clearing the SSN whitelist on sidecar
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackWhitelistClear(uint8_t *pOutbuf)
{
    if(!pOutbuf) return -EINVAL;

    *pOutbuf = eSC_INTF_CLEAR_WHITELIST;
    return 1;
}


/*!
 * Form a command for retrieving WattNode (CCS meter data) from sidecar
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackWattnodePoll(uint8_t *pOutbuf)
{
    sSCCmdHeader_t *pCmd = (sSCCmdHeader_t*)pOutbuf;
    if(!pOutbuf) return -EINVAL;

    pCmd->dev = ePLC_INTF_WNPOLL;

    return sizeof(*pCmd);
}

/*!
 * Form a command for setting filter options on sidecar
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 * @param pFilter is a pointer to filtering structure to pack into pOutbuf
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackFilterSet(uint8_t *pOutbuf, const sSC_filtering_options *pFilter)
{
    sSCCmdFilter_t *pCmd = (sSCCmdFilter_t*)pOutbuf;
    if(!pOutbuf) return -EINVAL;
    pCmd->dev = eSC_INTF_SET_FILTERING;
    pCmd->filter.filtering_options = le32toh(pFilter->filtering_options);
    return 1+sizeof(sSC_filtering_options);
}

/*!
 * Form a command for sending a whitelist of SSN to sidecar
 *
 * @param pOutbuf is a pointer to a buffer in which the sidecar packet will
 *        be placed.  It should be SCOOTER_MAX_PACKET large
 * @param pList is the whitelist structure to pack for transmitting
 *
 * @returns the length of valid bytes in pOutbuf or a value less than 0 to
 *          indicate error
 *
 */
int32_t ScooterPackWhitelistWrite(uint8_t *pOutbuf, const sSC_whitelist *pList)
{
    sSCCmdWhiteList_t *pCmd = (sSCCmdWhiteList_t*)pOutbuf;
    int i, len = 0;

    if(!pOutbuf) return -EINVAL;

    pCmd->dev = eSC_INTF_WRITE_WHITELIST;
    len += 1;

    pCmd->wl.num_whitelist_elements = le32toh(pList->num_whitelist_elements);
    len += 4;

    for(i = 0; i < pList->num_whitelist_elements; i++)
    {
        pCmd->wl.mi_short_serial_number[i] = le32toh(pList->mi_short_serial_number[i]);
        len +=4;
    }

    return len;
}

int32_t ScooterPackPLCStatsReset(uint8_t *pOutbuf)
{
    sSCCmdHeader_t *pCmd = (sSCCmdHeader_t*)pOutbuf;
    if(!pOutbuf) return -EINVAL;

    pCmd->dev = ePLC_INTF_RESET_STATS;
    return sizeof(*pCmd);
}

int32_t ScooterPackSCStatsReset(uint8_t *pOutbuf)
{
    sSCCmdHeader_t *pCmd = (sSCCmdHeader_t*)pOutbuf;
    if(!pOutbuf) return -EINVAL;

    pCmd->dev = ePLC_INTF_RESET_SCSTATS;
    return sizeof(*pCmd);
}

int32_t ScooterPackSCReset(uint8_t *pOutbuf)
{
    sSCCmdHeader_t *pCmd = (sSCCmdHeader_t*)pOutbuf;
    if(!pOutbuf) return -EINVAL;

    pCmd->dev = eSC_INTF_RESET;
    return sizeof(*pCmd);
}

int32_t ScooterPackWattnodeStatsReset(uint8_t *pOutbuf)
{
    sSCCmdHeader_t *pCmd = (sSCCmdHeader_t*)pOutbuf;
    if(!pOutbuf) return -EINVAL;

    pCmd->dev = ePLC_INTF_RESET_WNSTATS;
    return sizeof(*pCmd);
}

int32_t ScooterPackAnalogDataRead(uint8_t *pOutbuf)
{
    sSCCmdHeader_t *pCmd = (sSCCmdHeader_t*)pOutbuf;
    if(!pOutbuf) return -EINVAL;

    pCmd->dev = ePLC_INTF_ADATA;
    return sizeof(*pCmd);
}

int32_t ScooterPackGridDataRead(uint8_t *pOutbuf)
{
    sSCCmdHeader_t *pCmd = (sSCCmdHeader_t*)pOutbuf;
    if(!pOutbuf) return -EINVAL;

    pCmd->dev = ePLC_INTF_GRID_DATA;
    return sizeof(*pCmd);
}

int32_t ScooterPackConfig(uint8_t *pOutbuf, const sSCConfig_t *pCfg)
{
    sSCCmdConfig_t *pCmd = (sSCCmdConfig_t*)pOutbuf;
    uint8_t *pBuf;
    uint32_t *pui32 = (uint32_t*)pCfg;
    int i;

    //! @note:  This function relies on the fact that the entire config structure
    //! is uint32_t's.  If \ref sSCConfig_t changes then this function should
    //! change as well
    pCmd->h.dev = eSC_INTF_SET_CONFIG;
    pBuf = (uint8_t*)&pCmd->config;

    for(i = 0; i < sizeof(sSCConfig_t) / sizeof(uint32_t); i++)
    {
        pBuf[0] = (pui32[i] >> 0) & 0xff;
        pBuf[1] = (pui32[i] >> 8) & 0xff;
        pBuf[2] = (pui32[i] >> 16) & 0xff;
        pBuf[3] = (pui32[i] >> 24) & 0xff;
        pBuf += 4;
    }
    return sizeof(sSCCmdConfig_t);
}

int32_t ScooterPackCTConfig(uint8_t *pOutbuf, sWN_set_ct_configuration_t *pConfig)
{
    sSCCmdWNConfig_t *pCmd = (sSCCmdWNConfig_t *)pOutbuf;
    uint8_t *pBuf = (uint8_t*)&pCmd->cfg;
    uint32_t *pui32 = (uint32_t*)pConfig;

    int i;

    pCmd->dev = eSC_INTF_SET_WN_CT_CONFIG;

    for(i = 0; i < sizeof(pCmd->cfg) / sizeof(uint32_t); i++)
    {
        pBuf[0] = (pui32[i] >> 0) & 0xff;
        pBuf[1] = (pui32[i] >> 8) & 0xff;
        pBuf[2] = (pui32[i] >> 16) & 0xff;
        pBuf[3] = (pui32[i] >> 24) & 0xff;
        pBuf += 4;
    }
    return sizeof(sSCCmdWNConfig_t);
}

int32_t ScooterPackResetSidecar(uint8_t *pOutbuf)
{
    if(!pOutbuf) return -EINVAL;

    *pOutbuf = eSC_INTF_RESET;
    return 1;
}

int32_t ScooterPackFFTStart(uint8_t *pOutbuf)
{
    if(!pOutbuf) return -EINVAL;

    *pOutbuf = eSC_INTF_START_HF_FFT;
    return 1;
}

int32_t ScooterPackFFTRead(uint8_t *pOutbuf, uint8_t bin, uint8_t nSamples, uint16_t idxStart)
{
    // account for a bug in sidecar firmware where it aligns sSCCmdGetFFTData_t with payload
    // (which mis-places the "dev" field

    sSCCmdGetFFTData_t *pCmd = (sSCCmdGetFFTData_t *)(pOutbuf+1);
    if(!pOutbuf) return -EINVAL;

    *pOutbuf =  eSC_INTF_READ_HF_FFT;
    pCmd->dev = eSC_INTF_READ_HF_FFT;
    pCmd->bin = bin;
    pCmd->len = nSamples;
    pCmd->offset = htole16(idxStart);
    return sizeof(*pCmd)+1;
}

int32_t ScooterFFTRead(hScooter_t hScooter,
                         eSCFFTBIN_t bin, int16_t *pFFTSamples)
{
    uint8_t pCmd[16],pRsp[256];
    sSCRspGetFFTData_t *pFFT = (sSCRspGetFFTData_t*)pRsp;

    uint32_t i, j, n;
    int32_t rc;
    size_t txlen, rxlen;
    eSCLun_t lun;
    if(bin > SCFFTBIN_L2_LF) return -EINVAL;
    if(!pFFTSamples) return -EINVAL;
    memset(pRsp, 0, sizeof(pRsp));
    pFFT->len = -1;

    for(i = 0; i < SC_FFT_SAMPLE_COUNT; i += 90)
    {
        n = MIN(90, SC_FFT_SAMPLE_COUNT - i);
        txlen = ScooterPackFFTRead(pCmd, bin, n, i);
        rxlen = sizeof(pRsp);
        rc = ScooterExecute(hScooter, pCmd, txlen, pRsp, &rxlen, &lun);
        fprintf(stderr,"Reading at offset %d, len %d, rx'ed %d\n", i, n, pFFT->len);
        if(rc) return rc;
        // for(j = 0; j < n; j++)
        for(j = 0; j < pFFT->len; j++)
        {
            pFFTSamples[i + j] = le16toh(pFFT->data[j]);
        }
    }

    return 0;
}
/*!
 * Given a response buffer from an Echo response, do necessary un
 *
 * @param pInBuf is the response generated from the last transactions
 * @param pEcho is a pointer to a char array
 * @param inlen is the length of the response buffer
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackEcho(const uint8_t *pInBuf, uint8_t *pEcho, uint32_t inlen)
{
    sSCRspHeader_t *pRspHeader = (sSCRspHeader_t*)pInBuf;

    if(pRspHeader->dev != eSC_INTF_ECHO) return -SBT_EMISMATCH;

    memcpy(pEcho, pInBuf + 1, inlen);
    return 0;
}

/*!
 * Given a response buffer from a WattNode Poll, do necessary un
 *
 * @param pInBuf is the response generated from the last transactions
 * @param pWNData is a pointer to a sWN_data structure in which a decoded
 *        response (with correct endianess) will be placed
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackWattnodePoll(const uint8_t *pInBuf, sWN_data *pWNData)
{
    sSCRspHeader_t *pRspHeader = (sSCRspHeader_t*)pInBuf;
    int i;
    uint32_t *pIntval = (uint32_t*)pWNData;

    if(!pInBuf || !pWNData) return -EINVAL;
    if(pRspHeader->dev != ePLC_INTF_WNPOLL) return -SBT_EMISMATCH;

    pInBuf += sizeof(sSCRspHeader_t);

    for(i = 0; i < sizeof(sWN_data) / sizeof(uint32_t); i++)
    {
        pIntval[i] = (pInBuf[3] << 24) | (pInBuf[2] << 16) |
                     (pInBuf[1] << 8)  | (pInBuf[0] << 0);
        pInBuf += 4;
    }
    return 0;
}

/*!
 * Given a response buffer from a WattNode "fast power" report, unpack it
 *
 * @param pInBuf is the response generated from the last transactions
 * @param pWNFast is a pointer to a sWN_FAST_Power structure in which a decoded
 *        response (with correct endianess) will be placed
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackWattnodeFastPower(const uint8_t *pInBuf,
        sWN_FAST_Power *pWNFast)
{
    sSCRspHeader_t *pRspHeader = (sSCRspHeader_t*)pInBuf;
    union { float f; uint32_t ui32Tmp; } itof;
    // uint32_t ui32Tmp;

    if(!pInBuf || !pWNFast) return -EINVAL;
    if(pRspHeader->dev != eSC_INTF_WN_FAST_POWER) return -SBT_EMISMATCH;

    pInBuf += sizeof(sSCRspHeader_t);

    itof.ui32Tmp = (pInBuf[3] << 24) | (pInBuf[2] << 16) | (pInBuf[1] << 8)  | (pInBuf[0] << 0);
    pWNFast->powerA = itof.f;
    LOG_VERBOSE_LO("powerA %d (0x%08x) %f\n", itof.ui32Tmp, itof.ui32Tmp, pWNFast->powerA);
    pInBuf += 4;

    itof.ui32Tmp = (pInBuf[3] << 24) | (pInBuf[2] << 16) | (pInBuf[1] << 8)  | (pInBuf[0] << 0);
    pWNFast->powerB = itof.f;
    LOG_VERBOSE_LO("powerB %d (0x%08x) %f\n", itof.ui32Tmp, itof.ui32Tmp, pWNFast->powerB);
    pInBuf += 4;

    itof.ui32Tmp = (pInBuf[3] << 24) | (pInBuf[2] << 16) | (pInBuf[1] << 8)  | (pInBuf[0] << 0);
    pWNFast->powerC = itof.f;
    LOG_VERBOSE_LO("powerC %d (0x%08x) %f\n", itof.ui32Tmp, itof.ui32Tmp, pWNFast->powerC);
    pInBuf += 4;

    return 0;
}


/*!
 * Given a response buffer from a sidecar version, do necessary un
 *
 * @param pInBuf is the response generated from the last transactions
 * @param exp is the device/command we're expecting the ack from
 * @param pAck is a pointer to a \ref sSCAck_t structure to populate
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackAck(const uint8_t *pInBuf, eSC_INTF_CMD exp,
                           sSCAck_t *pAck)
{
    sSCRspAck_t *pRsp = (sSCRspAck_t *)pInBuf;

    if(!pInBuf || !pAck) return -EINVAL;

    if(pRsp->dev != exp) return -SBT_EMISMATCH;

    pAck->status = pRsp->ack.status;
    return 0;
}

int32_t ScooterUnpackWhitelistResponse(const uint8_t *pInBuf,
                                       sSC_SetWhitelistResponse *pWLRsp)
{
    sSC_RspWhiteListWrite_t *pRsp = (sSC_RspWhiteListWrite_t *)pInBuf;

    if(!pInBuf || !pWLRsp) return -EINVAL;

    if(pRsp->dev != eSC_INTF_WRITE_WHITELIST) return -SBT_EMISMATCH;

    pWLRsp->result = le32toh(pRsp->rsp.result);
    return 0;
}

/*!
 * Given a response buffer from a sidecar version, do necessary un
 *
 * @param pInBuf is the response generated from the last transactions
 * @param pVersion is a pointer to a \ref sSC_version structure to populate
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackVersion(const uint8_t *pInBuf, sSC_version *pVersion)
{
    sSCRspVersion_t *pResp = (sSCRspVersion_t *)pInBuf;
    sSC_revision_v1 *pRev1 = (sSC_revision_v1 *)&pResp->ver;
    sSC_version *pRev2 = (sSC_version *)&pResp->ver;

    if(pResp->dev != eSC_INTF_VERSION) return -SBT_EMISMATCH;

    pVersion->sc.v1.major = le16toh(pRev1->major);
    pVersion->sc.v1.build = le16toh(pRev1->build);
    pVersion->sc.v1.fwrevision = le32toh(pRev1->fwrevision);

    switch(pVersion->sc.v1.major)
    {
        case 1:
            return 0;

        case 2:
            pVersion->sc.v2.hwrevision = le32toh(pRev2->sc.v2.hwrevision);
            pVersion->sc.v2.short_serial = le32toh(pRev2->sc.v2.short_serial);
            memcpy(pVersion->sc.v2.long_serial, pRev2->sc.v2.long_serial,
                    sizeof(pRev2->sc.v2.long_serial));
            pVersion->wn.model = le32toh(pRev2->wn.model);
            pVersion->wn.fwrevision = le32toh(pRev2->wn.fwrevision);
            pVersion->wn.serialnumber = le32toh(pRev2->wn.serialnumber);
            return 0;

        default:
            return -SBT_EBADVALUE;
    }
}

/*!
 * Given a response buffer from a sidecar PLC stats read, unpack the fields
 *
 * @param pInBuf is the response generated from the last transactions
 * @param pPLCStats is a pointer to a \ref tPlcStatistics structure to populate
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackPLCStatsRead(const uint8_t *pInBuf, sSCPLCStats_t *pPLCStats)
{
    sSCRspPLCStats_t *pResp = (sSCRspPLCStats_t *)pInBuf;
    uint8_t *pData = (uint8_t*)&pResp->stats;

    if(pResp->dev != eSC_INTF_READ_STATS) return -SBT_EMISMATCH;

    pPLCStats->address_term_count = (pData[3] << 24) | (pData[2] << 16) |
                                    (pData[1] << 8) | (pData[0] << 0);
    pData += 4;

    pPLCStats->rx_myMessages = (pData[3] << 24) | (pData[2] << 16) |
                               (pData[1] << 8) | (pData[0] << 0);
    pData += 4;

    pPLCStats->tx_count = (pData[3] << 24) | (pData[2] << 16) |
                          (pData[1] << 8) | (pData[0] << 0);
    pData += 4;

    pPLCStats->len_errors = (pData[3] << 24) | (pData[2] << 16) |
                            (pData[1] << 8) | (pData[0] << 0);
    pData += 4;

    pPLCStats->crc_errors = (pData[1] << 8) | (pData[0] << 0);
    pData += 2;

    pPLCStats->max_trackers = pData[0];
    pData++;

    pPLCStats->rxAvgRSSI = pData[0];
    pData++;

    pPLCStats->rxMaxPeakRSSI = pData[0];
    pData++;

    pPLCStats->rxNoiseFloor = pData[0];
    pData++;

    pPLCStats->max_tracker_matched = pData[0];
    pData++;

    pPLCStats->groupsSlotsWithActivePLC = (pData[1] << 8) | (pData[0] << 0);
    pData += 2;

    return 0;
}

/*!
 * Given a response buffer from a sidecar analog data, parse and populate the
 * fields in pAdata
 *
 * @param pInBuf is the response generated from the last transactions
 * @param pAdata is a pointer to a \ref sSCAnalogData structure to populate
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackAnalogDataRead(const uint8_t *pInBuf, sSCAnalogData *pAdata)
{
    sSCRspAnalogData_t *pRsp = (sSCRspAnalogData_t*)pInBuf;
    uint8_t *pData = (uint8_t*)&pRsp->adata;
    uint32_t *pConv = (uint32_t*)pAdata;
    int i = 0;

    if(pRsp->dev != eSC_INTF_ADATA) return -SBT_EMISMATCH;

    // temperature
    pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
    pData += 4; i++;

    // L1 VRMS
    pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
    pData += 4; i++;

    // L2 VRMS
    pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
    pData += 4; i++;

    // CT1 RMS
    pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
    pData += 4; i++;

    // CT2 RMS
    pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
    pData += 4; i++;

    // CT3 RMS
    pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
    pData += 4; i++;

    return 0;
}

/*!
 * Given a response buffer from a sidecar grid data, parse and populate the
 * fields in pGdata
 *
 * @param pInBuf is the response generated from the last transactions
 * @param pGdata is a pointer to a \ref sSCGridData structure to populate
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackGridDataRead(const uint8_t *pInBuf, sSCGridData *pGdata)
{
    sSCRspGridData_t *pRsp = (sSCRspGridData_t*)pInBuf;
    uint8_t *pData = (uint8_t*)&pRsp->gdata;
    uint32_t *pConv = (uint32_t*)pGdata;
    int i = 0;
    int j = 0;

    if(pRsp->dev != eSC_INTF_GRID_DATA) return -SBT_EMISMATCH;

    for (j = 0; j < 5; j++)
    {

        // 0th harmonic
        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        // 1st harmonic
        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        // 3rd harmonic
        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        // 5th harmonic
        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        // 7th harmonic
        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        // 9th harmonic
        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;

        pConv[i] = (pData[3] << 24) | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4; i++;
    }

    return 0;
}


/*!
 * Given a response buffer from a sidecar stats read, unpack the fields
 *
 * @param pInBuf is the response generated from the last transactions
 * @param pSCStats is a pointer to a \ref sSC_stats structure to populate
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackSCStatsRead(const uint8_t *pInBuf, sSC_stats *pSCStats)
{
    sSCRspSCStats_t *pResp = (sSCRspSCStats_t *)pInBuf;
    sSC_comms_v2 *pv2comms;
    int i=2;
    uint8_t *pData = (uint8_t*)&pResp->stats;

    if(pResp->dev != eSC_INTF_READ_SCSTATS) return -SBT_EMISMATCH;

    pv2comms = &pSCStats->pvs.v2;
    while (i--)
    {
        pv2comms->ui32RxFrames = (pData[3] << 24) | (pData[2] << 16)
                                 | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32TxFrames = (pData[3] << 24) | (pData[2] << 16)
                                 | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32FramingErrors = (pData[3] << 24) | (pData[2] << 16)
                                      | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32CRCErrors = (pData[3] << 24) | (pData[2] << 16)
                                  | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32Overlap = (pData[3] << 24) | (pData[2] << 16)
                                | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32Unknown = (pData[3] << 24) | (pData[2] << 16)
                                | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32RawMode = (pData[3] << 24) | (pData[2] << 16)
                                | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32RxFramingErrors = (pData[3] << 24) | (pData[2] << 16)
                                        | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32RxOverrunErrors = (pData[3] << 24) | (pData[2] << 16)
                                        | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32RxBufferOverrunErrors = (pData[3] << 24)
            | (pData[2] << 16) | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;

        pv2comms->ui32TXOverlap = (pData[3] << 24) | (pData[2] << 16)
                                  | (pData[1] << 8) | (pData[0] << 0);
        pData += 4;
        pv2comms = &pSCStats->wn.v2;
    }

    pSCStats->ui32UptimeSecs = (pData[3] << 24) | (pData[2] << 16)
                                 | (pData[1] << 8) | (pData[0] << 0);

    return 0;
}


/*!
 * Given a response buffer from a wattnode stats read, unpack the fields
 *
 * @param pInBuf is the response generated from the last transactions
 * @param pWNstats is a pointer to a \ref sWN_stats structure to populate
 *
 * @returns 0 for success, < 0 for error
 *
 */
int32_t ScooterUnpackWattnodeStatsRead(const uint8_t *pInBuf,
                                           sWN_stats *pWNstats)
{
    sSCRspWNStats_t *pResp = (sSCRspWNStats_t *) pInBuf;
    uint8_t *pData = (uint8_t*) &pResp->stats;

    if (pResp->dev != eSC_INTF_READ_WNSTATS) return -SBT_EMISMATCH;

    pWNstats->uptimeSecs = (pData[3] << 24) | (pData[2] << 16) |
                            (pData[1] << 8) | (pData[0] << 0);
    pData += 4;

    pWNstats->crcErrorCount = (pData[3] << 24) | (pData[2] << 16)
                              | (pData[1] << 8) | (pData[0] << 0);
    pData += 4;

    pWNstats->frameErrorCount = (pData[3] << 24) | (pData[2] << 16)
                                | (pData[1] << 8) | (pData[0] << 0);
    pData += 4;

    pWNstats->packetErrorCount = (pData[3] << 24) | (pData[2] << 16)
                                 | (pData[1] << 8) | (pData[0] << 0);
    pData += 4;

    pWNstats->overrunCount = (pData[3] << 24) | (pData[2] << 16)
                             | (pData[1] << 8) | (pData[0] << 0);
    pData += 4;

    pWNstats->errorStatus = (pData[3] << 24) | (pData[2] << 16)
                             | (pData[1] << 8) | (pData[0] << 0);
    pData += 4;


    return 0;
}

int32_t ScooterUnpackConfig(const uint8_t *pInBuf, sSCConfig_t *pCfg)
{
    sSCRspConfig_t *pRsp = (sSCRspConfig_t*)pInBuf;
    uint8_t *pBuf = (uint8_t*)&pRsp->config;
    uint32_t *pui32 = (uint32_t*)pCfg;
    int i;

    //! @note:  This function relies on the fact that the entire config structure
    //! is uint32_t's.  If \ref sSCConfig_t changes then this function should
    //! change as well
    if(pRsp->dev != eSC_INTF_SET_CONFIG) return -SBT_EMISMATCH;

    for(i = 0; i < sizeof(sSCConfig_t) / sizeof(uint32_t); i++)
    {
        pui32[i] = (pBuf[3] << 24) | (pBuf[2] << 16) | (pBuf[1] << 8) | pBuf[0];
        pBuf += 4;
    }
    return 0;

}


/*!
 * @}
 */














//
// Given a command or response buffer, nest into to and return the type of
// command along with a pointer to the actual payload.  For a "version 1"
// command pRealCmd will come out equal to pBuf.  For a "version 2" command
// pRealCmd will be pCmd->payload.
//
static uint8_t getCommand(uint8_t *pBuf, uint8_t **pRealCmd)
{
    sSCCmdV2_t *pCmd = (sSCCmdV2_t *)pBuf;

    if(*pBuf == eSC_INTF_CMD_VERSION2)
    {
        return getCommand(pCmd->payload, pRealCmd);
    }

    if(pRealCmd) *pRealCmd = pBuf;
    return *pBuf;
}

//
// Read a scooter response (could be version 1 or version 2 response) using
// the caller-supplied buffer.  Handle any version-2-isms and then shift the
// payload into the beginning of caller-supplied buffer.
//
static int32_t readL2(hScooter_t handle, uint8_t *pRxBuf, size_t *rxbuflen, eSCLun_t *pLun,
                        uint32_t timeout)
{
    int32_t rc;

    rc = fifoRemove(handle->readfifo, pRxBuf, rxbuflen, timeout);
    if(rc != 0)
    {
    	LOG_ERROR("fifoRemove returned %d\n",rc);
    	return rc;
    }
//
//    rc = ConnectionReadL2(handle->hConn,pRxBuf,*rxbuflen,timeout);
//    if(rc != 0)
//    {
//    	LOG_ERROR("fifoRemove returned %d\n",rc);
//    	return rc;
//    }

    if(pRxBuf[0] == eSC_INTF_CMD_VERSION2)
    {
        sSCRspV2_t *pResp = (sSCRspV2_t*)pRxBuf;
        uint16_t flags;

        flags = pResp->header.flags;

        if(flags)
        {
            if(!handle->pfnCB)
            {
                LOG_WARNING("Incoming sidecar alerts but no registered callback to handle them!\n");
            }
            else
            {
                // call the provided callback
                handle->pfnCB(handle->cbcookie, le16toh(flags), 0, 0);
            }
        }
		if (pLun)
		{
			*pLun = pResp->header.lun;
		}
		*rxbuflen -= sizeof(*pResp);
        memmove(pRxBuf, pResp->payload, *rxbuflen);
    }
    else
    {
        // not much work to do here... just return the "version 1" payload up
        // to the caller
		if (pLun)
		{
			*pLun = SC_DEFAULT_LUN;
		}
    }

    return rc;
}

static uint32_t handleRead(hScooter_t handle, uint8_t *pTxbuf,
        uint8_t *pRxBuf, size_t *rxbuflen, eSCLun_t *pLun)
{
    int32_t rc;

    //
    // All we have to do is read back the response...
    //
    rc = readL2(handle, pRxBuf, rxbuflen, pLun, 2000);
    return rc;

}

static uint32_t handleWritePlc(hScooter_t handle, uint8_t *pTxbuf,
        uint8_t *pRxBuf, size_t *rxbuflen)
{
    sSCCmdWritePLC_t *pCmd = (sSCCmdWritePLC_t*)pTxbuf;

    int32_t rc;

    if ((pCmd->flags & PLC_FLAGS_SYNCHRONOUS_PLC) == 0)
    {
        LOG_VERBOSE_MED("Async PLC...\n");
        // no ack request.  nothing to do here...
        return 0;
    }

    // We could end up waiting for the ack for a long time.  We can release
    // the write lock since we won't actually be writing anyhthing after this
    pthread_mutex_unlock(&handle->writemutex);

    LOG_VERBOSE_MED("Waiting for PLC TX Ack\n");
    *rxbuflen = 2;

    // here we have to wait for the longest PLC packet possible to transmit
    // and then for a 6-7 byte response to come back @ 38.4K
    //
    do
    {
        acknak_t an;
        size_t rl;
        rl = sizeof(an);
        rc = fifoRemove(handle->ackfifo, (uint8_t*)&an, &rl, 6000);
        if(rc != 0)
        {
            LOG_WARNING("Error (%d) while waiting for TX ack\n",
                        rc);
            return rc;
        }
        
        if (an.ackdev == eSC_INTF_ACK) switch (an.reason)
        {
            case 0x00:
            case PLC_STATUS_OK:
                handle->bWaitingForAck = false;
                return 0;

            case PLC_STATUS_FAIL:
                handle->bWaitingForAck = false;
                return -SBT_EPLCERROR;

            case PLC_STATUS_BUSY:
                // this shouldn't be possible
                handle->bWaitingForAck = false;
                return -SBT_EPLCBUSY;

            default:
                handle->bWaitingForAck = false;
                return -SBT_EBADVALUE;
        }
        else if ((an.ackdev == eSC_INTF_NACK)) // && (an.reason == PLC_STATUS_STRETCH))
        {
            continue;
        }
        else
        {
            LOG_CRIT("Unexpected response dev/rsn = %d/%d\n", an.ackdev, an.reason);
            handle->bWaitingForAck = false;
            return -SBT_EBADVALUE;
        }
    } while(1);

    // we shouldn't get here...
    handle->bWaitingForAck = false;
    return -SBT_EBADVALUE;
}

#if defined SELFTEST
#include <stdio.h>
#include <string.h>

//! \addtogroup scooterapi_tests
//! @{

#define TEST(desc, call, test, rc, line) \
    do { \
        printf("%40.40s:", desc); \
        rc = call; \
        if (rc test) { \
            printf(" [PASS]\n"); \
        } else { \
            printf(" [FAIL] (line %d)\n", line); \
            bFail = true; \
        } \
    } while(0)

static int32_t metercb(void *pCookie, const uint8_t *pBuf)
{
    static const char* wnstrings[] = {
        "Power A",
        "Power B",
        "Power C",
        "Power (VARs) A",
        "Power (VARs) B",
        "Power (VARs) C",
        "Power (Apparent) A",
        "Power (Apparent) B",
        "Power (Apparent) C",
        "Line A voltage",
        "Line B voltage",
        "Line C voltage",
        "Current A",
        "Current B",
        "Current C",
        "Freq",

        "Energy A",
        "Energy B",
        "Energy C",
        "Energy + A",
        "Energy + B",
        "Energy - A",
        "Energy - B",
        "Energy Reac A",
        "Energy Reac B",
        "Energy Reac C",

        "Power Factor A",
        "Power Factor B",
        "Power Factor C",

        "Uptime",
        "Error Status",
        "CRC Error count",
        "Frame Error count",
        "Packet Error count",
        "Overrun count",
        "CT Directions",
        "CT Amps A",
        "CT Amps B",
        "CT Amps C"
    };
    sWN_data wndata;
    int j;
    float *pF = (float*)&wndata;
    uint32_t *pUI = (uint32_t*)&wndata;
    int32_t *pSI = (int32_t*)&wndata;

    ScooterUnpackWattnodePoll(pBuf, &wndata);
    for(j = 0; j < sizeof(wndata) / sizeof(uint32_t); j++)
    {
        double d;
        switch(j)
        {
            case 0: case 1: case 2: case 3:
            case 4: case 5: case 6: case 7:
            case 8: case 9: case 10: case 11:
            case 12: case 13: case 14: case 15:
            case 26: case 27: case 28:
                d = pF[j];
                LOG_INFO("%20.20s : %3.3f (0x%08x)\n", wnstrings[j], d, pUI[j]);
                break;

            case 16: case 17: case 18: case 19:
            case 20: case 21: case 22: case 23:
            case 24: case 25:
                d = (double)pSI[j] / 100.0;
                LOG_INFO("%20.20s : %3.3f (0x%08x)\n", wnstrings[j], d, pSI[j]);
                break;

            default:
                LOG_INFO("%20.20s : %d (0x%08x)\n", wnstrings[j], pUI[j], pUI[j]);
                break;

        }
    }


    return 0;
}

int main(int argc, char*argv[])
{
    connection_handle_t hConn;
    hScooter_t hScooter;
    int timeout;
    int32_t rc;
    bool bFail = false;

    LogInit(NULL, stderr);

    rc = ConnectionOpen("uart:/dev/ttyUSB3", &hConn);
    assert(rc == 0);

    timeout=15;
    // hold off on opening the scooter api.
    while(timeout--)
    {
        printf("ConnectionOpen %d\n", timeout);
        sleep(1);
    }
    rc = ScooterNew(&hScooter, hConn);
    assert(rc == 0);

    timeout=15;
    // hold off on registering the WN meter callback.
    while(timeout--)
    {
        printf("ScooterNew %d\n", timeout);
        sleep(1);
    }

    ScooterRegisterMeterCallback(hScooter, metercb, 0);

    timeout = 20;
    while(timeout--)
    {
        sleep(1);
    }

    ConnectionClose(hConn);
    return bFail;

}
//! @}
#endif


// Copyright (c) 2018 Enphase Energy, Inc. All rights reserved.
//
// This file contains reference encoder/decoder functions for
// use by the SunPower gateway interfacing to the Enphase Module
// Control Protocol message format.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>
#include "enphase/emuutil.h"

/* Table of CRC values for high�order byte */
static const uint16_t  modbusCrc[] = {
    0x0000, 0xc0c1, 0xc181, 0x0140,  0xc301, 0x03c0, 0x0280, 0xc241,
    0xc601, 0x06c0, 0x0780, 0xc741,  0x0500, 0xc5c1, 0xc481, 0x0440,
    0xcc01, 0x0cc0, 0x0d80, 0xcd41,  0x0f00, 0xcfc1, 0xce81, 0x0e40,
    0x0a00, 0xcac1, 0xcb81, 0x0b40,  0xc901, 0x09c0, 0x0880, 0xc841,
    0xd801, 0x18c0, 0x1980, 0xd941,  0x1b00, 0xdbc1, 0xda81, 0x1a40,
    0x1e00, 0xdec1, 0xdf81, 0x1f40,  0xdd01, 0x1dc0, 0x1c80, 0xdc41,
    0x1400, 0xd4c1, 0xd581, 0x1540,  0xd701, 0x17c0, 0x1680, 0xd641,
    0xd201, 0x12c0, 0x1380, 0xd341,  0x1100, 0xd1c1, 0xd081, 0x1040,
    0xf001, 0x30c0, 0x3180, 0xf141,  0x3300, 0xf3c1, 0xf281, 0x3240,
    0x3600, 0xf6c1, 0xf781, 0x3740,  0xf501, 0x35c0, 0x3480, 0xf441,
    0x3c00, 0xfcc1, 0xfd81, 0x3d40,  0xff01, 0x3fc0, 0x3e80, 0xfe41,
    0xfa01, 0x3ac0, 0x3b80, 0xfb41,  0x3900, 0xf9c1, 0xf881, 0x3840,
    0x2800, 0xe8c1, 0xe981, 0x2940,  0xeb01, 0x2bc0, 0x2a80, 0xea41,
    0xee01, 0x2ec0, 0x2f80, 0xef41,  0x2d00, 0xedc1, 0xec81, 0x2c40,
    0xe401, 0x24c0, 0x2580, 0xe541,  0x2700, 0xe7c1, 0xe681, 0x2640,
    0x2200, 0xe2c1, 0xe381, 0x2340,  0xe101, 0x21c0, 0x2080, 0xe041,
    0xa001, 0x60c0, 0x6180, 0xa141,  0x6300, 0xa3c1, 0xa281, 0x6240,
    0x6600, 0xa6c1, 0xa781, 0x6740,  0xa501, 0x65c0, 0x6480, 0xa441,
    0x6c00, 0xacc1, 0xad81, 0x6d40,  0xaf01, 0x6fc0, 0x6e80, 0xae41,
    0xaa01, 0x6ac0, 0x6b80, 0xab41,  0x6900, 0xa9c1, 0xa881, 0x6840,
    0x7800, 0xb8c1, 0xb981, 0x7940,  0xbb01, 0x7bc0, 0x7a80, 0xba41,
    0xbe01, 0x7ec0, 0x7f80, 0xbf41,  0x7d00, 0xbdc1, 0xbc81, 0x7c40,
    0xb401, 0x74c0, 0x7580, 0xb541,  0x7700, 0xb7c1, 0xb681, 0x7640,
    0x7200, 0xb2c1, 0xb381, 0x7340,  0xb101, 0x71c0, 0x7080, 0xb041,
    0x5000, 0x90c1, 0x9181, 0x5140,  0x9301, 0x53c0, 0x5280, 0x9241,
    0x9601, 0x56c0, 0x5780, 0x9741,  0x5500, 0x95c1, 0x9481, 0x5440,
    0x9c01, 0x5cc0, 0x5d80, 0x9d41,  0x5f00, 0x9fc1, 0x9e81, 0x5e40,
    0x5a00, 0x9ac1, 0x9b81, 0x5b40,  0x9901, 0x59c0, 0x5880, 0x9841,
    0x8801, 0x48c0, 0x4980, 0x8941,  0x4b00, 0x8bc1, 0x8a81, 0x4a40,
    0x4e00, 0x8ec1, 0x8f81, 0x4f40,  0x8d01, 0x4dc0, 0x4c80, 0x8c41,
    0x4400, 0x84c1, 0x8581, 0x4540,  0x8701, 0x47c0, 0x4680, 0x8641,
    0x8201, 0x42c0, 0x4380, 0x8341,  0x4100, 0x81c1, 0x8081, 0x4040
};


/* convert a character to int */
static uint8_t
emuCToI(int achar)
{
  uint8_t retVal = 0;

  if (isxdigit(achar)) {
      if ((achar >= '0') && (achar <= '9')) {
          retVal = achar - '0';
      } else if ((achar >= 'a') && (achar <= 'f')) {
          retVal = achar - 'a' + 10;
      } else if ((achar >= 'A') && (achar <= 'F')) {
          retVal = achar - 'A' + 10;
      }
  }
  return retVal;

}

int
emuutlBcdBytesToStr(unsigned char * bcd_ptr,
                    unsigned char bcd_len,
                    char * str_ptr,
                    char str_len) {
  int rc = 0;
  int i;

  if ((0 == bcd_ptr) || (0 == str_ptr)) {
    return -1; // invalid inputs
  }

  if (str_len < (bcd_len*2)) {
    return -2; // string buffer too small
  }

  memset(str_ptr, 0, str_len);
  for (i=0; i< bcd_len; i++) {
    unsigned char val = bcd_ptr[i];
    sprintf(&str_ptr[i*2],"%x", ((val>>4) & 0x0f));
    sprintf(&str_ptr[(i*2)+1],"%x", (val & 0x0f));
  }

  return rc;
}

int
emuutilStrToBcdBytes(const char * str_ptr,
                     char str_len,
                     unsigned char * bcd_ptr,
                     unsigned char bcd_len) {
  int rc = 0;
  int i;

  memset(bcd_ptr, 0, bcd_len);

  for (i=0; i<str_len; i++) {
    int bcd_offset = i/2;

    // check for output buffer too small
    if (bcd_offset > bcd_len) {
      rc = -1;
      break;
    }

    /* char to int conversion */
    unsigned char val = emuCToI(str_ptr[i]);

    /* pack nibbles */
    if ((i%2) == 0) {
      bcd_ptr[bcd_offset] |= (val & 0x0F) << 4 ;
    } else {
      bcd_ptr[bcd_offset] |= (val & 0x0F);
    }
  }

  return rc;
}

int
emuutilPartNumToStr(tPartNumber part_num,
                    char * str_ptr,
                    char str_len) {
  if (str_ptr == NULL) {
    // invalid inputs.
    return -1;
  }

  if (str_len < cPartNumInStrFmtLen) {
    // Insufficient output buffer space
    return -2;
  }

  if ((part_num.dMajor == 0) && (part_num.dMinor == 0) && (part_num.dMaint == 0)) {
      snprintf(str_ptr, str_len,
              "%02hhx%01hhx-"
              "%01hhx%02hhx%02hhx-"
              "r%02hhx",
              part_num.idb[0],
              (uint8_t)((part_num.idb[1] >> 4) & 0xf),
              (uint8_t)(part_num.idb[1] & 0xf),
              part_num.idb[2],
              part_num.idb[3],
              part_num.manuRev);
  } else {
      snprintf(str_ptr, str_len,
                "%02hhx%01hhx-"
                "%01hhx%02hhx%02hhx-"
                "r%02hhx-v%02hhx.%02hhx.%02hhx",
                part_num.idb[0],
                (uint8_t)((part_num.idb[1] >> 4) & 0xf),
                (uint8_t)(part_num.idb[1] & 0xf),
                part_num.idb[2],
                part_num.idb[3],
                part_num.manuRev,
                part_num.dMajor,
                part_num.dMinor,
                part_num.dMaint);
  }
  return 0;
}


int
emuutilStrToPartNum(const char * str_ptr,
                    tPartNumber * part_num) {
  int rc = 0;
  int len = (int) strlen(str_ptr);

  if ((len != 13) && (len != 23)) {
    // invalid str length.
    return -1;
  }

  if ((str_ptr[3] != '-') ||
      (str_ptr[9] != '-') ||
      (str_ptr[10] != 'r')) {
    // invalid short format
    return -2;
  }

  if (len == 23) {
    if ((str_ptr[13] != '-') ||
        (str_ptr[14] != 'v') ||
        (str_ptr[17] != '.') ||
        (str_ptr[20] != '.')) {
      // invalid long format.
      return -3;
    }
  }

  part_num->idb[0]  = ((emuCToI(str_ptr[0]) << 4) +  emuCToI(str_ptr[1]));
  part_num->idb[1]  = ((emuCToI(str_ptr[2]) << 4) +  emuCToI(str_ptr[4]));
  part_num->idb[2]  = ((emuCToI(str_ptr[5]) << 4) +  emuCToI(str_ptr[6]));
  part_num->idb[3]  = ((emuCToI(str_ptr[7]) << 4) +  emuCToI(str_ptr[8]));
  part_num->manuRev = ((emuCToI(str_ptr[11]) << 4) + emuCToI(str_ptr[12]));
  if (len == 23) {
    part_num->dMajor  = ((emuCToI(str_ptr[15]) << 4) + emuCToI(str_ptr[16]));
    part_num->dMinor  = ((emuCToI(str_ptr[18]) << 4) + emuCToI(str_ptr[19]));
    part_num->dMaint  = ((emuCToI(str_ptr[21]) << 4) + emuCToI(str_ptr[22]));
  } else {
    part_num->dMajor = part_num->dMinor = part_num->dMaint = 0;
  }

  return rc;
}


int
emuutilSerialNumToStr(tSerialNumber serial_num,
                      char        * str_ptr,
                      int           str_len) {

  if (str_ptr == NULL) {
    // Invalid params
    return -1;
  }

  if (str_len < cSerialNumStrLen) {
    // Insufficient string buffer length.
    return -2;
  }

  snprintf(str_ptr, str_len,
           "%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx",
           serial_num.byteArr[0], serial_num.byteArr[1],
           serial_num.byteArr[2], serial_num.byteArr[3],
           serial_num.byteArr[4], serial_num.byteArr[5]);
  return 0;
}


int
emumtilStrToSerialNum(const char * str_ptr,
                      tSerialNumber * serial_num) {
  char s[25];             // e.g. "000000000000123" for s/n "123"
  int i;                  // index into pSerial->byteArr[]
  const char * p, *q;     // begin and end ptrs into s[]
  bool all_zeros = true;  // until proven otherwise

  // for ease of subsequent conversion, ensure at least 12 digits
  sprintf(s, "000000000000%.12s", str_ptr);

  // count the number of digits; all else will be ignored
  for (p = s, q = &s[0] + 12; isdigit(*q); p++, q++);

  // convert to BCD
  for (i = 0; i < cSerialNumBytesLen; i++) {
      char d = (*p++ - '0') << 4;
      d |= *p++ - '0';
      all_zeros &= (d == 0x00);
      serial_num->byteArr[i] = (uint8_t)d;
  }

  // A blank serial number must start with 0xff
  if (all_zeros) {
      serial_num->byteArr[0] = 0xff;
  }

  return 0;
}


int
emuutlStrToAsicId(const char * str_ptr,
                  tAsicId * asic_id) {
  int bufIdx = 0;
  int i;

  memset(asic_id, 0, sizeof(*asic_id));

  for (i = 0; i < cAsicIdSize; i++) {
      asic_id->asicId[i] = (char) ((emuCToI(str_ptr[bufIdx]) << 4) + emuCToI(str_ptr[bufIdx+1]));
      bufIdx += 2;
  }
  return 0;
}

int
emuutlAsicIdToStr(tAsicId asic_id,
                  char * str_ptr,
                  int str_len) {

  int rc=0;
  rc = emuutlBcdBytesToStr(asic_id.asicId, sizeof(asic_id.asicId),
                           str_ptr, str_len);
  return rc;
}

/*! emuutlCrc16 - Compute CRC-16 as used in target devices. */
/** emuutlCrc16 - Compute CRC-16 as used in target devices.

    This function performs the same crc16 algorithm as used in downstream
    device.

    The CRC is started by first preloading a the 16-bit CRC register with the
    seed. Then a process begins of applying successive eight-bit bytes of data
    to the current contents of the register.

     During generation of the CRC, each eight-bit character is exclusive ORed
    with the register contents. Then the result is shifted in the direction of
    the least significant bit (LSB), with a zero filled into the most
    significant bit (MSB) position. The LSB is extracted and examined. If the
    LSB was a 1, the register is then exclusive ORed with a preset, fixed
    value. If the LSB was a 0, no exclusive OR takes place.

     This process is repeated until eight shifts have been performed. After
    the last (eighth) shift, the next eight-bit byte is exclusive ORed with
    the register's current value, and the process repeats for eight more
    shifts as described above. The final contents of the register, after all
    the bytes of the message have been applied, is the CRC value.

     Steps for generating the CRC-16 checksum:

     1. Polynomial: binary number 1010 0000 0000 0001, or A0 01 (hex).

     2. Load a 16-bit CRC register with initial value FF FF (hex).

     3. Exclusive OR the first data byte with the low-order byte of the 16-bit
    CRC register. Store the result in the 16-bit CRC register.

     4. Shift the 16-bit CRC register one bit to the right.

     5. If the bit shifted out to the right is one, Exclusive OR the 16-bit
    CRC register with the new polynomial, store the result in the 16-bit CRC
    registers. Return to step 4.

     6. If the bit shifted out to the right is zero, return to step 4.

     7. Repeat steps 4 and 5 until 8 shifts have been performed.

     8. Exclusive OR the next data byte with the 16-bit CRC register.

     9. Repeat steps 4 through 7 until all bytes of the frame are Exclusive
    Ored with the 16-bit register and shifted 8 times.

     10. The content of the 16-bit register is the checksum and is appended to
    the end of the frame.

     On the receiver side, handle the 16-bit CRC register as above and may
    look for correct CRC: a. process all the data up to the CRC word itself
    and compare internal CRC register to CRC word b. process the data up to
    and including the CRC, and the resulting CRC (ie: the remainder) should be
    0x0000.

    NOTE:  The implementation below uses a lookup table.

    @param [in] pData pointer to data to be summed.
    @param [in] dataLen length of data to be summed.
    @return (uint16_t) running sum.

*/
uint16_t
emuutlCrc16(void            * pData,
            size_t            dataLen)
{
    uint16_t                   tmp, sum = 0xffff;
    size_t                      i;

    /* emuutlCrc16 */

    for (i=0; i < dataLen; i++) {
        tmp = sum ^ (0x00ff & ((uint16_t) ((uint8_t *)pData)[i]));
        sum = (sum >> 8) ^ modbusCrc[tmp & 0xff];
    }
    return sum;
} /* END of emuutlCrc16 */

/*! emuutlCrc16Update - update for running crc16 sum. */
/** emuutlCrc16Update - update for running crc16 sum.

    For running calculation of sum.  On init sum should be 0xffff in
    input.

    @param [in] sum running sum, 0xffff on first call.
    @param [in] pData pointer to data to be summed.
    @param [in] dataLen length of data to be summed.
    @return (uint16_t) running sum.

*/
void
emuutlCrc16Update(uint16_t       * sum,
                  void            * pData,
                  u_int32_t         dataLen)
{
    uint8_t                    tmp;
    u_int32_t                   i;

    /* emuutlCrc16 */
    for (i=0; i < dataLen; i++) {
        tmp = (*sum ^ ((uint16_t)*(((uint8_t *)pData) + i))) & 0xff;
        *sum = ((uint16_t)(*sum >> 8)) ^ modbusCrc[tmp];
    }
} /* END of emuutlCrc16Update */

/*! emuutlCrc16TableGen - Code to generate crc16 table. */
/** emuutlCrc16TableGen - Code to generate crc16 table.

    This function is included for documentation purposes, but we don't
    call it.  This table is defined above in the modbusCrc table.
*/
void
emuutlCrc16TableGen(void)
{
    uint16_t                   crc16tbl[256];
    int                         i, j;
    uint16_t                   sum, c;

    /* emuutlCrc16TableGen */
    for (i=0; i < 256; i++) {
        sum = 0;
        c   = (uint16_t) i;

        for (j=0; j<8; j++) {
            if ((sum ^ c) & 0x0001) {
                sum = ( sum >> 1 ) ^ 0xa001;
            } else {
                sum =   sum >> 1;
            }
            c = c >> 1;
        }
        crc16tbl[i] = sum;
    }
    for (i=0; i < 256; i++) {
        if ( (i % 8) == 0 ) fprintf(stdout, "\n\t");
        fprintf(stdout, "0x%04x, ", crc16tbl[i]);
        if ( (i % 8) == 3 ) fprintf(stdout, " ");
    }
    fprintf(stdout, "\n");
} /* END of emuutlCrc16TableGen */

/*! emuutlCrcTblCheck - check crc table results. */
/** emuutlCrcTblCheck - check crc table results.

    Perform canned check on crc table and results.
    @return (int) ENP_ERROR or ENP_OK.
*/
int
emuutlCrcTblCheck(void)
{
    int                         retVal;
    uint16_t                   tsum1, tsum2, tsum3;
    uint8_t                    tval1[16] = {0x00, 0x01, 0x02, 0x03,
                                             0x04, 0x05, 0x06, 0x07,
                                             0x08, 0x09, 0x0a, 0x0b,
                                             0x0c, 0x0d, 0x0e, 0x0f};
    char                        tval2[] = "123456789";
    char                        tval3[] =
             "Lorem ipsum dolor sit amet, consectetur adipisicing elit, "
             "sed do eiusmod tempor incididunt ut labore et dolore magna "
             "aliqua. Ut enim ad minim veniam, quis nostrud exercitation "
             "ullamco laboris nisi ut aliquip ex ea commodo consequat. "
             "Duis aute irure dolor in reprehenderit in voluptate velit "
             "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint "
             "occaecat cupidatat non proident, sunt in culpa qui officia "
             "deserunt mollit anim id est laborum.";
    int                         i, len;

    /* emuutlCrcTblCheck */
    tsum1 = emuutlCrc16(tval1, sizeof(tval1));
    tsum2 = emuutlCrc16(tval2, strlen(tval2));

    // Use the "running sum" version of the computation.
    tsum3 = 0xffff;
    len = (int) strlen(tval3);
    for (i=0; i < len; i++) {
        emuutlCrc16Update(&tsum3,&tval3[i],1);
    }
    if ( (tsum1 != 0xe7b4) || (tsum2 != 0x4b37) || (tsum3 != 0x4abb) ) {
        retVal = ENP_ERROR;
    } else {
        retVal = ENP_OK;
    }
    return retVal;
} /* END of emuutlCrcTblCheck */


/*! emuutlMD5DigestStrToHash - convert md5 hash string to hash */
/** emuutlMD5DigestStrToHash - convert md5 hash string to hash

    Convert md5 hash string to hash.
*/
void
emuutlMD5DigestStrToHash(char       * hashStr,
                         uint8_t   * hash)
{
    char                      * achar = &hashStr[0];
    int                         i;
    int                         strLen = (cMD5DigestLen * 2);

    /* emuutlMD5DigestStrToHash */

    for (i=0; i < strLen; i+=2) {
        if ((isxdigit((int)*achar) && isxdigit(*(achar +1)))) {
            hash[i>>1] = (uint8_t)
                ((emuutlDigitToInt(*achar) << 4) +
                 (emuutlDigitToInt(*(achar+1))));
        } else {
            hash[0]  = 0xde; hash[1]  = 0xad;
            hash[2]  = 0xbe; hash[3]  = 0xef;
            hash[4]  = 0xde; hash[5]  = 0xad;
            hash[6]  = 0xbe; hash[7]  = 0xef;
            hash[8]  = 0xde; hash[9]  = 0xad;
            hash[10] = 0xbe; hash[11] = 0xef;
            hash[12] = 0xde; hash[13] = 0xad;
            hash[14] = 0xbe; hash[15] = 0xef;
            break;
        }
        achar+=2;
    }

} /* END of emuutlMD5DigestStrToHash */

/*! emuutlDigitToInt - a replacement for digittoint missing on ARM. */
/** emuutlDigitToInt - a replacement for digittoint missing on ARM.

*/
int
emuutlDigitToInt(int achar)
{
    int                         retVal = 0;

    /* emuutlDigitToInt */

    if (isxdigit(achar)) {
        if ((achar >= '0') && (achar <= '9')) {
            retVal = achar - '0';
        } else if ((achar >= 'a') && (achar <= 'f')) {
            retVal = achar - 'a' + 10;
        } else if ((achar >= 'A') && (achar <= 'F')) {
            retVal = achar - 'A' + 10;
        }
    }
    return retVal;

} /* END of emuutlDigitToInt */

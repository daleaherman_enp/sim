/* Copyright (c) 2006-2008 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _timer_h_
#define _timer_h_


/** @file timer.h
 *  @brief Timer Support
 *
 *  This file @c timer.h provides the Timer Support
 *
 */

/** @page timerHPg Header: timer.h Timer Support

@section timerHName timer.h HEADER NAME

    timer.h -- Timer Support

@section timerHSynop timer.h SYNOPSIS

    This file provides the EMU's Something.

@section timerHDesc timer.h DESCRIPTION

    This file, @c timer.h provides the Timer Support.

*/


/* system headers */
#include <stddef.h>
#include <stdlib.h>
#include "include/enphase.h"

/**
 * @defgroup timerHGroups timer Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines -------------------------- */

/**
 * @defgroup timerHDefines timer Header Defines
 */
/*@{*/

//#if defined(JACKDAWS) || defined(FEATURE_FAST_DISCOVERY)
#define NEED_GETTIMEOFDAY
//#endif /* if defined(JACKDAWS) || defined(FEATURE_FAST_DISCOVERY) */

typedef enum _eTimerUser
{
    cTimerUserFallback = 0,
    cTimerUserCommTest,
    cTimerUserRxCtx0,
    cTimerUserRxCtx1,
    cTimerUserDownLoad,
    cTimerUserDiscoverSquelch,
    cTimerUserNumberOf
} eTimerUser;

#define cTimerMaxTicks          0xffff
#define cTimerMaxTicksSec       (0xffff / 10)

#define cTimerOneHourSecs       3600
#define cTimerHalfDaySecs       43200
#define cTimerOneDaySecs        86400
#define cTimerWeekDaySecs       604800

// 10Hz (100.000 msec tick) - Always 100ms tick regardless of grid frequency: made sure of in procload.
#define cTimerUsecTick          100000
#define cTimerMsecPerTick       100
#define cTimerTicksPerSec       10

typedef char                    ttimerInt;
typedef unsigned char           ttimerUInt;
typedef unsigned int            ttimerId;
typedef unsigned char           ttimerBool;
#if 0
struct  timespec {
    int32_t                     tv_sec;
    int32_t                     tv_nsec;
};

typedef struct timespec         ttimerSpec;

typedef struct timeval {
    int32_t                     tv_sec;
    int32_t                     tv_usec;
};
typedef struct timeval          ttimerVal;
#endif
typedef u_int16_t               ttimerTicks;

/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs ------------------------- */

/**
 * @defgroup timerHTypedefs timer Header Typedefs
 */
/*@{*/


typedef void (* ttimerCbFp) (u_int8_t timerId);

/*@}*/  /* END of timer Header Typedef Group */

/* ------------------------------------ Extern Declarations -------------- */

/**
 * @defgroup timerHExtdata timer Header Extern Data Definitions
 */
/*@{*/


/*@}*/  /* END of timer Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined -------------- */

/**
 * @defgroup timerHGlobdata timer Header Global Data Defined
 */
/*@{*/

/*@}*/  /* END of timer Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined -------------- */

/**
 * @defgroup timerHStatdata timer Header Static Data Defined
 */
/*@{*/
/*@}*/  /* END of timer Header Static Data Defined Group */

/* ------------------------------------ Prototypes ----------------------- */

/**
 * @defgroup timerHProto timer Header Prototypes
 */
/*@{*/

#if 0

/*@}*/  /* END of timer Header Prototypes Group */

/*@}*/  /* END timer Header Definitions and Declarations */

#if defined(NEED_GETTIMEOFDAY)
/** gettimeofday - gettimeofday for keil environment. */
extern ttimerInt
//gettimeofday(ttimerVal _keil_xdata * tv);
#endif /* if defined(NEED_GETTIMEOFDAY) */

/** timerPeriodicTick - handle the period timer tick. */
//void
//timerPeriodicTick(void);

/** timerStart - renew an existing timer. */
extern bool
timerStart(u_int8_t                     userId,
           ttimerTicks                  ticks,
           ttimerBool                   autoRenew);

/** timerStop - stop an existing timer. */
extern void
timerStop(u_int8_t          userId);

/** timerInit - timer module initialization. */
extern u_int8_t
timerInit(void);
#endif

#endif /* _timer_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ----------------------------------------------------------------------- */

/* END of timer.h */



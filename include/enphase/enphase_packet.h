/*!
 * @copyright
 * Copyright 2018-2019, SunPower Corporation
 * All rights reserved.
 *
 * @file enphase_packet.h
 * Enphase Packet CoDec Function Definitions.
 *
 * @date July 5, 2018
 * @author
 */

#ifndef __PVSLIB_ENPHASE_PACKET_H
#define __PVSLIB_ENPHASE_PACKET_H

#include <sys/types.h>

/*!
 * \addtogroup enphase_packet_api
 * @{
 */

#define ENPHASE_MAX_PACKET 768

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _tIntervalLongData
{
    unsigned long long joulesProduced;
    unsigned long long joulesUsed;
    unsigned long long sumLeadingVAr;
    unsigned long long sumLaggingVAr;
} tIntervalLongData;

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif

/******************************************************************************
 * scooterProto.h
 *
 * This file defines all the protocol definitions between the host (PVS5 for
 * example) and the Sidecar (the PLC bridge).
 *
 * Copyright (c) 2015 Solar Bridge Technologies.  All rights reserved.T
 *
 * Created on: Jan 5, 2015
 *
 * Author: Miles Bintz <mbintz@sunpower.com>
 *         Dale Herman <dale.herman@sunpowercorp.com>
 *         Dayo Lawal <dayo.lawal@sunpowercorp.com>
 *
 ****************************************************************************/

#ifndef _SCOOTERPROTO_H_
#define _SCOOTERPROTO_H_

#include <stdint.h>
typedef uint64_t systime_t;
/*!
 * \addtogroup sidecar_proto Sidecar Protocol Definition
 * @{
 */

// all structures should be packed (no padding) otherwise it is unlikely that
// the structures will maintain the same size/shape across platforms.
#pragma pack(push, 1)

//
//! Enumerations of the various Version 1 Sidecar/PLC dev-board commands.
//! Everyone one of the following commands was implemented during the sidecar
//! version 1 protocol epoch.  All Version 1 command/response behavior can be
//! encapsulated in version 2 (Command 0xAA) with Logical Unit 1.
//
typedef enum
{
    //! NULL Command should simply respond with ACK OK.                     <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_NULL_COMMAND = 0,
    //! @todo remove references to ePLC_INTF*
    ePLC_INTF_NULL_COMMAND = eSC_INTF_NULL_COMMAND,

    //! Write to PLC transceiver.                                           <br>
    //! Command Format: \ref sSCCmdWritePLC_t                               <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_WRITE_PLC = 1,
    ePLC_INTF_WRITE_TX = eSC_INTF_WRITE_PLC,

    //! Read from PLC transceiver.                                          <br>
    //! Command Format: Not a command.  This is an async response.          <br>
    //! Response Format:  \ref sSCRspReadPLC_t                              <br>
    eSC_INTF_READ_PLC = 2,

    // ePLC_INTF_LOCAL_MODBUS,  //! Write to internal Modbus units
    // ePLC_INTF_EXTERN_MODBUS, //! Write to external (RS-485/RS-232) Modbus
    // units

    //! Send an echo request to sidecar.                                    <br>
    //! Command Format: \ref sSCCmdPing_t                                   <br>
    //! Response Format: \ref sSCRspPing_t                                  <br>
    eSC_INTF_ECHO  = 4,
    ePLC_INTF_ECHO = eSC_INTF_ECHO,

    //! Send an upgrade payload to sidecar.                                 <br>
    //! Command format: \ref sSCCmdUpgrade_t                                <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    //! Where dev = eSC_INTF_UPGRD_STAT, status is one of \ref
    //! sSCUpgradeStatus_t
    eSC_INTF_UPGRD_DATA  = 5,
    ePLC_INTF_UPGRD_DATA = eSC_INTF_UPGRD_DATA,

    //! Tell sidecar to commit to the upgrade data that was just transferred.
    //! <br> Command format: \ref eSCCmdHeader_t <br> Response Format: \ref
    //! eSCRspAck_t                                   <br> Where dev =
    //! eSC_INTF_UPGRD_STAT, status is one of \ref eSCUpgradeStatus_t
    eSC_INTF_UPGRD_COMMIT  = 6,
    ePLC_INTF_UPGRD_COMMIT = eSC_INTF_UPGRD_COMMIT,

    //! A sidecar device type indicating response is to an.
    //! \ref eSC_INTF_UPGRD_DATA command                                    <br>
    //! Command Format:  Not a command                                      <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_UPGRD_STAT  = 7,
    ePLC_INTF_UPGRD_STAT = eSC_INTF_UPGRD_STAT,

    //! Ask sidecar to repond with version information.                     <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspVersion_t                               <br>
    eSC_INTF_VERSION  = 8,
    ePLC_INTF_VERSION = eSC_INTF_VERSION,

    // ePLC_INTF_SEND_RX = 0x40,
    // ePLC_INTF_SET_RX_GAIN,
    // ePLC_INTF_SET_TX_AMPLITUDE,

    //! Reset PLC counts/stats.                                             <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_RESET_STATS  = 0x43,
    ePLC_INTF_RESET_STATS = eSC_INTF_RESET_STATS,

    //! Read PLC counts/stats from sidecar                                  <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspPLCStats_t                              <br>
    eSC_INTF_READ_STATS  = 0x44,
    ePLC_INTF_READ_STATS = eSC_INTF_READ_STATS,

    //! A sidecar device type indicating a response is to a side command    <br>
    //! Command Format: Not a command                                       <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_ACK  = 0x45,
    ePLC_INTF_ACK = eSC_INTF_ACK,

    //! A sidecar device type indicating a response is to a side command    <br>
    //! Command Format: Not a command                                       <br>
    //! Response Format: \ref sSCRspNack_t <br>
    eSC_INTF_NACK  = 0x46,
    ePLC_INTF_NACK = eSC_INTF_NACK,

    //! Reset the entire sidecar / plc-dev board (reset the PSoC)           <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_RESET  = 0x47,
    ePLC_INTF_RESET = eSC_INTF_RESET,

    //! Reset sidecar's  stats/counts that track UART comms and other
    //! system events                                                       <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_RESET_SCSTATS  = 0x48,
    ePLC_INTF_RESET_SCSTATS = eSC_INTF_RESET_SCSTATS,

    //! Command to read  stats/counts that track UART comms and other system
    //! events                                                              <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspSCStats_t                               <br>
    eSC_INTF_READ_SCSTATS  = 0x49,
    ePLC_INTF_READ_SCSTATS = eSC_INTF_READ_SCSTATS,

    //! Set the sidecar configuration options (options to configure the sidecar
    //! settings)                                                           <br>
    //! Command Format: \ref sSCCmdConfig_t                                 <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_SET_CONFIG  = 0x4a,
    ePLC_INTF_SET_CONFIG = eSC_INTF_SET_CONFIG,

    //! Read analog data from the sidecar                                   <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspAnalogData_t                            <br>
    eSC_INTF_ADATA  = 0x4b,
    ePLC_INTF_ADATA = eSC_INTF_ADATA,

    //! Send the sidecar a whitelist of MI serial numbers to listen to      <br>
    //! Command Format: \ref sSCCmdWhiteList_t                              <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_WRITE_WHITELIST  = 0x4c,
    ePLC_INTF_WRITE_WHITELIST = eSC_INTF_WRITE_WHITELIST,

    //! Clear the whitelist                                                 <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_CLEAR_WHITELIST  = 0x4d,
    ePLC_INTF_CLEAR_WHITELIST = eSC_INTF_CLEAR_WHITELIST,

    //! Set the bitmask indicating which types of packet filters are enabled
    //! <br> Command Format: \ref sSCCmdFilter_t <br> Response Format: \ref
    //! sSCRspAck_t                                   <br>
    eSC_INTF_SET_FILTERING  = 0x4e,
    ePLC_INTF_SET_FILTERING = eSC_INTF_SET_FILTERING,

    //! Reset the sticky status flags according to a bitmask                <br>
    //! Command Format: \ref sSCCmdResetStatusFlags_t                       <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_RESET_STATUS_FLAGS  = 0x4f,
    ePLC_INTF_RESET_STATUS_FLAGS = eSC_INTF_RESET_STATUS_FLAGS,

    //! interrogate the watt-node module for comms and diagnostic stats     <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspWNStats_t                               <br>
    eSC_INTF_READ_WNSTATS  = 0x60,
    ePLC_INTF_READ_WNSTATS = eSC_INTF_READ_WNSTATS,

    //! zero out any stats that can be zero'ed                              <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspAck_t                                   <br>
    eSC_INTF_RESET_WNSTATS  = 0x61,
    ePLC_INTF_RESET_WNSTATS = eSC_INTF_RESET_WNSTATS,

    //! read the watt-node metering data                                    <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspWNData_t                                <br>
    eSC_INTF_WNPOLL  = 0x62,
    ePLC_INTF_WNPOLL = eSC_INTF_WNPOLL,

    //! perform grid analysis                                               <br>
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspGridData_t                              <br>
    eSC_INTF_GRID_DATA  = 0x63,
    ePLC_INTF_GRID_DATA = eSC_INTF_GRID_DATA,
    //! modify the watt-node CT configuration                              <br>
    //! Command Format: \ref sSCCmdWNConfig_t                    <br>
    //! Response Format: \ref sSCRspAck_t                                  <br>
    eSC_INTF_SET_WN_CT_CONFIG  = 0x64,
    ePLC_INTF_SET_WN_CT_CONFIG = eSC_INTF_SET_WN_CT_CONFIG,
    //! modify the SC-PVS UART Divider                                     <br>
    //! Command Format: \ref sSCCmdUartDivider_t                           <br>
    //! Response Format: \ref sSCRspAck_t                                  <br>
    eSC_INTF_SET_UART_DIVIDER  = 0x65,
    ePLC_INTF_SET_UART_DIVIDER = eSC_INTF_SET_UART_DIVIDER,

    //! interrogate the csma for comms and diagnostic stats                <br>
    //! Command Format: \ref sSCCmdHeader_t                                <br>
    //! Response Format: \ref sSCRspCSMAStats_t                            <br>
    eSC_INTF_READ_CSMASTATS  = 0x66,
    ePLC_INTF_READ_CSMASTATS = eSC_INTF_READ_CSMASTATS,

    eSC_INTF_READ_HISTOGRAM  = 0x67,
    ePLC_INTF_READ_HISTOGRAM = eSC_INTF_READ_HISTOGRAM,

    //! start the FFT                <br>
    //!                                         <br>
    //!                                     <br>
    eSC_INTF_START_HF_FFT  = 0x68,
    ePLC_INTF_START_HF_FFT = eSC_INTF_START_HF_FFT,
    //! read the FFT                   <br>
    //!                                         <br>
    //!                                     <br>
    eSC_INTF_READ_HF_FFT   = 0x69,
    ePLC_INTF_READ_HF_FFT  = eSC_INTF_READ_HF_FFT,
    eSC_INTF_WN_FAST_POWER = 0x6A,

    //*************************************************************************
    //
    //! Command Format: \ref sSCCmdHeader_t                                 <br>
    //! Response Format: \ref sSCRspE_ASIC_RSSI_t                           <br>
    eSC_INTF_GET_E_ASIC_RSSI  = 0x6B,
    ePLC_INTF_GET_E_ASIC_RSSI = eSC_INTF_GET_E_ASIC_RSSI,

    eSC_INTF_E_ASIC_SET_CONFIG  = 0x6C,
    ePLC_INTF_E_ASIC_SET_CONFIG = eSC_INTF_E_ASIC_SET_CONFIG,

    //*************************************************************************
    //
    eSC_TESTINTF_NULL_COMMAND = 0x80,
    eSC_TESTINTF_READ_INTERNAL_VOLTAGES,
    eSC_TESTINTF_GET_VERSION,            // get FW/HW version information
    eSC_TESTINTF_READ_AC_LINE_VOLTAGES,  // PSOC will measure RMS voltage on
                                         // L1/L2 and report values
    eSC_TESTINTF_READ_CT_VOLTAGES,   // PSOC will measure RMS voltage on ALL CT
                                     // inputs and report values - should do
                                     // individually to make sure no xtalk.
    eSC_TESTINTF_PLC_LOOPBACK_TEST,  // sends PLC command and checks for
                                     // successful message recieved and RSSI
    eSC_TESTINTF_PLC_ENABLE_TEST,  // disables the PLC_N_WRITE_ENABLE and sends
                                   // PLC command, RSSI should measure low and
                                   // no message received.
    eSC_TESTINTF_PLC_TX_CAP_TEST,  // do not enable the TX CAP, should detect
                                   // change in RSSI ???
    eSC_TESTINTF_WRITE_TEST_DATA,  // write the reserved 28 bytes to the test
                                   // segment sSC_EEPROM_TestData
    eSC_TESTINTF_READ_TEST_DATA,   // read the 28 bytes from the test segment
    eSC_TESTINTF_SET_LED,  // set the two lEDS according to sSC_led_interface
    eSC_TESTINTF_SUPERVISORY_TEST,  // stops the watchdog, should generate
                                    // reset, set alert in status
    eSC_TESTINTF_RESET_STATUS,      // reset the status word in order to test
                                    // watchdog/supervisor
    eSC_TESTINTF_READ_RTS,          // returns current value of RTS in
                                    // sSC_cts_rts_interface
    eSC_TESTINTF_WRITE_CTS,         // sets the CTS to 0,1 based on
                                    // sSC_cts_rts_interface
    eSC_TESTINTF_READ_STATUS,       // return the status word
    eSC_TESTINTF_RESET,
    eSC_TESTINTF_WRITE_BOOT_DATA,
    eSC_TESTINTF_READ_BOOT_DATA,
    eSC_TESTINTF_WRITE_CAL_DATA,
    eSC_TESTINTF_READ_CAL_DATA,
    eSC_TESTINTF_SET_SERIAL,
    eSC_TESTINTF_READ_SERIAL,
    //
    //*************************************************************************

    //! Encapsulate a V1 command within a V2 command, enabling the sidecar to
    //! respond with a V2 response which sends back alert information       <br>
    //! Command Format: \ref sSCCmdV2_t (Version 1 command is V2 payload) <br>
    //! Response Format: \ref sSCRspV2_t (Version 1 response is V2 payload) <br>
    eSC_INTF_CMD_VERSION2 = 0xAA,
    eSC_INTF_CMD_VERSION  = eSC_INTF_CMD_VERSION2,
} ePLC_INTF_CMD,
    eSC_INTF_CMD;

//
//! A request to transmit PLC can result in one of the following outcomes
//
typedef enum
{
    //! The PLC message was transmitted successfully.  We use a non-zero value
    //! to lower the chances of false positives
    PLC_STATUS_OK = 0x80,

    //! We failed to transmit the message for an unknown reason
    PLC_STATUS_FAIL = 0x81,

    //! @todo The PLC channel remained busy for too long.  If CSMA times out
    //! then the message will be delivered anyway but if there is enough
    //! energy in the band then chances are low that it will be delivered
    //! successfully
    PLC_STATUS_BUSY = 0x82,

    //! The PLC transceiver is still waiting for the band to become free.
    PLC_STATUS_STRETCH = 0x83,
} eSCPLCStatus_t;

//
//! Enumeration to specify the logical unit (LUN)
//
typedef enum
{
    //! The default LUN for sidecar.
    SC_DEFAULT_LUN = 0x01,

    //! UART communication bus
    SC_UART_LUN = 0x02,

    //! PLC communication bus
    SC_PLC_BUS_LUN = 0x03,

    //! SC manufacturing protocol
    SC_MANUFACTURING_TEST_LUN = 0x05,
    SC_E_ASIC_PLC             = 0x06,

    //! E-ASIC management functions (e.g., firmware upgrade, version request,
    //! reboot, etc.)
    SC_E_ASIC_MGMT = 0x07

} eSCLun_t;

//
//! If this bit is set it indicates that the sidecar has reset since this bit
//! was last cleared.  This bit can be cleared with the
//! \ref eSC_INTF_RESET_STATUS_FLAGS command
//
#define SSCCMDHEADERV2_FLAGS_RESET 0x0001
//
//! This bit indicates that, per the metering module, more energy is being
//! produced than is being consumed.  This bit can only be cleared by reducing
//! production to a level less than or equal to consumption.
//
#define SSCCMDHEADERV2_FLAGS_EXPORT 0x0002
//
//! This bit indicates whether the PLC transmitter is busy or free.  The PLC
//! transmitter can only hold one message at a time.  This bit replaces
//! the original TXACK functionality.  Any V2 command's response can be used
//! to poll this bit.  PLC transactions should only be sent when this bit is
//! cleared.   This bit clears itself once sidecar is available to accept
//
#define SCCMDHEADERV2_FLAGS_PLCTXBUSY 0x0004
//
//! This bit indicates whether CSMA is actively holding off a PLC transmission
//! This should not be confused with CSMA enabled.  (Obviously it can't be
//! active unless it is enabled.)
//
#define SCCMDHEADERV2_FLAGS_CSMAACTIVE 0x0008
#define SCCMDHEADERV2_FLAGS_METERDATA_RDY 0x0010
#define SCCMDHEADERV2_FLAGS_METERDATA_ERR_COMM 0x0020
#define SCCMDHEADERV2_FLAGS_METERDATA_ERR_EEP 0x0040
#define SCCMDHEADERV2_FLAGS_ECHO_RQST 0x8000
//*****************************************************************************
//
// Command header definitions
//
//*****************************************************************************

//
//! All sidecar commands have a device/command as the first byte transmitted.
//! All sidecar responses are also initiated with a device indicating the type
//! of response
//
typedef struct
{
    //! The Type of command or response being transmitted.  See \ref
    //! eSC_INTF_CMD
    uint8_t dev;
} sSCCmdHeader_t, sSCRspHeader_t;

#define SCCMDHEADERV2_SEQUENCE_START (0x80)
#define SCCMDHEADERV2_SEQUENCE_START_MASK (0x80)
#define SCCMDHEADERV2_SEQUENCE_CONT (0x40)
#define SCCMDHEADERV2_SEQUENCE_PACKETS_LEFT_MASK (0x0F)
//
//! Sidecar can encapsulate version 1 commands in the version 2 behavior enabled
//! with \ref eSC_INTF_CMD_VERSION2.  This is the version 2 header.
//
typedef struct
{
    //! This is the same byte position as the version 1 device/command byte.
    //! The value for the version 2 device is \ref eSC_INTF_CMD_VERSION2
    uint8_t dev;

    //! Logical unit selector.  LUN1 implements the the sidecar version 1
    //! protocol
    uint8_t lun;

    //! Flags can toggle binary settings within the sidecar.  @todo determine
    //! whether a flag sticks or is set only for the duration of this one
    //! command-response
    uint16_t flags;
    uint8_t sequence;
    uint8_t packetSize;

} sSCCmdHeaderV2_t;

typedef struct
{
    //! should always be set to \ref eSC_INTF_RESET_STATUS_FLAGS
    sSCCmdHeader_t header;

    //! which flags to reset
    uint32_t resetFlagsMask;

} sSCCmdResetStatusFlags_t;
//
//! When sidecar is responding to a version 2 command it sends back this header
//! before the version 2 payload
//
typedef struct
{
    //! The response should come from the same device as the command.  In this
    //! case that means \ref eSC_INTF_CMD_VERSION2
    uint8_t dev;

    //! Response LUN should be the same LUN used in \ref sSCCmdHeaderV2_t
    uint8_t lun;

    //! If the sidecar has any events that need immediate servicing then it
    //! should set flags indicating as much.  See \ref ScooterCallbackSet() to
    //! understand where these alerts are processed
    uint32_t alerts;

} sSCRspHeaderV2_t;
typedef struct
{
    //! All commands and responses have a header
    sSCCmdHeaderV2_t v2_header;
    sSCRspHeader_t header;
    uint16_t rssi_idle;
    uint16_t rssi_msg;

} sSCRspE_ASIC_RSSI_t;

//
//! An Ack/NAck payload indicates success or the cause of failure for the last
//! command
typedef struct
{
    //! One of \ref eSCPLCStatus_t
    uint8_t status;
} sSCAck_t, sSCNAck_t;

//
//! Every command should respond with either a command-specific response OR
//! this generic response.
//
typedef struct
{
    //! All version 1 responses should respond with \ref eSC_INTF_ACK as the
    //! device
    uint8_t dev;

    //! The \ref sSCAck_t payload
    sSCAck_t ack;
} sSCRspAck_t;

//
//! The generic response for a NAck
//
typedef struct
{
    //! All version 1 responses should respond with \ref eSC_INTF_NACK as the
    //! device
    uint8_t dev;

    //! The \ref sSCNAck_t payload
    sSCNAck_t nack;
} sSCRspNAck_t;

//
//! The generic form of a version 1 command/response
//
typedef struct
{
    //! All commands and responses have a header (the device/command byte)
    sSCRspHeader_t header;

    //! And a payload that varies depending on which type of command/response it
    //! was
    uint8_t payload[];
} sSCRsp_t, sSCCmd_t;

//
//! The generic form a version 2 command/response.  Both command and response
//! headers have the same size/shape with the flags field (for command) changing
//! to an alerts field (for response)
//
typedef struct
{
    //! All commands and responses have a header
    sSCCmdHeaderV2_t header;

    //! And a payload that varies in length depending on the type of command
    uint8_t payload[];
} sSCRspV2_t, sSCCmdV2_t;

//*****************************************************************************
//
// PLC Write Command
//
//*****************************************************************************

// clang-format off
//! The PLC packets are transmitted with no acknowledgement/response from
//! the transeiver
//
#define PLC_FLAGS_SILENT                0x00
//
//! When transmitting a command to the PLC transciever, request an ACK when the
//! payload is finished transmitting.
//
#define PLC_FLAGS_SYNCHRONOUS_PLC       0x01
//
//! Request an Ack in the form of \ref sSCRspAck_t to indicate that the payload
//! was accepted for PLC transmission.  It does NOT indicate that the PLC
//! transmission is complete.  It ONLY indicates that the sidecar accepted the
//! command.  Use this mode in conjunction with
//! \ref sSCRspV2_t responses and checking for the flag
//! \ref SCCMDHEADERV2_FLAGS_PLCTXBUSY
//
#define PLC_FLAGS_ACKCMD                0x02
//
//! Request an ?? in the form of \ref ??_t to indicate that a message was not
//! detected in the JAM window.  Use this mode in conjunction with
//! \ref
//! \ref SCCMDHEADERV2_FLAGS_???

#define PLC_FLAGS_QUICK_NO_MESSAGE      0x04
#define PLC_FLAGS_RSVD3                 0x08
#define PLC_FLAGS_RSVD4                 0x10
#define PLC_FLAGS_RSVD5                 0x20
#define PLC_FLAGS_RSVD6                 0x40
#define PLC_FLAGS_RSVD7                 0x80
// clang-format on

//
//! Send a command to the PLC transceiver
//
typedef struct
{
    //! Sidecar command header
    sSCCmdHeader_t h;

    //! One or more of \ref PLC_FLAGS_RQACK
    uint8_t flags;

    //! A variable lenght payload
    uint8_t payload[];
} sSCCmdWritePLC_t;
typedef struct
{
    //! All commands and responses have a header
    sSCCmdHeaderV2_t header;
    uint32_t encrypt;

} sSCCmdE_ASIC_ENCRPYT_t;

//
//! Error definitions
//! PLC_RESPONSE_ERROR_NONE        0x00
//!     No error
//
#define PLC_RESPONSE_ERROR_NONE 0x00
//
//! PLC_RESPONSE_ERROR_NO_MESSAGE  0x01
//!     No message during Unicast mode was seen in expected timeframe
//!     Requires PLC_FLAGS_QUICK_NO_MESSAGE to be sent with \ref
//!     sSCCmdWritePLC_t
//
#define PLC_RESPONSE_ERROR_NO_MESSAGE 0x01

//
//! The PLC transceiver has sent a PLC payload in this format.  When allocated
//! this buffer should be able to hold an entire max-sized COBS frame (though
//! the actual incoming message may be smaller than that).  If error is
//! \ref PLC_RESPONSE_ERROR_NO_MESSAGE then the contents of payload are
//! undefined.
//
typedef struct
{
    //! Sidecar response header
    sSCRspHeader_t h;

    //! now a possible error flag see \ref PLC_RESPONSE_ERROR_NO_MESSAGE
    uint8_t error;

    //! A variable length payload
    uint8_t payload[];
} sSCRspReadPLC_t;

//*****************************************************************************
//
// Set various sidecar operating modes
//
//*****************************************************************************

// clang-format off
//! Option to enable/disable PLC RAW mode \ref sSCConfig_t
#define OPT_RAW_MODE                        0x00000001

//! Option to enable/disable loopback mode \ref sSCConfig_t
#define OPT_PLC_LOOPBACK_MODE               0x00000002

//! Option to enable/disable CSMA \ref sSCConfig_t
#define OPT_CSMA_ENABLED                    0x00000004

//! Option to set CSMA NACK response interval \ref sSCConfig_t
#define OPT_CSMA_NACK_INTERVAL              0x00000008

//! Option to set CSMA level \ref sSCConfig_t
#define OPT_CSMA_LEVEL                      0x00000010

//! Option to set the first slot time of the CSMA free window \ref sSCConfig_t

#define OPT_CSMA_NUM_BACKOFF_SLOTS          0x00000020

//! Option to set the CSMA free window width \ref sSCConfig_t
#define OPT_CSMA_TIME_FOR_CHANNEL_FREE      0x00000040

//! Option to set the CSMA slot interval \ref sSCConfig_t
#define OPT_CSMA_MAX_WAIT_TIME              0x00000080
#define OPT_CSMA_BACKOFF_TIME_PER_SLOT      0x00000100

//! Option to enable/disable GroupJAM \ref sSCConfig_t
#define OPT_GROUPJAM_ENABLED                0x00000200
#define OPT_GROUPJAM_START_DETECT_OFFSET    0x00000400
#define OPT_GROUPJAM_DETECTION_WINDOW_LENGTH 0x00000800
#define OPT_GROUPJAM_SLACK_WIDTH            0x00001000

//! Option to set the PLC transmit level  \ref sSCConfig_t
#define OPT_PLC_TRANSMIT_LEVEL              0x00002000

//! Option to set the UART Baud  \ref sSCConfig_t
#define OPT_UART_BAUD_DIVISOR               0x00004000
#define OPT_CLEAR_METER_EEPROM              0x00008000
#define OPT_SET_CT_SCALING                  0x00010000

#define OPT_QNR_START_DETECT_OFFSET         0x00020000
#define OPT_QNR_DETECTION_WINDOW_LENGTH     0x00040000

//! Option to enable/disable PLC test mode \ref sSCConfig_t
#define OPT_PLC_TEST_MODE                   0x00080000

//! Option to set histogram dBmV level \ref sSCConfig_t
#define OPT_SET_HIST_LEVEL                  0x00100000
#define OPT_SET_WN_ASYNC                    0x00200000

#define WN_ASYNC_POLL_OFF                   0x00000000
#define WN_ASYNC_POLL_SLOW                  0x00000001
#define WN_ASYNC_POLL_FAST_POWER            0x00000002


#define E_ASIC_OPT_PLC_HW_ENCRYPT    0x00000001
#define E_ASIC_OPT_PLC_PLCCFG_MODE   0x00000002
#define E_ASIC_OPT_PLC_ACKSEC_MODE   0x00000004
#define E_ASIC_OPT_DOMAIN_FILTER     0x00000008
#define E_ASIC_OPT_SET_DOMAIN        0x00000010
#define E_ASIC_OPT_EMU_FILTER        0x00000020
#define E_ASIC_OPT_TX_STRENGTH       0x00000040
#define E_ASIC_OPT_L2_ENABLE         0x00000080
#define E_ASIC_OPT_EQ8_ENABLE        0x00000100
#define E_ASIC_OPT_PLAN              0x00000200
#define E_ASIC_OPT_BAUD              0x00000400
#define E_ASIC_OPT_PLC_MODULATION    0x00000800
#define E_ASIC_OPT_PLC_PREAMBLE      0x00001000
#define E_ASIC_OPT_PLC_REED          0x00002000
#define E_ASIC_OPT_PLC_CHANNEL       0x00004000
#define E_ASIC_OPT_IQ8_ENABLE        0x00008000
#define E_ASIC_OPT_PLC_SQUELCH       0x00010000


#define E_ASIC_OPT_SET_SN            0x00020000
#define E_ASIC_OPT_CURRENT_CONFIG_FLAGS (0x00FFFFF)

#if 0
#define cPmiDefaultTxGainDb               -2 // Tx gain in -dB
#define cPmiDefaultSquelchLevel           74 // 74 dBuV
#define cPmiChannelNumOf 3

typedef enum  {
	cPmiModemTypeAurora = 0,
	cPmiModemTypeCorona,
	cPmiModemTypeNumOf
}ePmiModemType;

/*! reed-solomon setting */
typedef enum  {
    cPmiReedSolomon3o4 = 0,
    cPmiReedSolomon1o2 = 1,
    cPmiReedSolomon1o3 = 2,
    cPmiReedSolomonNumOf
} ePmiReedSolomon;

/*! modulation scheme */
typedef enum  {
    cPmiModScheme4MSK,
    cPmiModSchemeFSK,
    cPmiModSchemeNFSK,
    cPmiModScheme2MSK,
    cPmiModSchemeNumOf
} ePmiModScheme;

/*! channnel plan */
typedef enum  {
  cPmiChanPlan103_123_143KHz, // formerly known as NA
  cPmiChanPlan103_110_116KHz, // formerly known as EU
  cPmiChanPlan90_110KHz, // new international channel plan
  cPmiChanPlanNumOf
} ePmiChanPlan;

/*! modem baud rate */
typedef enum {
    cPmiBaud5K,
    cPmiBaud20K,
    cPmiBaudNumOf
} ePmiBaud;

/*! encryption type */
typedef enum   {
    cPmiCryptTypeNone = 0,
    cPmiCryptTypeSw,
    cPmiCryptTypeHw,
    cPmiCryptTypeNumOf
} ePmiCryptType;

/*! encryption key handling */
typedef enum   {
    cPmiCryptKeyTypeNone = 0,
    cPmiCryptKeyTypePreShared,
    cPmiCryptKeyTypeExchange,
    cPmiCryptKeyTypeNumOf
} ePmiCryptKeyType;

typedef enum  {
  cPreambleType64,
  cPreambleType108,
  cPreambleType153,
  cPreambleTypeNumOf
} ePmiPreambleType;
#endif

typedef enum
{
	ePLCCFGModeOff,
	ePLCCFGModeDetected,
	ePLCCFGModeParanoid
}eE_ASICPLCCFGMode;
typedef enum
{
	eACKSECModeOff,
	eACKSECModeOff_1,
	eACKSECModeParanoid,
}eE_ASICACKSECMode;

typedef struct {
	uint32_t flags;
	uint32_t plcHWEncryptEnabled;
	uint32_t plcPLCCFGMode;
	uint32_t plcACKSECMode;
	uint32_t plcDomainFilterEnabled;
	uint32_t plcEmuFilterEnabled;
	uint8_t domain[6];
	int32_t plcTXSignalStrength;
	uint32_t plcL2IsEnabled;
	uint32_t plcEQ8ModeEnabled;
	uint32_t plcIQ8ModeEnabled;
	uint8_t plcPlan; // ePmiChanPlan
	uint8_t plcBaud; // ePmiBaud
	uint8_t plcModulationScheme; // ePmiModScheme
	uint8_t plcPreamble; //ePmiPreambleType
	uint8_t plcReedSolomon; // ePmiReedSolomon
	uint8_t plcChannel;
	uint8_t plcSquelchDbuv;
	uint8_t usmSerial[6];
} sE_ASIC_Config;

typedef struct
{
    sSCCmdHeaderV2_t header;
    uint8_t cmd;
    sE_ASIC_Config config;
} sE_ASIC_SetConfig;

//
//! Enumeration to specify the mode
//
typedef enum
{
    //! disable raw mode
    PLC_CLR_RAW_MODE = 0,

    //! enable raw mode
    //! Raw mode enables the sidecar to pass all PLC data to the UART regardless
    //! of whether it was a well-framed PLC packet
    PLC_SET_RAW_MODE,

} eSC_MODE_COMMANDS,
    ePLC_MODE_COMMANDS;

//
//! Enumeration to enable/disable PLC loopback mode
//
typedef enum
{
    //! disable loopback
    PLC_DISABLE_LOOPBACK_MODE = 0,

    //! enable loopback
    //! Direct the PLC transceiver to loop back on itself.
    PLC_ENABLE_LOOPBACK_MODE,

} eSC_PLC_LOOPBACK_COMMANDS,
    ePLC_PLC_LOOPBACK_COMMANDS;

//
//! command structure for configuring sidecar
//! @note See \ref ScooterPackConfig and \ref ScooterUnpackConfig
//
typedef struct
{
    //! Bit field to determine which configuration option to set            <br>
    uint32_t flags;

    //! For \ref eSC_INTF_SET_CONFIG mode is one of \ref eSC_MODE_COMMANDS <br>
    uint32_t mode;

    //! For \ref eSC_INTF_SET_CONFIG mode is one of \ref
    //! eSC_PLC_LOOPBACK_COMMANDS   <br>
    uint32_t plc_loopback_enabled;

    //! CSMA CONFIGURATION

    //! Determines if CSMA/CA is enabled.                    <br>
    uint32_t csma_enabled;

    //! Sets time between NACK responses during CSMA/CA backoff. <br>
    uint32_t csma_nack_interval;

    //! Sets the CSMA/CA level in dBmV                  <br>
    uint32_t csma_level;

    //! Sets the number of timeslots used for backoff. Each slot is \ref
    //! backoff_time_per_slot long. <br>
    uint32_t csma_num_backoff_slots;

    //! Sets the CSMA/CA time that BIU must be low for channel to be free in
    //! 250usec increments. <br>
    uint32_t csma_time_for_channel_free;

    //! Sets the CSMA/CA maximum wait time for the channel to be free 250usec.
    //! Afterwards, just go. This is set quite high by default 20s (4 * 20 *
    //! 1000) to account for long searches during other systems discovery.
    //! Nominally, this could/should be set to under 2 seconds if a nearby
    //! discovery is not expected<br>
    uint32_t csma_max_wait_time;
    //! Sets the window time for each backoff slot, needs to be long enough to
    //! get new RSSI 250usec  <br>
    uint32_t csma_backoff_time_per_slot;

    //! GROUPJAM Configuration
    //! Determines if GroupJAM is enabled.                    <br>
    uint32_t groupjam_enabled;
    //! Time from the end of the transmit when SC will start looking for an MI
    //! response <br>
    uint32_t groupjam_start_detect_offset;
    //! Length of time from the \ref groupjam_start_detect_offset that the
    //! incoming RSSI will be evaluated before deciding that an incoming message
    //! is not going to occur. <br>
    uint32_t groupjam_detection_window_length;
    //! SC will JAM SEARCH/'Group with Limit' from the end of \ref
    //! groupjam_detection_window_length for the remainder of the \ref
    //! timeslot_multiplier_10msec minus the \ref groupjam_slack_width rounded
    //! to the nearest byte period (1/2400 * 8)
    uint32_t groupjam_slack_width;

    uint32_t plc_uiTransmitLevelHi;  // scaled to x*100000
    uint32_t plc_uiTransmitLevelLo;
    uint32_t uart_baudDivisor;          // baud is 480000000/6/brd
    uint32_t production_wn_ct_scaling;  // float gain * 100000

    //! Quick no response configuration
    //!
    //! Sets the first slot time of the quick no response window in 250usec
    //! increments. <br>
    uint32_t qnr_start_detect_offset;  // when to start average

    //! Sets the quick no response window width in 250usec increments. <br>
    uint32_t qnr_detection_window_length;  // how many to sample over

    //! Enables or disables Sidecars's PLC channel test mode
    uint32_t plc_testmode_enabled;

    //! Sets the histogram collection level in dBmV                  <br>
    uint32_t hist_level;

    uint32_t wn_async_mode;
} sSCConfig_t;

//
//! command for configuring sidecar.
//! @note See \ref ScooterPackConfig and \ref ScooterUnpackConfig
//
typedef struct
{
    //! Sidecar command header
    sSCCmdHeader_t h;

    //! Set configuration structure
    sSCConfig_t config;

} sSCCmdConfig_t;

//
//! Sidecar configuration response packet
//! @note See \ref ScooterPackConfig and \ref ScooterUnpackConfig
//
typedef struct
{
    //! Set configuration device
    uint8_t dev;

    //! Set configuration structure
    sSCConfig_t config;
} sSCRspConfig_t;

#define CSMA_MAX_SLOTS 10
typedef struct
{
    uint32_t csmaNumTimesHadToWait;
    uint32_t csmaNumTimesSlotSelected[CSMA_MAX_SLOTS];
    uint32_t csmaNumTimesSlotLost[CSMA_MAX_SLOTS];
    uint32_t csmaNumTimesSlotWon[CSMA_MAX_SLOTS];
    uint32_t csmaNumTimesWaitedMaxTime;
    uint32_t csmaAvgWaitTime;
    uint32_t csmaMinWaitTime;
    uint32_t csmaMaxWaitTime;
} sSCCSMAStats_t;

typedef struct
{
    uint8_t dev;
    sSCCSMAStats_t csmaStats;
} sSCRspCSMAStats_t;
//*****************************************************************************
//
// Echo requests
//
//*****************************************************************************

// 254 = max cobs, 1 = device/command, 1 = len, 2 = crc
#define SCMAXPING (254 - 1 - 1 - 2)

//
//! Packet for sending an echo request to sidecar
//
typedef struct
{
    //! The length of the data in pBuf
    uint8_t len;

    //! variable length buffer containing echo data
    uint8_t pBuf[];
} sSCCmdPing_t;

//
//! Packet for receiving an echo request from sidecar
//
typedef struct
{
    //! The length of the data in pBuf
    uint8_t len;

    //! variable length buffer containing echo data
    uint8_t pBuf[];
} sSCRspPing_t;

//*****************************************************************************
//
// Upgrade commands
//
//*****************************************************************************

//! Enumeration of the different types of failures that can be encountered
//! during sidecar upgrade
typedef enum
{
    //! No error
    UPGRD_STATS_OK = 0,

    //! frame error (invalid message length)
    UPGRD_ERROR_FRAME,

    //! non-consecutive addresses
    UPGRD_ERROR_ADDR_GAP,

    //! the address is outside of the valid range (too high)
    UPGRD_ERROR_SIZE,

    // internal PSoC interface error
    UPGRD_ERROR_PSOC,

    //! FLASH write error
    UPGRD_ERROR_FLASH,

    //! FLASH write error
    UPGRD_ERROR_FLASH_CMP,

    //! image file verification error
    UPGRD_ERROR_CRC32,

    //! EEPROM read or write error
    UPGRD_ERROR_EEPROM
} eSCUpgradeStatus_t,
    ePLC_UPGRD_STATUS;

//! sidecar upgrade payload
typedef struct
{
    //! offset of this 128 bytes of upgrade data
    uint32_t address;

    //! Number of valid bytes in this upgrade frame
    uint32_t no_of_valid_bytes;

    //! buffer of upgrade data
    uint8_t data_bytes[128];
} sSCUpgradeData_t;

//! command format for sidecar upgrade
typedef struct
{
    //! device should be \ref eSC_INTF_UPGRD_DATA
    uint8_t dev;

    //! the payload defined in \ref sSCUpgradeData_t
    sSCUpgradeData_t data;
} sSCCmdUpgrade_t;

//*****************************************************************************
//
// Version querying
//
//*****************************************************************************

//! Legacy version structure -- as reported by PLC Dev board
typedef struct
{
    //! The build number as generated by bamboo
    uint16_t build;

    //! Major is 1 for PLC dev board and 2 for sidecar
    uint16_t major;

    //! The SVN version of the source
    uint32_t fwrevision;
} sSC_revision_v1;

//! This SC Revision structure is only available on version 2+ boards (ie. on
//! sidecars but not PLC dev boards)
typedef struct
{
    //! The build number as generated by bamboo (same as in \ref
    //! sSC_revision_v1)
    uint16_t build;

    //! Major is 2 for sidecar boards
    uint16_t major;

    //! The SVN version of the source (same as in \ref sSC_revision_v1)
    uint32_t fwrevision;

    //! The hardware revision of the sidecar board
    uint32_t hwrevision;

    //! Sidecars have long serial numbers just like MIs, actually, potentially a
    //! bit longer...
    uint8_t long_serial[24];

    //! Sidecars also have short serial numbers
    uint32_t short_serial;
} sSC_revision_v2;

//! The universal version stucture
typedef union
{
    //! Contains the legacy version of the sidecar revision response
    sSC_revision_v1 v1;

    //! Also contains the new version of the sidecar revision response
    sSC_revision_v2 v2;
} sSC_revision;

//! Watt-Node revision structure.  This data is only available on sidecar
//! (not on PLC dev boards).  See the WattNode register spec to further
//! understand what each of these fields are.
typedef struct
{
    //! WattNode serial number
    uint32_t serialnumber;

    //! WattNode model number (Should be 400 decimal for sidecar's internal
    //! unit)
    uint32_t model;

    //! WattNode firmware version number
    uint32_t fwrevision;

} sWN_revision;

//! Version 2 of the sidecar version structure is a superset of both the Sidecar
//! version and the integrated wattnode version
typedef struct
{
    //! Sidecar version structure as defined in \ref sSC_revision
    sSC_revision sc;

    //! WattNode version structure as defined in \ref sWN_revision
    sWN_revision wn;
} sSC_version;

//! A version response has a dev/command header followed by the version payload
typedef struct
{
    //! Version response should have a device of be \ref eSC_INTF_VERSION
    uint8_t dev;

    //! Version response as defined in \ref sSC_version
    sSC_version ver;
} sSCRspVersion_t;

//*****************************************************************************
//
// Stats gathering
//
//*****************************************************************************

//! Early versions of sidecar firmware reported the following UART / system
//! statistics
typedef struct
{
    //! The number of valid COBS frames received
    uint32_t ui32RxFrames;

    //! The number of COBS frames transmitted
    uint32_t ui32TxFrames;

    //! The number of COBS framing errors (bytes received exceeds max COBS
    //! payload)
    uint32_t ui32FramingErrors;

    //! CRC error across COBS frame
    uint32_t ui32CRCErrors;

    //! The number of times we attempt to start a PLC transmit while one is
    //! already in flight
    uint32_t ui32Overlap;

    //! @todo write me
    uint32_t ui32Unknown;

    //! @todo write me
    uint32_t ui32RawMode;
} sSC_comms_v1;

//! enhanced version of the communication stats which can handle the same
//! kind of semantics that the wattnode reports.
typedef struct
{
    //! Same as in \ref sSC_comms_v1
    uint32_t ui32RxFrames;

    //! Same as in \ref sSC_comms_v1
    uint32_t ui32TxFrames;

    //! Same as in \ref sSC_comms_v1
    uint32_t ui32FramingErrors;

    //! Same as in \ref sSC_comms_v1
    uint32_t ui32CRCErrors;

    //! Same as in \ref sSC_comms_v1
    uint32_t ui32Overlap;

    //! Same as in \ref sSC_comms_v1
    uint32_t ui32Unknown;

    //! Same as in \ref sSC_comms_v1
    uint32_t ui32RawMode;

    //! @todo write me
    uint32_t ui32RxFramingErrors;

    //! @todo write me
    uint32_t ui32RxOverrunErrors;

    //! @todo write me
    uint32_t ui32RxBufferOverrunErrors;

    //! @todo write me
    uint32_t ui32TXOverlap;
} sSC_comms_v2;

//! Take the larger of the two structures when requesting comms info since
//! we don't know which rev of sidecar we're talking to
typedef union
{
    //! storage for early comms stats
    sSC_comms_v1 v1;

    //! storage for current comms stats
    sSC_comms_v2 v2;
} sSC_comms;

//! Sidecar stats include both communication statistics with the PVS host (via
//! UART) and with the sidecar (via MODBUS)
typedef struct
{
    //! host communication stats
    sSC_comms pvs;

    //! WattNode (modbus slave) communication stats
    sSC_comms wn;
    sSC_comms e_asic;
    //! Sidecar uptime in seconds
    uint32_t ui32UptimeSecs;
} sSC_stats;

//! a stats response includes a device and paylaod
typedef struct
{
    //! device should be \ref eSC_INTF_READ_SCSTATS when querying sidecar stats
    uint8_t dev;

    //! \ref sSC_stats payload
    sSC_stats stats;
} sSCRspSCStats_t;

#if 0
//! PLC stats for the PLC transceiver
typedef struct {
    //! @todo write me
    uint32_t address_term_count;

    //! @todo write me
    uint32_t rx_myMessages;

    //! @todo write me
    uint32_t tx_count;

    //! @todo write me
    uint32_t len_errors;


    //! @todo write me
    uint16_t crc_errors;

    //! @todo write me
    uint8_t  max_trackers;

    //! @todo write me
    uint8_t rxAvgRSSI;

    //! @todo write me
    uint8_t rxMaxPeakRSSI;

    //! @todo write me
    uint8_t max_tracker_matched;
    uint16_t groupsSlotsWithActivePLC;
} tPlcStatistics;
#endif

//! PLC stats for the PLC transceiver
typedef struct
{
    //! @todo write me
    uint32_t address_term_count;

    //! @todo write me
    uint32_t rx_myMessages;

    //! @todo write me
    uint32_t tx_count;

    //! @todo write me
    uint32_t len_errors;

    //! @todo write me
    uint16_t crc_errors;

    //! @todo write me
    uint8_t max_trackers;

    //! @todo write me
    uint8_t rxAvgRSSI;

    //! @todo write me
    uint8_t rxMaxPeakRSSI;

    //! @todo write me
    uint8_t rxNoiseFloor;

    //! @todo write me
    uint8_t max_tracker_matched;
    uint16_t groupsSlotsWithActivePLC;
} sSCPLCStats_t;

//! @todo write me
typedef struct
{
    //! @todo write me
    uint8_t dev;

    //! @todo write me
    sSCPLCStats_t stats;
} sSCRspPLCStats_t;

//! Diagnostic information as reported by the internal watt-node module
typedef struct
{
    //! This 32 bit long integer counts the number of seconds the meter has
    //! been running since the last power failure or reset. Resets can be
    //! caused by power brownouts or severe errors.
    uint32_t uptimeSecs;

    //! The following four registers report communication error counts. Each
    //! register counts up to 32767 and stops. All four of these registers are
    //! reset to zero whenever power is cycled or by writing zero to any of
    //! them.
    //! The number of Modbus packets with an invalid CRC (cyclic redundancy
    //! check).
    uint32_t crcErrorCount;

    //! the number of Modbus packets with framing errors. A framing error can
    //! indicate bad baud rate, bad parity setting, RS-485 noise or
    //! interference, or an RS-485 bus collision. (which shouldn't happen on a
    //! closed/private RS-485 bus)
    uint32_t frameErrorCount;

    //! The number of Modbus packets that could not be parsed.
    uint32_t packetErrorCount;

    //! The number of times the Modbus input buffer has been overrun. The
    //! buffer is 256 bytes and normal requests are less than 80 bytes, so an
    //! overrun normally indicates non-Modbus traffic on the RS-485 bus or
    //! severe noise on the bus.
    uint32_t overrunCount;

    //! Report invalid modbus requests or other invalid operations.
    uint32_t errorStatus;
} sWN_stats;

//! Response to a \ref eSC_INTF_READ_WNSTATS command
typedef struct
{
    //! should be \ref eSC_INTF_READ_WNSTATS
    uint8_t dev;

    //! wattnote stats payload as defined in \ref sWN_stats
    sWN_stats stats;
} sSCRspWNStats_t;

typedef struct
{
    uint8_t dev;
    uint8_t lun;
    uint32_t flags;
    uint8_t payload[];
} sSC_cmd_v2_t;

typedef struct
{
    float fTemperature;
    float fAC_L1_Vrms;
    float fAC_L2_Vrms;
    float fCT_1_Vrms;
    float fCT_2_Vrms;
    float fCT_3_Vrms;
} sSCAnalogData;

typedef struct
{
    uint8_t dev;
    sSCAnalogData adata;
} sSCRspAnalogData_t;

typedef struct
{
    float a;
    float b;
} sSCHarmonic_t;

typedef enum
{
    SC_GRID_HARMONICS = 6
} eSCGridData_t;

typedef struct
{
    sSCHarmonic_t H_L1_V[SC_GRID_HARMONICS];
    sSCHarmonic_t H_L2_V[SC_GRID_HARMONICS];
    sSCHarmonic_t H_CT1_A[SC_GRID_HARMONICS];
    sSCHarmonic_t H_CT2_A[SC_GRID_HARMONICS];
    sSCHarmonic_t H_CT3_A[SC_GRID_HARMONICS];

} sSCGridData;

typedef struct
{
    uint8_t dev;
    sSCGridData gdata;
} sSCRspGridData_t;

typedef enum
{
    SCFFTBIN_FSK   = 0,
    SCFFTBIN_L1_HF = 1,
    SCFFTBIN_L1_LF = 2,
    SCFFTBIN_L2_HF = 3,
    SCFFTBIN_L2_LF = 4,
    SCFFTBIN_LAST
} eSCFFTBIN_t;

#define SC_FFT_SAMPLE_COUNT 257  // DC @ index
typedef struct
{
    uint8_t dev;
    uint8_t bin;      // 0 to 4 (NUMBER_OF_VALID_FFT_BINS-1)
                      // Bin 0 = FFT post FSK band-pass - DC to 130KHz
                      // Bin 1 = FFT of L1 20KHz to 100KHz
                      // Bin 2 = FFT of L1 DC to 20KHz
                      // Bin 3 = FFT of L2 20KHz to 100KHz
                      // Bin 4 = FFT of L2 DC to 20KHz
    uint8_t len;      // number of samples to copy
    uint16_t offset;  // index into data (16 bit data)
} sSCCmdGetFFTData_t;

typedef struct
{
    uint8_t dev;
    uint8_t len;
    uint16_t data[];
} sSCRspGetFFTData_t;

typedef struct
{
    uint8_t dev;
    float powerA;
    float powerB;
    float powerC;
} sWN_FAST_Power;

typedef struct
{
    float PowerA;  //
    float PowerB;
    float PowerC;
    float PowerReacA;
    float PowerReacB;
    float PowerReacC;
    float PowerApparentA;
    float PowerApparentB;
    float PowerApparentC;
    float VoltA;
    float VoltB;
    float VoltC;
    float CurrentA;
    float CurrentB;
    float CurrentC;
    float Freq;
    int32_t EnergyAInt;
    int32_t EnergyBInt;
    int32_t EnergyCInt;
    int32_t EnergyPosAInt;
    int32_t EnergyPosBInt;

    int32_t EnergyNegAInt;
    int32_t EnergyNegBInt;

    int32_t EnergyReacAInt;
    int32_t EnergyReacBInt;
    int32_t EnergyReacCInt;

    float PowerFactorA;
    float PowerFactorB;
    float PowerFactorC;
    uint32_t UptimeSecs;
    uint32_t ErrorStatus;
    uint32_t CrcErrorCount;
    uint32_t FrameErrorCount;
    uint32_t PacketErrorCount;
    uint32_t OverrunCount;
    uint32_t CtDirections;
    uint32_t CtAmpsA;
    uint32_t CtAmpsB;
    uint32_t CtAmpsC;
} sWN_data;

#define SC_RSSI_HISTOGRAM 0
#define SC_RSSI_BUSY_HISTOGRAM 1
#define SC_RSSI_FREE_HISTOGRAM 2

typedef struct
{
    //! Sidecar command header
    uint8_t dev;
    uint16_t index;  // index of histogram to read
    uint8_t name;    // histogram name
} sSCCmdReadHistogram_t;

typedef struct
{
    uint16_t length;
    uint32_t data[32];
} sSCHistogram_t;

typedef struct
{
    uint8_t dev;
    sSCHistogram_t histogram;
} sSCRspReadHistogram_t;

typedef struct
{
    uint8_t dev;
    sWN_data data;
} sSCRspWNData_t;

// allow the PVS to tell SC to only forward messages from these MI's. Even
// though interface can only write 60 at a time, SC will maintain 512 slots.
typedef struct
{
    uint32_t num_whitelist_elements;  // how many in this write
    uint32_t
        mi_short_serial_number[60];  // entire array does not need to be sent
} sSC_whitelist;

typedef struct
{
    uint8_t dev;
    sSC_whitelist wl;
} sSCCmdWhiteList_t;

// clang-format off
#ifndef _MIPS_ARCH
#define SC_FILTER_WHITELIST         0x00000001
#define SC_FILTER_ON_PACKET_TYPE    0x00000002
#else
#define SC_FILTER_WHITELIST         0x01000000
#define SC_FILTER_ON_PACKET_TYPE    0x02000000
#endif
// clang-format on

typedef struct
{
    uint32_t filtering_options;  // bitmask. 1 to enable filtering
} sSC_filtering_options;

typedef struct
{
    uint8_t dev;
    sSC_filtering_options filter;
} sSCCmdFilter_t;

typedef struct
{
    int32_t
        result;  // less than 0 if new list could not be added (too many limit
                 // of 512), otherwise number in current list after add
} sSC_SetWhitelistResponse;

typedef struct
{
    uint8_t dev;
    sSC_SetWhitelistResponse rsp;
} sSC_RspWhiteListWrite_t;

//  Time      Host        Sidecar       MI
//   1      Write TX
//   2        Poll        PLC TX
//   3        Poll                    PLC RX
//   4        Poll         ACK         Process
//   5      RX SC Ack                  Process
//   6        Poll                     Process
//   7        Poll                    PLC TX
//   8        Poll        PLC RX
//   9      RX MI Rsp

// the following structure contains the converted internal voltages as read by
// the PSOC using its internal ADC
typedef struct
{
    float fTemperature;
    float fVref1_4;
    float fDCunregulated;
    float fDCregulated;
    float fReg_3_3;
    float fRegulated_4_0;
    float fISO;
    float fRsvd2;
} sSC_internal_volatge_data;

// the following structure is used to return the LINE voltages as RMS. It is
// assumed that the input signals are AC
typedef struct
{
    float fAC_L1_Vrms;
    float fAC_L2_Vrms;
} sSC_line_voltage_data;
// the following structure is used to return the CT voltages as RMS. It is
// assumed that the input signals are AC
typedef struct
{
    float fCT_1_Vrms;
    float fCT_2_Vrms;
    float fCT_3_Vrms;
} sSC_ct_voltage_data;

// this structure can be used to set the LED pins high (1) or low (0)
typedef struct
{
    uint8_t bRxLED;
    uint8_t bTxLED;
} sSC_led_interface;

typedef struct
{
    uint8_t bCTS_RTS;  // for testing CTS, write 1 and 0 and make sure output
                       // matches, For testing RTS, set RTS 1/0 and read back
} sSC_cts_rts_interface;

typedef struct
{
    uint8_t uiGenericValue;
} sSC_generic_response_data;

// The following structure contains the revision data (software and hardware)
// for the test code and the actual code.  The test code will be distinguished
// from the actual code by @todo - finish this sentence.
typedef struct
{
    uint16_t build;
    uint16_t major;
    uint32_t fwrevision;
    uint32_t hwrevision;
} sSC_test_revision;

typedef struct
{
    uint8_t ucTestData[28];  // test data such as device SN, PSOC will calculate
                             // CRC32 and store in EEPROM, 2 rows will be
                             // reserved. (2 * 16) - 4 = 28
    uint32_t
        uiStatus;  // used when reading back, 0 means OK, other means an error
} sSC_EEPROM_TestData;

typedef struct
{
    uint8_t ucBootData[16];
} sSC_EEPROM_BootData;

// clang-format off
#define WN_CT_CONFIG_SET_AMPS_A         0x00000001
#define WN_CT_CONFIG_SET_AMPS_B         0x00000002
#define WN_CT_CONFIG_SET_AMPS_C         0x00000004
#define WN_CT_CONFIG_SET_DIRECTIONA     0x00000008
#define WN_CT_CONFIG_SET_DIRECTIONB     0x00000010
#define WN_CT_CONFIG_SET_DIRECTIONC     0x00000020
// clang-format on

typedef struct
{
    /***************************************************************
     * Register   Description                   Range       Default
     *  1604      PhaseA CtAmps consumption	    0 to 6000	100
     *  1605      PhaseB CtAmps consumption	    0 to 6000	100
     *  1606      PhaseC CtAmps production	    0 to 6000	50
     *  1607      Use this to flip CT           0 to 7      b000
     *              orientations in software.
     ***************************************************************/
    uint32_t bOperationBitmask;
    uint32_t CtAmpsA;
    uint32_t CtAmpsB;
    uint32_t CtAmpsC;
    uint32_t CtDirectionA;
    uint32_t CtDirectionB;
    uint32_t CtDirectionC;
} sWN_set_ct_configuration_t;

typedef struct
{
    //! device should be \ref eSC_INTF_SET_WN_CT_CONFIG
    uint8_t dev;

    //!
    sWN_set_ct_configuration_t cfg;
} sSCCmdWNConfig_t;

typedef struct
{
    uint8_t long_serial[24];  // NULL terminated ASCII serial number up to 23
                              // bytes long
    uint32_t short_serial;    // short serial. May be unused
    uint32_t uiStatus;
} sSC_EEPROM_SerialNumber;

typedef struct
{
    uint8_t uiPLC_Test_Result;
    uint8_t uiRSSI;
} sSC_plc_loopback_result_data;

#define SC_TESTINTF_STATUS_RESET \
    (0x00000001)  // this bit will be set on PSOC reset, used to determine if
                  // SUPERVISOR kicked and resets PSOC
// more status bits as required...
//

typedef struct
{
    uint32_t uiStatus;  // current status bits - defined as SC_TESTINTF_STATUS_*
} sSC_status_data;

/*!
 * @}
 */

/* TEST SEQUENCE WITH PSOC
 *
 * After shorts, opens, power supplies, analog measurements etc,
 *
 * Download SC Test Application to PSOC (Segger) FLASH
 * AC Power applied
 * Wait 1 sec
 * 1) Read Version (eSC_TESTINTF_GET_VERSION) - return sSC_revision - Isolated
 * UART (38,400, n, 8, 1) 2) Read Version (eSC_TESTINTF_GET_VERSION) - return
 * sSC_revision - Metering UART (38,400, n, 8, 1) Version data should be <TODO>,
 * both should match 3) Reset Status (eSC_TESTINTF_RESET_STATUS) returns
 * sSC_generic_response_data Wait 3 seconds to make sure no reset 4) Read Status
 * (eSC_TESTINTF_READ_STATUS) - returns sSC_status_data,
 * SC_TESTINTF_STATUS_RESET should be clear 5) Start Supervisory Test
 * (eSC_TESTINTF_SUPERVISORY_TEST) - returns sSC_generic_response_data Wait 3
 * seconds for reset to occur (PSOC will stop toggling WD pin) 6) Read Status
 * (eSC_TESTINTF_READ_STATUS) - returns sSC_status_data,
 * SC_TESTINTF_STATUS_RESET should be set 7) Read internal DC voltages
 * (eSC_TESTINTF_READ_INTERNAL_VOLTAGES) returns sSC_internal_volatge_data
 *      Pass/Fail criteria <TODO>
 * 8) Read AC Line voltages (eSC_TESTINTF_READ_AC_LINE_VOLTAGES) - returns
 * sSC_line_voltage_data Pass/Fail criteria <TODO> 9) Apply AC signal to CT1  -
 * Read CT Voltages (eSC_TESTINTF_READ_CT_VOLTAGES) returns -
 * sSC_ct_voltage_data CT1 should be <TODO> CT2, CT3 should be <TODO> 10) Repeat
 * 9 applying AC signal to CT2, CT3 respectively 11) Write LED Rx, TX using
 * (eSC_TESTINTF_SET_LED) and sSC_led_interface, apply four patterns, make sure
 * all measure correctly 12) Write RTS 0/1 using (eSC_TESTINTF_WRITE_RTS) and
 * sSC_cts_rts_interface, check digital level high and low 13) Set CTS 0/1 and
 * read back using eSC_TESTINTF_READ_CTS returning sSC_cts_rts_interface 14) Set
 * PLC open, Send eSC_TESTINTF_PLC_LOOPBACK_TEST - returns
 * sSC_plc_loopback_result_data Pass/Fail criteria <TODO> 15) Set PLC impedence
 * A Send eSC_TESTINTF_PLC_LOOPBACK_TEST - returns sSC_plc_loopback_result_data
 *     Pass/Fail criteria <TODO>
 * 16) Set PLC impedence B Send eSC_TESTINTF_PLC_LOOPBACK_TEST - returns
 * sSC_plc_loopback_result_data Pass/Fail criteria <TODO> 17) Set PLC impedence
 * A Send eSC_TESTINTF_PLC_ENABLE_TEST - returns sSC_plc_loopback_result_data
 *     Pass/Fail criteria <TODO>
 * 18) Set PLC impedence A Send eSC_TESTINTF_PLC_TX_CAP_TEST - returns
 * sSC_plc_loopback_result_data Pass/Fail criteria <TODO> 19) Write SN/Test
 * results eSC_TESTINTF_WRITE_TEST_DATA and sSC_EEPROM_TestData - returns
 * sSC_generic_response_data 20) Read SN/Test results
 * eSC_TESTINTF_READ_TEST_DATA returns returns sSC_generic_response_data
 */

#pragma pack(pop)

#endif

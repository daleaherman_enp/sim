/**
 * @copyright
 * Copyright 2018, SunPower Corp
 * All rights reserved.
 *
 * @file transciever.hh
 * Transciever Interface definition.  This interface defines a class that
 * writes synchronously and receives asynchronously.
 *
 * @date 2018-Jun-25
 * @author Miles Bintz <mbintz@sunpower.com>
 */

#ifndef _TRANSCIEVER_HH_
#define _TRANSCIEVER_HH_

#include <stdlib.h>
#include <stdint.h>

#include <exception>

//! @addtogroup libcomm2_api
//! @{

void dump(uint8_t *pBuf, uint32_t length);

//! A purely virtual class to define a  generic callback interface
class CallbackIF
{
    public:
        virtual ~CallbackIF() = 0;

        // The prefix "cbif" is to prevent naming clashes.
        //! This function is called with len bytes in pBuf
        //! it returns the number of bytes consumed from pBuf
        virtual int cbifCallbackFunction(uint8_t *pBuf, size_t len) = 0;
};

//! A purely virtual class to define a generic transciever interface
class TranscieverIF
{
    public:
        virtual ~TranscieverIF() = 0;

        //! Open the requested device based on pDevStr
        virtual void Open(const char* pDevStr) = 0;

        //! Close the device
        virtual void Close() = 0;

        //! Register a callback with the transciever interface
        virtual void CallbackSet(CallbackIF *cb) = 0;

        //! Set the read timeout where applicable
        virtual void ReadTimeoutSet(uint32_t to) = 0;

        //! return the read timeout
        virtual uint32_t ReadTimeoutGet() = 0;

        //! Write the buffer to the transmitter portion of the object
        virtual int32_t Write(uint8_t *pBuf, size_t length) = 0;

        //! Flush all unread characters from the input side
        virtual void Flush() = 0;

        //! Set debugging parameters for this module
        virtual void DebugSet(uint32_t ui32Flags) = 0;

        //! Return the device name used to open this object
        virtual char* DeviceName() = 0;

};

//! @}
#endif

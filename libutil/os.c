/*
 * @copyright
 * Copyright 2015, Sunpower Corp.
 * All rights reserved.
 *
 * @file os.c
 *  OS abstraction/helper functions
 *
 * @date Apr 9, 2015
 * @author Miles Bintz <mbintz@sunpower.com>
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include "sbt/os.h"
#include "sbt/log.h"
#include "sbt/errno.h"

int tsleep(uint32_t usec)
{
    struct timespec req, rem;
    int rc;

    req.tv_nsec = ((usec % 1000000) * 1000);
    req.tv_sec = usec / 1000000;
    do
    {
        rc = nanosleep(&req, &rem);
        if(rc != 0) req = rem;
    } while(rc && errno == EINVAL);
    if(!rc) return 0;

    LOG_WARNING("nanosleep() failed with errno = %d\n", errno);
    return -errno;
}

#define RSIZE 4096
char* GetChildOutput(const char *argv[], uint32_t timeout)
{
    int filedes[3] = {0,0,0};
    pid_t pid;
    int rc;

    // Make a pipe.  Get two file descriptors -- the read end and write end.
    if (pipe(filedes) == -1)
    {
        LOG_ERROR("Unable to create pipes\n");
        return 0;
    }

    filedes[2] = dup(STDERR_FILENO);
    // fork our process in prep for exec'ing a child process to capture output
    // from
    pid = fork();
    if (pid == -1)
    {
        LOG_ERROR("Unable to fork\n");
        close(filedes[0]);
        close(filedes[1]);
        return 0;
    }
    else if (pid == 0)
    {
        // this is the child process - get ready to exec.  Set STDOUT to our
        // pipe's fileno
        rc = dup2(filedes[1], STDOUT_FILENO);
        if (rc == -1)
        {
            LOG_ERROR("dup failed: %s\n", strerror(errno));
            return 0;
        }
        dup2(filedes[2], STDERR_FILENO);

        close(filedes[2]);
        close(filedes[1]);
        close(filedes[0]);

        // the child process inherits our STDOUT which has been re-swizzled
        rc = execv(argv[0], (char* const*)argv);
        // we only get here if exec fails...
        LOG_WARNING("Unable to spawn process '%s': %s\n", argv[0], strerror(errno));
        exit(0);
    }
    else
    {
        struct pollfd poll_fd;
        char *in = (char*)malloc(RSIZE);
        ssize_t r, bufsize, total = 0;
        bufsize = RSIZE;

        // close the write-side of the pipe so that we get appropriate signals
        // when the child exits
        close(filedes[1]);

        // don't block on our read.
        if (fcntl(filedes[0], F_SETFL, O_NONBLOCK) == -1)
        {
            LOG_ERROR("\n");
            return 0;
        }

        assert(in);

        poll_fd.fd = filedes[0];
        poll_fd.events = POLLIN;
        poll_fd.revents = 0;

        // we're the parent.  Start reading from the child.
        rc = 0;
        while(1)
        {
            // wait up to timeout ms for data to flow from our child process
            rc = poll(&poll_fd, 1, timeout);
            if(rc <= 0)
            {
                // timeout -- we're done
                break;
            }
            r = read(filedes[0], in + total, bufsize - total);
            if (r < 0)
            {
                LOG_WARNING("Read from child failed: '%s'\n", strerror(errno));
                break;
            }

            total += r;
            if ((r > 0) && (total % RSIZE) == 0)
            {
                // looks like there's more, realloc to make room
                in = (char*)realloc(in, total + RSIZE);
                bufsize += RSIZE;
            }
            else
            {
                rc = waitpid(pid, 0, WNOHANG);
                if(rc == pid) break;
            }
        }
        close(filedes[0]);

        if(total == 0)
        {
            free(in);
            return 0;
        }

        // make sure our input is null terminated.
        in[total++] = 0;

        // and adjust the size of the buffer to be just right
        in = (char*)realloc(in, total);
        return in;
    }
}


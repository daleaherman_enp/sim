/* Copyright (c) 2006-2008 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */


#ifndef _emuutl_h_
#define _emuutl_h_


/** @file emuutl.h
 *  @brief EMU Thread Interface
 *
 *  This file @c emuutl.h provides the EMU utility interface.
 *
 */

/** @page emuutlHPg Header: emuutl.h EMU utility interface

@section emuutlHName emuutl.h HEADER NAME

    emuutl.h -- EMU Utility Interface

@section emuutlHSynop emuutl.h SYNOPSIS

    This file provides the EMU's utility interface.

@section emuutlHDesc emuutl.h DESCRIPTION

    This file, @c emuutl.h provides the EMU utility interface.

*/


/* system headers */
#include <stddef.h>
#include <stdlib.h>
#include "include/mcp_sunpower.h"
#include "include/enphase.h"
#include "include/mcp.h"


/**
 * @defgroup emuutlHGroups emuutl Header Definitions and Declarations
 */
/*@{*/


/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup emuutlHDefines emuutl Header Defines
 */
/*@{*/


/** mEmuTimerSubSecs subtracts seconds to a timeval struct: (*tvp) + secs --> (*vvp).
*/
#define mEmuTimerSubSecs(tvp, secs, vvp)                                \
    do {                                                                \
        (vvp)->tv_sec = (tvp)->tv_sec - secs;                           \
        (vvp)->tv_usec = (tvp)->tv_usec;                                \
    } while (0)

/** The mEmuTimerSub subtracts timeval structs (*tvp) - (*uvp) --> (*vvp).
    Use gettimeofday to set (*tvp) at beginning, and (*uvp) add end,
    then use mEmuTimerSub to do the subtraction.
*/
#define mEmuTimerSub(tvp, uvp, vvp)                                     \
    do {                                                                \
        (vvp)->tv_sec = (tvp)->tv_sec - (uvp)->tv_sec;                  \
        (vvp)->tv_usec = (tvp)->tv_usec - (uvp)->tv_usec;               \
        if ((vvp)->tv_usec < 0) {                                       \
            (vvp)->tv_sec--;                                            \
            (vvp)->tv_usec += 1000000;                                  \
        }                                                               \
    } while (0)

/*! return floating delta of two timevals */
#define mEmuSubtractTimeval(delta, t1, t2)                              \
    do {                                                                \
        (delta) = (((t1).tv_sec + ((t1).tv_usec/(1000000.0))) -         \
                 ((t2).tv_sec + ((t2).tv_usec/(1000000.0))));           \
    } while (0)

/*! copy two timevals, t1 <-- t2 */
#define mEmuCopyTimeval(t1, t2)                                         \
    do {                                                                \
        (t1).tv_sec = (t2).tv_sec;                                      \
        (t1).tv_usec = (t2).tv_usec;                                    \
    } while (0)

#define cemuutlPartNumFmtFullLen 24
#define cemuutlPartNumFmtShortLen 14

#define mNextBye

/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs -------------------------- */

/**
 * @defgroup emuutlHTypedefs emuutl Header Typedefs
 */
/*@{*/



/*@}*/  /* END of emuutl Header Typedef Group */

/* ------------------------------------ Extern Declarations ---------- */

/**
 * @defgroup emuutlHExtdata emuutl Header Extern Data Definitions
 */
/*@{*/


/*@}*/  /* END of emuutl Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup emuutlHGlobdata emuutl Header Global Data Defined
 */
/*@{*/

/*@}*/  /* END of emuutl Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup emuutlHStatdata emuutl Header Static Data Defined
 */
/*@{*/



/*@}*/  /* END of emuutl Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup emuutlHProto emuutl Header Prototypes
 */
/*@{*/

#if defined(__KEIL__)
#if defined(DEBUGGER) && (DEBUGGER == 1)
/** putstr - put string to serial output.  */
extern void
putstr(char * str);
#endif /* if defined(DEBUGGER) && (DEBUGGER == 1) */
#endif /* if defined(__KEIL__) */


#if defined(__KEIL__) && (defined(DBGJD) && (DBGJD == 1))
extern void
bytestostr(u_int8_t                   * bytes,
           u_int16_t                    numBytes,
           char _keil_xdata           * str);
#endif /* if defined(__KEIL__) && (DBGCMU == 1) */

#if defined(__KEIL__) && (defined(DBGJD) && (DBGJD == 1))
extern bool
isdigit(char h);
#endif /* if defined(__KEIL__) && (DBGCMU == 1) */

#if defined(__KEIL__) && (defined(DBGJD) && (DBGJD == 1))
extern bool
isxdigit(char h);
#endif /* if defined(__KEIL__) && (DBGCMU == 1) */

#if defined(__KEIL__) && (defined(DBGJD) && (DBGJD == 1))
extern u_int8_t
xasciival(char h);
#endif /* if defined(__KEIL__) && (DBGCMU == 1) */

extern int
emuutlPartNumCmp(tPartNumber               * p1,
                 tPartNumber               * p2);

extern void
emuutlTimerClear(struct icarus_timeval * tval);

#if defined(PLCDBG) && (PLCDBG == 1)
/** emuutlDumpModData - Dump module data. */
extern void
emuutlDumpModData(char                        * hdrStr,
                  unsigned                      addr,
                  unsigned                      count,
                  unsigned char               * xpdata);
#endif /* if (defined(PLCDBG) && (PLCDBG == 1)) */

/** emuutlFletcher8BitCsum - Calculate checksum. */
extern u_int16_t
emuutlFletcher8BitCsum(u_int8_t _keil_xdata * m,
                       int                    len);

/** emuutlMcpMsgCsum - Calculate checksum. */
extern u_int16_t
emuutlMcpMsgCsum(tMcpMsg _keil_xdata * m);

/** emuutlCheckMcpCsum - Check checksum. */
extern int
emuutlCheckMcpCsum(tMcpMsg _keil_xdata * m);

extern u_int16_t
emuutlCrc16(u_int8_t _keil_xdata * m, u_int16_t _keil_xdata len);

extern void debugPutPlcCtlStr(void);

/*@}*/  /* END of emuutl Header Prototypes Group */

/*@}*/  /* END emuutl Header Definitions and Declarations */


#endif /* _emuutl_h_ */


/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of emuutl.h */



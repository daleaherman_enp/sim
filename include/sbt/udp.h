/*
* @copyright
* Copyright 2015, SunPower Corp.
* All rights reserved.

* @file udp.h
*
*
* @date Feb 10, 2015
*
* @author  Sergio Davila <sdavila@sunpowercorp.com>
*/
#ifndef UDP_H_
#define UDP_H_

#include <stddef.h>

typedef struct udp_t *udp_handle_t;

typedef int32_t (*pfnUdpCB_t)(void *pCookie, uint8_t *pBuf, const size_t len);

//! @breif A function pointer to a callback to receive decoded COBS frames
typedef int32_t (*pfnUdpCB_t)(void *pCookie, uint8_t *pBuf, const size_t len);

//! @brief Set UDP message debugging mode.
extern int32_t UDPDebugSet(udp_handle_t This, uint32_t ui32Flags);

//! @brief Set UDP message timeout in milliseconds
extern int32_t UDPTimeoutReadSet(udp_handle_t This, uint32_t ms);

//! @brief Set UDP message timeout in milliseconds
extern int32_t UDPTimeoutReadGet(udp_handle_t This, uint32_t *ms);

//! @brief allocate and construct a new connection object
extern int32_t UDPConnectionNew(udp_handle_t *pHandle);

//! @brief destruct and free a connection object
extern int32_t UDPConnectionDelete(udp_handle_t This);

//! @brief Close the UDP connection.
extern int32_t UDPClose(udp_handle_t This);

//! @brief Flush the UDP input.
extern int32_t UDPFlush(udp_handle_t This);

//! @brief Open the UDP device.
extern int32_t UDPDeviceOpen(udp_handle_t This, const uint8_t *device);

//! @brief Read a packet from the UDP.
extern int32_t UDPRead(udp_handle_t This, uint8_t *packet, size_t *length,
                       int32_t *device);

//! @brief Write a packet to the UDP.
extern int32_t UDPWrite(udp_handle_t This, const uint8_t *packet,
                        size_t length, int32_t device);

//! @brief Write a packet to the UDP connection with only L2 processing
extern int32_t UDPWriteL2(udp_handle_t hThis, const uint8_t *packet,
                          size_t length);

//! @brief Read a packet from the UDP connection with only L2 processing
extern int32_t UDPReadL2(udp_handle_t hThis, uint8_t *packet,
                          size_t *length, uint32_t timeout);

//! @brief Register a callback to be called when the read thread has a valid COBS frame.
extern int32_t UDPRegisterCallback(udp_handle_t hThis, pfnUdpCB_t pfn, void *pCookie,
                                   pfnUdpCB_t *oldpfn, void **ppOldCookie);



#endif


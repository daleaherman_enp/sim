/*!
 * @copyright
 * Copyright 2018-2019, SunPower Corporation
 * All rights reserved.
 *
 * @file enphase_command.h
 *  Enphase protocol packet builders
 *
 * @date July 8, 2018
 * @author  Dale Herman
 */

#ifndef ENPHASE_COMMAND_H_
#define ENPHASE_COMMAND_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef FFS
#define FFS(x) __builtin_ffs((x))
#endif

/**¬
 * Return the string corresponding to the response status¬
 * @param s the bit position to query the string for¬
 * @return a constant string naming the status bit¬
 */
const char *enphase_response_status_string(uint8_t s);

int32_t enphase_pack_poll_command_search(uint8_t *packet, uint8_t *pDomain,
                                         uint16_t logAddr);
int32_t enphase_pack_squelch(uint8_t *packet, tSerialNumber *devSerialNum,
                             uint16_t squelchTimeInSecs);
int32_t enphase_pack_association(uint8_t *packet, uint8_t *domainAddr,
                                 tSerialNumber *devSerialNum,
                                 uint16_t logicalAddress);
int32_t enphase_pack_start_stop_power_now(uint8_t *packet,
                                          tSerialNumber *devSerialNum,
                                          uint32_t bStart);
int32_t enphase_pack_poll_request_data_tlv(uint8_t *packet, uint8_t *domainAddr,
                                           uint16_t logicalAddr,
                                           uint16_t reqFlags,
                                           tSerialNumber *devSerialNum, uint8_t mode);
int32_t enphase_pack_load_pv_data(uint8_t *packet, tSerialNumber *devSerialNum,
                                  tEEPROM_PV_MODULE_ENTRY *moduleData);
int32_t enphase_unpack_deviceInfo_tlv(uint8_t *packet, uint32_t *msgTime,
                                      uint8_t *status,
                                      tMcpPollRspDevInfo *devInfoP);
int32_t enphase_unpack_imageInfo_tlv(uint8_t *packet, uint32_t *msgTime,
                                     uint8_t *status, tImageMetaData *imgInfoP);
int32_t enphase_unpack_agfInfo_tlv(uint8_t *packet, uint32_t *msgTime,
                                   uint8_t *status,
                                   tMcpAgfLoadedRecStatus *agfRecStatus);
int32_t enphase_unpack_interval_data_tlv(uint8_t *packet, uint32_t *msgTime,
                                         uint8_t *status,
                                         tMcpPcuPwrIntervalData *intervalDataP,
                                         tIntervalLongData *intervalLongP);
int32_t enphase_unpack_counter_data_tlv(uint8_t *packet, uint32_t *msgTime,
                                        uint8_t *status,
                                        tMcpCounterData *counterDataP);
int32_t enphase_unpack_conditionData_tlv(uint8_t *packet, uint32_t *msgTime,
                                         uint8_t *status,
                                         tMcpConditionData *conditionDataP);
int32_t enphase_pack_set_groupMask(uint8_t *packet, uint8_t *domainAddr,
                                   uint16_t logicalAddress,
                                   tSerialNumber *devSerialNum,
                                   uint32_t groupMask);
int32_t enphase_pack_unassociate_all(uint8_t *packet);
int32_t enphase_pack_unassociate_specific(uint8_t *packet, uint8_t *domain);
int32_t enphase_pack_tlv_agf(uint8_t *packet, uint8_t *domainAddr,
                             uint16_t logicalAddress, uint32_t groupMask,
                             tMcpAgfRecord *pAgfRecord);
int32_t enphase_pack_exp_tlv_agf(uint8_t *packet, uint8_t *domainAddr,
                                 uint16_t logicalAddress, uint32_t groupMask,
                                 tMcpAgfRecHeader *pRecordHdr);
int32_t enphase_pack_condition_poll_ack(uint8_t *packet, uint8_t seqNum,
                                        uint8_t *domain, uint16_t modAddr);
int32_t enphase_pack_imginfo(uint8_t *packet, tMcpImageInfoMsg *pUGInfoMsg);
int32_t enphase_pack_imgrspinfo(uint8_t *packet, tMcpImageRspMsg *pImgRspMsg);
int32_t enphase_pack_v6_telemetry_1(uint8_t *packet, uint8_t *domainAddr,
                                    uint16_t modAddr);
int32_t enphase_pack_poll_control(uint8_t *packet, uint8_t *domain,
                                  uint16_t modAddr, uint16_t flags);
int32_t enphase_pack_clear_agf(uint8_t *packet, tSerialNumber *devSerialNum);
int32_t enphase_pack_ack_dev_info(uint8_t *packet, uint8_t *domainAddr,
                                  tSerialNumber *devSerialNum,
                                  uint16_t logicalAddress);
int32_t enphase_pack_poll_dev_info(uint8_t *packet, uint8_t *domainAddr,
                                   tSerialNumber *devSerialNum,
                                   uint16_t logicalAddress);
int32_t enphase_unpack_poll_dev_info(uint8_t *packet,
                                     tMcpDevInfoMsg_Ver3 *pPacket);
int32_t enphase_pack_poll_new_device(uint8_t *packet);
int32_t enphase_pack_poll_envoy_unassociate_all(uint8_t *packet);
int32_t enphase_pack_poll_envoy_unassociate_domain(uint8_t *packet,
                                                   uint8_t *domainAddr);
int32_t enphase_pack_data_rd(uint8_t *packet,
                                  char *domain,
                                  uint16_t logicalAddress, uint32_t mem_addr, uint16_t len, uint8_t cmd_type, uint8_t data_type);
int32_t enphase_pack_vrise_bc(uint8_t *packet, uint8_t *domainAddr, uint32_t groupMask, uint32_t mvRmsLL1, uint32_t mvRmsLL2, uint32_t mvRmsLL3);
int32_t enphase_unpack_debug_memr_data(uint8_t *packet, uint32_t *mem, uint32_t *count, uint8_t**pBuf, uint8_t cmd_type, uint8_t data_type);
int32_t enphase_unpack_skip_counters(uint8_t *packet, tMcpDebugSkipDataHeron *pSkip);
int32_t enphase_pack_set_offgrid_mode(uint8_t *packet, uint8_t *domainAddr, tSerialNumber *devSerialNum, uint16_t logicalAddress, uint16_t backup_timeout_secs, uint16_t mode);
int32_t enphase_pack_reset_offgrid_mode(uint8_t *packet, uint8_t *domainAddr, tSerialNumber *devSerialNum, uint16_t logicalAddress, uint16_t mode);
int32_t enphase_pack_query_offgrid_mode(uint8_t *packet, uint8_t *domainAddr, tSerialNumber *devSerialNum, uint16_t logicalAddress, uint16_t mode);
int32_t enphase_unpack_query_rsp_offgrid_mode(uint8_t *packet, uint8_t *mode, uint16_t *backupTime, uint16_t *timeLeft);
int32_t enphase_pack_mps_mode(uint8_t *packet, uint8_t *domainAddr, tSerialNumber *devSerialNum, uint16_t logicalAddress, uint8_t enable, uint16_t mode);

#ifdef __cplusplus
}
#endif

#endif  // ENPHASE_COMMAND_H_

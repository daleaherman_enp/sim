/*!
 * MIMEControl.hh
 *
 * Copyright (c) 2018 SunPower Corp.
 *
 *  Created on: Aug 15, 2018
 *      Author: Miles Bintz <mbintz@sunpower.com>
 */

#ifndef _MULTITOOL_HH_
#define _MULTITOOL_HH_

#include <map>
#include <string>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <getopt.h>

struct CmdArg;
typedef std::map<std::string,CmdArg> ArgMap_t;
typedef ArgMap_t::iterator AMIt_t;

class CmdUsage;
typedef std::map<std::string,CmdUsage*> UsageMap_t;
typedef UsageMap_t::iterator UMIt_t;

//! This structure describes one argument that a command line tool implements
struct CmdArg
{
    public:
        //! the help string associated with this option
        const char* help;

        //! same values as `struct option: has_arg`.  Namely,
        //!    no_argument, required_argument, or optional_argument
        //! If the option is no_argument then bPresent will be set
        int has_arg;

        //! short-option flavor of the argument.  ex:  --option => -o
        char shortopt;

        //! Whether or not the command line requires this argument to be
        //! specified
        bool bOptional;

        //! false unless the user put the switch on the command line
        int bPresent;

        //! the string that followed the argument (when applicable)
        char *pVal;
};

#define HELPARG { "help", { "show this help", no_argument, 'h', true } }

//! This class maintains a collection of arguments and handles the parsing
//! of those arguments placing the values into each argument object
class CmdUsage
{
    private:
        //! This list of args.  This isn't allocated.  It's passed in.  Do NOT
        //! attempt to free it in the destructor
        ArgMap_t *m_pArgs;

        //! This holds the name of the command-line tool
        const char* m_name;

    public:
        //! Initialize a usage parser given the name of a command and a list
        //! of arguments.
        CmdUsage(const char* name, ArgMap_t &args);

        //! Digest the CLI arguments provided to the multi-tool.  Return
        //! the array index (to argv[]) of the first unsupported argument.
        //! On error, return a negative value.
        int Parse(uint32_t argc, char* const* argv);

        //! Pretty-print the usage for this command
        void Show(FILE *f = stdout);

        //! CmdUsage is the object the user wants to interact with.
        //! Provide the [] operator so they can grab an agurment by
        //! name.
        CmdArg* operator[](const std::string &opt);
};

class MultitoolCollection_IF;

//! A MultitoolCommand_IF is thing that normalizes how a multi-tool
//! executable (like busbox) can offer a function to the collective.
class MultitoolCommand_IF
{
    public:
        MultitoolCommand_IF();

        //! The command collection (main entry point) constructs the
        //! individual command objects and passes us a reference back to
        //! it.  The command objects use that reference to register
        //! themselves with the command collection.
        MultitoolCommand_IF(MultitoolCollection_IF &mtcol);

        //! pure virtual destructor to force this declaration to be
        //! purely abstract
        virtual ~MultitoolCommand_IF();

        //! The entry point (analogous to main() ) for this command
        virtual int Execute(uint32_t argc, const char* argv[]) = 0;

        //! Ask this tool to dump its usage
        virtual void Usage(FILE *f) = 0;
};

//! Implement a MultitoolCommand_IF
class MultitoolCommand : public MultitoolCommand_IF
{
    protected:
        //! Multitool commands must provide their own (static local scope)
        //! arguments when implementing the contructor
        CmdUsage *m_pUsage;

    public:
        MultitoolCommand();
        MultitoolCommand(MultitoolCollection_IF &mtcol);
        ~MultitoolCommand();

        virtual int Execute(uint32_t argc, const char *argv[]) override = 0;
        virtual void Usage(FILE *f) override;
};

#define MULTITOOLCMD(N) class N : public MultitoolCommand \
{ \
    public: \
        N(MultitoolCollection_IF &coll); \
        ~N(); \
        int Execute(uint32_t argc, const char* argv[]) override; \
}

#define MULTITOOLCMD_CUSTOM(N, B) class N : public B \
{ \
    public: \
        N(MultitoolCollection_IF &coll); \
        ~N(); \
        int Execute(uint32_t argc, const char* argv[]) override; \
}

//! A multitool collection is a container for Multitool commands. This
//! interface allows the a user of the interface to pass argc and argv
//! to the Execute function and the requested tool will run.
class MultitoolCollection_IF
{
    public:
        //! Tell the collection what its canonical name is.  ie.
        //! what argv[0] would be if not invoked through a symlink
        virtual void NameSet(const std::string &n) = 0;

        //! The implementor of a MultitoolCollection must create
        //! multitool command objects who will call us back and register
        //! with us.
        virtual void Register(const char* name, CmdUsage &a,
            MultitoolCommand_IF &cmd) = 0;

        //! Ask the multitool collection to do whatever the command line
        //! said to do
        virtual int Execute(uint32_t argc, const char *argv[]) = 0;
};

typedef std::map<std::string,MultitoolCommand_IF*> FuncMap_t;
typedef FuncMap_t::iterator FIt_t;

class MultitoolCollection : public MultitoolCollection_IF
{
    protected:
        //! A map/dictionary of command names to
        FuncMap_t m_Functions;
        UsageMap_t m_Args;
        std::string m_name;

    public:
        void NameSet(const std::string &n);
        void Register(const char* name, CmdUsage &a,
            MultitoolCommand_IF &cmd);
        virtual int Execute(uint32_t argc, const char *argv[]);
};
#endif /* _MULTITOOL_HH_ */

/* Copyright (c) 2006-2014 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef AGF_STRUCTS_H_
#define AGF_STRUCTS_H_

/** @file agf_structs.h
 *  @brief High Level AGF Definitions
 *
 *  This file @c agf_structs.h provides enPhase agf definitions.
 */

/** @page agfHPg Header: agf_structs.h enPhase AGF Definitions

 @section agfHName agf_structs.h HEADER NAME

 agf_structs.h -- enPhase AGF Definitions.

 @section agfHSynop agf_structs.h SYNOPSIS

 This file, @c agf_structs.h, provides definitions for agf/rule 21
 functionality.

 */

#if defined(SUNPOWER)
#pragma pack(push,1)
#endif

 
#include "mcp_enphase.h"

/**
 * @defgroup agfHGroups agf Header Definitions and Declarations
 */
/*@{*/

/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup agfHDefines agf Header Defines
 */
/*@{*/

/*defines the values for agf function enable/disable*/
#define cAgfFuncEnable            0x01
#define cAgfFuncDisable           0x00

#define cAgfScaleBy1000           1000
#define cAgfScaleBy10             10
#define cAgf24BitDataMask         0xFFFFFF

#define cAgfRegionNA              0
#define cAgfRegionEU              1

#define cAgfTrans6Points    6
#define cAgfTrans8Points    8
#define cAgfTrans10Points  10
#define cAgfTransPtPair     2
#define cAgfTransPtTriple   3
#define cAgfTransPtQuad     4

#define cAgfTrans2Values    2
#define cAgfTrans2Points    2
#define cAgfTrans2Curves    2
#define cAgfTransCurveHigh  0
#define cAgfTransCurveLow   1

// The MS Nibble of the record data structure flag word
// will be reserved for the Envoy to set for reverse transform
// functionality. The nibble values will be an enum specified
// in the record defination. Zero will not be used.
#define cAgfEnvoyDataMask         0xF0000000

/*@}*//* END of agf Defines Group */

/* ------------------------------------ Typedefs -------------------------- */

/**
 * @defgroup agfHTypedefs agf Header Typedefs
 */
/*@{*/

///////////////////////////////////////////////////////////////////////////////
// AGF COMMON TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This is the basic building block for AGF data types.  It is 32 bits.
 */
typedef int32_t tAgfInverterDataWord;

/**
 * Common AGF query message struct.  'type' field is not used.
 */
typedef struct _tAGFQueryMsg
{
    tAgfInverterDataWord    functionType;

} tAGFQueryMsg;

/**
 * The basic curve point. Contains two datawords.
 */
typedef struct _tAgfCurvePoint
{
    tAgfInverterDataWord    value[cAgfTrans2Values];

} tAgfCurvePoint;

/**
 * This is an inverter curve point. It consists of two basic curve points.
 */
typedef struct _tAgfInverterCurvePoint
{
    tAgfCurvePoint  point[cAgfTrans2Points];

} tAgfInverterCurvePoint;

/**
 * This is the basic inverter curve. It contains 10 inverter curve points.
 */
typedef struct _tAgfInverterCurve
{
    tAgfInverterCurvePoint  points[cAgfTrans10Points];

} tAgfInverterCurve;

/**
 * This is the VRT inverter curve. It contains 8 inverter curve points.
 */
typedef struct _tAgfInverterCurveVRT
{
    tAgfInverterCurvePoint  points[cAgfTrans8Points];

} tAgfInverterCurveVRT;

/**
 * This is the WVAR inverter curve. It contains 10 inverter curve points.
 */
typedef struct _tAgfInverterCurveWVAR
{
    tAgfCurvePoint  points[cAgfTrans10Points];

} tAgfInverterCurveWVAR;


/**
 * This is the basic inverter curve pair which consists of two inverter curves.
 */
typedef struct _tAgfInverterCurvePair
{
    tAgfInverterCurve   curve[cAgfTrans2Curves];

} tAgfInverterCurvePair;

/**
 * This is the VRT inverter curve pair which consists of two VRT inverter curves.
 */
typedef struct _tAgfInverterCurvePairVRT
{
    tAgfInverterCurveVRT   curve[cAgfTrans2Curves];

} tAgfInverterCurvePairVRT;

///////////////////////////////////////////////////////////////////////////////
// AGF VVAR TYPE DEFS
///////////////////////////////////////////////////////////////////////////////


/**
 * This structure is a control structure that contains flags for the various
 * AGF functionality to indicate if it is enabled or disabled.
 */
typedef struct _tAGFVVARMsg
{

    /**
     * This field should be set to either cAGF_VVAR_ENABLE or cAGF_VVAR_DISABLE
     * as defined above.
     */
    tAgfInverterDataWord    isEnabled;

    tAgfInverterDataWord    startPoint;
    tAgfInverterDataWord    stopPoint;

    tAgfInverterCurvePair   curves;

    tAgfInverterDataWord    maxSlopeDecrease;
    tAgfInverterDataWord    maxSlopeIncrease;

    /**
     * This field should be set to either cAgfTypeACurve or
     * cAGF_TYPE_B_CURVE as defined above.
     */
    tAgfInverterDataWord    TC;
    tAgfInverterDataWord    typeBMin;
    tAgfInverterDataWord    typeBMax;

} tAgfVVARMsg;

#define cAgfVvarEnable              0x00000001
#define cAgfVvarReactivePriority    0x01000000

#define cAgfVv11MinQxPt          (-1.0)
#define cAgfVv11MaxQxPt          (1.0)
#define cAgfVv11DefaultStopPoint (350.0)

#define cAgfVVARVoltageZero (0.0)
#define cAgfVVARVoltageMax (600.0)

#define cAgfVvarFlagsHeco              0x80000000
#define cAgfVvarFlagsP1547             0x00000002
#define cAgfVvarHecoCurveTypeMask      0xff000000
#define cAgfVvarHecoTimeConstMask      0x0000ffff
#define cAgfVvarHecoCurveTypeMax       255
#define cAgfVvarHecoCurveTypeShift     24
#define cAgfVvarHecoMinTimeShift       0
#define cAgfVvarHecoMinTimeConst       0.0
#define cAgfVvarHecoMaxTimeConst       655.35
#define cAgfVvarHecoTimeConstScale     100
#define cAgfVvarP1547MinVRef           0.9
#define cAgfVvarP1547MaxVRef           1.1
#define cAgfVvarP1547VRefScale         1000
#define cAgfVvarP1547MinVRefTimeConst  0
#define cAgfVvarP1547MaxVRefTimeConst  65535

typedef enum _eAgfVvarRecType {
    cAgfVvarRecTypeNotUsed = 0,
    cAgfVvarRecTypeVV11,
    cAgfVvarRecTypeVV12,
    cAgfVvarRecTypeVV13,
    cAgfVvarRecTypeVV14,
    cAgfVvarRecTypeNumOf,
} _eAgfVvarRecType;

///////////////////////////////////////////////////////////////////////////////
// AGF FRT TYPE DEFS
///////////////////////////////////////////////////////////////////////////////


/**
 * This structure is a control structure that contains data for the
 * AGF FRT functionality.
 */
typedef struct _tAgfFRTMsg
{
    tAgfInverterCurvePair   curves;
    tAgfInverterDataWord    maxTime;
    tAgfInverterDataWord    time;

} tAgfFRTMsg;

#define cAgfFrtHighLowTimeMin (0)
#define cAgfFrtHighLowTimeMax (255)
#define cAgfFrtHighLowTimeDefault (6)

///////////////////////////////////////////////////////////////////////////////
// AGF VRT TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF VRT functionality.
 */
typedef struct _tAgfVRTMsg
{
    tAgfInverterCurvePairVRT    curves;
    tAgfInverterDataWord        maxTime;

} tAgfVRTMsg;

#define cVRTCessationBit   0x01000000

///////////////////////////////////////////////////////////////////////////////
// AGF FIXED POWER FACTOR TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF FPF functionality.
 */
typedef struct _tAgfFPFMsg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterDataWord    lockInVoltage;
    tAgfInverterDataWord    powerFactor;    // abs of power factor
    tAgfInverterDataWord    lockOutVoltage;
    tAgfInverterDataWord    startpoint;

} tAgfFPFMsg;

#define cAgfLockVoltageDefault (0.0)
#define cAgfLockVoltageMax (600.0)
#define cAgfFpfScale       (0x400000)
#define cAgfFpfMaxPositive (1.0)
#define cAgfFpfMinPositive (0.068)
#define cAgfFpfMaxNegitive (-0.068)
#define cAgfFpfMinNegitive (-1.0)
#define cAgfFpfNegMask     (0xFF000000)
#define cAgfFpfReactivePriority    0x01000000
#define cAgfFpfStartPointScale     256
#define cAgfFpfStartPointMin       (0.0)
#define cAgfFpfStartPointMax       (100.0)
#define cAgfFpfStartPointDefault   (0.0)

///////////////////////////////////////////////////////////////////////////////
// AGF POWER RATE LIMITING TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF PRL functionality.
 */
typedef struct _tAgfPRLMsg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterDataWord    limit;

} tAgfPRLMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF SOFT START TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF SS functionality.
 */
#define cAgfSsFreqHzScale         (256)
#define cAgfSsRcFreqLowDefaultNA  (59.3 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqHighDefaultNA (60.5 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqLowMinNA      (50 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqLowMaxNA      (60 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqHighMinNA     (60 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqHighMaxNA     (67 * cAgfSsFreqHzScale)

#define cAgfSsRcFreqLowDefaultEU  (45 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqHighDefaultEU (55 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqLowMinEU      (40 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqLowMaxEU      (50 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqHighMinEU     (50 * cAgfSsFreqHzScale)
#define cAgfSsRcFreqHighMaxEU     (57 * cAgfSsFreqHzScale)

#define cAgfSsVoltToMVScale       (cAgfScaleBy1000)
#define cAgfSsRcVoltLowDefaultNA  (106 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltHighDefaultNA (127 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltLowMinNA      ( 96 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltLowMaxNA      (240 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltHighMinNA     (120 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltHighMaxNA     (288 * cAgfSsVoltToMVScale)

#define cAgfSsRcVoltLowDefaultEU  (184 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltHighDefaultEU (276 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltLowMinEU      (184 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltLowMaxEU      (240 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltHighMinEU     (230 * cAgfSsVoltToMVScale)
#define cAgfSsRcVoltHighMaxEU     (288 * cAgfSsVoltToMVScale)

#define cAgfSsRcAiDefault       (18300)
#define cAgfSsRcAiMin           (120)
#define cAgfSsRcAiMax           (32767)
#define cAgfSsRcPRRDefault      (1500)
#define cAgfSsRcPRRMin          (0)
#define cAgfSsRcPRRMax          (60000)
#define cAgfSsUseAiTime         (0x00000001)
#define cAgfSsUseAtPwrUp        (0x00000002)
#define cAgfSsUseAfterVoltTrip  (0x00000004)
#define cAgfSsUseAfterFreqTrip  (0x00000008)
#define cAgfSsUseAfterDcInjTrip (0x00000010)
#define cAgfSsAiDefault         (18300)
#define cAgfSsAiMin             (120)
#define cAgfSsAiMax             (32767)
#define cAgfSsAiShortFailTime   (3)
#define cAgfSsAiShortFailTimeCycMin (0)
#define cAgfSsAiShortFailTimeCycMax (255)
#define cAgfSsAiShortTime       (5)
#define cAgfSsAiShortTimeCycMin (120)
#define cAgfSsAiShortTimeCycMax (32767)

// NSR-B reconnect times for Region EU
#define cAgfSsReconnectAiTimeNsrb (5)
#define cAgfSsAiTimeNsrb (5)
#define cAgfSsAiShortTimeNsrb (5)


typedef struct _tAgfSSMsg
{
    tAgfInverterDataWord    mode;
    tAgfInverterDataWord    reconnectFrequencyLow;
    tAgfInverterDataWord    reconnectFrequencyHigh;
    tAgfInverterDataWord    reconnectVoltageLow;
    tAgfInverterDataWord    reconnectVoltageHigh;
    tAgfInverterDataWord    reconnectAiTime;
    tAgfInverterDataWord    reconnectPowerRampRate;
    tAgfInverterDataWord    shortAiFailTime;
    tAgfInverterDataWord    shortAiTime;
    tAgfInverterDataWord    aiTime;
} tAgfSSMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF VW TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF VW functionality.
 */
typedef struct _tAgfVWMsg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterCurvePair   curves;
    tAgfInverterDataWord    maxSlopeDecrease;
    tAgfInverterDataWord    maxSlopeIncrease;

    /**
     * This field should be set to either cAgfTypeACurve or
     * cAGF_TYPE_B_CURVE as defined above.
     */
    tAgfInverterDataWord    curveType;
    tAgfInverterDataWord    typeBMin;
    tAgfInverterDataWord    typeBMax;
    tAgfInverterDataWord    timeConstant;

} tAgfVWMsg;

// Same as tAgfVWMsg.  Re-Defined here as TRX requires a unique message type for
// each transform type.
typedef struct _tAgfVW52Msg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterCurvePair   curves;
    tAgfInverterDataWord    maxSlopeDecrease;
    tAgfInverterDataWord    maxSlopeIncrease;

    /**
     * This field should be set to either cAgfTypeACurve or
     * cAGF_TYPE_B_CURVE as defined above.
     */
    tAgfInverterDataWord    curveType;
    tAgfInverterDataWord    typeBMin;
    tAgfInverterDataWord    typeBMax;
    tAgfInverterDataWord    timeConstant;

} tAgfVW52Msg;

#define cAgfVwFlagsEnabled    0x00000001
#define cAgfVwFlagsHeco       0x80000000
#define cAgfVwFlagsPriority   0x00010000
#define cAgfVwFlagsPreDisturb 0x01000000
#define cAgfVwHecoMaxTimeConst       655.35
#define cAgfVwHecoTimeConstScale     100

#define cAgfVw51MinPwrPct     (0.0)
#define cAgfVw51MaxPwrPct     (1.0)
#define cAgfVwScaleTimeConstant       (0x400000)    // 1
#define cAgfVw52ScaleNegTimeConstant  (0x800000)    // -1
#define cAgfVw51DefaultTimeConstant   (cAgfVwScaleTimeConstant)
#define cAgfVw52DefaultTimeConstant   (cAgfVwScaleTimeConstant | cAgfVw52ScaleNegTimeConstant)
#define cAgfVwMaxTimeConstant         (cAgfVwScaleTimeConstant)
#define cAgfVwMinTimeConstant         (cAgfVw52ScaleTimeConstant)
#define cAgfVwMinTimeConstantSec    (0.0)
#define cAgfVw52MinPwrPct     (-1.0)
#define cAgfVw52MaxPwrPct     (0.0)

///////////////////////////////////////////////////////////////////////////////
// AGF INV2 TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF INV2 functionality.
 */
typedef struct _tAgfINV2Msg
{
    tAgfInverterDataWord    isEnabled;
    tAgfInverterDataWord    powerMax;
    tAgfInverterDataWord    rampRate;
    tAgfInverterDataWord    timeWindow;
    tAgfInverterDataWord    timeoutPeriod;

} tAgfINV2Msg;

/*defines for inv2 mode selection parameter*/
#define cAgfInv2FlagsEnableMask             0x00000001
#define cAgfInv2FlagsEnabled                0x00000001
#define cAgfInv2FlagsDisabled               0x00000000

#define cAgfInv2FlagsRampMask               0x00000002
#define cAgfInv2FlagsRamp                   0x00000000
#define cAgfInv2FlagsStep                   0x00000002

#define cAgfInv2FlagsPowerCmdMask           0x00000004
#define cAgfInv2FlagsAbsolute               0x00000000
#define cAgfInv2FlagsRelativeDown           0x00000004

#define cAgfInv2FlagsPowerTypeMask          0x00000008
#define cAgfInv2FlagsReal                   0x00000000
#define cAgfInv2FlagsApparent               0x00000008

#define cAgfInv2FlagsOff                    0x00000010
#define cAgfInv2FlagsOffNone                0x00000000
#define cAgfInv2FlagsOffAll                 0x00000010

#define cAgfInv4FlagsSleepModeMask          0x00000020
#define cAgfInv4FlagsSleepModeOff           0x00000000
#define cAgfInv4FlagsSleepModeOn            0x00000020

#define cAgfInv2PowerMaxScale               (256)

#define cAgfInv2RelPcuPowerMax              (100)
#define cAgfInv2RelPcuPowerMin              (0)
#define cAgfInv2RelPcuPowerDefault          (0)
#define cAgfInv2AbsPcuPowerMax              (100)
#define cAgfInv2AbsPcuPowerMin              (0)
#define cAgfInv2AbsPcuPowerDefault          (0)

// Relative power for ACB will probably never be used
// just here as a defination
#define cAgfInv2RelAcbPowerMax              (100)
#define cAgfInv2RelAcbPowerMin              (0)
#define cAgfInv2RelAcbPowerDefault          (0)
#define cAgfInv2AbsAcbPowerMax              (100)
#define cAgfInv2AbsAcbPowerMin              (-100)
#define cAgfInv2AbsAcbPowerDefault          (0)

#define cAgfInv2TimeoutPeriodMask           0x00FFFFFF
#define cAgfInv2TimeoutPeriodMin            0
#define cAgfInv2TimeoutPeriodMax            cAgfInv2TimeoutPeriodMask

///////////////////////////////////////////////////////////////////////////////
// AGF WP TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF WP functionality.
 */
typedef struct _tAgfWPMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    wstr;
    tAgfInverterDataWord    pfstr;
    tAgfInverterDataWord    wstop;
    tAgfInverterDataWord    pfstop;
    tAgfInverterDataWord    slope;
    tAgfInverterDataWord    lockInVoltage;
    tAgfInverterDataWord    lockOutVoltage;

} tAgfWPMsg;

#define cAgfWp41Enable            0x00000001
#define cAgfWpReactivePriority    0x01000000
#define cAgfWp41Scale             0x00400000
#define cAgfWp41PfMin             0.707
#define cAgfWp41PfMax             1.0
#define cAgfWp41RatedmWatts       285000
#define cAgfWp41WattMinPercentage 0.0
#define cAgfWp41WattMaxPercentage 100.0

///////////////////////////////////////////////////////////////////////////////
// AGF TV TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF TV functionality.
 */
typedef struct _tAgfTVMsg
{
    tAgfInverterDataWord    isEnabled;

} tAgfTVMsg;

///////////////////////////////////////////////////////////////////////////////
// AGF FW TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF FW functionality.
 */
typedef struct _tAgfFWMsg
{
    tAgfInverterDataWord    fW21Enable;
    tAgfInverterDataWord    fW21FrequencyStart;
    tAgfInverterDataWord    fW21FrequencyStop;
    tAgfInverterDataWord    fW21frequencyStopLow;
    tAgfInverterDataWord    fW21WattGrad;  			    //This parameter defines the rate at which the algorithm will begin limiting power if it is enabled. (%/Hz * 65536)
    tAgfInverterDataWord    fW21StopWGRAHi;		        //This parameter defines the maximum rate at which power will be ramped up to the maximum. (%/Minute * 65536)
    tAgfInverterDataWord    fW21StopWGRA;    			//This parameter defines the minimum rate at which power will be ramped up to the maximum. (%/Minute * 65536)
    tAgfInverterDataWord    fW21TC;
    tAgfInverterDataWord    fW21WattGradUF;				//This parameter defines the rate at which the algorithm will begin increasing power if it is enabled and the frequency drops below PARAM_FW21_HZ_STOP_LOW (%/Hz * 65536)

} tAgfFWMsg;

// FW22/FWCHA structure.  Very similar to FW21, but a few naming changes to line-up
// with Andy's AGF record description.
typedef struct _tAgfFW22Msg
{
    tAgfInverterDataWord    modeSelection;
    tAgfInverterDataWord    frequencyStart;
    tAgfInverterDataWord    frequencyStop;
    tAgfInverterDataWord    frequencyStopLow;
    tAgfInverterDataWord    frequencySlope;
    tAgfInverterDataWord    frequencyMaxTimeSlope;
    tAgfInverterDataWord    frequencyTimeSlope;
    tAgfInverterDataWord    frequencyStopTime;
    tAgfInverterDataWord    frequencyStartDelay;

} tAgfFW22Msg;

/*defines for the fw21 mode selection parameter*/
#define cAgfFwEnable            0x00000001
#define cAgfFwHystereis         0x00000002
#define cAgfFwSlopeSelection    0x00000004
#define cAgfFwFlagsHeco         0x80000000
#define cAgfFwFlagsPriority     0x00010000
#define cAgfFwStartDelayDefault 0
#define cAgfFwStartDelayMin     0
#define cAgfFwStartDelayMax     255
#define cAgfFwStartDelayMask    0x000000FF
#define cAgfFwUnderFreqMask     0x00FFFFFF
#define cAgfFwUnderFreqDataMask 0xFFFFFF00
#define cAgfFwUnderFreqShift    8
#define cAgfFwUnderFreqMin      1.0
#define cAgfFwUnderFreqMax      500.0
#define cAgfFwUnderFreqDefault  33.3
#define cAgfFwUnderFreqScale    65.536
#define cAgfFwHecoStopTimeMax     1000
#define cAgfFwHecoStopTimeMask    0x0000FFFF
#define cAgfFwHecoTimeConstMask   0xFFFF0000
#define cAgfFwHecoTimeConstShift  16
#define cAgfFwHecoMinTimeConst    0.0
#define cAgfFwHecoMaxTimeConst    655.35
#define cAgfFwHecoTimeConstScale  100

typedef struct _tAgfPLPMsg
{
    tAgfInverterDataWord    maxVar;
    tAgfInverterDataWord    maxWatt;
} tAgfPLPMsg;

#define cAgfPLPScale       0x00010000

///////////////////////////////////////////////////////////////////////////////
// AGF AC Average TYPE DEFS
///////////////////////////////////////////////////////////////////////////////

/**
 * This structure is a control structure that contains flags for the
 * AGF ACAVE functionality.
 */
typedef struct _tAgfACAVEMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    aveHigh;

} tAgfACAVEMsg;

#define cAgfACAVEEnable            0x00000001
#define cAgfACAVEDefaultNA         264000
#define cAgfACAVEDefaultEU         277000
#define cAgfACAVEHighMin           208000
#define cAgfACAVEHighMax           300000

typedef struct _tAgfISLNDMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    tone;
    tAgfInverterDataWord    harmonics_diff;
    tAgfInverterDataWord    harmonics_increase;
    tAgfInverterDataWord    harmonics_max;
    tAgfInverterDataWord    fundamental_diff;
    tAgfInverterDataWord    freq_deviation;

} tAgfISLNDMsg;

#define cAgfISLNDEnable            0x00000001
#define cAgfISLNDJapanMode         0x00000002
#define cAgfISLNDOffForDetection   0x00000004

#define cAgfISLNDToneAmpMask       0xFF000000
#define cAgfISLNDToneAmpShift      24
#define cAgfISLNDToneAmpDefault    4
#define cAgfISLNDToneAmpMin        0
#define cAgfISLNDToneAmpMax        16

#define cAgfISLNDToneMaxMask       0x00FF0000
#define cAgfISLNDToneMaxShift      16
#define cAgfISLNDToneMaxDefault    8
#define cAgfISLNDToneMaxMin        0
#define cAgfISLNDToneMaxMax        16

#define cAgfISLNDToneCycMask       0x0000FF00
#define cAgfISLNDToneCycShift      8
#define cAgfISLNDToneCycDefault    10
#define cAgfISLNDToneCycMin        10
#define cAgfISLNDToneCycMax        255

#define cAgfISLNDToneWaitMask      0x000000FF
#define cAgfISLNDToneWaitShift     0
#define cAgfISLNDToneWaitDefault   10
#define cAgfISLNDToneWaitMin       10
#define cAgfISLNDToneWaitMax       255

#define cAgfISLNDHarmDiffDefault   1899
#define cAgfISLNDHarmDiffMin       0
#define cAgfISLNDHarmDiffMax       20000

#define cAgfISLNDHarmMaxDefault    3000
#define cAgfISLNDHarmMaxMin        2000
#define cAgfISLNDHarmMaxMax        20000

#define cAgfISLNDHarmIncreDefault  400
#define cAgfISLNDHarmIncreMin      100
#define cAgfISLNDHarmIncreMax      1000

#define cAgfISLNDFundDiffDefault   200000
#define cAgfISLNDFundDiffMin       0
#define cAgfISLNDFundDiffMax       200000

#define cAgfISLNDFreqDevDefault    0.047  // 12 = .047 * 256
#define cAgfISLNDFreqDevMin        0.01
#define cAgfISLNDFreqDevMax        0.1
#define cAgfISLNDFreqDevScale      256

typedef struct _tAgfIACMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    fast;
    tAgfInverterDataWord    slow;

} tAgfIACMsg;

#define cAgfIACDisableDCInj            0x00000000
#define cAgfIACEnableSlowDCInj         0x00000001
#define cAgfIACEnableFastDCInj         0x00000002
#define cAgfIACFastMin                 0
#define cAgfIACFastMax                 32767
#define cAgfIACFastDefaultEU           4096
#define cAgfIACFastDefaultNA           0
#define cAgfIACSlowMin                 0
#define cAgfIACSlowMax                 4096
#define cAgfIACSlowDefaultEU           256
#define cAgfIACSlowDefaultNA           0
#define cAgfIACNsrbMilliAmpPerPgd      0x00000001
#define cAgfIACNsrbPgdLimit            24            // number of devices NSR-B can support - needed?
#define cAgfIACNsrbMilliAmpMax         0xb3b0        // 46,000 mAmp
#define cAgfIACNsrbMilliAmpMin         0xa           // 10 mAmp
#define cAgfIACNsrbMilliAmpDefault     0x3e8         // 1,000 mAmp

typedef struct _tAgfVECTMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    trip_angle;
    tAgfInverterDataWord    trip_time;
    tAgfInverterDataWord    trip_angle_islanding;

} tAgfVECTMsg;

#define cAgfVECTEnable            0x00000001
#define cAgfVECTReconnectOnTrip   0x00000002
#define cAgfVECTNotRegularTrip    0x00000004
#define cAgfVECTAngleDefault      0
#define cAgfVECTAngleMin          0
#define cAgfVECTAngleMax          255
#define cAgfVECTAngleIslndDefault 12
#define cAgfVECTAngleIslndMin     0
#define cAgfVECTAngleIslndMax     255
#define cAgfVECTTimeCycDefault    6
#define cAgfVECTTimeCycMin        1
#define cAgfVECTTimeCycMax        12

typedef struct _tAgfROCOFMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterDataWord    cycles;
    tAgfInverterDataWord    constant;

} tAgfROCOFMsg;

#define cAgfROCOFEnable            0x00000001
#define cAgfROCOFReconnectTrip     0x00000002
#define cAgfROCOFTimeCycleMin      0
#define cAgfROCOFTimeCycleMax      600
#define cAgfROCOFTimeCycleDefault  5
#define cAgfROCOFConstantScale     33554
#define cAgfROCOFConstantMin       0.05
#define cAgfROCOFConstantMax       5.0
#define cAgfROCOFConstantDefault   1.0


typedef struct _tAgfWVARMsg
{
    tAgfInverterDataWord    enable;
    tAgfInverterCurveWVAR   curves;
    tAgfInverterDataWord    maxSlopeDecrease;
    tAgfInverterDataWord    maxSlopeIncrease;
    tAgfInverterDataWord    timeConstant;

} tAgfWVARMsg;

#define cAgfWVARPtMinCnt          cAgfTrans6Points
#define cAgfWVARPtMaxCnt          cAgfTrans10Points
#define cAgfWVAREnable            0x00000001
#define cAgfWVARConstantScale     33554
#define cAgfWVARConstantMin       0.05
#define cAgfWVARConstantMax       5.0
#define cAgfWVARConstantDefault   1.0
#define cAgfWVARMinPowerPt       (-1.0)
#define cAgfWVARMaxPowerPt        1.0
#define cAgfWVAREnablePtCntMask   0x00000F00
#define cAgfWVAREnablePtCntShift  8
#define cAgfWVARCurveScale        0x00400000

typedef struct _tAgfCookieData
{
    uint8_t  type;
    uint8_t  version;
    uint8_t  md5[8];
} tAgfCookieData;

typedef struct _tAgfCookieMsg
{
    uint8_t       count;
    uint8_t       fillBytes[3];
    tAgfCookieData cookie[1];
} tAgfCookieMsg;

typedef struct _tAgfTransPcuCurveData {
    /* the points (vx and qx pair) that we'll receive from rest */
    int    hCurveLNCount;
    float  hCurveLN[cAgfTrans10Points][cAgfTransPtPair];
    uint16_t hPointMask;

    /* the points (vx and qx pair) that we'll receive from rest */
    int    lCurveLNCount;
    float  lCurveLN[cAgfTrans10Points][cAgfTransPtPair];
    uint16_t lPointMask;
} tAgfTransPcuCurveData;

#define cagfCurvePtDataType 0

typedef struct _tAgfPts
{
    uint8_t                 type;
    uint8_t                 fill[3];
    uint8_t                 recOpd[8];    // corresponding record's opaque data
    tAgfTransPcuCurveData    cd;
} tAgfPts;

typedef struct _tAgfCurvePts
{
    tAgfPts    pts;
} tAgfCurvePts;

///////////////////////////////////////////////////////////////////////////////
// AGF Set Group Id
///////////////////////////////////////////////////////////////////////////////


typedef union _uAdcRecords {
    tAgfCookieMsg           agfCookie;    // agf_structs.h
    tAgfVVARMsg             agfVVAR;    // agf_structs.h
    tAgfFRTMsg              agfFRT;
    tAgfVRTMsg              agfVRT;
    tAgfFPFMsg              agfFPF;     // fixed power factor
    tAgfPRLMsg              agfPRL;     // power rate limiting
    tAgfSSMsg               agfSS;      // soft start
    tAgfVWMsg               agfVW;    // voltage watts 51
    tAgfINV2Msg             agfINV2;
    tAgfWPMsg               agfWP;
    tAgfTVMsg               agfTV;
    tAgfFWMsg               agfFW;
    tAgfPLPMsg              agfPLP;
    tAgfISLNDMsg            agfISLND;
    tAgfIACMsg              agfIAC;
    tAgfVECTMsg             agfVECT;
    tAgfROCOFMsg            agfROCOF;
    tAgfACAVEMsg            agfACAVE;
    tAgfCurvePts            agfCurvePts;
    tAgfVW52Msg             agfVW52;
    tAgfFW22Msg             agfFW22;
    tAgfWVARMsg             agfWVAR;
} uAdcRecords;

/*@}*//* END of agf Header Typedef Group */

/* ------------------------------------ Extern Declarations --------------- */

/**
 * @defgroup agfHExtdata agf Header Extern Data Definitions
 */
/*@{*/
/*@}*//* END of agf Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup agfHGlobdata agf Header Global Data Defined
 */
/*@{*/
/*@}*//* END of agf Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup agfHStatdata agf Header Static Data Defined
 */
/*@{*/
/*@}*//* END of agf Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup agfHProto agf Header Prototypes
 */
/*@{*/

/*@}*//* END of agf Header Prototypes Group */

/*@}*//* END agf Header Definitions and Declarations */

#if defined(SUNPOWER)
#pragma pack(pop)
#endif

#endif /* AGF_STRUCTS_H_ */

/* ------------------------------------------------------------------------
 $Log:$
 ------------------------------------------------------------------------ */

/* END of agf_structs.h */

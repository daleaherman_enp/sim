/**
 * @copyright
 * Copyright 2018, SunPower Corp
 * All rights reserved.
 *
 * @file COBSUART.hh
 * UART abstraction layer.  This layer is what contains the OS
 * specific bits.  If you need to port between Windows and Linux
 * then pivot on this file.
 *
 * @date 2018-Jun-25
 * @author Miles Bintz <mbintz@sunpower.com>
 */

#ifndef _COBSUART_HH_
#define _COBSUART_HH_

#include <stdlib.h>
#include <stdint.h>

#include "spwr/transcieverif.hh"
#include "spwr/uartcore.hh"

#define COBS_MAX_BLOCK  254
#define cobs_get_required_size(x) ((x) + 3)

//! @addtogroup libcomm2_api
//! @{

class COBSCodec
{
    public:
        enum cobs_state_t
        {
            COBS_WAITING,
            COBS_INGESTING,
            COBS_OVERRUN,
            COBS_DONE
        };

    private:
        uint8_t m_rxbuf[256];
        uint32_t m_cobslength;
        int m_decodedlength;

        cobs_state_t m_cobsstate;

    public:

        COBSCodec();

        //! Return the COBS state after ingesting the latest byte
        cobs_state_t AddByte(char in);

        //! Reset the state machine and prep for the next packet
        void Reset();

        //! Decode the received cobs-encoded payload
        uint8_t* Decode();

        //! Return the length of the decoded packet.  Not available until
        //! after Decode() is called
        //! @return
        size_t DecodedLen();

        static int cobs_encode(const uint8_t *in_packet, size_t size, uint8_t *out_packet);
};

//! A purely virtual class to define an OS-agnostic UART interface
class COBSUart : public TranscieverIF, public CallbackIF
{
    private:
        static const uint16_t crc16_table[256];
        static uint16_t crc16ccitt(uint8_t *pBuf, size_t len, uint16_t init);

        TranscieverIF *m_pCore;
        COBSCodec codec;

        char *m_pDevName;
        CallbackIF *m_cb;
        uint32_t m_debug;

    public:
        COBSUart();
        ~COBSUart();

        int cbifCallbackFunction(uint8_t *pBuf, size_t len);

        //! Ask the OS to open the UART device as described in pDevStr
        void Open(const char* pDevStr);

        //! Ask the COBS layer to wrap the UARTCore object in pCore
        void Open(TranscieverIF *pCore);

        //! Close the UART device at the OS layer
        void Close();

        //! Register a callback with the COBS codec
        void CallbackSet(CallbackIF *cb);

        //! Set the read timeout where applicable
        void ReadTimeoutSet(uint32_t to);

        //! return the read timeout
        uint32_t ReadTimeoutGet();

        //! Write the buffer to the COBS Encoder
        int32_t Write(uint8_t *pBuf, size_t length);

        //! Flush all unread characters from the COBS input
        void Flush();

        //! Set debugging parameters for this module
        void DebugSet(uint32_t ui32Flags);

        //! Return the device name used to open this UART
        char* DeviceName();
};

//! @}


#endif

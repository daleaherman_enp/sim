/*!
 * @Copyright
 *  Copyright (c) 2015 Sunpower Corp.  All rights reserved.
 *
 * @file crc.h
 *  Utility functions for calculating CRCs
 *
 * @date
 *  Jun 17, 2015
 *
 * @author
 *  Miles Bintz <mbintz@sunpower.com>
 *
 */

#include <stdlib.h>
#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

uint16_t crc16ccitt(uint8_t *pBuf, size_t len, uint16_t init);
uint16_t calc_crc16ccitt_slow(uint8_t *data, size_t cnt, uint16_t init);
uint32_t crc32_s(const void *buf, uint32_t size);
unsigned long crc32(const void *buf, uint32_t size);
uint16_t Fletcher8BitCsum(uint8_t *m, uint16_t len);

#ifdef __cplusplus
}
#endif

/* Copyright (c) 2010-2011 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _pmi_h_
#define _pmi_h_

/** @file pmi.h
 *  @brief PMI process header file
 *
 *  Header file for the PMI process
 *
 */

/** @page pmiHPg Header: pmi.h libpmi header file

@section pmiHName pmi.h HEADER NAME

    pmi.h -- PMI process header file

@section pmiHSynop pmi.h SYNOPSIS

    Header file for the PMI process

@section pmiHDesc pmi.h DESCRIPTION

    Key concepts related to PMI interaction are:
    - Main Application Transmit messages in the ring will have the form:
            tPmiOverrideData
            <unpacked L2 frame>
            <mcp message content (MCP at latest version)>
        transmit buffer pointer returned in get buffer call points to
        beginning of unpacked L2 frame.

    - Main Application Receive messages in the ring will have the form:
            <unpacked L2 frame>
            <mcp message content (MCP at latest version)>

    - Tap Application Transmit messages in the ring will have
        the form:
            tPmiOverrideData
            <unpacked L2 frame>
            <mcp message content (MCP at end-point supported version)>

    - Tap Application Receive messages in the ring will have the form:
            <unpacked L2 frame>
            <mcp message content (MCP at latest version)>

*/

/* system headers */
#if defined(EMU)
#include <lua.h>
#endif /* if defined(EMU) */

/**
 * @defgroup pmiHGroups pmi Header Definitions and Declarations
 */
/*@{*/

/* ------------------------------------ Defines --------------------------- */

/**
 * @defgroup pmiHDefines pmi Header Defines
 */
/*@{*/

#define cPmiHaastDefaultTxPwrLevel      0x1c // -3.10 dB (1 below highest)
#define cPmiDefaultTxGainDb               -3 // Tx gain in -dB

#define cPmiRaven1DefaultSquelchLevel   0x6E // 74 dBuV, hysteresis = 3
#define cPmiDefaultSquelchLevel           74 // 74 dBuV

#define cPmiDefaultAdaptiveSquelchEnable   1

/*! The cPmiPldPmiCfgFallbackSecs value is set to 20 minutes under the
    assumption the gateway (Envoy) will send the plcCfg MCP message at least
    often as every 15 minutes, regardless of the time required for overall
    polling cycles.

    When the gateway sends plcCfg messages it does so with the "previous" and
    "default" PMI configuration.  The data contained in the plcCfg message is
    "current" PMI configuration. The gateway sends these plcCfg messages at the
    beginning of every poll cycle, and every 15 minutes into the polling
    cycles if the polling cycle runs longer.

    On the PLD if a timer based on the cPmiPldPmiCfgFallbackSecs time expires,
    then the PLD will execute a PMI configuration change to region-appropriate
    PMI default configuration.
*/

#define cPmiPldPmiCfgFallbackSecs       3300  // 55 minutes randomised + 254 -255 secs

/* Macro to extract channel plan number from a struct of type
   tPmiDataAuroraMode */
#define mPmiGetChanPlan(f)                                \
    (((f).plan1 << 1) | ((f).plan0))

/* Macro to set channel plan number in a struct of type
   tPmiDataAuroraMode */
#define mPmiSetChanPlan(f, plan)                          \
    {                                                     \
        (f).plan1 = ((plan) & 0x2) >> 1;                  \
        (f).plan0 = (plan) & 0x1;                         \
    }

/*@}*/  /* END of Defines Group */

/* ------------------------------------ Typedefs -------------------------- */

/**
 * @defgroup pmiHTypedefs pmi Header Typedefs
 */
/*@{*/

/*! modem type */
typedef enum _ePmiModemType {
    cPmiModemTypeCorona,
    cPmiModemTypeAurora,
    cPmiModemTypeNumOf
} ePmiModemType;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiModemTypeStrTbl[cPmiModemTypeNumOf][8] = {
    //"none", "ariane",
    "corona", "aurora"
};
#else
extern const char _keil_code pmiModemTypeStrTbl[cPmiModemTypeNumOf][8];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

typedef enum _ePmiDir {
    cPmiDirReceive,
    cPmiDirRx = cPmiDirReceive,
    cPmiDirTransmit,
    cPmiDirTx = cPmiDirTransmit,
    cPmiDirNumOf
} ePmiDir;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char pmiDirStrTbl[cPmiDirNumOf][4] = { "rx", "tx" };
#else
extern const char pmiDirStrTbl[cPmiDirNumOf][4];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */


typedef enum _ePmiUseCfg {
    cPmiUseCfgDefault,
    cPmiUseCfgCurrent,
    cPmiUseCfgPrevious,
    cPmiUseCfgDiscover,
    cPmiUseCfgNumOf
} ePmiUseCfg;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char pmiUseCfgStrTbl[cPmiUseCfgNumOf][6] = { "dflt", "curr",
                                                   "prev", "dcvr" };
#else
extern const char pmiUseCfgStrTbl[cPmiUseCfgNumOf][6];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

typedef enum _ePmiCfgInUse {
    cPmiCfgInUseAuto,
    cPmiCfgInUseUserSet,
    cPmiCfgInUseDiscover,

    cPmiCfgInUseNumOf
} ePmiCfgInUse;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char pmiCfgInUseStrTbl[cPmiCfgInUseNumOf][9] = {
    "auto", "manual", "discover"
};
#else
extern const char pmiCfgInUseStrTbl[cPmiCfgInUseNumOf][6];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */


typedef struct _tPmiEnables {
    u_int8_t          modem[cPmiModemTypeNumOf][cPmiDirNumOf];
} tPmiEnables;


/*! transmit level settings */
typedef enum _ePmiTxLevel {
    cPmiTxLevelNumOf
} ePmiTxLevel;

/*! reed-solomon setting */
typedef enum _ePmiReedSolomon {
    cPmiReedSolomon1o2,
    cPmiReedSolomon3o4,
    cPmiReedSolomonNumOf
} ePmiReedSolomon;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiRsStrTbl[cPmiReedSolomonNumOf][4] = {
    "1/2", "3/4"
};
#else
extern const char _keil_code pmiRsStrTbl[cPmiReedSolomonNumOf][4];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

/*! modulation scheme */
typedef enum _ePmiModScheme {
    cPmiModScheme4MSK,
    cPmiModSchemeFSK,
    cPmiModSchemeNFSK,
    cPmiModScheme2MSK,
    cPmiModSchemeNumOf
} ePmiModScheme;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiModSchemeStrTbl[cPmiModSchemeNumOf][6] = {
    "4msk", "fsk", "nfsk", "2msk"
};
#else
extern const char _keil_code pmiModSchemeStrTbl[cPmiModSchemeNumOf][6];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

/*! modem baud rate */
typedef enum _ePmiBaud{
    cPmiBaud5K,
    cPmiBaud20K,
    cPmiBaudNumOf
} ePmiBaud;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiBaudStrTbl[cPmiBaudNumOf][6] = {
    "5K", "20K"
};
#else
extern const char _keil_code pmiBaudStrTbl[cPmiBaudNumOf][6];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

/*! channnel plan */
typedef enum _ePmiChanPlan {
    cPmiChanPlan103_123_143KHz,
    cPmiChanPlan103_110_116KHz,
    cPmiChanPlan90_110KHz,
    cPmiChanPlanNumOf
} ePmiChanPlan;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiChanPlanStrTbl[cPmiChanPlanNumOf][16] = {
    "103_123_143kHz", "103_110_116kHz", "90_110kHz"
};
#else
extern const char _keil_code pmiChanPlanStrTbl[cPmiChanPlanNumOf][16];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

/*! channel to use within the plan */
typedef enum _ePmiChannel {
    cPmiChannel3,
    cPmiChannel1, // mid channel in legacy implementation
    cPmiChannel0, // low channel in legacy implementation
    cPmiChannel2, // high channel in legacy implementation
    cPmiChannelNumOf
} ePmiChannel;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiChannelStrTbl[cPmiChannelNumOf][6] = {
    "Ch3", "Ch1", "Ch0", "Ch2"
};
#else
extern const char _keil_code pmiChannelStrTbl[cPmiChannelNumOf][6];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

typedef struct _tPmiChannelInd {
    u_int8_t              chanPlan;  // use ePmiChanPlan
    u_int8_t              channel;   // use ePmiChannel
} tPmiChannelInd;

/*! encryption type */
typedef enum _ePmiCryptType {
    cPmiCryptTypeNone,
    cPmiCryptTypeSw,
    cPmiCryptTypeHw,
    cPmiCryptTypeNumOf
} ePmiCryptType;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiCryptTypeStrTbl[cPmiCryptTypeNumOf][6] = {
    "none", "sw", "hw"
};
#else
extern const char _keil_code pmiCryptTypeStrTbl[cPmiCryptTypeNumOf][6];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

/*! encryption key handling */
typedef enum _ePmiCryptKeyType {
    cPmiCryptKeyTypeNone,
    cPmiCryptKeyTypePreShared,
    cPmiCryptKeyTypeExchange,
    cPmiCryptKeyTypeNumOf
} ePmiCryptKeyType;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiCryptKeyTypeStrTbl[cPmiCryptKeyTypeNumOf][6] = {
    "none", "psk", "kex"
};
#else
extern const char _keil_code pmiCryptKeyTypeStrTbl[cPmiCryptKeyTypeNumOf][6];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

/*! crypto PMI bit field mapping */
typedef struct _tPmiCryptMode {
    u_int8_t            type    : 3; // ePmiCryptType
    u_int8_t            keyType : 3; // ePmiCryptKeyType
    u_int8_t            keyIdx  : 2; // key index (0/1)
} tPmiCryptMode;

typedef union _uPmiCryptMode {
    u_int8_t            u8;
    tPmiCryptMode       f;
} uPmiCryptMode;

/*! definition of the different fields in crypto structure */
typedef enum _ePmiCryptField {
    cPmiCryptFieldType,
    cPmiCryptFieldKeyType,
    cPmiCryptFieldKeyIdx,
    cPmiCryptFieldNumOf
} ePmiConfigField;

#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiCryptFieldStrTbl[cPmiCryptFieldNumOf][12] = {
    "crypt-type", "key-type", "psk-idx"
};
#else
extern const char _keil_code pmiCryptFieldStrTbl[cPmiCryptFieldNumOf][12];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

/*
    jrm ??         set raven modem mode (see below)

          | 7 | 6 | 5   4 | 3 | 2 | 1  0 |
          +---+---+---+---+---+---+------+
            |   |     |     |   |      +-- channel: Ch3:0, Ch1:1,
            |   |     |     |   |                   Ch0:2, Ch2:3
            |   |     |     |   +-- channel plan bit 0
            |   |     |     |
            |   |     |     +-- baud: 0: 5K, 1: 20K
            |   |     |
            |   |     +-- modulation scheme: 4msk:0, fsk:1, nfsk:2, 2msk:3
            |   |
            |   +-- reed-solomon: 0: 1/2, 1: 3/4
            |
            +-- channel plan bit 1.
                ( channel plan bit 1 ) << 1 | channel plan bit 0:
                103_123_143KHz: 0
                103_110_116KHz: 1
                90_110KHz     : 2

Dimensions:
  - 1 bit  7:7 -- channel plan bit 1
  - 1 bit  6:6 -- RS-mode 1/2 or 3/4
  - 2 bits 5:4 -- modulation scheme: 4msk:0, fsk:1, nfsk:2, 2msk:3
  - 1 bit  3:3 -- baud: 5K:0, 20K:1
  - 1 bit  2:2 -- channel plan bit 0
  - 2 bits 1:0 -- channel: none:0, mid:1, low:2, high:3

  Note: 20K baud modes are only available with Aurora 2.x and later.

  Val   RS  Mod  Baud   Plan             Chan
    0x01  1/2 4msk 5K   103_123_143KHz   Ch1
    0x02  1/2 4msk 5K   103_123_143KHz   Ch0
    0x03  1/2 4msk 5K   103_123_143KHz   Ch2
    0x05  1/2 4msk 5K   103_110_116KHz   Ch1
    0x06  1/2 4msk 5K   103_110_116KHz   Ch0      // channel not allowed
    0x07  1/2 4msk 5K   103_110_116KHz   Ch2      // channel not allowed

    0x11  1/2 fsk  5K   103_123_143KHz   Ch1
    0x12  1/2 fsk  5K   103_123_143KHz   Ch0
    0x13  1/2 fsk  5K   103_123_143KHz   Ch2
    0x15  1/2 fsk  5K   103_110_116KHz   Ch1
    0x16  1/2 fsk  5K   103_110_116KHz   Ch0      // channel not allowed
    0x17  1/2 fsk  5K   103_110_116KHz   Ch2      // channel not allowed

    0x21  1/2 nfsk 5K   103_123_143KHz   Ch1
    0x22  1/2 nfsk 5K   103_123_143KHz   Ch0
    0x23  1/2 nfsk 5K   103_123_143KHz   Ch2
    0x25  1/2 nfsk 5K   103_110_116KHz   Ch1
    0x26  1/2 nfsk 5K   103_110_116KHz   Ch0
    0x27  1/2 nfsk 5K   103_110_116KHz   Ch2

    0x41  3/4 4msk 5K   103_123_143KHz   Ch1
    0x42  3/4 4msk 5K   103_123_143KHz   Ch0
    0x43  3/4 4msk 5K   103_123_143KHz   Ch2
    0x45  3/4 4msk 5K   103_110_116KHz   Ch1
    0x46  3/4 4msk 5K   103_110_116KHz   Ch0      // channel not allowed
    0x47  3/4 4msk 5K   103_110_116KHz   Ch2      // channel not allowed

    0x51  3/4 fsk  5K   103_123_143KHz   Ch1
    0x52  3/4 fsk  5K   103_123_143KHz   Ch0
    0x53  3/4 fsk  5K   103_123_143KHz   Ch2
    0x55  3/4 fsk  5K   103_110_116KHz   Ch1
    0x56  3/4 fsk  5K   103_110_116KHz   Ch0      // channel not allowed
    0x57  3/4 fsk  5K   103_110_116KHz   Ch2      // channel not allowed

    0x61  3/4 nfsk 5K   103_123_143KHz   Ch1
    0x62  3/4 nfsk 5K   103_123_143KHz   Ch0
    0x63  3/4 nfsk 5K   103_123_143KHz   Ch2
    0x65  3/4 nfsk 5K   103_110_116KHz   Ch1
    0x66  3/4 nfsk 5K   103_110_116KHz   Ch0
    0x67  3/4 nfsk 5K   103_110_116KHz   Ch2

    This next block are the Aurora 2.x 20KBaud modes. Aurora 2.x is also
    backward compatible with the modes above.

    0x09  1/2 4msk 20K  103_123_143KHz   Ch1
    0x0a  1/2 4msk 20K  103_123_143KHz   Ch0
    0x0b  1/2 4msk 20K  103_123_143KHz   Ch2
    0x0d  1/2 4msk 20K  103_110_116KHz   Ch1      // channel not allowed
    0x0e  1/2 4msk 20K  103_110_116KHz   Ch0      // channel not allowed
    0x0f  1/2 4msk 20K  103_110_116KHz   Ch2      // channel not allowed

    0x39  1/2 2msk 20K  103_123_143KHz   Ch1
    0x3a  1/2 2msk 20K  103_123_143KHz   Ch0
    0x3b  1/2 2msk 20K  103_123_143KHz   Ch2
    0x3d  1/2 2msk 20K  103_110_116KHz   Ch1
    0x3e  1/2 2msk 20K  103_110_116KHz   Ch0      // channel not allowed
    0x3f  1/2 2msk 20K  103_110_116KHz   Ch2      // channel not allowed

    0x49  3/4 4msk 20K  103_123_143KHz   Ch1
    0x4a  3/4 4msk 20K  103_123_143KHz   Ch0
    0x4b  3/4 4msk 20K  103_123_143KHz   Ch2
    0x4d  3/4 4msk 20K  103_110_116KHz   Ch1      // channel not allowed
    0x4e  3/4 4msk 20K  103_110_116KHz   Ch0      // channel not allowed
    0x4f  3/4 4msk 20K  103_110_116KHz   Ch2      // channel not allowed

    0x79  3/4 2msk 20K  103_123_143KHz   Ch1
    0x7a  3/4 2msk 20K  103_123_143KHz   Ch0
    0x7b  3/4 2msk 20K  103_123_143KHz   Ch2
    0x7d  3/4 2msk 20K  103_110_116KHz   Ch1
    0x7e  3/4 2msk 20K  103_110_116KHz   Ch0      // channel not allowed
    0x7f  3/4 2msk 20K  103_110_116KHz   Ch2      // channel not allowed
*/
typedef struct _tAuroraMode {
    u_int8_t        channel   : 2; // use ePmiChannel
    u_int8_t        plan0     : 1; // Bit0 of plan number
                                   // use ePmiChanPlan
    u_int8_t        baud      : 1; // use ePmiBaud

    u_int8_t        scheme    : 2; // use ePmiModScheme
    u_int8_t        rs        : 1; // use ePmiReedSolomon
    u_int8_t        plan1     : 1; // Bit1 of plan number
} tAuroraMode;

typedef union _uAuroraMode {
    u_int8_t                u8;
    tAuroraMode             f;
} uAuroraMode;

/*! definition of the aurora fields */
typedef enum _ePmiAuroraField {
    cPmiAuroraFieldTxLvl,
    cPmiAuroraFieldRs,
    cPmiAuroraFieldModScheme,
    cPmiAuroraFieldChanPlan,
    cPmiAuroraFieldChannel,
    cPmiAuroraFieldFlags,
    cPmiAuroraFieldTxGainDb,
    cPmiAuroraFieldNumOf
} ePmiAuroraField;


#if (!defined(__KEIL__) ||                       \
     ((defined(DEBUGGER) && (DEBUGGER == 1)) &&  \
     (defined(PLCDBG) && (PLCDBG == 1))))
#if defined(DEFINE_PMI_STR_TBLS)
const char _keil_code pmiAuroraFieldStrTbl[cPmiAuroraFieldNumOf][12] = {
    "tx-lvl", "rs", "mod-scheme", "chan-plan", "channel", "flags", "tx-gain-db"
};
#else
extern const char _keil_code pmiAuroraFieldStrTbl[cPmiAuroraFieldNumOf][12];
#endif /* if defined(DEFINE_PMI_STR_TBLS) */
#endif /* should include string table */

/*! aurora modem type settings */
typedef enum _ePmiAuroraConfigFlag {
    cPmiAuroraConfigFlagDbValid         = (cBit0)
} ePmiAuroraConfigFlag;

typedef struct _tPmiAuroraConfig {
    uAuroraMode         mode;               /* mode bitmap */
    u_int8_t            txLvl;              /* ePmiTxLevel (FA6C reg setting) */
    u_int8_t            flags;
    int8_t              txGainDb;           /* transmit gain in dB */

    u_int8_t            fillb1;
    u_int8_t            fillb2;
    u_int8_t            fillb3;
    u_int8_t            fillb4;
} tPmiAuroraConfig;

/*! union of modem type configuration */
typedef union _uPmiModemConfig {
    tPmiAuroraConfig    aurora;
} uPmiModemConfig;

/*! Override that is put in the meta data at the beginning of a transmit-path
  message. This information is also provide in the receive-path processing. */
typedef struct _tPmiConfigData {
    u_int8_t        modem;                  /* ePmiModemType */
    u_int8_t        filla2;
    u_int8_t        filla3;
    u_int8_t        filla4;

    u_int16_t       cryptOverride;          /* 1-override value/0-use def/ept*/
    u_int16_t       configOverride;         /* 1-override value/0-use def/ept*/

    uPmiModemConfig config;                 /* modem configuration */

    uPmiCryptMode   crypt;                  /* crypto configuration */
    u_int8_t        fillb2;
    u_int8_t        fillb3;
    u_int8_t        fillb4;

    u_int8_t        fillc1;
    u_int8_t        fillc2;
    u_int8_t        fillc3;
    u_int8_t        fillc4;
} tPmiConfigData;

/*! structure containing the global modem configuration information (not
    changing per-message */
typedef enum _ePmiGlobalFlag {
    cPmiGlobalFlagDbuvValid         = (cBit0)
} ePmiGlobalFlag;

typedef struct _tPmiGlobal {
    u_int8_t        squelch;
    u_int8_t        flags;
    u_int8_t        squelchDbuv;
    u_int8_t        adaptiveSquelchEnable;

    u_int8_t        fillb1;
    u_int8_t        fillb2;
    u_int8_t        fillb3;
    u_int8_t        fillb4;
} tPmiGlobal;

/*! structure containing all aspects of PMI configuration */
typedef struct _tPmiData {
    tPmiEnables     enables;
    tPmiConfigData  cfg;
    tPmiGlobal      glb;
} tPmiData;


/*@}*/  /* END of pmi Header Typedef Group */

/* ------------------------------------ Extern Declarations ---------- */

/**
 * @defgroup pmiHExtdata pmi Header Extern Data Definitions
 */
/*@{*/
/*@}*/  /* END of pmi Header Extern Definitions Group */

/* ------------------------------------ Global Data Defined --------------- */

/**
 * @defgroup pmiHGlobdata pmi Header Global Data Defined
 */
/*@{*/
/*@}*/  /* END of pmi Header Global Data Defined Group */

/* ------------------------------------ Static Data Defined --------------- */

/**
 * @defgroup pmiHStatdata pmi Header Static Data Defined
 */
/*@{*/


/*! Define fallback system default for PMI configuration data for:
        North America.

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! NOTE: Take care, changing these defaults should only be done with
    !!       extreme care.  Also this header need to be maintained in
    !!       synchronized fashion in both the Envoy repos and the PLD repos.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#if defined(DEFINE_PMI_DATA_DEFAULTS)
#if defined(AURDEF)

const tPmiData _keil_code pmiDataDefaultsFor103_123_143KHz = {
    // enables corona rx and tx only
    {
        // modem
        {
            // corona enables
            {0,0},
            // aurora enables
            {1,1}
        }
    },
    // cfg
    {
        // default modem

        // NOTE: For North America, if we knew that all raven1-based PLD would
        // supported by gateway (Envoy) software at release 3.0.0 with Envoyr
        // (with raven1) or later then, for this type of PLD we could have the
        // default, i.e.  fallback, PMI configuration default modem to be
        // aurora, but since the is not likely to be the case all the time the
        // fallback configuration uses the corona modem.  Remember its just a
        // fallback, when the PLD receives a plcCfg message it will change its
        // current PMI configuration accordingly.

        cPmiModemTypeAurora,
        // fills
        0,0,0,
        // cryptOverride
        0,
        // configOverride
        0,
        // config (uPmiModemConfig)
        {
            // aurora
            {
                // mode
                {
                    // u8
                    // modem 0x51 - cPmiReedSolomon3o4,cPmiModSchemeFSK,
                    //              cPmiChanPlan103_123_143KHz,cPmiChannel1
                    0x51
                },
                // txLvl
                cPmiRaven1DefaultTxPwrLevel,
                // flags
                cPmiAuroraConfigFlagDbValid,
                // txGainDb
                cPmiDefaultTxGainDb,
                // fills
                0, 0, 0, 0
            },
        },
        // crypt (uPmiCryptMode)
        {0x00},
        // fills
        0, 0, 0, 0, 0, 0, 0
    },
    // glb
    {
        // squelch
        cPmiRaven1DefaultSquelchLevel,
        // flags
        cPmiGlobalFlagDbuvValid,
        // squelchDbuv
        cPmiDefaultSquelchLevel,
        // adaptiveSquelchEnable
        cPmiDefaultAdaptiveSquelchEnable,
        // fills
        0, 0, 0, 0
    }

}; // END of pmiDataDefaultsFor103_123_143KHz

#else /* if defined(AURDEF) */

const tPmiData _keil_code pmiDataDefaultsFor103_123_143KHz = {
    // enables corona rx and tx only
    {
        // modem
        {
            // corona enables
            {1,1},
            // aurora enables
            {1,1}
        }
    },
    // cfg
    {
        // default modem

        // NOTE: For North America, if we knew that all raven1-based PLD would
        // supported by gateway (Envoy) software at release 3.0.0 with Envoyr
        // (with raven1) or later then, for this type of PLD we could have the
        // default, i.e.  fallback, PMI configuration default modem to be
        // aurora, but since the is not likely to be the case all the time the
        // fallback configuration uses the corona modem.  Remember its just a
        // fallback, when the PLD receives a plcCfg message it will change its
        // current PMI configuration accordingly.

        cPmiModemTypeCorona,
        // fills
        0,0,0,
        // cryptOverride
        0,
        // configOverride
        0,
        // config (uPmiModemConfig)
        {
            // aurora
            {
                // mode
                {
                    // u8
                    // modem 0x53 - cPmiReedSolomon3o4,cPmiModSchemeFSK,
                    //              cPmiChanPlan103_123_143KHz,cPmiChannel2
                    0x53
                },
                // txLvl
                cPmiHaastDefaultTxPwrLevel,
                // flags
                cPmiAuroraConfigFlagDbValid,
                // txGainDb
                cPmiDefaultTxGainDb,
                // fills
                0, 0, 0, 0
            },
        },
        // crypt (uPmiCryptMode)
        {0x00},
        // fills
        0, 0, 0, 0, 0, 0, 0
    },
    // glb
    {
        // squelch
        cPmiRaven1DefaultSquelchLevel,
        // flags
        cPmiGlobalFlagDbuvValid,
        // squelchDbuv
        cPmiDefaultSquelchLevel,
        // adaptiveSquelchEnable
        cPmiDefaultAdaptiveSquelchEnable,
        // fills
        0, 0, 0, 0
    }

}; // END of pmiDataDefaultsFor103_123_143KHz

#endif /* if defined(AURDEF) */

#else
extern const tPmiData _keil_code pmiDataDefaultsFor103_123_143KHz;
#endif /* if defined(DEFINE_PMI_DATA_DEFAULTS) */


/*! Define fallback system default for PMI configuration data for:
        Europe.

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! NOTE: Take care, changing these defaults should only be done with
    !!       extreme care.  Also this header need to be maintained in
    !!       synchronized fashion in both the Envoy repos and the PLD repos.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#if defined(DEFINE_PMI_DATA_DEFAULTS)
const tPmiData _keil_code pmiDataDefaultsFor103_110_116KHz = {
    // enables corona rx and tx only
    {
        // modem
        {
            // corona enables
            {0,0},
            // aurora enables
            {1,1}
        }
    },
    // cfg
    {
        // default modem
        cPmiModemTypeAurora,
        // fills
        0,0,0,
        // cryptOverride
        0,
        // configOverride
        0,
        // config (uPmiModemConfig)
        {
            // aurora
            {
                // mode
                {
                    // u8
                    // modem 0x65 - cPmiReedSolomon3o4,cPmiModSchemeNFSK,
                    //              cPmiChanPlan103_110_116KHz,cPmiChannel1
                    0x65
                },
                // txLvl
                cPmiHaastDefaultTxPwrLevel,
                // flags
                cPmiAuroraConfigFlagDbValid,
                // txGainDb
                cPmiDefaultTxGainDb,
                // fills
                0, 0, 0, 0
            },
        },
        // crypt (uPmiCryptMode)
        {0x00},
        // fills
        0, 0, 0, 0, 0, 0, 0
    },
    // glb
    {
        // squelch
        cPmiRaven1DefaultSquelchLevel,
        // flags
        cPmiGlobalFlagDbuvValid,
        // squelchDbuv
        cPmiDefaultSquelchLevel,
        // adaptiveSquelchEnable
        cPmiDefaultAdaptiveSquelchEnable,
        // fills
        0, 0, 0, 0
    }

}; // END of pmiDataDefaultsFor103_110_116KHz
#else
extern const tPmiData _keil_code pmiDataDefaultsFor103_110_116KHz;
#endif /* if defined(DEFINE_PMI_DATA_DEFAULTS) */


#if defined(DEFINE_PMI_DATA_DEFAULTS)
const tPmiData _keil_code pmiDataDefaultsFor90_110KHz = {
    // enables corona rx and tx only
    {
        // modem
        {
            // corona enables
            {0,0},
            // aurora enables
            {1,1}
        }
    },
    // cfg
    {
        // default modem

        // Set default modem as Aurora for 90_110KHz frequency plan

        cPmiModemTypeAurora,
        // fills
        0,0,0,
        // cryptOverride
        0,
        // configOverride
        0,
        // config (uPmiModemConfig)
        {
            // aurora
            {
                // mode
                {
                    // u8
                    // modem 0xD1 - cPmiReedSolomon3o4,cPmiModSchemeFSK,
                    //              cPmiChanPlan90_110KHz,cPmiChannel1
                    0xD1
                },
                // txLvl
                cPmiHaastDefaultTxPwrLevel,
                // flags
                cPmiAuroraConfigFlagDbValid,
                // txGainDb
                cPmiDefaultTxGainDb,
                // fills
                0, 0, 0, 0
            },
        },
        // crypt (uPmiCryptMode)
        {0x00},
        // fills
        0, 0, 0, 0, 0, 0, 0
    },
    // glb
    {
        // squelch
        cPmiRaven1DefaultSquelchLevel,
        // flags
        cPmiGlobalFlagDbuvValid,
        // squelchDbuv
        cPmiDefaultSquelchLevel,
        // adaptiveSquelchEnable
        cPmiDefaultAdaptiveSquelchEnable,
        // fills
        0, 0, 0, 0
    }

}; // END of pmiDataDefaultsFor90_110KHz
#else
extern const tPmiData _keil_code pmiDataDefaultsFor90_110KHz;
#endif /* if defined(DEFINE_PMI_DATA_DEFAULTS) */

/*@}*/  /* END of pmi Header Static Data Defined Group */

/* ------------------------------------ Prototypes ------------------------ */

/**
 * @defgroup pmiHProto pmi API Header Prototypes
 */
/*@{*/

/*! pmiGetTxBuf - return the next available transmit ring buffer for app */
/*! pmiGetTxBuf - return the next available transmit ring buffer for app

    Get the next available buffer on the transmit ring associated with the
    pmiHandle.
*/

/*@}*/  /* END of pmi API Header Prototypes Group */

/**
 * @defgroup pmiHProto pmi Header Prototypes
 */
/*@{*/

#if defined(EMU)

/*! Write the enables to the Lua table */
extern void
pmiluaPutPlcEnables(lua_State      *L,
                    tPmiEnables    *enables);

/*! Populate the enables structure from Lua table */
extern int
pmiluaGetPlcEnables(lua_State      *L,
                    tPmiEnables    *enables);

/*! Write the cfg structure to the Lua table */
extern void
pmiluaPutPlcCfg(lua_State      *L,
                tPmiConfigData *cfg);

/*! Populate the cfg structure from the Lua table */
extern int
pmiluaGetPlcCfg(lua_State      *L,
                tPmiConfigData *cfg);

/*! Write the glb structure to the Lua table */
extern void
pmiluaPutPlcGlb(lua_State      *L,
                tPmiGlobal     *glb);

/*! Populate the glb structure from the Lua table */
extern int
pmiluaGetPlcGlb(lua_State      *L,
                tPmiGlobal     *glb);

/*! Save the whole PMI data to a Lua table */
extern void
pmiluaPutPmiData(lua_State     *L,
                 tPmiData      *pmidata);

/*! Populate the PMI data full structure */
extern int
pmiluaGetPmiData(lua_State     *L,
                 tPmiData      *pmidata);

#endif /* defined(EMU) */

/*@}*/  /* END of pmi Header Prototypes Group */

/*@}*/  /* END pmi Header Definitions and Declarations */

#endif /* _pmi_h_ */

/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of pmi.h */



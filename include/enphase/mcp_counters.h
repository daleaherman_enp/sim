/* Copyright (c) 2006-2011 enPhase Energy, Inc. All rights reserved. */
/* $Id:$ */
/* $Header:$ */

#ifndef _mcp_counters_h_
#define _mcp_counters_h_

#if defined(PCU)
#define 

#elif defined(ENPHASE)
#define  __attribute__((__packed__))
#include "include/enphase.h"

#elif defined(SUNPOWER)
#define  PACKING __attribute__((__packed__))
#elif defined(UT)
#define 

#else
#error "Please specify build type in the Makefile"

#endif

/** @file mcp_counters.h
 *  Header for Module Control Protocol (MCP) counters
 *
 */

/** Documentation for tPlcCounters:

    This block of counters are defined, in a derived manner, based on the
    workings of the device driver interaction with the Ariane communications
    device and on the basis of layer 2 frame reception and transmission.

    - rssi (rxEmuSsi) - This is the receive signal strength indication.  It is
    currently based on a 0 - 4095 ssi scale.

    - issi (idleLineSsi) - This is the idle line signal strength indication,
    currently based on 0 - 4095 ssi scale.  There is an open issue on this
    reading that the device does not always take the reading at the correct
    time, thus resulting in occasional bogus readings.  The value of this
    reading, assuming its a good reading, is that it would tell you where the
    noise floor is for the communications line.

    - tssi (txSsi) - This is a transmit signal strength indication.
    This reading is to be taken during device frame transmission.  This
    reading is of little value since normally it would max-out.  For this
    reason it is not deisplayed.

    - rx frms (allRxFrames) - This is a count of all frames received by the
    device driver.

    - bcast rx frms (bcastRxFrames) - This is the count of frames received
    that are unacknowledged frames.  Its a bit of a misnomer, technically its
    not really just broadcast frames, its broadcast and single-cast
    unacknowledged frames.

    - ack rx frms (ackRxFrames) - This is the count of acknowledged frames.

    - me rx frms (forMeRxFrames) - This is the count of frames directly
    addressed to the device.

    - rx frm errs (rxFrameErrors) - The receive frame is errored and is
    discarded.  Frame reception is aborted.

    - rx overruns (rxOverRuns) - The communications chip over-wrote the
    receiver register because the device driver did not read the last value
    soon enough.  Frame reception is aborted.

    - collisions (collisions) - The communications chip detected another
    trasmission while contending for the channel.  Frame transmission is
    aborted.

    - ac no rsp (ackNoResponseCnt) - This is the count of the number of
    acknowlege frames sent for which not acknowledge indication (at layer 1)
    was received.

    - frm too long (frameTooLongCnt) - The count of the number of times there
    was an attempt to transmit too many characters.  Frame transmission is
    aborted.

    - tx underruns (txUnderRuns) - The device driver did not feed the
    transmission data soon enough after reading the last input code.  Frame
    transmission is aborted.

    - tx overruns (txOverRuns) - The count of the number of times the
    communications device transmitter did not read the timer status.

    - tx frms (txFrames) - The count of the number frames transmitted by the
    communications device.

    - unexp chip rsps (unexpectedChipResponses) - The count of the number of
    times that the device driver received unexpected input from the
    communications device.

    - abnormal line busy events (abnormalLineBusyEvents) - The count of the
    number of times the latey 1 line is unexpectantly found to be busy.  This
    is not used on the non-EMU side of the communications line.

    - rsvd nibble errs (reservedNibbleErrors) - The count of the number
    reseved-nibble (undefined) value were received by the device driver from
    the communications device.

    - invalid protocol id (invalidProtocolId) - The count of the number of
    time that a received frame contained an unrecognized protocol id value.

*/
typedef struct  _tPlcCounters
{
    uint16_t allRxFrames;
    uint16_t bcastRxFrames;

    uint16_t ackRxFrames;
    uint16_t forMeRxFrames;

    uint16_t rxFrameErrors;
    uint16_t rxOverRuns;

    uint16_t collisions;
    uint16_t ackNoResponseCnt;

    uint16_t frameTooLongCnt;
    uint16_t txUnderRuns;

    uint16_t txOverRuns;
    uint16_t txFrames;

    uint16_t unexpectedChipResponses;
    uint16_t abnormalLineBusyEvents;

    uint16_t reservedNibbleErrors;
    uint16_t invalidProtocolId;
} tPlcCounters;

typedef struct  _tAuroraDetailedCounters
{
    uint32_t totBaudsCnt;
    uint32_t corrHdrSymbCnt;
    uint32_t uncorrRsPyldSymbCnt;
    uint32_t corrPyldSymbCnt;
    uint32_t pyldSymbCnt;

    uint16_t cryptKey0PktCnt;
    uint16_t cryptKey1PktCnt;
    uint16_t snr;
    uint16_t fill;

    uint16_t ravPreamblesCnt;
    uint16_t okHdrCrcCnt;
    uint16_t badHdrCrcCnt;
    uint16_t goodHdrsCnt;
    uint16_t uncorrHdrsCnt;
    uint16_t pyldCrcErrCnt;

    uint16_t macTxPktOutCnt;
    uint16_t macRxOvflwCnt;
    uint16_t macRxPktOutCnt;
    uint16_t macRxPktInCnt;

} tAuroraDetailedCounters;

/** Document tControllerCnts:

    This block of counters are an output of the application-layer of
    the communications protocol.  This is layer 3.

    - rx msgs (rxMsgs) - The count of application layer received messages.

    - tx msgs (txMsgs) - The count of application layer transmitted messages.

    - Fastpath messages (fpMsgs) - Count of recieved fastpath messages

    - Phase Aware messages (paMsgs) - Count of messages. In Envoy this
      is a tx count, on pcu this is an rx count.

    - tx full (txFull) - The count of the number of times the application
    layer transmit message ring was found to be full.

    - bad mhdr (badMsgHdr) - Count of the number of invalid (corrupted)
    application message headers were received.

    - bad proto (badProto) - Count of the number of application messages
    received with bad l2 protocol value.

    - bad scope (badScopePt) - Count of the number of applications messages
    received with bad l2 scope indication, e.g. a broadcast to all EMU seen
    at a PCU.

    - other modid (otherModId) - Count of the number of applications messages
    directed at a device other than the current one and passed from the
    device driver to the application layer.

    - csum errs (checksumBad) - At the application layer there a 2-byte
    checksum for the entire application message.  This is the count of the
    number of application messages that failed the application message
    checksum check.

    - ignored other domain (ignoredOtherDomain) - Count of number of
    message received at the application layer that are for devices on
    another EMU domain.

    - other domain scan msgs (otherDomainScanMsgs) - Not used.

    - other domain directed (otherDomainDirected) - Not used.

    - bad dir (badDir) - Not used

    - repeated rx msgs (repeatedRxMsgs) - Count of message received that were
    (most likely) retransmissions.

    - f3 (fill3) - A reserved counter location.


*/
typedef struct  _tControllerCnts
{
    uint16_t rxMsgs;
    uint16_t txMsgs;
    uint16_t fpMsgs;
    uint16_t paMsgs;

    uint16_t txFull;
    uint16_t badMsgHdr;
    uint16_t badProto;
    uint16_t badScopePt;

    uint16_t otherModId;
    uint16_t checksumBad;
    uint16_t ignoredOtherDomain;
    uint16_t otherDomainScanMsgs;

    uint16_t otherDomainDirected;
    uint16_t badDir;
    uint16_t repeatedRxMsgs;
    uint16_t fill3;
} tControllerCnts;

#define cMcpSsiFlagsMsk 0xffc0
#define cMcpSsiFlagsShf 6
#define cMcpSsiFlagInDbuV 0x200   // SSI is in dBuV
#define cMcpSsiFlagFsk4Msk 0x100  // Aurora FSK and 4MSK are available
/** The cMcpSsiFlagImgUpgEnh1 flag indicates the following capabilities:

    1) Parm load to offline side if indicated.
    2) Uses reduced data in imgRsp message:
    3) Can run at line rate for fastest upgrade modulation mode.
    4) Also possibly: understands imgInfo marked for metadata-only upgrade.
       This last one is for the case that only the parm needed update on
       the standby side, so for copy switch we need only update procload
       metadata with new load-date ... if this device supports this and
       the procload image itself does not need to be updated.
       When an upgrade uses this feature the cMcpImgInfoFlagsMetadataUpgOnly
       flag in the imgInfo message is set.


*/
#define cMcpSsiFlagImgUpgEnh1 0x080
#define cMcpSsiFlagPlcSec 0x040  // AES ASIC-based PLC (mcpsec) Security

#define cMcpSsiPlcIdxMsk 0x003f
#define cMcpSsiPlcIdxShf 0

/*! enum of PLC indices reported by PLDs. These directory correspond to a
    preferred configuration in the pldconf.lua configuration file (part of
    the device image package). Each index can correspond to a different
    preferred PLC mode and is not necessarily tied to the PLDs default mode.

    Currently we have defined:
        - Original      - anything that is before this support was added. Will
                          default to the mode used in R3.0.0 for NA and EU
        - Raven1Resi    - Starts with M215 used in non-commercial sites. These
                          are likely to want a robust mode, without requiring
                          higher bandwidth.
        - Raven1Commerc - First user of this is M277 where high bandwidth is
                          important and a clean comm environment is expected.
        - Aurora2Resi   - EU region, Aurora preferred
        - Heron2Asic    - Heron2 boards used in all regions

    Other indices can be added as other modes are required. The pldconf_plc.lua
    configuration file can be changed at any time to re-define the comm
    configuration tied to any of these indices.
*/
typedef enum _eMcpSsiPlcIdx
{
    cMcpSsiPlcIdxOriginal,          // 0
    cMcpSsiPlcIdxRaven1Resi,        // 1
    cMcpSsiPlcIdxRaven1Commercial,  // 2
    cMcpSsiPlcIdxAurora2Resi,       // 3
    cMcpSsiPlcIdxHeron2Asic,        // 4

    cMcpSsiPlcIdxNumOf
} eMcpSsiPlcIdx;

/*
idleLineSsi    - SSI value when PLC is idle
rxEmuSsi       - SSI value when PLC is recieving a frame
squelch        - configured value, currently defaults to 0x6E, can be set via
pmi config message flagsAndPlcIdx - McpSsiPlcId  and  McpSsiFlag defined above

*/
typedef struct  _tMcpSsiMeasures
{
    uint16_t idleLineSsi;
    uint16_t rxEmuSsi;
    uint16_t squelch;
    uint16_t flagsAndPlcIdx;
} tMcpSsiMeasures;

#endif /* _mcp_counters_h_ */

/* ------------------------------------------------------------------------
   $Log:$
   ------------------------------------------------------------------------ */

/* END of mcp_counters.h */

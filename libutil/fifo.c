/*!
 * @copyright
 * Copyright (c) 2015, SunPower Corp.  All rights reserved.
 *
 * @file
 * fifo.c
 *
 * @date
 * Jun 30, 2015
 *
 * @author
 * Miles Bintz <mbintz@sunpower.com>
 */
#ifdef WIN32
#include <WinSock2.h>
#endif
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#ifndef WIN32
#include <pthread.h>
#include <semaphore.h>
#include <sys/time.h>
#else
#include "win32/pthread.h"
#include "win32/semaphore.h"
#include "win32/win_utils.h"
#define pthread_condattr_setclock(x,y) (0)
#endif // !WIN32

#include <assert.h>
#include <signal.h>

#include "sbt/errno.h"
#include "sbt/os.h"
#include "sbt/fifo.h"
#include "sbt/log.h"

#ifndef MIN
#define MIN(A,B) ((A) < (B) ? (A) : (B))
#endif

struct sMsg_t
{
    uint32_t len;
    uint8_t pBuf[];
};

struct sFIFO_t
{
    pthread_mutex_t mutex;
    pthread_cond_t msgavailable;
    uint32_t maxmsgs;
    uint32_t msgSize;
    uint32_t index;
    uint32_t outdex;
    uint8_t pStorage[];
};

/*!
 * \addtogroup fifo_api
 * @{
 */

/*!
 * Create a fifo object
 *
 * @param fifo is a pointer to a fifo handle in which place to the new fifo object
 * @param nMsgs is the number of messages to create in this fifo
 * @param msgSize is the size of each message in the fifo
 *
 * @return 0 for success, < 0 for error
 */
int32_t fifoCreate(hFifo *fifo, uint32_t nMsgs, uint32_t msgSize)
{
    struct sFIFO_t *pThis;
    uint32_t slen;
    int rc;
    pthread_condattr_t attr;

    if (!fifo) return -EINVAL;
    if (nMsgs == 0) return -EINVAL;
    if (msgSize == 0) return -EINVAL;

    slen = sizeof(struct sFIFO_t) + (nMsgs * (sizeof(struct sMsg_t) + msgSize));
    pThis = (struct sFIFO_t *) malloc(slen);
    if (!pThis) return -ENOMEM;
    memset((void*) pThis, 0, slen);

    *fifo = pThis;
    pThis->maxmsgs = nMsgs;
    pThis->msgSize = msgSize;

    rc = pthread_condattr_init(&attr);
    assert(rc == 0);

#ifdef __linux__
    rc = pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
    assert(rc == 0);
#endif

    rc = pthread_cond_init(&pThis->msgavailable, &attr);
    assert(rc == 0);

    rc = pthread_condattr_destroy(&attr);
    assert(rc == 0);

    rc = pthread_mutex_init(&pThis->mutex, NULL);
    assert(rc == 0);

    return 0;
}

/*!
 * destroy the fifo object
 *
 * @param fifo is a handle to the fifo object to destroy
 *
 * @return 0 for success
 */
int32_t fifoDestroy(hFifo fifo)
{
    int rc;

    rc = pthread_mutex_lock(&fifo->mutex);
    rc = pthread_mutex_unlock(&fifo->mutex);
    assert(rc == 0);

    rc = pthread_mutex_destroy(&fifo->mutex);
    assert(rc == 0);

    rc = pthread_cond_destroy(&fifo->msgavailable);
    assert(rc == 0);

    free((void*) fifo);
    return 0;

}

static int32_t _next(hFifo fifo, int idx)
{
    idx++;
    if ((uint32_t) idx >= fifo->maxmsgs) idx = 0;
    return idx;
}

/*!
 * add an element to the fifo
 *
 * @param fifo is a handle to the fifo object to add to
 * @param pItem is a pointer to the item to add
 * @param len is the lenght of the data in pItem
 *
 * @return 0 for success, -ENOBUFS if the fifo is full
 */
int32_t fifoAdd(hFifo fifo, void *pItem, uint32_t len)
{
    int rc;
    int next;
    bool bEmpty = false;
    struct sMsg_t *pMsg;
    uint32_t minlen;

    if (len > fifo->msgSize) return -EINVAL;

    rc = pthread_mutex_lock(&fifo->mutex);
    assert(rc == 0);

    // we only need to signal when we're empty (because we only block when
    // we're empty)
    if (fifo->index == fifo->outdex)
    {
        bEmpty = true;
    }

    next = _next(fifo, fifo->index);

    // if our next index would be the outdex then we have a fifo-full condition
    if (next == fifo->outdex)
    {
        rc = pthread_mutex_unlock(&fifo->mutex);
        assert(rc == 0);
        return -ENOBUFS;
    }

    pMsg = (struct sMsg_t *) (fifo->pStorage
        + (fifo->index * (sizeof(struct sMsg_t) + fifo->msgSize)));

    minlen = MIN(len, fifo->msgSize);
    if (minlen < len)
    {
        LOG_WARNING("message truncated!\n");
    }
    pMsg->len = minlen;
    // LOG_VERBOSE_HI("Adding message of length %d\n", len);

    // otherwise, there's room
    memcpy(pMsg->pBuf, pItem, minlen);

    fifo->index = next;
    rc = pthread_mutex_unlock(&fifo->mutex);
    assert(rc == 0);

    if (bEmpty) pthread_cond_signal(&fifo->msgavailable);

    return 0;
}

/*!
 * Remove an element from the fifo.  Block when empty if requested.
 *
 * @param fifo is a handle to the fifo object to receive from
 * @param pItem is a pointer to a caller-supplied buffer in which to place
 *        the message
 * @param pLen on input is the length of the caller supplied buffer, on output
 *        is the length of the message placed in pItem.  This can be 0 to
 *        simply remove a message without placing it in the callers buffers
 * @param timeoutms specifies the number of milliseconds to block for.  0 to
 *        not block at all.  -1 to block forever.
 *
 * @return 0 on success, -ETIMEDOUT if there's no data
 */
int32_t fifoRemove(hFifo fifo, void *pItem, size_t *pLen, int32_t timeoutms)
{
    int rc;
    struct timespec now;
    uint32_t sec, nsec;
    struct sMsg_t *pMsg;
    uint32_t minlen;
    bool bEmpty = false;

    if (!pItem) return -EINVAL;

    rc = pthread_mutex_lock(&fifo->mutex);
    assert(rc == 0);

    if (fifo->index == fifo->outdex)
    {
        bEmpty = true;
    }

    if ((timeoutms == 0) && (bEmpty))
    {
        pthread_mutex_unlock(&fifo->mutex);
//    	LOG_WARNING("Timeout %d\n",rc);
        *pLen = 0;
        return -ETIMEDOUT;
    }

    // we only block if the FIFO was empty on entry
    if ((bEmpty) && (timeoutms > 0))
    {
		rc = clock_gettime(CLOCK_REALTIME, &now);
        sec = timeoutms / 1000;
        timeoutms -= (sec * 1000);
        nsec = timeoutms * 1000000;
        now.tv_nsec += nsec;
        if (now.tv_nsec > 999999999)
        {
            now.tv_sec++;
            now.tv_nsec -= 1000000000;
        }
        now.tv_sec += sec;
		
        rc = pthread_cond_timedwait(&fifo->msgavailable, &fifo->mutex, &now);
        if (rc)
        {
            // LOG_WARNING("Timeout %d\n",rc);
            pthread_mutex_unlock(&fifo->mutex);
            *pLen = 0;
            return -rc;
        }
    }
    else if ((bEmpty) && (timeoutms == -1))
    {
        rc = pthread_cond_wait(&fifo->msgavailable, &fifo->mutex);
        if (rc)
        {
            // LOG_WARNING("Timeout %d\n",rc);
            pthread_mutex_unlock(&fifo->mutex);
            *pLen = 0;
            return -rc;
        }
    }

    // we can break out of the cond_wait because of either a timeout or a
    // signal

    pMsg = (struct sMsg_t *) (fifo->pStorage
        + (fifo->outdex * (sizeof(struct sMsg_t) + fifo->msgSize)));

    // by the time we get here we can pull one message out of the FIFO
    if (pMsg->len == 0)
    {
#if 0
        raise(SIGTRAP);
#endif
        assert(0);
    }
    if (*pLen == 0)
    {
#if 0
        raise(SIGTRAP);
#endif
        // assert(0);
        return -ENODATA;
    }

    minlen = MIN(*pLen, pMsg->len);
    memcpy(pItem, pMsg->pBuf, minlen);
    *pLen = minlen;
    fifo->outdex = _next(fifo, fifo->outdex);

    rc = pthread_mutex_unlock(&fifo->mutex);
    assert(rc == 0);

    return 0;
}

/*!
 * clear all messages out of the fifo
 *
 * @param fifo is a handle to fifo object
 *
 * @return 0 for success
 */
int32_t fifoFlush(hFifo fifo)
{
    int rc;

    rc = pthread_mutex_lock(&fifo->mutex);
    assert(rc == 0);

    fifo->outdex = fifo->index = 0;

    rc = pthread_mutex_unlock(&fifo->mutex);
    assert(rc == 0);

    return 0;
}

/*!
 * @}
 */

#if defined(SELFTEST)
#include <unistd.h>
#include <string.h>

#define TESTENTRIES 4
#define TESTLEN 16

pthread_t thid;

void* reader(void *pArgs)
{
    hFifo fifo = (hFifo)pArgs;
    uint8_t pOut[TESTLEN];
    size_t rxlen;
    int rc;

    rxlen = TESTLEN;
    rc = fifoRemove(fifo, pOut, &rxlen, 100);
    assert(rc == 0);
    assert(pOut[0] == 0);

    rxlen = TESTLEN;
    rc = fifoRemove(fifo, pOut, &rxlen, 100);
    assert(rc == 0);
    assert(pOut[0] == 1);

    rxlen = TESTLEN;
    rc = fifoRemove(fifo, pOut, &rxlen, -1);
    assert(rc == 0);
    assert(pOut[0] == 2);

    rxlen = TESTLEN;
    rc = fifoRemove(fifo, pOut, &rxlen, 10);
    assert(rc == 0);
    assert(pOut[0] == 3);

    rxlen = TESTLEN;
    rc = fifoRemove(fifo, pOut, &rxlen, 1);
    assert(rc == 0);
    assert(pOut[0] == 4);

    return 0;
}

int main()
{
    hFifo fifo;
    uint8_t pOut[TESTLEN], pIn[TESTENTRIES + 1][TESTLEN];
    size_t rxlen;

    int rc;
    int i;

    // allocate 5 things to send to a FIFO with 4 entries
    for(i = 0; i < TESTENTRIES + 1; i++)
    {
        memset(pIn[i], i, TESTLEN);
    }

    // test creation
    rc = fifoCreate(&fifo, TESTENTRIES, TESTLEN);
    assert(rc == 0);

    // can we remove from an empty fifo?  no timeout, shouldn't block
    rxlen = TESTLEN;
    rc = fifoRemove(fifo, pOut, &rxlen, 0);
    assert(rc == -ETIMEDOUT);

    // fill with 3 entries.  Given our implementation we can fit 1 entry less
    // than the created size.  Should all pass.
    for(i = 0; i < (TESTENTRIES - 1); i++)
    {
        rc = fifoAdd(fifo, (void*)pIn[i], TESTLEN);
        assert(rc == 0);
    }

    // add the 4th.  Should fail.  (full)
    rc = fifoAdd(fifo, (void*)pIn[i], TESTLEN);
    assert(rc == -ENOBUFS);

    // remove the 4.  Should not block.
    for(i = 0; i < (TESTENTRIES - 1); i++)
    {
        rxlen = TESTLEN;
        rc = fifoRemove(fifo, pOut, &rxlen, 0);
        assert(rxlen == TESTLEN);
        assert(rc == 0);
    }

    // remove what's not there.  should fail like above.
    rxlen = TESTLEN;
    rc = fifoRemove(fifo, pOut, &rxlen, 0);
    assert(rc == -ETIMEDOUT);

    // refill with entries
    for(i = 0; i < (TESTENTRIES - 1); i++)
    {
        rc = fifoAdd(fifo, (void*)pIn[i], TESTLEN);
        assert(rc == 0);
    }

    // flush 'em all.  should pass.
    rc = fifoFlush(fifo);
    assert(rc == 0);

    // and remove what's not there.  should fail.
    rxlen = TESTLEN;
    rc = fifoRemove(fifo, &pOut, &rxlen, 0);
    assert(rc == -ETIMEDOUT);

    pthread_create(&thid, NULL, reader, fifo);

    usleep(90 * 1000);
    rc = fifoAdd(fifo, (void*)pIn[0], TESTLEN);
    assert(rc == 0);

    usleep(90 * 1000);
    rc = fifoAdd(fifo, (void*)pIn[1], TESTLEN);
    assert(rc == 0);

    usleep(2000 * 1000);
    rc = fifoAdd(fifo, (void*)pIn[2], TESTLEN);
    assert(rc == 0);

    usleep(5);
    rc = fifoAdd(fifo, (void*)pIn[3], TESTLEN);
    rc = fifoAdd(fifo, (void*)pIn[4], TESTLEN);
    assert(rc == 0);

    return 0;
}
#endif

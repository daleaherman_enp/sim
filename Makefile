#**************************************************************************
# @file  Makefile
#  Powermanager build system
# 
# @copyright
#  Copyright (c) 2014 SolarBridge Technologies.  All rights reserved.
#
# @created Sep 25, 2014
#
# @author Miles Bintz <m.bintz@solarbridgetech.com>
#
#**************************************************************************

#
# Generate an absolute path for the top of the project (right now
# we are at the top)
#
SBTTOP ?= $(shell (cd ..; pwd))

#
# Let the rest of the world know we're tops 
#
export SBTTOP

include $(SBTTOP)/makedefs

#
# The list of all things to recurse into
#
# APPS := mime plctools foo plc_test_suite
APPS :=  plctools 
#ifneq ($(OPENWRT_BUILD),1)
#APPS += simulator
#endif

SUBDIRS := $(APPS) json libutil libcomms

.PHONY : $(APPS)

#
# Whatever the user specified as a target, pass it into each subdir. 
# Yes, this means each subdir should have a common set of make targets
# (all and clean, maybe install).  If a target is NA then stub it out
#
MAKECMDGOALS ?= all

#
# We specify our pre-requisites horizontally.  This makes them
# parallelizable.
#
all:
	$(AT)for APP in $(APPS); do \
		$(MAKE) -C $$APP $(MAKECMDGOALS) || exit 1 ; \
	done
	$(MAKE) -C libcomms unittests
	$(MAKE) -C libgrid unittests
	$(MAKE) -C libutil unittests
	$(MAKE) -C mime unittests

clean:
	$(AT)for DIR in $(SUBDIRS); do \
		$(MAKE) -C $$DIR clean; \
	done

world:
	ARCH=NATIVE $(MAKE)
	ARCH=ARM $(MAKE)
	ARCH=MIPS $(MAKE)
	# $(MAKE) -C docs

mrproper:
	ARCH=NATIVE $(MAKE) clean
	ARCH=ARM $(MAKE) clean
	ARCH=MIPS $(MAKE) clean

install:
	ARCH=NATIVE $(MAKE) -C mime install
	ARCH=NATIVE $(MAKE) -C plctools install
	ARCH=NATIVE $(MAKE) -C simulator install
	ARCH=MIPS $(MAKE) -C mime install
	ARCH=MIPS $(MAKE) -C plctools install
	ARCH=MIPS $(MAKE) -C simulator install
	# $(MAKE) -C docs install

